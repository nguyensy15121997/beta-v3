document.addEventListener("DOMContentLoaded", function() {
    "use strict";
    const list = new List('table-default', {
        sortClass: 'table-sort',
        listClass: 'table-tbody',
        valueNames: [
            'sort-name',
            'sort-email',
            'sort-ideas',
            'sort-remaining-words',
            { attr: 'data-date', name: 'sort-date' },
            { attr: 'data-last-login', name: 'sort-last-login' },
        ],
        // page: 10,
        // pagination: {
        //     innerWindow: 1,
        //     left: 0,
        //     right: 0,
        //     paginationClass: "pagination",
        // },
    });
})