$(document).ready(function () {
    if (location.href.includes('store') || location.href.includes('openai')) {
        window.history.pushState("", "", location.origin + '/dashboard/user/ideas/' + $('#save-locale').data('project-id'));
        if ($('#save-locale').data('value') == 'ar') {
            $('#change-language-firka').attr('href', location.origin + '/dashboard/user/ideas/' + $('#save-locale').data('project-id'));
        } else {
            $('#change-language-firka').attr('href', location.origin + '/ar/dashboard/user/ideas/' + $('#save-locale').data('project-id'));
        }
    }
        $('.tooltiptext').css('opacity', 1);
    setTimeout(() => {
        
        if ($('.tooltiptext').html() != $('#save-locale').data('generate')) {
            $('.tooltiptext').css('opacity', 0);
        }
    }, 10000)
    localStorage.setItem('is_edit_show', 'hide');
    var queryString = location.search;
    var params = new URLSearchParams(queryString);
    var tab = params.get('tab');
    if (tab == 'marketing' || localStorage.getItem('show_tab') == 'marketing') {
        $('#tab-concept').removeClass('active');
        $('#tab-launch').removeClass('active');
        $('#tab-marketing').addClass('active');
        $('#tab-tasks').removeClass('active');
        $('#marketing').show();
        $('#concept').hide();
        $('#launch').hide();
        $('#tasks').hide();
        $('.tab-marketing').show();
        $('.tab-concept').hide();
        $('.tab-launch').hide();
        $('.float-edit-botton').hide();
        localStorage.removeItem('show_tab');
        $('.float-plus-botton').show();
    } else if(tab == 'launch') {
        $('#tab-marketing').removeClass('active');
        $('#tab-concept').removeClass('active');
        $('#tab-launch').addClass('active');
        $('#tab-tasks').removeClass('active');
        $('#concept').hide();
        $('#marketing').hide();
        $('#launch').show();
        $('#tasks').hide();
        $('.tab-marketing').hide();
        $('.tab-concept').hide();
        $('.tab-launch').show();
        $('.float-edit-botton').show();
        $('.float-plus-botton').show();
    } else if(tab == 'tasks') {
        $('#tab-marketing').removeClass('active');
        $('#tab-concept').removeClass('active');
        $('#tab-launch').removeClass('active');
        $('#tab-tasks').addClass('active');
        $('#concept').hide();
        $('#marketing').hide();
        $('#tasks').show();
        $('#launch').hide();
        $('.tab-marketing').hide();
        $('.tab-concept').hide();
        $('.tab-launch').hide();
        $('.float-edit-botton').hide();
        $('.float-plus-botton').hide();
    } else {
        $('#tab-marketing').removeClass('active');
        $('#tab-launch').removeClass('active');
        $('#tab-concept').addClass('active');
        $('#tab-tasks').removeClass('active');
        $('#concept').show();
        $('#marketing').hide();
        $('#launch').hide();
        $('#tasks').hide();
        $('.tab-marketing').hide();
        $('.tab-concept').show();
        $('.tab-launch').hide();
        $('.float-edit-botton').show();
        $('.float-plus-botton').show();
    }
    let arr = ['swot', 'leanCanvas', 'competitiveAnalysis', 'userPersonas', 'storytelling', 'userStories'];
    arr.forEach((type) => {
        if (localStorage.getItem(type + '_collapse_' + $('#save-locale').data('project-id')) == 'hide') {
            $('#' + type + '-minius').hide();
            $('#' + type + '-plus').show();
            $('#' + type + '-content').hide();
        } else {
            $('#' + type + '-minius').show();
            $('#' + type + '-plus').hide();
            $('#' + type + '-content').show();
        }
    })
    
    $('#tab-marketing').on('click', function() {
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?tab=marketing';
        window.history.pushState({path:newurl},'',newurl);
        $('#tab-marketing').addClass('active');
        $('#tab-concept').removeClass('active');
        $('#tab-launch').removeClass('active');
        $('#tab-tasks').removeClass('active');
        $('#concept').hide();
        $('#marketing').show();
        $('#launch').hide();
        $('#tasks').hide();
        $('.tab-marketing').show();
        $('.tab-concept').hide();
        $('.tab-launch').hide();
        $('.float-plus-botton').show();
        $('.float-edit-botton').hide();
    });
    $('#tab-concept').on('click', function() {
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?tab=concept';
        window.history.pushState({path:newurl},'',newurl);
        $('#tab-concept').addClass('active');
        $('#tab-marketing').removeClass('active');
        $('#tab-launch').removeClass('active');
        $('#tab-tasks').removeClass('active');
        $('#concept').show();
        $('#marketing').hide();
        $('#launch').hide();
        $('#tasks').hide();
        $('.tab-marketing').hide();
        $('.tab-concept').show();
        $('.tab-launch').hide();
        $('.float-plus-botton').show();
        $('.float-edit-botton').show();
    });
    $('#tab-launch').on('click', function() {
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?tab=launch';
        window.history.pushState({path:newurl},'',newurl);
        $('#tab-marketing').removeClass('active');
        $('#tab-concept').removeClass('active');
        $('#tab-launch').addClass('active');
        $('#tab-tasks').removeClass('active');
        $('#concept').hide();
        $('#marketing').hide();
        $('#launch').show();
        $('#tasks').hide();
        $('.tab-marketing').hide();
        $('.tab-concept').hide();
        $('.tab-launch').show();
        $('.float-plus-botton').show();
        $('.float-edit-botton').show();
    });
    
    $('#tab-tasks').on('click', function() {
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?tab=tasks';
        window.history.pushState({path:newurl},'',newurl);
        $('#tab-marketing').removeClass('active');
        $('#tab-concept').removeClass('active');
        $('#tab-launch').removeClass('active');
        $('#tab-tasks').addClass('active');
        $('#concept').hide();
        $('#marketing').hide();
        $('#tasks').show();
        $('#launch').hide();
        $('.tab-marketing').hide();
        $('.tab-concept').hide();
        $('.tab-launch').hide();
        $('.float-plus-botton').hide();
        $('.float-edit-botton').hide();
    
    });
    $('#tab-startupNaming').on('click', function() {
        if (!$('#tab-startupNaming').hasClass('active')) {
            $('#tab-startupNaming').addClass('active');
        }
        $('#tab-brandWheel').removeClass('active');
        $('#tab-elevatorPitch').removeClass('active');
        $('#startupNaming').show();
        $('#brandWheel').hide();
        $('#elevatorPitch').hide();
    });
    $('#tab-brandWheel').on('click', function() {
        if (!$('#tab-brandWheel').hasClass('active')) {
            $('#tab-brandWheel').addClass('active');
        }
        $('#tab-startupNaming').removeClass('active');
        $('#tab-elevatorPitch').removeClass('active');
        $('#brandWheel').show();
        $('#startupNaming').hide();
        $('#elevatorPitch').hide();
    });
    $('#tab-elevatorPitch').on('click', function() {
        if (!$('#tab-elevatorPitch').hasClass('active')) {
            $('#tab-elevatorPitch').addClass('active');
        }
        $('#tab-startupNaming').removeClass('active');
        $('#tab-brandWheel').removeClass('active');
        $('#startupNaming').hide();
        $('#brandWheel').hide();
        $('#elevatorPitch').show();
    });
    
    $('.hide-popup').on('click', function() {
        let buttonName = $(this).html();
        if (buttonName == 'Save changes') {
            let type = localStorage.getItem('type');
            let name = localStorage.getItem('name');
            let key = localStorage.getItem('key');
            let value = $('#popup-value').val();
            let id = localStorage.getItem('id');

            let data = {
                type: type,
                id: id,
                key: key,
                value: value,
            };

            if ($('#save-locale').data('value') == 'ar') {
                if (type == 'customerInterview') {
                    data.key += '_ar';
                } else {
                    data.key += '_abr';
                }
            }
            $.ajax({
                url: $('#save-locale').data('route-update'),
                type: 'POST',
                data: data,
                success: function(response) {
                    if (response.is_success) {
                        toastr.success(response.message);
                        if (type == 'userStories' || type == 'userPersonas' || type ==
                            'competitiveAnalysis') {
                            $('#' + type + '-' + key + '-' + id).html(value);
                        } else {
                            console.log('#' + type + '-' + key);
                            $('#' + type + '-' + key).html(value);
                        }
                    } else {
                        toastr.error('Something Wents Wrong.');
                    }
                },
                error: function(response) {
                    toastr.error('Something Wents Wrong.');
                }
            })
        }
        $('#exampleModalCenter').removeClass('show');
        $('#exampleModalCenter').css('display', 'none');
    });
    $('.edit-popup').on('click', function() {
        let type = $(this).attr("data-type");
        let key = $(this).attr("data-key");
        let name = $(this).attr("data-name");
        let value = $(this).attr("data-value");
        let id = $(this).attr("data-id");
        localStorage.setItem('type', type);
        localStorage.setItem('key', key);
        localStorage.setItem('name', name);
        localStorage.setItem('value', value);
        localStorage.setItem('id', id);
        $('#popup-title').html('Edit ' + name);
        $('#popup-value').val(value);
        $('#exampleModalCenter').addClass('show');
        $('#exampleModalCenter').css('display', 'block');
    });
    $('.show-anwser').on('click', function() {
        let target = $(this).data('target');
        if ($(target + ' div').hasClass('show')) {
            $(target + ' div').removeClass('show');
        } else {
            $(target + ' div').addClass('show');
        }
    });
    var type = $('#save-locale').data('project-type');
    if (type == 'template') {
        $('.pro_type').addClass('d-none');
    } else {
        $('.pro_type').removeClass('d-none');
    }
    
    
    $('.px-2rem button').mouseenter(function() {
        $('#regenerate').fadeIn();
    });
    
    // Bắt sự kiện khi di chuột ra
    $('.px-2rem button').mouseleave(function() {
        if (!$('#regenerate').hasClass('still')) {
            $('#regenerate').fadeOut();
        }
    });
    
    $('.chevron-down').on('click', function() {
        $(this).hide();
        $(this).next('.chevron-up').show();
        $('#list-' + $(this).data('type')).hide();
    });
    
    $('.chevron-up').on('click', function() {
        $(this).hide();
        $(this).prev('.chevron-down').show();
        $('#list-' + $(this).data('type')).show();
    });
});

function caculatorChart(percent) {
    console.log(percent);
    return 123;
}

function createUserStories(url) {
    $('#create-one-user-stories').addClass('loading').css('pointer-events', 'none');
    $.ajax({
        url: url,
        type: 'POST',
        success: function(response) {
            if (response.is_success) {
                location.reload();
            }
        },
        error: function(response) {
        }
    });
};

function createUserPersonas(url) {
    $('#create-one-user-personas').addClass('loading').css('pointer-events', 'none');
    $.ajax({
        url: url,
        type: 'POST',
        success: function(response) {
            if (response.is_success) {
                location.reload();
            }
        },
        error: function(response) {
        }
    });
};

function showEditPopup(id, type, key, name, value) {
    if (value == "swotAnalysis-strengths" || value == "swotAnalysis-weaknesses" || value == "swotAnalysis-opportunities" || value == "swotAnalysis-threats" || value.includes('acceptance_criterea') || value.includes('storytelling-')) {
        value = $('#' + value).html();
    }
    localStorage.setItem('type', type);
    localStorage.setItem('key', key);
    localStorage.setItem('name', name);
    localStorage.setItem('value', value);
    localStorage.setItem('id', id);
    $('#popup-title').html('Edit ' + name);
    $('#popup-value').val(value);
    $('#exampleModalCenter').addClass('show');
    $('#exampleModalCenter').css('display', 'block');
}
function toggleEditButton() {
    if (localStorage.getItem('is_edit_show') == 'show') {
        localStorage.setItem('is_edit_show', 'hide');
        $('.bg-edit').css('background', 'transparent');
        $('.edit-popup').hide();
        $('.edit-popup-pre').hide();
    } else {
        localStorage.setItem('is_edit_show', 'show');
        $('.bg-edit').css('background', '#333333');
        $('.edit-popup').show();
        $('.edit-popup-pre').show();
    }
}

function collapseByType(type, btn_name) {
    if (btn_name == 'minius') {
        $('#' + type + '-minius').hide();
        $('#' + type + '-plus').show();
        $('#' + type + '-content').hide();
        localStorage.setItem(type + '_collapse_' + $('#save-locale').data('project-id'), 'hide');
    } else {
        $('#' + type + '-minius').show();
        $('#' + type + '-plus').hide();
        $('#' + type + '-content').show();
        localStorage.setItem(type + '_collapse_' + $('#save-locale').data('project-id'), 'show');
    }
}

function addQueryStringToCurrentUrl(query) {
    // Get the current URL
    const currentUrl = location.href;

    // Check if the current URL already has a query string
    const separator = currentUrl.includes('?') ? '&' : '?';

    // Concatenate the query string to the current URL
    const modifiedUrl = `${currentUrl}${separator}query=${encodeURIComponent(query)}`;

    // Push the modified URL to the browser history without reloading the page
    history.pushState({ path: modifiedUrl }, '', modifiedUrl);
}

function deleteProjectDocument(url) {
    let link = $('#save-locale').data('route-idea-show');
    $.ajax({
        url: url,
        type: 'GET',
        success: function(response) {
            if (response.type == 'success') {
                location.reload();
            }
        },
        error: function(response) {
        }
    });
}

function selectLogo(image, overlay, icon) {
    $('.logo-wrapper').removeClass('selected');
    $('.selected-icon').hide();
    $('.overlay-logo').hide();
    $(this).addClass('selected');
    $('#' + overlay).show();
    $('#' + icon).show();
    $('#logo-selected').val($('#' + image).attr('src'));
    $('#selected-logo').prop('disabled', false);
}

function regenerateLogo() {
    $('#chooseLogo').modal('hide');
    $('#add-new-floating').hide();
    $('.tooltiptext').css('opacity', 1);
    $('.tooltiptext').html($('#save-locale').data('generate'));
    $('.spinner_button svg').hide();
    $('.spinner-border').show();
    let url = $('#save-locale').data('generate-logo');
    $.ajax({
        url: url,
        type: 'POST',
        success: function(response) {
            console.log(response);
            if (response.is_success) {
                $('#add-new-floating').show();
                $('.tooltiptext').css('opacity', 0);
                $('.tooltiptext').html('');
                $('.spinner_button svg').show();
                $('.spinner-border').hide();
                showSelectLogo(response.logos);
            }
        },
        error: function(response) {
        }
    });
}

function generateLogo() {
    let url = $('#save-locale').data('generate-logo');
    $.ajax({
        url: url,
        type: 'POST',
        success: function(response) {
            console.log(response);
            if (response.is_success) {
                $('#add-new-floating').show();
                $('.tooltiptext').css('opacity', 0);
                $('.tooltiptext').html('');
                $('.spinner_button svg').show();
                $('.spinner-border').hide();
                showSelectLogo(response.logos);
            }
        },
        error: function(response) {
        }
    });
}

function generateProjectInfo(url) {
    $(document).ajaxSend(function() {
        $("#overlay").fadeIn(300);
    });
    $.ajax({
        url: url,
        type: 'POST',
        success: function(response) {
            if (response.success) {
                location.reload();
            }
        },
        error: function(response) {
            response = response.responseJSON;
        }
    }).done(function() {
        setTimeout(function() {
            $("#overlay").fadeOut(300);
        }, 500);
    });
    $('#fullPageLoader').hide();
}

function regenerateProjectInfo(id, type) {
    $('#regenerate').addClass('still');
    $('.ti-reload').addClass('rotate-slowly');
    $('#add-new-floating').hide();
    $('.tooltiptext').css('opacity', 1);
    $('.tooltiptext').html($('#save-locale').data('generate'));
    $('.spinner_button svg').hide();
    $('.spinner-border').show();
    let data = {
        id: id,
        type: type
    };
    $.ajax({
        url: $('#save-locale').data('route-regenerate'),
        type: 'POST',
        data: data,
        success: function(response) {
            if (response.is_success) {
                $('#add-new-floating').show();
                location.reload();
            }
        },
        error: function(response) {
            response = response.responseJSON;
        }
    });
    
}

function deleteLandingPage(id) {
    let data = {
        id: id
    };
    $.ajax({
        url: $('#save-locale').data('route-delete-landing'),
        type: 'POST',
        data: data,
        success: function(response) {
            if (response.is_success) {
                toastr.success('Deleted Success');
                if (location.href.includes('launch')) {
                    location.reload();
                } else {
                    location.href = location.href + '?tab=launch';
                }
            } else {
                toastr.error('Something Wents Wrong.')
            }
        },
        error: function(response) {
            toastr.error('Something Wents Wrong.');
        }
    });
}

function deleteProjectInfo(id, type, body_selector) {
    let data = {
        id: id,
        type: type
    };
    $.ajax({
        url: $('#save-locale').data('route-delete'),
        type: 'POST',
        data: data,
        success: function(response) {
            if (response.is_success) {
                toastr.success('Delete Success');
                location.reload();
                $(body_selector).hide();
            } else {
                toastr.error('Something Wents Wrong.');
            }
        },
        error: function(response) {
            $('.loader-wrapper').addClass('d-none')
            toastr.error('Something Wents Wrong.');
        }
    }).done(function() {
        setTimeout(function() {
            $("#overlay").fadeOut(300);
        }, 500);
    });
}

function showSelectLogo(logos) {
    if (logos.length == 2) {
        $('#chooseLogo .src-1').attr("src", logos[0]);
        $('#chooseLogo .src-2').attr("src", logos[1]);
        $('#chooseLogo').modal('show');
    } else {
        toastr.error('Something Wents Wrong.');
    }
} 

function updateDataEdit(selector, data, project_id) {
    $(selector + ' form').attr('action', location.origin + '/dashboard/user/openai/projects/projects/' + project_id + '/task/update/' + data['id']);
    $(selector + ' #title').val(data['title']).prop('disabled', false);
    $(selector + ' #description').val(data['description']).prop('disabled', false);
    $(selector + ' #estimated_hrs').val(data['estimated_hrs']).prop('disabled', false);
    $(selector + ' #priority').val(data['priority']).prop('disabled', false);
    $(selector + ' #start_date').val(data['start_date']).prop('disabled', false);
    $(selector + ' #end_date').val(data['end_date']).prop('disabled', false);
    $(selector + ' .save').show();
    $(selector).modal('show');
}

function viewTask(selector, data, project_id) {
    $(selector + ' form').attr('action', location.origin + '/dashboard/user/openai/projects/projects/' + project_id + '/task/update/' + data['id']);
    $(selector + ' #title').val(data['title']).prop('disabled', true);
    $(selector + ' #description').val(data['description']).prop('disabled', true);
    $(selector + ' #estimated_hrs').val(data['estimated_hrs']).prop('disabled', true);
    $(selector + ' #priority').val(data['priority']).prop('disabled', true);
    $(selector + ' #start_date').val(data['start_date']).prop('disabled', true);
    $(selector + ' #end_date').val(data['end_date']).prop('disabled', true);
    $(selector + ' .save').hide();
    $(selector + ' #popup-title-update').hide();
    $(selector + ' #popup-title-view').show();
    $(selector).modal('show');
}

function showEdit(selector) {
    $(selector + ' #title').prop('disabled', false);
    $(selector + ' #description').prop('disabled', false);
    $(selector + ' #estimated_hrs').prop('disabled', false);
    $(selector + ' #priority').prop('disabled', false);
    $(selector + ' #start_date').prop('disabled', false);
    $(selector + ' #end_date').prop('disabled', false);
    $(selector + ' .save').show();
    $(selector + ' #popup-title-update').show();
    $(selector + ' #popup-title-view').hide();
}