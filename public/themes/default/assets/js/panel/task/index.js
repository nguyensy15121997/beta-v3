var now = "{{__('Now')}}";

/*For Task Move To Update Stage*/
!function (a) {
    "use strict";
    var t = function () {
        this.$body = a("body")
    };
    t.prototype.init = function () {
        a('[data-plugin="dragula"]').each(function () {
            var t = a(this).data("containers"), n = [];
            if (t) for (var i = 0; i < t.length; i++) n.push(a("#" + t[i])[0]); else n = [a(this)[0]];
            var r = a(this).data("handleclass");
            r ? dragula(n, {
                moves: function (a, t, n) {
                    return n.classList.contains(r)
                }
            }) : dragula(n).on('drop', function (el, target, source, sibling) {
                var sort = [];
                $("#" + target.id + " > div").each(function () {
                    sort[$(this).index()] = $(this).attr('id');
                });

                var id = el.id;
                var old_stage = $("#" + source.id).data('status');
                var new_stage = $("#" + target.id).data('status');
                var project_id = '{{$project->id}}';

                $("#" + source.id).parent().find('.count').text($("#" + source.id + " > div").length);
                $("#" + target.id).parent().find('.count').text($("#" + target.id + " > div").length);
                // $.ajax({
                //     url: '{{route('tasks.update.order',[$project->id])}}',
                //     type: 'PATCH',
                //     data: {id: id, sort: sort, new_stage: new_stage, old_stage: old_stage, project_id: project_id},
                //     success: function (data) {

                //         // console.log(data);
                //     }
                // });
            });
        })
    }, a.Dragula = new t, a.Dragula.Constructor = t
}(window.jQuery), function (a) {
    "use strict";
    a.Dragula.init()
}(window.jQuery);

$(document).ready(function () {
    /*Set assign_to Value*/
    $(document).on('click', '.add_usr', function () {
        var ids = [];
        $(this).toggleClass('selected');
        var crr_id = $(this).attr('data-id');
        if ($('#usr_icon_' + crr_id).hasClass('fa-plus')) {
            $('#usr_icon_' + crr_id).removeClass('fa-plus');
            $('#usr_icon_' + crr_id).addClass('fa-check');
        } else {
            $('#usr_icon_' + crr_id).removeClass('fa-check');
            $('#usr_icon_' + crr_id).addClass('fa-plus');
        }
        $('.selected').each(function () {
            ids.push($(this).attr('data-id'));
        });
        $('input[name="assign_to"]').val(ids);
    });

    $(document).on("click", ".del_task", function () {
        var id = $(this);
        $.ajax({
            url: $(this).attr('data-url'),
            type: 'DELETE',
            dataType: 'JSON',
            success: function (data) {
                $('#' + data.task_id).remove();
            },
        });
    });

    /*For Task Comment*/
    $(document).on('click', '#comment_submit', function (e) {
        var curr = $(this);
        var comment = $.trim($("#form-comment textarea[name='comment']").val());
        if (comment != '') {
            $.ajax({
                url: $("#form-comment").data('action'),
                data: {comment: comment},
                type: 'POST',
                success: function (data) {
                    data = JSON.parse(data);
                     var avatar = (data.user.avatar) ? "src='{{$logo_path}}/" + data.user.avatar + "'" : "avatar='" + data.user.img_avatar + "'";
                    var html = "<div class='list-group-item px-0'>" +
                        "                    <div class='row align-items-center'>" +
                        "                        <div class='col-auto'>" +
                        "                            <a href='#' class='avatar avatar-sm rounded-circle'>" +
                        "                                <img " + avatar + " alt='" + data.user.name + "'>" +
                        "                            </a>" +
                        "                        </div>" +
                        "                        <div class='col ml-n2'>" +
                        "                            <p class='d-block h6 text-sm font-weight-light mb-0 text-break'>" + data.comment + "</p>" +
                        "                            <small class='d-block'>" + now + "</small>" +
                        "                        </div>" +
                        "                        <div class='col-auto'><a href='#' class='delete-comment' data-url='" + data.deleteUrl + "'><i class='fas fa-trash-alt text-danger'></i></a></div>" +
                        "                    </div>" +
                        "                </div>";

                    $("#comments").prepend(html);
                    $("#form-comment textarea[name='comment']").val('');
                    load_task(curr.closest('.side-modal').attr('id'));
                    
                },
                error: function (data) {
                    
                }
            });
        } else {
            
        }
    });

    $(document).on("click", ".delete-comment", function () {
        var btn = $(this);

        $.ajax({
            url: $(this).attr('data-url'),
            type: 'DELETE',
            dataType: 'JSON',
            success: function (data) {
                load_task(btn.closest('.side-modal').attr('id'));
                
                btn.closest('.list-group-item').remove();
            },
            error: function (data) {
                data = data.responseJSON;
                if (data.message) {
                    
                } else {
                    
                }
            }
        });
    });

    /*For Task Checklist*/
    $(document).on('click', '#checklist_submit', function () {
        var name = $("#form-checklist input[name=name]").val();
        if (name != '') {
            $.ajax({
                url: $("#form-checklist").data('action'),
                data: {name: name},
                type: 'POST',
                success: function (data) {
                    data = JSON.parse(data);
                    load_task($('.side-modal').attr('id'));
                    
                    var html = '<div class="card border shadow-none checklist-member">' +
                        '                    <div class="px-3 py-2 row align-items-center">' +
                        '                        <div class="col-10">' +
                        '                            <div class="custom-control custom-checkbox">' +
                        '                                <input type="checkbox" class="custom-control-input" id="check-item-' + data.id + '" value="' + data.id + '" data-url="' + data.updateUrl + '">' +
                        '                                <label class="custom-control-label h6 text-sm" for="check-item-' + data.id + '">' + data.name + '</label>' +
                        '                            </div>' +
                        '                        </div>' +
                        '                        <div class="col-auto card-meta d-inline-flex align-items-center ml-sm-auto">' +
                        '                            <a href="#" class="action-item delete-checklist" role="button" data-url="' + data.deleteUrl + '">' +
                        '                                <i class="fas fa-trash-alt text-danger"></i>' +
                        '                            </a>' +
                        '                        </div>' +
                        '                    </div>' +
                        '                </div>'

                    $("#checklist").append(html);
                    $("#form-checklist input[name=name]").val('');
                    $("#form-checklist").collapse('toggle');
                },
                error: function (data) {
                    data = data.responseJSON;
                    
                }
            });
        } else {
            
        }
    });

    $(document).on("change", "#checklist input[type=checkbox]", function () {
        $.ajax({
            url: $(this).attr('data-url'),
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {
                load_task($('.side-modal').attr('id'));
                
            },
            error: function (data) {
                data = data.responseJSON;
                if (data.message) {
                    
                } else {
                    
                }
            }
        });
    });

    $(document).on("click", ".delete-checklist", function () {
        var btn = $(this);
        $.ajax({
            url: $(this).attr('data-url'),
            type: 'DELETE',
            dataType: 'JSON',
            success: function (data) {
                load_task($('.side-modal').attr('id'));
                
                btn.closest('.checklist-member').remove();
            },
            error: function (data) {
                data = data.responseJSON;
                if (data.message) {
                    
                } else {
                    
                }
            }
        });
    });

    /*For Task Attachment*/
    $(document).on('click', '#file_submit', function () {
        var file_data = $("#task_attachment").prop("files")[0];
        if (file_data != '' && file_data != undefined) {
            var formData = new FormData();
            formData.append('file', file_data);

            $.ajax({
                url: $("#file_submit").data('action'),
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                success: function (data) {
                   // alert(data);
                    $('#task_attachment').val('');
                    
                    data = JSON.parse(data);
                    load_task(data.task_id);
                    

                    var delLink = '';
                    if (data.deleteUrl.length > 0) {
                        delLink = '<a href="#" class="action-item delete-comment-file" role="button" data-url="' + data.deleteUrl + '">' +
                            '                                        <i class="fas fa-trash"></i>' +
                            '                                    </a>';
                    }
                },
                error: function (data) {

                    data = data.responseJSON;
                    if (data) {
                           
                        $('#file-error').text(data.errors.file[0]).show();
                    } else {
                        
                    }
                }
            });
        } else {
            
        }
    });

    $(document).on("click", ".delete-comment-file", function () {
        var btn = $(this);
        $.ajax({
            url: $(this).attr('data-url'),
            type: 'DELETE',
            dataType: 'JSON',
            success: function (data) {
                load_task(btn.closest('.side-modal').attr('id'));
                
                btn.closest('.task-file').remove();
            },
            error: function (data) {
                data = data.responseJSON;
                if (data.message) {
                    
                } else {
                    
                }
            }
        });
    });

    /*For Favorite*/
    $(document).on('click', '#add_favourite', function () {
        $.ajax({
            url: $(this).attr('data-url'),
            type: 'POST',
            success: function (data) {
                if (data.fav == 1) {
                    $('#add_favourite').addClass('action-favorite');
                } else if (data.fav == 0) {
                    $('#add_favourite').removeClass('action-favorite');
                }
            }
        });
    });

    /*For Complete*/
    $(document).on('change', '#complete_task', function () {
        $.ajax({
            url: $(this).attr('data-url'),
            type: 'POST',
            success: function (data) {
                if (data.com == 1) {
                    $("#complete_task").prop("checked", true);
                } else if (data.com == 0) {
                    $("#complete_task").prop("checked", false);
                }
                $('#' + data.task).insertBefore($('#task-list-' + data.stage + ' .empty-container'));
                load_task(data.task);
            }
        });
    });

    /*Progress Move*/
    $(document).on('change', '#task_progress', function () {
        var progress = $(this).val();
        $('#t_percentage').html(progress);
        $.ajax({
            url: $(this).attr('data-url'),
            data: {progress: progress},
            type: 'POST',
            success: function (data) {
                load_task(data.task_id);
            }
        });
    });
});

function load_task(id) {
    $.ajax({
        url: "{{route('projects.tasks.get','_task_id')}}".replace('_task_id', id),
        dataType: 'html',
        success: function (data) {
            $('#' + id).html('');
            $('#' + id).html(data);
        }
    });
}