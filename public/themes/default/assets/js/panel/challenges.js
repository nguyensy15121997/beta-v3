//admin.openai.custom.form
$(document).ready(function () {
    "use strict";
    
});

function challengeSave() {
    "use strict";

    document.getElementById("submit_challenge").disabled = true;
    document.getElementById("submit_challenge").innerHTML = magicai_localize.please_wait;

    var formData = new FormData();
    formData.append('title', $("#title").val());
    formData.append('description', $("#description").val());
    
    if ($('#image').val() != 'undefined') {
        formData.append('image', $('#image').prop('files')[0]);
    }
    
    $.ajax({
        type: "post",
        url: "/dashboard/user/challenge/store",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            console.log(data);
            toastr.success(data['message']);
            document.getElementById("submit_challenge").disabled = false;
            document.getElementById("submit_challenge").innerHTML = "Submit";
            setTimeout(function () {
                location.href = '/dashboard/user/challenge/' + data['id'];
            }, 500);
        },
        error: function (data) {
            var err = data.responseJSON.errors;
            $.each(err, function (index, value) {
                toastr.error(value);
            });
            document.getElementById("submit_challenge").disabled = false;
            document.getElementById("submit_challenge").innerHTML = "Submit";
        }
    });
    return false;
}

function deleteChallenge(url) {
    "use strict";
    
    $.ajax({
        type: "delete",
        url: url,
        contentType: false,
        processData: false,
        success: function (data) {
            toastr.success(data['message']);
            location.reload();
        },
        error: function (data) {}
    });
    return false;
}

function goToChallengeDetails(url) {
    location.href = url;
}
