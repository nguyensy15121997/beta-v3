@extends('panel.layout.app_without_sidebar')
@section('title', __('Reset Password'))
@section('content')

    <div class="page page-auth page-login">
        <div class="absolute top-0 left-0 right-0 container-fluid">
            <div class="items-center p-8 row text-center">
                <div class="col">
                    <a href="{{route('index')}}" class="navbar-brand navbar-brand-autodark">
                        @if(isset($setting->logo_dashboard))
                            <img src="/{{$setting->logo_dashboard_path}}" @if ( isset($setting->logo_dashboard_2x_path) ) srcset="/{{$setting->logo_dashboard_2x_path}} 2x" @endif alt="{{$setting->site_name}}" class="group-[.navbar-shrinked]/body:hidden dark:hidden">
                            <img src="/{{$setting->logo_dashboard_dark_path}}" @if ( isset($setting->logo_dashboard_dark_2x_path) ) srcset="/{{$setting->logo_dashboard_dark_2x_path}} 2x" @endif alt="{{$setting->site_name}}" class="hidden group-[.navbar-shrinked]/body:hidden dark:block">
                        @else
                            <img src="/{{$setting->logo_path}}" @if ( isset($setting->logo_2x_path) ) srcset="/{{$setting->logo_2x_path}} 2x" @endif alt="{{$setting->site_name}}" class="group-[.navbar-shrinked]/body:hidden dark:hidden">
                            <img src="/{{$setting->logo_dark_path}}" @if ( isset($setting->logo_dark_2x_path) ) srcset="/{{$setting->logo_dark_2x_path}} 2x" @endif alt="{{$setting->site_name}}" class="hidden group-[.navbar-shrinked]/body:hidden dark:block">
                        @endif
                    </a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row items-stretch justify-center min-h-[100vh] max-lg:py-11">
                <div class="flex flex-col justify-center col-12 col-sm-9 col-lg-8">
                    <div class="row">
                        <div class="mx-auto col-12 col-sm-9 col-lg-6">
                            <h1 class="text-center mb-[25px]">{{__('Forgot Password')}}</h1>
                            <form novalidate="novalidate" id="password_reset_form" onsubmit="return PasswordResetMailForm();">
                                <div class="mb-[20px]">
                                    <label class="form-label">{{__('Email Address')}}</label>
                                    <input type="email" class="form-control" id="password_reset_email" placeholder="{{__('your@email.com')}}" autocomplete="off" required>
                                </div>
                                <div class="row">
                                    <div class="col !text-end">
                                        <a href="{{route('login')}}">{{__('Remember Your Password?')}}</a>
                                    </div>
                                </div>
                                <div class="row mt-[25px]">
                                    <div class="col">
                                        <button id="PasswordResetFormButton" form="password_reset_form" class="btn btn-primary w-100">{{__('Send Instructions')}}</button>
                                    </div>
                                </div>
                                <!-- TODO Openai Demo -->
                            </form>
                            @if($setting->register_active == 1)
                                <div class="mt-20 text-center text-muted">
                                    {{__("Don't have account yet?")}} <a href="{{route('register')}}" tabindex="-1" class="font-medium underline">{{__('Sign up')}}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
