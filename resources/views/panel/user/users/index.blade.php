@extends('panel.layout.app')
@section('title', __('Users'))
@section('content')
<div class="page-header">
	<div class="container-xl">
        <div class="row g-2 items-center flex justify-between">
            <div class="col">
                <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.index')) }}" class="page-pretitle flex items-center">
                    <svg class="!me-2 rtl:-scale-x-100" width="8" height="10" viewBox="0 0 6 10" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M4.45536 9.45539C4.52679 9.45539 4.60714 9.41968 4.66071 9.36611L5.10714 8.91968C5.16071 8.86611 5.19643 8.78575 5.19643 8.71432C5.19643 8.64289 5.16071 8.56254 5.10714 8.50896L1.59821 5.00004L5.10714 1.49111C5.16071 1.43753 5.19643 1.35718 5.19643 1.28575C5.19643 1.20539 5.16071 1.13396 5.10714 1.08039L4.66071 0.633963C4.60714 0.580392 4.52679 0.544678 4.45536 0.544678C4.38393 0.544678 4.30357 0.580392 4.25 0.633963L0.0892856 4.79468C0.0357141 4.84825 0 4.92861 0 5.00004C0 5.07146 0.0357141 5.15182 0.0892856 5.20539L4.25 9.36611C4.30357 9.41968 4.38393 9.45539 4.45536 9.45539Z" />
                    </svg>
                    {{ __('Back to dashboard') }}
                </a>
                <h2 class="page-title mb-2">
                    {{ __('Users') }}
                </h2>
            </div>
        </div>
	</div>
</div>
<!-- Page body -->
<div class="page-body">
	<div class="container-xl">
            <div class="card">
                <div id="table-default" class="card-table table-responsive">
                    <table id="userTable" class="table">
                        <thead>
                            <tr>
                                <th><button class="table-sort" data-sort="sort-name">{{ __('Name') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-email">{{ __('Email') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-ideas">{{ __('Ideas') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-remaining-words">{{ __('Remaining Words') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-date">{{ __('Created At') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-last-login">{{ __('Last Login') }}</th>
                            </tr>
                        </thead>
                        <tbody class="table-tbody align-middle text-heading">
                            @foreach ($data as $entry)
                                <tr>
                                    <td class="sort-name">{{ $entry->fullName() }}</td>
                                    <td class="sort-email">{{ $entry->email }}</td>
                                    <td class="sort-ideas">{{ $entry->projects()->count() }}</td>
                                    <td class="sort-remaining-words">{{ $entry->remaining_words }}</td>
                                    <td class="sort-date" data-date="{{ strtotime($entry->created_at) }}">
                                        <p class="m-0">{{ date('j.n.Y', strtotime($entry->created_at)) }}</p>
                                        <p class="m-0 text-muted">{{ date('H:i:s', strtotime($entry->created_at)) }}
                                        </p>
                                    </td>
                                    <td class="sort-last-login" data-last-login="{{ strtotime($entry->last_login ?? $entry->created_at) }}">
                                        <p class="m-0">{{ date('j.n.Y', strtotime($entry->last_login ?? $entry->created_at)) }}</p>
                                        <p class="m-0 text-muted">{{ date('H:i:s', strtotime($entry->last_login ?? $entry->created_at)) }}
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                    <div
                        class="flex items-center border-solid border-t border-r-0 border-b-0 border-l-0 border-[--tblr-border-color] px-[--tblr-card-cap-padding-x] py-[--tblr-card-cap-padding-y] [&_.rounded-md]:rounded-full">
                        {{-- <ul class="pagination m-0 ms-auto p-0"></ul> --}}
                        <div class="m-0 ms-auto p-0">{{ $data->links() }}</div>
                    </div>
                </div>
            </div>
	</div>
</div>
@endsection
@section('script')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            "use strict";
            const list = new List('table-default', {
                sortClass: 'table-sort',
                listClass: 'table-tbody',
                valueNames: [
                    'sort-name',
                    'sort-email',
                    'sort-ideas',
                    'sort-remaining-words',
                    { attr: 'data-date', name: 'sort-date' },
                    { attr: 'data-last-login', name: 'sort-last-login' },
                ],
                // page: 10,
                // pagination: {
                //     innerWindow: 1,
                //     left: 0,
                //     right: 0,
                //     paginationClass: "pagination",
                // },
            });
        })
    </script>
@endsection
