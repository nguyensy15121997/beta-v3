@extends('panel.layout.app', ['disable_header' => true, 'disable_navbar' => true, 'disable_footer' => true])

@section('content')
    <style>
        .lang_ar.lqd-generator-sidebar-collapsed .lqd-generator-sidebar {
                --tw-translate-x: calc((100% - 35px)) !important;
        }
        .lang_ar.lqd-generator-sidebar-collapsed form.invisible {
            --tw-translate-x: 50%;
        }
    </style>
    <div
        class="lang_{{ app()->getLocale() }} lqd-generator-v2 group/generator [--editor-bb-h:40px] [--editor-tb-h:50px] [--sidebar-w:min(440px,90vw)]"
        :class="{ 'lqd-generator-sidebar-collapsed': sideNavCollapsed }"
        x-data="generatorV2"
    >
        @include('panel.user.generator.components.sidebar')
        @include('panel.user.generator.components.editor')
    </div>
@endsection
