@extends('panel.layout.app')
@section('title', __('Analytics'))
@section('content')
<div class="page-header">
	<div class="container-xl">
        <div class="row g-2 items-center flex justify-between">
            <div class="col">
                <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.index')) }}" class="page-pretitle flex items-center">
                    <svg class="!me-2 rtl:-scale-x-100" width="8" height="10" viewBox="0 0 6 10" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M4.45536 9.45539C4.52679 9.45539 4.60714 9.41968 4.66071 9.36611L5.10714 8.91968C5.16071 8.86611 5.19643 8.78575 5.19643 8.71432C5.19643 8.64289 5.16071 8.56254 5.10714 8.50896L1.59821 5.00004L5.10714 1.49111C5.16071 1.43753 5.19643 1.35718 5.19643 1.28575C5.19643 1.20539 5.16071 1.13396 5.10714 1.08039L4.66071 0.633963C4.60714 0.580392 4.52679 0.544678 4.45536 0.544678C4.38393 0.544678 4.30357 0.580392 4.25 0.633963L0.0892856 4.79468C0.0357141 4.84825 0 4.92861 0 5.00004C0 5.07146 0.0357141 5.15182 0.0892856 5.20539L4.25 9.36611C4.30357 9.41968 4.38393 9.45539 4.45536 9.45539Z" />
                    </svg>
                    {{ __('Back to dashboard') }}
                </a>
                <h2 class="page-title mb-2">
                    {{ __('Analytics') }}
                </h2>
            </div>
        </div>
	</div>
</div>
<!-- Page body -->
<div class="page-body">
	<div class="container-xl">
	    <div class="row row-deck row-cards max-xl:[--tblr-gutter-y:1.5rem]">
			<div class="col-sm-6 col-xl-3">
				<div class="card card-sm">
					<div class="card-body">
						<div class="row align-items-center">
							<div class="col-auto">
								<span class="avatar bg-white dark:!bg-[rgba(255,255,255,0.05)]">
									<svg class="svgusermanage" xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 24 24" stroke="var(--lqd-vertical-nav-link-color)" stroke-width="1.5" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path d="M9 7m-4 0a4 4 0 1 0 8 0a4 4 0 1 0 -8 0"></path>
                                        <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                        <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
                                    </svg>
								</span>
							</div>
							<div class="col">
								<p class="font-weight-medium mb-1">
									{{__('Users')}}
								</p>
								<h3 class="text-[20px] mb-0 flex items-center">
									{{ $data['user_count'] }}
                                </h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-xl-3">
				<div class="card card-sm">
					<div class="card-body">
						<div class="row align-items-center">
							<div class="col-auto">
								<span class="avatar bg-white dark:!bg-[rgba(255,255,255,0.05)]">
								    <svg class="svgblog" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke="var(--lqd-vertical-nav-link-color)" stroke-width="1.5" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                        <path d="M4 20h4l10.5 -10.5a1.5 1.5 0 0 0 -4 -4l-10.5 10.5v4"></path>
                                        <path d="M13.5 6.5l4 4"></path>
                                    </svg>
								</span>
							</div>
							<div class="col">
								<p class="font-weight-medium mb-1">
									{{__('Total Ideas')}}
								</p>
								<h3 class="text-[20px] mb-0 flex items-center">
									{{ $data['idea_count'] }}
                                </h3>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="col-sm-6 col-xl-3">
				<div class="card card-sm">
					<div class="card-body">
						<div class="row align-items-center">
							<div class="col-auto">
							    <svg class="svgaichat" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke="var(--lqd-vertical-nav-link-color)" stroke-width="1.5" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path d="M3 20l1.3 -3.9a9 8 0 1 1 3.4 2.9l-4.7 1"></path>
                                    <path d="M12 12l0 .01"></path>
                                    <path d="M8 12l0 .01"></path>
                                    <path d="M16 12l0 .01"></path>
                                </svg>
							</div>
							<div class="col">
								<p class="font-weight-medium mb-1">
									{{__('Fikra Experts Chats')}}
								</p>
								<h3 class="text-[20px] mb-0 flex items-center">
									{{ $data['chat_count'] }}
								</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-xl-3"></div>
			<div class="col-md-6">
				<div class="card"">
					<div class="card-header">
						<h3 class="card-title text-heading">{{__('Concept Tools')}}</h3>
					</div>
					<div class="card-table table-responsive grow">
						<table class="table table-vcenter">
							<thead>
								<tr>
									<th>{{__('Tools Name')}}</th>
									<th>{{__('Total')}}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ __('SWOT Analysis') }}</td>
									<td>{{ $data['swot_analyses_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('Lean Canvas') }}</td>
									<td>{{ $data['lean_canvases'] }}</td>
								</tr>
								<tr>
									<td>{{ __('User Personas') }}</td>
									<td>{{ $data['user_personas_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('User Stories') }}</td>
									<td>{{ $data['user_stories_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('Competitive Analysis') }}</td>
									<td>{{ $data['competitive_analysis_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('Storytelling') }}</td>
									<td>{{ $data['storytelling_count'] }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title text-heading">{{__('Marketing Tools')}}</h3>
					</div>
					<div class="card-table table-responsive grow">
						<table class="table table-vcenter">
							<thead>
								<tr>
									<th>{{ __('Marketing Name') }}</th>
									<th>{{ __('Total') }}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ __('Article Generator') }}</td>
									<td>{{ $data['article_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('Blog Post Ideas') }}</td>
									<td>{{ $data['blog_idea_count'] }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
    <script>
        
    </script>
@endsection
