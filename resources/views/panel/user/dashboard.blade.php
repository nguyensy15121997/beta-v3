@extends('panel.layout.app')
@section('title', __('Dashboard'))

@section('content')
    <!-- Create Submit Idea Modal -->
    <div class="modal fade" id="creatSubmitIdeaModal" tabindex="-1" aria-labelledby="creatSubmitIdeaLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ LaravelLocalization::localizeUrl( route('dashboard.user.openai.projects.store')) }}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="creatSubmitIdeaLabel">{{ __('Submit a New Idea') }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <label for="name" class="form-label">{{ __('Idea Title') }}</label>
                        <input type="text" class="form-control" name="name" placeholder="{{ __('Give your idea a title') }}" required>
                        <br>
                        <label class="form-label">{{ __('Idea Description') }}</label>
                        <textarea class="form-control" id="description" name="description" rows="12" placeholder="{{ __('Comprehensively explain your idea') }}"
                            maxlength="1000" required="required"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                            data-bs-dismiss="modal">{{ __('Cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container-xl">
            <div class="row g-2 items-center justify-between max-md:flex-col max-md:items-start max-md:gap-4">
                <div class="col">
                    <div class="page-pretitle">
                        {{ __('Fikra Space') }}
                    </div>
                    <h2 class="mb-2 page-title">
                        {{ __('Welcome') }}, {{ \Illuminate\Support\Facades\Auth::user()->name }}!
                    </h2>
                </div>
                <div class="col-auto">
                    <div class="btn-list">
                        <a @if ($app_is_demo) onclick="return toastr.info('This feature is disabled in Demo version.')" @else data-bs-toggle="modal" data-bs-target="#creatSubmitIdeaModal" @endif
                            class="btn btn-primary items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="!me-2" width="18" height="18"
                                viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                <path d="M12 5l0 14" />
                                <path d="M5 12l14 0" />
                            </svg>
                            {{ __('Submit a New Idea') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">
            <div class="row row-deck row-cards">
<!--                <div class="col-12">
                    @if (view()->exists('panel.admin.custom.user.payment.subscriptionStatus'))
                        @include('panel.admin.custom.user.payment.subscriptionStatus')
                    @else
                        @include('panel.user.finance.subscriptionStatus')
                    @endif
                </div>
-->

                @if ($ongoingPayments != null)
                    <div class="col-12">
                        @include('panel.user.finance.ongoingPayments')
                    </div>
                @endif

                <div class="col-12">
                    <div class="card">
                        <div class="card-header justify-between">
                            <h3 class="card-title text-heading">{{ __('Ideas') }}</h3>
                            <div class="btn-list">
                                
                            </div>
                        </div>
                        <div class="card-table table-responsive">
                            <table class="table table-vcenter">
                                <tbody>
                                    @php
                                        $ideas = Auth::user()->projects()->orderBy('created_at', 'desc')->take(3)->paginate(10);
                                    @endphp
                                    @if ($ideas->count() > 0)
                                        @foreach ($ideas as $entry)
                                            <tr class="hover:bg-black hover:bg-opacity-[0.03] link">
                                                <td class="w-1 !pe-0">
                                                    <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.show', $entry->id)) }}"
                                                        class="block text-truncate text-heading hover:no-underline">
                                                        <span class="avatar w-[43px] h-[43px] [&amp;_svg]:w-[20px] [&amp;_svg]:h-[20px]" style="background: #A3D6C2">
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="48" viewBox="0 96 960 960" width="48"><path d="M140 936q-24.75 0-42.375-17.625T80 876V216l67 67 66-67 67 67 67-67 66 67 67-67 67 67 66-67 67 67 67-67 66 67 67-67v660q0 24.75-17.625 42.375T820 936H140Zm0-60h310V596H140v280Zm370 0h310V766H510v110Zm0-170h310V596H510v110ZM140 536h680V416H140v120Z"></path></svg>
                                                        </span>
                                                    </a>
                                                </td>
                                                <td class="td-truncate">
                                                    <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.show', $entry->id)) }}"
                                                        class="block text-truncate text-heading hover:no-underline">
                                                        <span class="font-medium">{{ $entry->name }}</span>
                                                        <br>
                                                        <span
                                                            class="italic text-muted opacity-80 dark:!text-white dark:!opacity-50">{{ $entry->description }}</span>
                                                    </a>
                                                </td>
                                                <td class="text-nowrap">
                                                    <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.show', $entry->id)) }}"
                                                        class="block text-truncate text-heading hover:no-underline">
                                                        <span class="text-heading">{{ __('Submited On') }}</span>
                                                        <br>
                                                        <span class="italic text-muted opacity-80 dark:!text-white dark:!opacity-50">{{ $entry->created_at->format('M d, Y') }}</span>
                                                    </a>
                                                </td>
                                                <td class="text-nowrap">
                                                    <a  data-url="{{ route('dashboard.user.openai.projects.destroy', $entry->id) }}"
                                                        class="btn p-0 border w-[36px] h-[36px] hover:bg-red-600 hover:text-white"
                                                        title="{{ __('Delete') }}"
                                                        onclick="
                                                        event.preventDefault();
                                                        return deleteIdea('{{ route('dashboard.user.openai.projects.destroy', $entry->id) }}');"
                                                        >
                                                        <svg width="10" height="10" viewBox="0 0 10 10" fill="currentColor"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M9.08789 1.74609L5.80664 5L9.08789 8.25391L8.26758 9.07422L4.98633 5.82031L1.73242 9.07422L0.912109 8.25391L4.16602 5L0.912109 1.74609L1.73242 0.925781L4.98633 4.17969L8.26758 0.925781L9.08789 1.74609Z" />
                                                        </svg>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-heading py-5">
                                            <td class="w-1 !pe-0">
                                            {{ __('You have no ideas. Submit one to get started.') }}
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        {{ $ideas->links('pagination::bootstrap-5') }}
                    </div>
                </div>


            <!--    <div class="col-md-6">-->
            <!--        <div class="card">-->
            <!--            <div class="card-header">-->
            <!--                <h3 class="card-title text-heading">{{ __('Favorite Templates') }}</h3>-->
            <!--            </div>-->
            <!--            <div class="card-table table-responsive">-->
            <!--                <table class="table table-vcenter">-->
            <!--                    <tbody>-->
            <!--                        @php-->
            <!--                            $plan = Auth::user()->activePlan();-->
            <!--                            $plan_type = 'regular';-->

            <!--                            if ($plan != null) {-->
            <!--                                $plan_type = strtolower($plan->plan_type);-->
            <!--                            }-->
            <!--                        @endphp-->
            <!--                        @foreach (\Illuminate\Support\Facades\Auth::user()->favoriteOpenai as $entry)-->
            <!--                            @php-->
            <!--                                $upgrade = false;-->
            <!--                                if ($entry->premium == 1 && $plan_type === 'regular') {-->
            <!--                                    $upgrade = true;-->
            <!--                                }-->
            <!--                            @endphp-->
            <!--                            <tr class="relative">-->
            <!--                                <td class="w-1 !pe-0">-->
            <!--                                    <span class="avatar w-[43px] h-[43px] [&_svg]:w-[20px] [&_svg]:h-[20px]"-->
            <!--                                        style="background: {{ $entry->color }}">-->
            <!--                                        @if ($entry->image !== 'none')-->
            <!--                                            {!! html_entity_decode($entry->image) !!}-->
            <!--                                        @endif-->

            <!--                                        @if ($entry->active == 1)-->
            <!--                                            <span class="badge bg-green !w-[9px] !h-[9px]"></span>-->
            <!--                                        @else-->
            <!--                                            <span class="badge bg-red !w-[9px] !h-[9px]"></span>-->
            <!--                                        @endif-->
            <!--                                    </span>-->
            <!--                                </td>-->
            <!--                                <td class="td-truncate">-->
            <!--                                    @if ($upgrade)-->
            <!--                                        <div-->
            <!--                                            class="absolute top-0 left-0 w-full h-full transition-all z-2 @if ($upgrade) bg-[rgba(var(--tblr-body-bg-rgb),0.75)] @endif">-->
            <!--                                            <div-->
            <!--                                                class="absolute top-4 left-1/2 z-10 bg-[#E2FFFC] text-black font-medium p-2 rounded-md">-->
            <!--                                                {{ __('Upgrade') }}-->
            <!--                                            </div>-->
            <!--                                            <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.payment.subscription')) }}"-->
            <!--                                                class="z-20 inline-block w-full h-full absolute top-0 left-0 overflow-hidden -indent-[99999px]">-->
            <!--                                                {{ __('Upgrade') }}-->
            <!--                                            </a>-->
            <!--                                        </div>-->
            <!--                                        <a href="#" class="text-heading hover:no-underline">-->
            <!--                                            <span class="font-medium">{{ __($entry->title) }}</span>-->
            <!--                                            <br>-->
            <!--                                            <span-->
            <!--                                                class="block italic text-muted opacity-80 text-truncate dark:!text-white dark:!opacity-50">{{ __($entry->description) }}</span>-->
            <!--                                        </a>-->
            <!--                                    @elseif ($entry->active == 1)-->
            <!--                                        <a href="@if ($entry->type == 'voiceover') {{ LaravelLocalization::localizeUrl(route('dashboard.user.openai.generator', $entry->slug)) }}@else {{ LaravelLocalization::localizeUrl(route('dashboard.user.openai.generator.workbook', $entry->slug)) }} @endif"-->
            <!--                                            class="text-heading hover:no-underline">-->
            <!--                                            <span class="font-medium">{{ __($entry->title) }}</span>-->
            <!--                                            <br>-->
            <!--                                            <span-->
            <!--                                                class="block italic text-muted opacity-80 text-truncate dark:!text-white dark:!opacity-50">{{ __($entry->description) }}</span>-->
            <!--                                        </a>-->
            <!--                                    @else-->
            <!--                                        <div class="text-heading hover:no-underline">-->
            <!--                                            <span class="font-medium">{{ __($entry->title) }}</span>-->
            <!--                                            <br>-->
            <!--                                            <span-->
            <!--                                                class="block italic text-muted opacity-80 text-truncate dark:!text-white dark:!opacity-50">{{ __($entry->description) }}</span>-->
            <!--                                        </div>-->
            <!--                                    @endif-->
            <!--                                </td>-->
            <!--                                <td class="text-nowrap">-->
            <!--                                    <span class="text-heading">{{ __('in Workbook') }}</span>-->
            <!--                                    <br>-->
            <!--                                    <span-->
            <!--                                        class="italic text-muted opacity-80">{{ $entry->created_at->format('M d, Y') }}</span>-->
            <!--                                </td>-->
            <!--                            </tr>-->
            <!--                            @if ($loop->iteration == 4)-->
            <!--                            @break-->
            <!--                        @endif-->
            <!--                    @endforeach-->
            <!--                </tbody>-->
            <!--            </table>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->

        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        // $('#delete_idea').on('click', function(event) {
        //     var url = this.data('url');
        //     $.ajax({
        //         url: url,
        //         type: 'DELETE',
        //         success: function(response) {
        //             toastr.success(response.message); 
        //             location.reload();
        //         },
        //         error: function(error) {toastr.error("{{ __('Error delete project:') }}", error);}
        //     })
        // });
        
        function goToDetailPage(id) {
            let url = "{{ route('dashboard.user.ideas.show', ':project') }}";
            url = url.replace(':project', id);
            location.href = url;
        }
    </script>
@endsection