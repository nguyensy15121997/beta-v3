@extends('panel.layout.app')
@section('title', __('Workbook'))
@section('content')
    <div class="page-header lang_{{app()->getLocale()}}">
        <div class="container-xl">
            <div class="row g-2 items-center">
                <div class="col">
                    <p class="page-pretitle">
                        {{ __('Edit your generations.') }}
                    </p>
                    <h2 class="page-title mb-2">
                        {{ __('Workbook') }}
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- Page body -->
    <div class="page-body pt-6 max-md:pt-3">
        <div class="container-xl">
            <div class="row">
                <div class="col-12"></div>
                <div class="col-lg-8 mx-auto">
                    @include('panel.user.openai.details')
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('script')
    <script src="/assets/libs/tinymce/tinymce.min.js" defer></script>
    <script src="/assets/js/panel/workbook.js"></script>
@endsection
