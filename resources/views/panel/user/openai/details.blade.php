@extends('panel.layout.app')
@section('title', __('Idea Details'))
@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/fontawesome.min.css"
        integrity="sha512-W5OxdLWuf3G9SMWFKJLHLct/Ljy7CmSxaABQLV2WIfAQPQZyLSDW/bKrw71Nx7mZKN5zcL+r8pRCZw+5bIoHHw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://unpkg.com/@icon/themify-icons/themify-icons.css">
    <style>
        .image-background {
            margin-top: 0px !important;
            background-image: url('/assets/img/browser_mockup.png');
            background-size: cover;
            background-repeat: no-repeat;
            min-height: 300px;
        }
        .image-iframe {
            min-height: 240px;
        }
        
        @media only screen and (min-width: 1200px) {
            .image-background {
                min-height: 400px;
            }
            .image-iframe {
                min-height: 330px;
            }
        }
        h6 .show-anwser:hover {
            transform: unset;
        }
        .table-vcenter.table {
            table-layout: unset;
        }
        tbody,td,tfoot,th,thead,tr {
            border-color: var(--tblr-border-color)
        }
        
        tr:last-child td {
            border-bottom: none
        }
        
        .table-vcenter>:not(caption)>*>* {
            padding: 1.1rem
        }
        
        .table thead th {
            padding-top: 1.75em;
            padding-bottom: 1.05em;
            border-color: var(--tblr-border-color);
            background: none;
            font-weight: var(--tblr-font-weight-medium)
        }
        .edit-popup {
            display: none;
        }
        .bg-edit {
            background-color: transparent;
        }
        .bottom-40 {
            bottom: 9rem;
        }
        .lang_ar th, .lang_ar button, .lang_ar p, .lang_ar td {
            text-align: right !important;
            direction: rtl;
        }
        .lang_ar ul {
            padding-inline-start: 0px;
        }
        .lang_ar .position-popup-100 {
            margin-right: -150px;
        }
        .lang_ar .show-anwser {
            justify-content: right !important;
        }
        .lang_ar .edit-popup {
            left: 5px;
        }
        .lang_ar .position-edit-15 {
            left: 15px;
            right: unset;
        }
        .lang_ar .postion-edit-5 {
            left: 5px;
            right: unset;
        }
        .lang_ar .postion-edit-10 {
            left: 10px;
            right: unset;
        }
        .lang_ar .postion-edit-5-15 {
            left: -15px;
            right: unset;
        }
        .lang_ar .position-edit-5-0 {
            left: 0px;
            right: unset;
        }
        .position-edit-5-0 {
            top: 5px;
            right: 0px;
        }
        .postion-edit-10 {
            top: 10px;
            right: 10px;
        }
        .postion-edit-5 {
            z-index: 999;
            top: 5px;
            right: 5px;
        }
        .postion-edit-5-15 {
            top: 5px;
            right: -15px;
        }
        .position-edit-15 {
            top: 15px;
            right: 15px;
        }
        #overlay {
            position: absolute;
            top: 0;
            z-index: 100;
            width: 100%;
            height: 100%;
            display: none;
            background: rgba(0, 0, 0, 0.6);
        }

        .cv-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .spinner {
            width: 40px;
            height: 40px;
            border: 4px #ddd solid;
            border-top: 4px #2e93e6 solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        }

        @keyframes sp-anime {
            100% {
                transform: rotate(360deg);
            }
        }

        .is-hide {
            display: none;
        }

        .btn-success {
            --bs-btn-bg: #21a29a !important;
            border: none;
        }

        .btn-success:hover {
            --bs-btn-bg: #2a2a2a !important;
            border: none;
        }

        .btn-danger {
            --bs-btn-bg: #fa690e !important;
            border: none;
        }

        .btn-danger:hover {
            --bs-btn-bg: #2a2a2a !important;
            border: none;
        }

        #swot-plus @keyframes gradientAnimation {
            0% {
                background-position: 0% 50%;
            }

            100% {
                background-position: 100% 50%;
            }
        }

        body.theme-10 .bg-primary {
            background: linear-gradient(45deg, #14a299, #36c291, #14a299, #36c291, #14a299) !important;
            background-size: 200% 200% !important;
            animation: gradientAnimation 10s linear infinite !important;
        }

        .card-faq {
            border: none;
        }

        .card-faq-header {
            display: block !important;
            border: none;
        }

        .card-faq-header button {
            width: 100%;
            padding: 10px 20px;
            font-size: 1.2em;
            font-weight: bold;
            color: #333;
            display: flex;
            justify-content: left;
            align-items: center
        }

        .card-faq-body {
            border-top: 1px solid #eee;
            display: none;
        }

        .card-faq-body.show {
            display: block;
        }

        /* Style for the name squares */
        #startupNaming>div {
            width: 330px;
            /* Adjust the width to your preference */
            height: 100px;
            /* Set the height equal to the width for squares */
            background-color: #077784;
            /* Background color for the squares */
            color: #fff;
            /* Text color */
            text-align: center;
            /* Center text horizontally */
            line-height: 100px;
            /* Center text vertically */
            margin: 5px;
            /* Add spacing between squares */
            display: inline-block;
            /* Display as inline-block to appear in a row */
            border-radius: 5px;
            /* Rounded corners (optional) */
            font-size: 14px;
            /* Font size for the text */
            font-weight: bold;
            /* Bold text (optional) */
        }

        /* Style for the tab content container (adjust as needed) */
        .tab-content {
            margin-top: 10px;
            /* Add space between tabs and content */
        }

        /* Style for the "Not generated" message (adjust as needed) */
        .tab-content:empty::before {
            content: "Not generated.";
            font-weight: bold;
            color: #d35400;
            /* Custom color for the message */
            display: block;
            text-align: center;
            margin-top: 10px;
            /* Adjust spacing */
        }

        /* Tab container */
        .tab-container {
            display: flex;
            margin-bottom: 20px;
        }

        /* Individual tab */
        .tab {
            padding: 10px 20px;
            cursor: pointer;
            border: 1px solid #cccccc;
            border-bottom: none;
            background-color: #f9f9f9;
            margin-right: 8px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            transition: background-color 0.2s ease-in-out;
        }

        /* Active tab */
        /* Tabs container */
        .tab-container {
            display: flex;
            margin-bottom: 1rem;
            /* Space below the tabs */
        }

        /* Individual tab */
        .tab {
            flex: 1;
            /* Each tab takes up equal width */
            text-align: center;
            /* Center the text inside the tab */
            padding: 0.75rem 1rem;
            cursor: pointer;
            border: 1px solid #ccc;
            /* Light grey border */
            background-color: #f9f9f9;
            /* Light grey background */
            transition: background-color 0.3s, border-color 0.3s;
        }

        /* Active tab */
        .tab.active {
            background-color: #fa690e;
            /* Orange background */
            color: white;
            /* White text */
            border-color: #fa690e;
            /* Orange border */
        }

        /* Non-active tabs hover effect */
        .tab:not(.active):hover {
            background-color: #f3f3f3;
            /* Slightly darker grey on hover */
        }

        /* Tab content container */
        .tab-content {
            display: none;
            /* Hide all tab content by default */
            padding: 1rem;
            border-top: none;
            /* Remove top border */
        }

        /* Display the active tab content */
        .tab-content.active {
            display: block;
        }
        
        .theme-dark .tab-header, .theme-dark .tab {
            color: rgb(97, 104, 118);
        }
        
        .theme-dark .canvas-block {
            border: 1px rgba(255, 255, 255, 0.04) solid;
        }
        
        .theme-dark .card-faq-header button {
            color: #f8fafc;
        }
        
        .theme-dark .table tbody tr:hover {
            background-color: rgb(97, 104, 118);
        }

        .tab-header {
            font-size: 13px;
            padding: 9px 6px;
            flex: 1;
            text-align: center;
            cursor: pointer;
            border: 1px solid #ccc;
            background-color: #f9f9f9;
            transition: background-color 0.3s, border-color 0.3s;
        }
        .tab-header.active {
            background-color: #fa690e;
            color: white;
            border-color: #fa690e;
        }

        .table {
            width: 100%;
            table-layout: fixed;
            /* This makes the table layout fixed */
            border-collapse: collapse;
        }

        .table th,
        .table td {
            padding: 10px;
            overflow-wrap: break-word;
            /* Ensure that words can be broken and wrapped */
            word-wrap: break-word;
            /* Legacy property for older browsers */
            white-space: normal;
            /* Ensures that the whitespace is handled normally */
        }

        /* Table header */
        .table thead th {
            background-color: #f2f2f2;
            /* Light grey header */
            padding: 1rem;
            text-align: left;
        }

        /* Table body */
        .table tbody td {
            padding: 1rem;
            text-align: left;
            border-top: 1px solid #e0e0e0;
            /* Light grey row borders */
        }

        /* Hover effect for rows */
        .table tbody tr:hover {
            background-color: #f9f9f9;
        }

        .blur-content {
            display: none !important;
        }

        .mesh-loader {
            position: fixed;
            /* Fixed position to cover the entire viewport */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255, 255, 255, 0.8);
            /* Semi-transparent background */
            z-index: 9999;
            /* High z-index to keep it above other content */
            display: flex;
            justify-content: center;
            /* Center horizontally */
            align-items: center;
            /* Center vertically */
        }

        .mesh-loader .circle {
            width: 25px;
            height: 25px;
            position: absolute;
            background: #fd661a;
            border-radius: 50%;
            margin: -12.5px;
            -webkit-animation: mesh 3s ease-in-out infinite;
            animation: mesh 3s ease-in-out infinite -1.5s;
        }

        .mesh-loader>div .circle:last-child {
            -webkit-animation-delay: 0s;
            animation-delay: 0s;
        }

        .mesh-loader>div {
            position: absolute;
            top: 50%;
            left: 50%;
        }

        .mesh-loader>div:last-child {
            -webkit-transform: rotate(90deg);
            transform: rotate(90deg);
        }

        @-webkit-keyframes mesh {
            0% {
                -webkit-transform-origin: 50% -100%;
                transform-origin: 50% -100%;
                -webkit-transform: rotate(0);
                transform: rotate(0);
            }

            50% {
                -webkit-transform-origin: 50% -100%;
                transform-origin: 50% -100%;
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }

            50.00001% {
                -webkit-transform-origin: 50% 200%;
                transform-origin: 50% 200%;
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform-origin: 50% 200%;
                transform-origin: 50% 200%;
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes mesh {
            0% {
                -webkit-transform-origin: 50% -100%;
                transform-origin: 50% -100%;
                -webkit-transform: rotate(0);
                transform: rotate(0);
            }

            50% {
                -webkit-transform-origin: 50% -100%;
                transform-origin: 50% -100%;
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }

            50.00001% {
                -webkit-transform-origin: 50% 200%;
                transform-origin: 50% 200%;
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform-origin: 50% 200%;
                transform-origin: 50% 200%;
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .swot-container {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            gap: 20px;
            max-width: 100%;
        }
        
        @media only screen and (max-width: 600px) {
            .swot-container {
                grid-template-columns: repeat(1, 1fr);
            }
        }
        
        @media only screen and (max-width: 700px) {
            .lean-canvas {
                display: block !important;
            }
        }

        .swot-section {
            border-radius: 20px;
            padding: 40px 50px 40px 50px;
            color: white;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100%;
        }

        .strengths {
            background-color: #fa690e;
            /* Light blue */
        }

        .weaknesses {
            background-color: #21a29a;
            /* Light orange */
        }

        .opportunities {
            background-color: #404040;
            /* Light green */
        }

        .threats {
            background-color: #000;
            /* Light red */
        }

        .swot-title {
            font-size: 24px;
            margin-bottom: 10px;
        }

        .swot-letter {
            font-size: 72px;
            font-weight: bold;
            opacity: 0.2;
            margin-top: -80px;
        }




        .lean-canvas {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            grid-gap: 10px;
            max-width: 100%;
            margin: 20px auto;
            font-family: Arial, sans-serif;
        }

        .canvas-block {
            border: 1px solid #e9e9e9;
            padding: 20px;
            border-radius: 5px;
        }

        .container {
            width: 100%;
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));
            grid-gap: 0.75rem;
        }

        .box {
            position: relative;
            width: auto;
            border-radius: 10px !important;
            height: 125px;
            background: #fff;
            border-radius: 5px;
            cursor: pointer;
            box-shadow: 10px 10px 15px rgba(59, 38, 38, 0.25);
        }

        .box::before {
            border-radius: 10px !important;

            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 6px;
            height: 100%;
            background: var(--color);
            transition: 0.5s ease-in-out;
        }

        .box:hover::before {
            width: 100%;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
        }


        .box .content {
            position: relative;
            display: flex;
            align-items: center;
            height: 100%;
        }

        .box .content .icon {
            position: relative;
            min-width: 70px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-size: 1.5em;
            color: var(--color);
            transition: 0.5s ease-in-out;
        }

        .box:hover .content .icon {
            color: #fff;
        }

        .box .content .text h3 {
            font-weight: 500;
            color: var(--color);
            transition: 0.5s ease-in-out;
        }

        .box .content .text p {
            font-size: 0.9em;
            color: #999;
            transition: 0.5s ease-in-out;
        }

        .box:hover .content .text h3,
        .box:hover .content .text p {
            color: #fff;
        }



        /* Assigning grid areas */
        #problem {
            grid-area: 1 / 1 / 2 / 3;
        }

        #solution {
            grid-area: 1 / 3 / 2 / 4;
        }

        #unique-value-prop {
            grid-area: 1 / 4 / 2 / 6;
        }

        #unfair-advantage {
            grid-area: 2 / 4 / 3 / 6;
        }

        #customer-segments {
            grid-area: 2 / 1 / 2 / 4;
        }

        #existing-alternatives {
            grid-area: 3 / 1 / 4 / 3;
        }

        #key-metrics {
            grid-area: 3 / 3 / 3 / 6;
        }

        #channels {
            grid-area: 0 / 3 / 5 / 4;
        }

        #revenue-streams {
            grid-area: 4 / 2 / 5 / 5;
        }

        #cost-structure {
            grid-area: 4 / 5 / 5 / 6;
        }

        h2 {
            font-size: 18px;
            color: #333;
            margin-bottom: 10px;
        }


        /* You might want to add media queries to ensure responsiveness */
        @media (max-width: 768px) {
            .lean-canvas {
                grid-template-columns: 1fr;
                grid-template-rows: auto;
                grid-template-areas:
                    "problem"
                    "solution"
                    "unique-value-prop"
                    "unfair-advantage"
                    "customer-segments"
                    "existing-alternatives"
                    "key-metrics"
                    "channels"
                    "cost-structure"
                    "revenue-streams";
            }

            .canvas-block {
                margin-bottom: 10px;
            }
        }


        .horizontal-menu {
            display: flex;
            justify-content: start;
            background-color: #fff;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            border-radius: 4px;
            border: 1px solid lightgrey;
            margin-top: 15px;
            margin-bottom: 15px;

        }

        .menu-item {
            position: relative;
            display: flex;
            align-items: center;
            padding: 18px 30px;
            border-right: 1px solid lightgrey;
            cursor: pointer;
            width: 500px;
            justify-content: space-between;
        }

        .menu-item:hover {
            background: #ff640f;
            transition: 0.4s;
        }

        .menu-item:hover .menu-text {
            color: #fff;
            transition: 0.4s;
        }

        .menu-item:hover .dropdown-icon {
            color: #fff;
            transition: 0.4s;
        }

        .menu-item:last-child {
            border-right: none;
        }

        .menu-text {
            margin-right: 5px;
            font-size: 15px;
            font-weight: 600;
        }

        .dropdown-icon {
            margin: 0 5px;
            width: 100px;
        }

        .counter {
            height: 30px;
            width: 30px;
            padding: 15px;
            background-color: #e9e9e9;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            font-weight: 500;
            font-size: 0.9em;
        }

        .pro-pill {
            background-color: #20a29b;
            color: #fff;
            font-size: 12px;
            padding: 3px 8px;
            border-radius: 10px;
            margin-left: 8px;
            display: inline-block;
        }
        
        .list-group-item.fs-5 {
            padding: 3px;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            top: 100%;
            left: 0;
            background-color: #f9f9f9;
            min-width: 100%;
            box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
            z-index: 1;
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
            transition: 0.6s;

        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }
        
        .pro-feature {
            color: #969696!important;
            pointer-events: none!important;
        }

        .menu-item:hover .dropdown-content {
            display: block;
        }

        .menu-item:hover .counter {
            background: #fff;
        }

        /* Optional: Add some styling for when the dropdown is active */
        .menu-item.active .dropdown-content {
            display: block;
        }

        /* Optional: Style for the dropdown icon when active */
        .menu-item.active .dropdown-icon {
            transform: rotate(180deg);
        }

        .wrapper {
            text-align: center;
            margin-bottom: 20px;
            margin-top: -50px;
        }

        .tabs {
            margin-top: 50px;
            font-size: 15px;
            padding: 0px;
            list-style: none;
            background: #fff;
            box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.1);
            display: inline-block;
            border-radius: 50px;
            position: relative;
        }

        .tabs a {
            text-decoration: none;
            color: #777;
            text-transform: uppercase;
            padding: 10px 20px;
            display: inline-block;
            position: relative;
            z-index: 1;
            transition-duration: 0.6s;
        }

        .tabs a.active {
            color: #fff;
        }

        .tabs a i {
            margin-right: 5px;
        }

        .tabs .selector {
            height: 100%;
            display: inline-block;
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: 1;
            border-radius: 50px;
            transition-duration: 0.6s;
            transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);

            background: #05abe0;
            background: -moz-linear-gradient(45deg, #05abe0 0%, #8200f4 100%);
            background: -webkit-linear-gradient(45deg, #05abe0 0%, #8200f4 100%);
            background: linear-gradient(45deg, #ff640f 0%, #b74304 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#05abe0', endColorstr='#8200f4', GradientType=1);
        }
        .card {
            margin-top: 15px;
        }
        .btn-tooltip {
          position: relative;
          display: inline-block;
          border-bottom: 1px dotted black;
        }
        
        .tooltiptext {
            visibility: visible;
            position: absolute;
            width: 120px;
            background-color: #555;
            color: #fff;
            text-align: center;
            padding: 5px 0;
            border-radius: 6px;
            z-index: 100;
            opacity: 0;
            transition: opacity 2s ease;
        }
        .btn-tooltip .tooltiptext::after {
            content: '';
            position: absolute;
            top: 50%;
            right: -10px; /* Điều chỉnh giá trị này để điều chỉnh vị trí của tam giác */
            border-width: 5px;
            border-style: solid;
            border-color: transparent transparent transparent #555; /* Thay đổi màu sắc tùy theo nhu cầu */
            transform: translateY(-50%);
        }
        
        .tooltip-left2 {
            top: 15px;
            bottom: auto;
            right: 105%;
        }
        
        .lang_ar .tooltip-left2 {
            right: unset;
            left: 105%;
        }
        
        .lang_ar .btn-tooltip .tooltiptext::after {
            top: 50%;
            right: unset;
            left: -9px; /* Điều chỉnh giá trị này để điều chỉnh vị trí của tam giác */
            transform: translateY(-50%);
            border-color: transparent #555 transparent transparent;
        }
        
        .btn-tooltip:hover .tooltiptext {
          visibility: visible;
        }
    </style>
@endpush
@section('content')
    @if (!empty($storytelling)
    || !empty($leanCanvas) 
    || !empty($swotAnalysis)
    || (!empty($userStories) && $userStories->count() > 0)
    || (!empty($userPersonas) && $userPersonas->count() > 0)
    || (!empty($competitiveAnalysis) && $competitiveAnalysis->count() > 0))
        <div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
            <button
                onclick="toggleEditButton()"
                class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
                type="button">
                <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
            </svg>
            </button>
        </div>
    @endif
    <div class="group float-button fixed bottom-40 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
        <button
            class="btn-tooltip collapsed peer h-14 w-14 translate-x-0 transform-gpu rounded-full border-none bg-transparent p-0 !shadow-lg !outline-none"
            type="button">
            <span
                class="spinner_button before:animate-spin-grow relative mb-1 inline-flex h-full w-full items-center justify-center overflow-hidden rounded-full before:absolute before:left-0 before:top-0 before:h-full before:w-full before:rounded-full">
                <svg class="relative transition-transform duration-300 group-hover:rotate-[135deg]"
                    xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 24 24" stroke-width="3"
                    stroke="#fff" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M12 5l0 14"></path>
                    <path d="M5 12l14 0"></path>
                </svg>
                <div class="spinner-border text-blue p-1" style="display: none"></div>
            </span>
            <span class="tooltiptext tooltip-left2">{{ __('Generate') }}</span>
        </button>
        <div class="position-popup-100 text-heading !invisible absolute bottom-full end-0 !mb-4 min-w-[145px] translate-y-2 rounded-md bg-[--tblr-body-bg] text-sm font-medium !opacity-0 shadow-[0_3px_12px_rgba(0,0,0,0.08)] transition-all before:absolute before:inset-x-0 before:-bottom-4 before:h-4 group-hover:!visible group-hover:translate-y-0 group-hover:!opacity-100 dark:bg-zinc-800"
            id="add-new-floating" style="width: 250px">
            <span
                class="text-heading block border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 !px-4 !py-2 text-opacity-50 last:border-0 dark:!border-white dark:!border-opacity-5">{{ __('Items:') }}</span>
            <ul class="m-0 list-none p-0"
            >
                @if (Auth::user()->isAdmin() || Auth::user()->id == 14 || Auth::user()->id == '14')
                    <li
                        class="tab-launch border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                        <a class="{{ activeRoute('dashboard.user.openai.list') }} d-flex items-center relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                            href="https://websites.fikrahub.com/login?email={{ Auth::user()->email }}&idea_id={{ $project->id }}"
                            >
                            {{ __('Generate Landing Page') }}
                        </a>
                    </li>
                @else
                    <li
                        class="tab-launch border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                        <a class="{{ activeRoute('dashboard.user.openai.list') }} pro-feature d-flex items-center relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                            >
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-lock-square-rounded-filled" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 2c-.218 0 -.432 .002 -.642 .005l-.616 .017l-.299 .013l-.579 .034l-.553 .046c-4.785 .464 -6.732 2.411 -7.196 7.196l-.046 .553l-.034 .579c-.005 .098 -.01 .198 -.013 .299l-.017 .616l-.004 .318l-.001 .324c0 .218 .002 .432 .005 .642l.017 .616l.013 .299l.034 .579l.046 .553c.464 4.785 2.411 6.732 7.196 7.196l.553 .046l.579 .034c.098 .005 .198 .01 .299 .013l.616 .017l.642 .005l.642 -.005l.616 -.017l.299 -.013l.579 -.034l.553 -.046c4.785 -.464 6.732 -2.411 7.196 -7.196l.046 -.553l.034 -.579c.005 -.098 .01 -.198 .013 -.299l.017 -.616l.005 -.642l-.005 -.642l-.017 -.616l-.013 -.299l-.034 -.579l-.046 -.553c-.464 -4.785 -2.411 -6.732 -7.196 -7.196l-.553 -.046l-.579 -.034a28.058 28.058 0 0 0 -.299 -.013l-.616 -.017l-.318 -.004l-.324 -.001zm0 4a3 3 0 0 1 2.995 2.824l.005 .176v1a2 2 0 0 1 1.995 1.85l.005 .15v3a2 2 0 0 1 -1.85 1.995l-.15 .005h-6a2 2 0 0 1 -1.995 -1.85l-.005 -.15v-3a2 2 0 0 1 1.85 -1.995l.15 -.005v-1a3 3 0 0 1 3 -3zm3 6h-6v3h6v-3zm-3 -4a1 1 0 0 0 -.993 .883l-.007 .117v1h2v-1a1 1 0 0 0 -1 -1z" fill="#20a29b" stroke-width="0" /></svg>
                            </span>
                            &nbsp{{ __('Generate Landing Page') }}
                            <span class="pro-pill">Pro</span>
                            </a>
                    </li>
                @endif
                <li
                    class="tab-launch border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} pro-feature d-flex items-center relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        >
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-lock-square-rounded-filled" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 2c-.218 0 -.432 .002 -.642 .005l-.616 .017l-.299 .013l-.579 .034l-.553 .046c-4.785 .464 -6.732 2.411 -7.196 7.196l-.046 .553l-.034 .579c-.005 .098 -.01 .198 -.013 .299l-.017 .616l-.004 .318l-.001 .324c0 .218 .002 .432 .005 .642l.017 .616l.013 .299l.034 .579l.046 .553c.464 4.785 2.411 6.732 7.196 7.196l.553 .046l.579 .034c.098 .005 .198 .01 .299 .013l.616 .017l.642 .005l.642 -.005l.616 -.017l.299 -.013l.579 -.034l.553 -.046c4.785 -.464 6.732 -2.411 7.196 -7.196l.046 -.553l.034 -.579c.005 -.098 .01 -.198 .013 -.299l.017 -.616l.005 -.642l-.005 -.642l-.017 -.616l-.013 -.299l-.034 -.579l-.046 -.553c-.464 -4.785 -2.411 -6.732 -7.196 -7.196l-.553 -.046l-.579 -.034a28.058 28.058 0 0 0 -.299 -.013l-.616 -.017l-.318 -.004l-.324 -.001zm0 4a3 3 0 0 1 2.995 2.824l.005 .176v1a2 2 0 0 1 1.995 1.85l.005 .15v3a2 2 0 0 1 -1.85 1.995l-.15 .005h-6a2 2 0 0 1 -1.995 -1.85l-.005 -.15v-3a2 2 0 0 1 1.85 -1.995l.15 -.005v-1a3 3 0 0 1 3 -3zm3 6h-6v3h6v-3zm-3 -4a1 1 0 0 0 -.993 .883l-.007 .117v1h2v-1a1 1 0 0 0 -1 -1z" fill="#20a29b" stroke-width="0" /></svg>
                        </span>
                        &nbsp{{ __('Generate Logo & Branding') }}
                        <span class="pro-pill">Pro</span>
                        </a>
                </li>
                <li
                    class="tab-concept border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        onclick="
                        $('#add-new-floating').hide();
                        $('.tooltiptext').css('opacity', 1);
                        $('.tooltiptext').html('{{ __('Generating...') }}');
                        $('.spinner_button svg').hide();
                        $('.spinner-border').show();
                event.preventDefault();
                document.getElementById('generate-swot-form').submit();">
                        {{ __('Generate SWOT Analysis') }}</a>
                </li>
                <li
                    class="tab-concept border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        onclick="
                        $('#add-new-floating').hide();
                        $('.tooltiptext').css('opacity', 1);
                        $('.tooltiptext').html('{{ __('Generating...') }}');
                        $('.spinner_button svg').hide();
                        $('.spinner-border').show();
                event.preventDefault();
                document.getElementById('generate-leancanvas-form').submit();">
                        {{ __('Generate Lean Canvas') }}</a>
                </li>
                <li
                    class="tab-concept border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        onclick="
                        $('#add-new-floating').hide();
                        $('.tooltiptext').css('opacity', 1);
                        $('.tooltiptext').html('{{ __('Generating...') }}');
                        $('.spinner_button svg').hide();
                        $('.spinner-border').show();
                event.preventDefault();
                document.getElementById('generate-personas-form').submit();">
                        {{ __('Generate User Personas') }}</a>
                </li>
                <li
                    class="tab-concept border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        onclick="
                        $('#add-new-floating').hide();
                        $('.tooltiptext').css('opacity', 1);
                        $('.tooltiptext').html('{{ __('Generating...') }}');
                        $('.spinner_button svg').hide();
                        $('.spinner-border').show();
                event.preventDefault();
                document.getElementById('generate-stories-form').submit();">
                        {{ __('Generate User Stories') }}</a>
                </li>
                <li
                    class="tab-concept border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} pro-feature relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                
                >
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-lock-square-rounded-filled" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 2c-.218 0 -.432 .002 -.642 .005l-.616 .017l-.299 .013l-.579 .034l-.553 .046c-4.785 .464 -6.732 2.411 -7.196 7.196l-.046 .553l-.034 .579c-.005 .098 -.01 .198 -.013 .299l-.017 .616l-.004 .318l-.001 .324c0 .218 .002 .432 .005 .642l.017 .616l.013 .299l.034 .579l.046 .553c.464 4.785 2.411 6.732 7.196 7.196l.553 .046l.579 .034c.098 .005 .198 .01 .299 .013l.616 .017l.642 .005l.642 -.005l.616 -.017l.299 -.013l.579 -.034l.553 -.046c4.785 -.464 6.732 -2.411 7.196 -7.196l.046 -.553l.034 -.579c.005 -.098 .01 -.198 .013 -.299l.017 -.616l.005 -.642l-.005 -.642l-.017 -.616l-.013 -.299l-.034 -.579l-.046 -.553c-.464 -4.785 -2.411 -6.732 -7.196 -7.196l-.553 -.046l-.579 -.034a28.058 28.058 0 0 0 -.299 -.013l-.616 -.017l-.318 -.004l-.324 -.001zm0 4a3 3 0 0 1 2.995 2.824l.005 .176v1a2 2 0 0 1 1.995 1.85l.005 .15v3a2 2 0 0 1 -1.85 1.995l-.15 .005h-6a2 2 0 0 1 -1.995 -1.85l-.005 -.15v-3a2 2 0 0 1 1.85 -1.995l.15 -.005v-1a3 3 0 0 1 3 -3zm3 6h-6v3h6v-3zm-3 -4a1 1 0 0 0 -.993 .883l-.007 .117v1h2v-1a1 1 0 0 0 -1 -1z" fill="#20a29b" stroke-width="0" /></svg>
                        </span>
                        &nbsp
                        {{ __('Generate Competitive Analysis') }}
                        <span class="pro-pill">Pro</span></a>
                </li>
                <li
                    class="tab-marketing border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        onclick="
                        localStorage.setItem('show_tab', 'marketing');
                        $('#add-new-floating').hide();
                        $('.tooltiptext').css('opacity', 1);
                        $('.tooltiptext').html('{{ __('Generating...') }}');
                        $('.spinner_button svg').hide();
                        $('.spinner-border').show();
                event.preventDefault();
                document.getElementById('generate-storytelling-form').submit();">
                        {{ __('Generate Brand Wheel') }}</a>
                </li>
                <li
                    class="tab-marketing border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} pro-feature d-flex items-center relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        >
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-lock-square-rounded-filled" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 2c-.218 0 -.432 .002 -.642 .005l-.616 .017l-.299 .013l-.579 .034l-.553 .046c-4.785 .464 -6.732 2.411 -7.196 7.196l-.046 .553l-.034 .579c-.005 .098 -.01 .198 -.013 .299l-.017 .616l-.004 .318l-.001 .324c0 .218 .002 .432 .005 .642l.017 .616l.013 .299l.034 .579l.046 .553c.464 4.785 2.411 6.732 7.196 7.196l.553 .046l.579 .034c.098 .005 .198 .01 .299 .013l.616 .017l.642 .005l.642 -.005l.616 -.017l.299 -.013l.579 -.034l.553 -.046c4.785 -.464 6.732 -2.411 7.196 -7.196l.046 -.553l.034 -.579c.005 -.098 .01 -.198 .013 -.299l.017 -.616l.005 -.642l-.005 -.642l-.017 -.616l-.013 -.299l-.034 -.579l-.046 -.553c-.464 -4.785 -2.411 -6.732 -7.196 -7.196l-.553 -.046l-.579 -.034a28.058 28.058 0 0 0 -.299 -.013l-.616 -.017l-.318 -.004l-.324 -.001zm0 4a3 3 0 0 1 2.995 2.824l.005 .176v1a2 2 0 0 1 1.995 1.85l.005 .15v3a2 2 0 0 1 -1.85 1.995l-.15 .005h-6a2 2 0 0 1 -1.995 -1.85l-.005 -.15v-3a2 2 0 0 1 1.85 -1.995l.15 -.005v-1a3 3 0 0 1 3 -3zm3 6h-6v3h6v-3zm-3 -4a1 1 0 0 0 -.993 .883l-.007 .117v1h2v-1a1 1 0 0 0 -1 -1z" fill="#20a29b" stroke-width="0" /></svg>
                        </span>
                        &nbsp{{ __('Generate Marketing Plan') }}
                        <span class="pro-pill">Pro</span>
                        </a>
                </li>
                {{-- <li
                    class="border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        onclick="
                event.preventDefault();
                $('#overlay').fadeIn(300);
                document.getElementById('generate-interviews-form').submit();">
                        {{ __('Generate Customer Interviews') }}</a>
                </li>
                <li
                    class="border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        onclick="
                event.preventDefault();
                $('#overlay').fadeIn(300);
                document.getElementById('generate-logo-form').submit();">
                        {{ __('Generate Logo') }}</a>
                </li>
                <li
                    class="border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                    <a class="{{ activeRoute('dashboard.user.openai.list') }} relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                        onclick="
                event.preventDefault();
                $('#overlay').fadeIn(300);
                document.getElementById('generate-stories-form').submit();">
                        {{ __('Generate Landing Page') }}</a>
                </li> --}}
            </ul>
        </div>
    </div>
    <div class="relative lang_{{app()->getLocale()}}" style="margin-left: 30px;margin-right: 30px;margin-top:15px;">
        <div id="overlay">
            <div class="cv-spinner">
                <span class="spinner"></span>
            </div>
        </div>
        <div class="col-xxl-12">
            <div class="row">
                <div class="col-xxl-12">
                    <div class="card bg-primary">
                        <div class="card-body">
                            <div class="d-block d-sm-flex align-items-center justify-content-between">
                                <div class="d-flex align-items-center">
                                    <img src="https://app.fikrahub.com/public/images/fh.jpg"
                                        style="height:80px;width:80px;border-radius:50%">
                                    <div class="combine">
                                        <h2 style="color:#fff;margin-left:20px;margin-right:20px;">{{ $project->name }}
                                        </h2>
                                        <div class="px-1 d-none">
                                            @if ($project->status == 'Finished')
                                                <div class="badge  bg-success p-2 px-1 rounded"> {{ __('Finished') }}
                                                </div>
                                            @elseif($project->status == 'Ongoing')
                                                <div class="badge  bg-secondary p-2 px-1 rounded">{{ __('Ongoing') }}
                                                </div>
                                            @else
                                                <div class="badge bg-warning p-2 px-1 rounded">{{ __('OnHold') }}
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="px-1 d-none">
                                        <span class="text-white text-sm">{{ __('Total Members') }}:</span>
                                        <h5 class="text-white text-nowrap">
                                            {{ (int) $project->users->count() }}</h5>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center py-3">
                                    <div class="px-0 px-sm-2 d-flex d-sm-block align-items-baseline">
                                        <span class="text-white text-sm">{{ __('Submitted On') }}:</span>
                                        <h4 class="text-white text-nowrap p-sm-0 px-1 pr-0">{{ $project->start_date }}
                                        </h4>
                                    </div>
                                </div>
                                <div class="d-flex  align-items-center py-sm-3 pb-3">
                                    <!-- Tabs -->
                                    <div class="d-flex">
                                        <div class="rounded-3 tab-header mr-2 active" data-target="#concept" id="tab-concept">{{ __('Concept') }}</div>
                                        <div class="rounded-3 tab-header mr-2" data-target="#marketing" id="tab-marketing">{{ __('Marketing') }}</div>
                                        <div class="rounded-3 tab-header mr-2" data-target="#launch" id="tab-launch">{{ __('Launch') }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        {{-- <div class="horizontal-menu">
            <div class="menu-item dropdown">
                <span class="menu-text">{{ __('Concept') }}</span>
                <i class="dropdown-icon">&#9662;</i>
                <span class="counter">0/2</span>
                <div class="dropdown-content">
                    <a href="#" class="generate"
                        onclick="
                        event.preventDefault();
                        $('#overlay').fadeIn(300);
                        document.getElementById('generate-personas-form').submit();">
                        Generate User Personas
                    </a>
                    <a href="#" class="generate"
                        onclick="event.preventDefault();
                    $('#overlay').fadeIn(300); document.getElementById('generate-stories-form').submit();">
                        Generate User Stories
                    </a>

                </div>
            </div>
            <div class="menu-item dropdown">
                <span class="menu-text">{{ __('Research') }}</span>
                <i class="dropdown-icon">&#9662;</i>
                <span class="counter research">0/3</span>
                <div class="dropdown-content">
                    <a href="#" class="generate"
                        onclick="event.preventDefault();
                    $('#overlay').fadeIn(300); document.getElementById('generate-swot-form').submit();">
                        Generate SWOT Analysis
                    </a>
                    <a href="#" class="generate"
                        onclick="event.preventDefault(); 
                    $('#overlay').fadeIn(300);  document.getElementById('generate-competitive-form').submit();">
                        Generate Competitive Analysis
                    </a><a href="#" class="generate"
                        onclick="event.preventDefault();
                    $('#overlay').fadeIn(300); document.getElementById('generate-leancanvas-form').submit();">
                        Generate Lean Canvas
                    </a>
                </div>
            </div>
            <div class="menu-item dropdown">
                <span class="menu-text">{{ __('Validation') }}</span>
                <i class="dropdown-icon">&#9662;</i>
                <span class="counter">0/2</span>
                <div class="dropdown-content">
                    <a href="#" class="generate"
                        onclick="event.preventDefault();
                    $('#overlay').fadeIn(300); document.getElementById('generate-interviews-form').submit();">
                        Generate Customer Interviews
                    </a>
                    <a href="#" class="generate"
                        onclick="event.preventDefault();
                    $('#overlay').fadeIn(300); document.getElementById('generate-logo-form').submit();">
                        Generate Logo
                    </a>
                </div>
            </div>
            <div class="menu-item dropdown">
                <span class="menu-text">{{ __('Promotion') }}</span>
                <i class="dropdown-icon">&#9662;</i>
                <span class="counter">0/2</span>
                <div class="dropdown-content">
                    <a href="https://pages.fikrahub.com/app/login.php?email=atoum@yotta.solutions" target="_blank">
                        Generate Landing Page
                    </a>
                    <a href="#" class="generate"
                        onclick="event.preventDefault(); document.getElementById('generate-storytelling-form').submit();">
                        Generate Brand Wheel
                    </a>
                </div>
            </div>
        </div> --}}

        <form action="{{ route('projects.generate-competitive-analysis', ['project' => $project->id]) }}" method="POST"
            id="generate-competitive-form" style="display: none;">
            @csrf
        </form>
        <form action="{{ route('projects.generate-user-stories', ['project' => $project->id]) }}" method="POST"
            id="generate-stories-form" style="display: none;">
            @csrf
        </form>
        <form action="{{ route('projects.generate-user-personas', ['project' => $project->id]) }}" method="POST"
            id="generate-personas-form" style="display: none;">
            @csrf
        </form>
        <form action="{{ route('projects.generate-swot', ['project' => $project->id]) }}" method="POST"
            id="generate-swot-form" style="display: none;">
            @csrf
        </form>
        <form action="{{ route('projects.generate-storytelling', ['project' => $project->id]) }}" method="POST"
            id="generate-storytelling-form" style="display: none;">
            @csrf
        </form>
        <form action="{{ route('projects.generate-lean-canvas', ['project' => $project->id]) }}" method="POST"
            id="generate-leancanvas-form" style="display: none;">
            @csrf
        </form>
        <form action="{{ route('projects.generate-logo', ['project' => $project->id]) }}" method="POST"
            id="generate-logo-form" style="display: none;">
            @csrf
        </form>


        <!-- Tab Content -->
        <div id="concept" style="display: block;">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h5 class="mb-0">{{ __('Description') }}</span></h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-3">
                            <p class="text-muted d-flex align-items-center mb-0">{{ $project->description }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- [ sample-page ] start -->
                <div class="col-sm-12">
                    <div class="row">
                        @if ($project)
    
                            <div id="swot-body" class="col-12 {{ is_null($swotAnalysis) ? 'blur-content' : '' }}">
                                <div class="card">
                                    <div class="card-header d-block">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div>
                                                <h5 class="mb-0">{{ __('SWOT Analysis') }}</h5>
                                            </div>
                                            <div>
                                                <button onclick="regenerateProjectInfo({{ $project->id }}, 'swotAnalysis')"
                                                    class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                                <button onclick="collapseByType('swot', 'minius')" id="swot-minius"
                                                    class="btn btn-success btn-sm"><i class="ti ti-minus"></i></button>
                                                <button onclick="collapseByType('swot', 'plus')" id="swot-plus"
                                                    style="display: none" class="btn btn-success btn-sm"><i
                                                        class="ti ti-plus"></i></button>
                                                <button
                                                    onclick="deleteProjectInfo({{ $project->id }}, 'swotAnalysis', '#swot-body')"
                                                    class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                            </div>
                                            <!-- Add this inside your Blade template where you show project details -->
                                        </div>
                                    </div>
                                    <div class="card-body p-1 p-sm-4" id="swot-content">
                                        <div class="swot-container">
                                            <!-- Strengths -->
                                            <div class="swot-section strengths position-relative">
                                                @if ($swotAnalysis)
                                                    @if ($swotAnalysis->strengths)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $swotAnalysis->id }}" data-type="swotAnalysis"
                                                            data-key="strengths" data-name="Strengths"
                                                            data-value="{{ $swotAnalysis->strengths }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <div class="swot-title">{{ __('Strengths') }}</div>
                                                <div class="swot-letter">S</div>
                                                <div class="swot-content" id="swotAnalysis-strengths">
                                                    {!! $swotAnalysis->strengths ?? 'Not generated.' !!}</div>
                                            </div>
    
                                            <!-- Weaknesses -->
                                            <div class="swot-section weaknesses position-relative">
                                                @if ($swotAnalysis)
                                                    @if ($swotAnalysis->weaknesses)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $swotAnalysis->id }}" data-type="swotAnalysis"
                                                            data-key="weaknesses" data-name="Weaknesses"
                                                            data-value="{{ $swotAnalysis->weaknesses }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <div class="swot-title">{{ __('Weaknesses') }}</div>
                                                <div class="swot-letter">W</div>
                                                <div class="swot-content" id="swotAnalysis-weaknesses">
                                                    {!! $swotAnalysis->weaknesses ?? 'Not generated.' !!}</div>
                                            </div>
    
                                            <!-- Opportunities -->
                                            <div class="swot-section opportunities position-relative">
                                                @if ($swotAnalysis)
                                                    @if ($swotAnalysis->opportunities)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $swotAnalysis->id }}" data-type="swotAnalysis"
                                                            data-key="opportunities" data-name="Opportunities"
                                                            data-value="{{ $swotAnalysis->weaknesses }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <div class="swot-title">{{ __('Opportunities') }}</div>
                                                <div class="swot-letter">O</div>
                                                <div class="swot-content" id="swotAnalysis-opportunities">
                                                    {!! $swotAnalysis->opportunities ?? 'Not generated.' !!}</div>
                                            </div>
    
                                            <!-- Threats -->
                                            <div class="swot-section threats position-relative">
                                                @if ($swotAnalysis)
                                                    @if ($swotAnalysis->threats)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $swotAnalysis->id }}" data-type="swotAnalysis"
                                                            data-key="threats" data-name="Threats"
                                                            data-value="{{ $swotAnalysis->threats }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <div class="swot-title">{{ __('Threats') }}</div>
                                                <div class="swot-letter">T</div>
                                                <div class="swot-content" id="swotAnalysis-threats">
                                                    {!! $swotAnalysis->threats ?? 'Not generated.' !!}</div>
                                            </div>
                                        </div>
    
                                    </div>
                                </div>
                            </div>
    
    
    
                            <div id="leanCanvas-body" class="col-12 {{ is_null($leanCanvas) ? 'blur-content' : '' }}">
                                <div class="card">
                                    <div class="card-header d-block">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div>
                                                <h5 class="mb-0">{{ __('Lean Canvas') }}</h5>
                                            </div>
                                            <div>
                                                <button onclick="regenerateProjectInfo({{ $project->id }}, 'leanCanvas')"
                                                    class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                                <button onclick="collapseByType('leanCanvas', 'minius')"
                                                    id="leanCanvas-minius" class="btn btn-success btn-sm"><i
                                                        class="ti ti-minus"></i></button>
                                                <button onclick="collapseByType('leanCanvas', 'plus')" id="leanCanvas-plus"
                                                    style="display: none" class="btn btn-success btn-sm"><i
                                                        class="ti ti-plus"></i></button>
                                                <button
                                                    onclick="deleteProjectInfo({{ $project->id }}, 'leanCanvas', '#leanCanvas-body')"
                                                    class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                            </div>
    
                                        </div>
                                    </div>
                                    <div class="card-body p-1 p-sm-4" id="leanCanvas-content">
    
                                        <main class="lean-canvas">
                                            <section class="canvas-block position-relative" id="problem">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->problem)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="problem" data-name="Problem"
                                                            data-value="{{ $leanCanvas->problem }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Problem') }}</h2>
                                                <p id="leanCanvas-problem">
                                                    {!! $leanCanvas->problem ?? 'Not generated.' !!}
                                                </p>
                                            </section>
                                            <section class="canvas-block position-relative" id="solution">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->solution)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="solution" data-name="Solution"
                                                            data-value="{{ $leanCanvas->solution }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Solution') }}</h2>
                                                <p id="leanCanvas-solution">{!! $leanCanvas->solution ?? 'Not generated.' !!}
                                                </p>
                                            </section>
                                            <section class="canvas-block position-relative" id="unique-value-prop">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->unique_value_proposition)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="unique_value_proposition" data-name="Unique Value Prop."
                                                            data-value="{{ $leanCanvas->unique_value_proposition }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Unique Value Prop') }}</h2>
                                                <p id="leanCanvas-unique_value_proposition">
                                                    {!! $leanCanvas->unique_value_proposition ?? 'Not generated.' !!}</p>
                                            </section>
                                            <section class="canvas-block position-relative" id="unfair-advantage">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->unfair_advantage)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="unfair_advantage" data-name="Unfair Advantage"
                                                            data-value="{{ $leanCanvas->unfair_advantage }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Unfair Advantage') }}</h2>
                                                <p id="leanCanvas-unfair_advantage">
                                                    {!! $leanCanvas->unfair_advantage ?? 'Not generated.' !!}</p>
                                            </section>
                                            <section class="canvas-block position-relative" id="customer-segments">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->customer_segments)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="customer_segments" data-name="Customer Segments"
                                                            data-value="{{ $leanCanvas->customer_segments }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Customer Segments') }}</h2>
                                                <p id="leanCanvas-customer_segments">
                                                    {!! $leanCanvas->customer_segments ?? 'Not generated.' !!}</p>
                                            </section>
                                            <section class="canvas-block position-relative" id="existing-alternatives">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->existing_alternatives)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="existing_alternatives" data-name="Existing Alternatives"
                                                            data-value="{{ $leanCanvas->existing_alternatives }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Existing Alternatives') }}</h2>
                                                <p id="leanCanvas-existing_alternatives">
                                                    {!! $leanCanvas->existing_alternatives ?? 'Not generated.' !!}</p>
                                            </section>
                                            <section class="canvas-block position-relative" id="key-metrics">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->key_metrics)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="key_metrics" data-name="Key Metrics"
                                                            data-value="{{ $leanCanvas->key_metrics }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Key Metrics') }}</h2>
                                                <p id="leanCanvas-key_metrics">
                                                    {!! $leanCanvas->key_metrics ?? 'Not generated.' !!}
                                                </p>
                                            </section>
                                            <section class="canvas-block position-relative" id="channels">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->channels)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="channels" data-name="Channels"
                                                            data-value="{{ $leanCanvas->channels }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Channels') }}</h2>
                                                <p id="leanCanvas-channels">{!! $leanCanvas->channels ?? 'Not generated.' !!}
                                                </p>
                                            </section>
                                            <section class="canvas-block position-relative" id="cost-structure">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->cost_structure)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="cost_structure" data-name="Cost Structure"
                                                            data-value="{{ $leanCanvas->cost_structure }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Cost Structure') }}</h2>
                                                <p id="leanCanvas-cost_structure">
                                                    {!! $leanCanvas->cost_structure ?? 'Not generated.' !!}</p>
                                            </section>
                                            <section class="canvas-block position-relative" id="revenue-streams">
                                                @if ($leanCanvas)
                                                    @if ($leanCanvas->revenue_streams)
                                                        <div class="edit-popup position-absolute position-edit-15"
                                                            data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                            data-key="revenue_streams" data-name="Revenue Streams"
                                                            data-value="{{ $leanCanvas->revenue_streams }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xl"></i></div>
                                                    @endif
                                                @endif
                                                <h2>{{ __('Revenue Streams') }}</h2>
                                                <p id="leanCanvas-revenue_streams">
                                                    {!! $leanCanvas->revenue_streams ?? 'Not generated.' !!}</p>
                                            </section>
                                        </main>
    
                                    </div>
                                </div>
                            </div>
                            <div id="userPersonas-body" class="col-12 {{ $userPersonas->isEmpty() ? 'blur-content' : '' }}">
                                <div class="card">
                                    <div class="card-header d-block">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div>
                                                <h5 class="mb-0">{{ __('User Personas') }}</h5>
                                            </div>
                                            <!-- Add this inside your Blade template where you show project details -->
    
                                            <div>
                                                <button onclick="regenerateProjectInfo({{ $project->id }}, 'userPersonas')"
                                                    class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                                <button onclick="collapseByType('userPersonas', 'minius')"
                                                    id="userPersonas-minius" class="btn btn-success btn-sm"><i
                                                        class="ti ti-minus"></i></button>
                                                <button onclick="collapseByType('userPersonas', 'plus')"
                                                    id="userPersonas-plus" style="display: none"
                                                    class="btn btn-success btn-sm"><i class="ti ti-plus"></i></button>
                                                <button
                                                    onclick="deleteProjectInfo({{ $project->id }}, 'userPersonas', '#userPersonas-body')"
                                                    class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body p-1 p-sm-4" id="userPersonas-content">
                                        <div class="swot-container">
                                            @if ($userPersonas->count() > 0)
                                                @foreach ($userPersonas as $user)
                                                    <div class="card shadow">
                                                        <div class="card-body">
                                                            @php
                                                                $number = rand(100, 500);
                                                            @endphp
                                                            @if (isset($user->url))
                                                            <img src="{{ url('').'/testimonialAvatar/'.$user->url }}"
                                                                class="card-img-top avatar mx-auto mt-3" alt="Avatar 1">
                                                            @endif
                                                            <h5 class="card-title text-center"></h5>
                                                            <ul class="list-group list-group-flush">
                                                                <li class="list-group-item fs-5 position-relative"><b>{{ __('Name') }}:</b>
                                                                    @if ($user->name)
                                                                        <div class="edit-popup position-absolute postion-edit-5-15"
                                                                            data-id="{{ $user->id }}"
                                                                            data-type="userPersonas" data-key="name"
                                                                            data-name="Name" data-value="{{ $user->name }}"
                                                                             role="button"><i
                                                                                class="ti ti-pencil-alt text-xs"></i></div>
                                                                    @endif
                                                                    <span id="userPersonas-name-{{ $user->id }}">
                                                                        {{ $user->name }}
                                                                    </span>
                                                                </li>
                                                                <li class="list-group-item fs-5"><b>{{ __('Age') }}:</b>
                                                                    @if ($user->age)
                                                                        <div class="edit-popup position-absolute postion-edit-5-15"
                                                                            data-id="{{ $user->id }}"
                                                                            data-type="userPersonas" data-key="age"
                                                                            data-name="Age" data-value="{{ $user->age }}"
                                                                             role="button"><i
                                                                                class="ti ti-pencil-alt text-xs"></i></div>
                                                                    @endif
                                                                    <span id="userPersonas-age-{{ $user->id }}">
                                                                        {{ $user->age }}
                                                                    </span>
                                                                </li>
                                                                <li class="list-group-item fs-5"><b>{{ __('Description') }}:</b>
                                                                    @if ($user->description)
                                                                        <div class="edit-popup position-absolute postion-edit-5-15"
                                                                            data-id="{{ $user->id }}"
                                                                            data-type="userPersonas" data-key="description"
                                                                            data-name="Description"
                                                                            data-value="{{ $user->description }}"
                                                                             role="button"><i
                                                                                class="ti ti-pencil-alt text-xs"></i></div>
                                                                    @endif
                                                                    <span id="userPersonas-description-{{ $user->id }}">
                                                                        {{ $user->description }}
                                                                    </span>
                                                                </li>
                                                                <li class="list-group-item fs-5"><b>{{ __('Goals') }}:</b>
                                                                    @if ($user->goals)
                                                                        <div class="edit-popup position-absolute postion-edit-5-15"
                                                                            data-id="{{ $user->id }}"
                                                                            data-type="userPersonas" data-key="goals"
                                                                            data-name="Goals" data-value="{{ $user->goals }}"
                                                                             role="button"><i
                                                                                class="ti ti-pencil-alt text-xs"></i></div>
                                                                    @endif
                                                                    <span id="userPersonas-goals-{{ $user->id }}">
                                                                        {{ $user->goals }}
                                                                    </span>
                                                                </li>
                                                                <li class="list-group-item fs-5"><b>{{ __('Needs') }}:</b>
                                                                    @if ($user->needs)
                                                                        <div class="edit-popup position-absolute postion-edit-5-15"
                                                                            data-id="{{ $user->id }}"
                                                                            data-type="userPersonas" data-key="needs"
                                                                            data-name="Needs" data-value="{{ $user->needs }}"
                                                                             role="button"><i
                                                                                class="ti ti-pencil-alt text-xs"></i></div>
                                                                    @endif
                                                                    <span id="userPersonas-needs-{{ $user->id }}">
                                                                        {{ $user->needs }}
                                                                    </span>
                                                                </li>
                                                                <li class="list-group-item fs-5"><b>{{ __('Pains') }}:</b>
                                                                    @if ($user->pains)
                                                                        <div class="edit-popup position-absolute postion-edit-5-15"
                                                                            data-id="{{ $user->id }}"
                                                                            data-type="userPersonas" data-key="pains"
                                                                            data-name="Pains" data-value="{{ $user->pains }}"
                                                                             role="button"><i
                                                                                class="ti ti-pencil-alt text-xs"></i></div>
                                                                    @endif
                                                                    <span id="userPersonas-pains-{{ $user->id }}">
                                                                        {{ $user->pains }}
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
    
                                    </div>
                                </div>
                            </div>
                            <div id="userStories-body" class="col-12 {{ $userStories->isEmpty() ? 'blur-content' : '' }}">
                                <div class="card">
                                    <div class="card-header d-block">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div>
                                                <h5 class="mb-0">{{ __('User Stories') }}</h5>
                                            </div>
                                            <!-- Add this inside your Blade template where you show project details -->
    
                                            <div>
                                                <button onclick="regenerateProjectInfo({{ $project->id }}, 'userStories')"
                                                    class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                                <button onclick="collapseByType('userStories', 'minius')"
                                                    id="userStories-minius" class="btn btn-success btn-sm"><i
                                                        class="ti ti-minus"></i></button>
                                                <button onclick="collapseByType('userStories', 'plus')" id="userStories-plus"
                                                    style="display: none" class="btn btn-success btn-sm"><i
                                                        class="ti ti-plus"></i></button>
                                                <button
                                                    onclick="deleteProjectInfo({{ $project->id }}, 'userStories', '#userStories-body')"
                                                    class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body p-1 p-sm-4" id="userStories-content">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>{{ __('User story') }}</th>
                                                    <th>{{ __('Acceptance Criteria') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($userStories as $user)
                                                    <tr>
                                                        <td class="position-relative">
                                                            @if ($user->user_story)
                                                                <div class="edit-popup position-absolute postion-edit-5"
                                                                    data-id="{{ $user->id }}" data-type="userStories"
                                                                    data-key="user_story" data-name="User Story"
                                                                    data-value="{{ $user->user_story }}"
                                                                     role="button"><i
                                                                        class="ti ti-pencil-alt text-xm"></i></div>
                                                            @endif
                                                            <span id="userStories-user_story-{{ $user->id }}">
                                                                {{ $user->user_story ?? 'Not generated.' }}
                                                            </span>
                                                        </td>
                                                        <td class="position-relative">
                                                            @if ($user->acceptance_criterea)
                                                                <div class="edit-popup position-absolute postion-edit-5"
                                                                    data-id="{{ $user->id }}" data-type="userStories"
                                                                    data-key="acceptance_criterea"
                                                                    data-name="Acceptance Criterea"
                                                                    data-value="{{ $user->acceptance_criterea }}"
                                                                     role="button"><i
                                                                        class="ti ti-pencil-alt text-xm"></i></div>
                                                            @endif
                                                            <span id="userStories-acceptance_criterea-{{ $user->id }}">
                                                                {!! $user->acceptance_criterea ?? 'Not generated.' !!}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                <!-- More rows as needed -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
    
    
                            <div id="competitiveAnalysis-body"
                                class="col-12 {{ $competitiveAnalysis->isEmpty() ? 'blur-content' : '' }}">
                                <div class="card">
                                    <div class="card-header d-block">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div>
                                                <h5 class="mb-0">{{ __('Competitive Analysis') }}</h5>
                                            </div>
                                            <!-- Add this inside your Blade template where you show project details -->
                                            <div>
                                                <button
                                                    onclick="regenerateProjectInfo({{ $project->id }}, 'competitiveAnalysis')"
                                                    class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                                <button onclick="collapseByType('competitiveAnalysis', 'minius')"
                                                    id="competitiveAnalysis-minius" class="btn btn-success btn-sm"><i
                                                        class="ti ti-minus"></i></button>
                                                <button onclick="collapseByType('competitiveAnalysis', 'plus')"
                                                    id="competitiveAnalysis-plus" style="display: none"
                                                    class="btn btn-success btn-sm"><i class="ti ti-plus"></i></button>
                                                <button
                                                    onclick="deleteProjectInfo({{ $project->id }}, 'competitiveAnalysis', '#competitiveAnalysis-body')"
                                                    class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body p-1 p-sm-4" id="competitiveAnalysis-content">
                                        <div class="card-faq w-full">
                                            @foreach ($competitiveAnalysis as $key => $competitive)
                                                <div class="card-faq-header position-relative" style='margin-top: 10px'
                                                    >
                                                    @if ($competitive->name)
                                                        <div class="edit-popup position-absolute postion-edit-10"
                                                            data-id="{{ $competitive->id }}" data-type="competitiveAnalysis"
                                                            data-key="name" data-name="Name"
                                                            data-value="{{ $competitive->name }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xm"></i></div>
                                                    @endif
                                                    <h6 class="mb-0">
                                                        <button id="competitiveAnalysis-name-{{ $competitive->id }}"
                                                            class="btn show-anwser" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapse_{{ $key }}" aria-expanded="true"
                                                            aria-controls="collapseOne">
                                                            {{ $competitive->name }}
                                                        </button>
                                                    </h6>
                                                </div>
                                                <div id="collapse_{{ $key }}" class="collapse show"
                                                    aria-labelledby="heading_{{ $key }}">
                                                    <div class="card-faq-body">
                                                        <ul class="list-group list-group-flush" style='margin-left: 10px'>
                                                            <li class="list-group-item fs-5 position-relative"><b>{{ __('Location') }}:</b>
                                                                @if ($competitive->location)
                                                                    <div class="edit-popup position-absolute postion-edit-5"
                                                                        data-id="{{ $competitive->id }}"
                                                                        data-type="competitiveAnalysis" data-key="location"
                                                                        data-name="Location"
                                                                        data-value="{{ $competitive->location }}"
                                                                         role="button"><i
                                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                                @endif
                                                                <span
                                                                    id="competitiveAnalysis-location-{{ $competitive->id }}">
                                                                    {{ $competitive->location }}
                                                                </span>
                                                            </li>
                                                            <li class="list-group-item fs-5"><b>{{ __('Strengths of Features') }}:</b>
                                                                @if ($competitive->strengths_features)
                                                                    <div class="edit-popup position-absolute postion-edit-5"
                                                                        data-id="{{ $competitive->id }}"
                                                                        data-type="competitiveAnalysis"
                                                                        data-key="strengths_features"
                                                                        data-name="Strengths Features"
                                                                        data-value="{{ $competitive->strengths_features }}"
                                                                         role="button"><i
                                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                                @endif
                                                                <span
                                                                    id="competitiveAnalysis-strengths_features-{{ $competitive->id }}">
                                                                    {{ $competitive->strengths_features }}
                                                                </span>
                                                            </li>
                                                            <li class="list-group-item fs-5"><b>{{ __('Weaknesses') }}:</b>
                                                                @if ($competitive->weaknesses)
                                                                    <div class="edit-popup position-absolute postion-edit-5"
                                                                        data-id="{{ $competitive->id }}"
                                                                        data-type="competitiveAnalysis" data-key="weaknesses"
                                                                        data-name="Weaknesses"
                                                                        data-value="{{ $competitive->weaknesses }}"
                                                                         role="button"><i
                                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                                @endif
                                                                <span
                                                                    id="competitiveAnalysis-weaknesses-{{ $competitive->id }}">
                                                                    {{ $competitive->weaknesses }}
                                                                </span>
                                                            </li>
                                                            <li class="list-group-item fs-5"><b>{{ __('Areas of Opportunity 1') }}:</b>
                                                                @if ($competitive->areas_opportunity)
                                                                    <div class="edit-popup position-absolute postion-edit-5"
                                                                        data-id="{{ $competitive->id }}"
                                                                        data-type="competitiveAnalysis"
                                                                        data-key="areas_opportunity"
                                                                        data-name="Areas Opportunity"
                                                                        data-value="{{ $competitive->areas_opportunity }}"
                                                                         role="button"><i
                                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                                @endif
                                                                <span
                                                                    id="competitiveAnalysis-areas_opportunity-{{ $competitive->id }}">
                                                                    {{ $competitive->areas_opportunity }}
                                                                </span>
                                                            </li>
                                                            <li class="list-group-item fs-5"></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <!--
                                                            <div class="col-xxl-12">
                                        <div class="row">
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div>
                                                            <h5 class="mb-0">{{ __('Suggested Domain Names') }}</h5>
                                                        </div>
                                                        <!-- Add this inside your Blade template where you show project details
                <form action="{{ route('projects.generate-swot', ['project' => $project->id]) }}" method="POST">
                    @csrf
                    <button type="submit" id="generate" class="btn btn-primary">Generate Suggestions</button>
                </form>
    
                                                    </div>
                                                </div>
                                                <div class="card-body p-1 p-sm-4 {{ is_null($swotAnalysis) ? 'blur-content' : '' }}">
    
    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div>
                                                            <h5 class="mb-0">{{ __('Customer Interviews') }}</h5>
                                                        </div>
                <form action="{{ route('projects.generate-swot', ['project' => $project->id]) }}" method="POST">
                    @csrf
                    <button type="submit" id="generate" class="btn btn-primary">Generate Questions</button>
                </form>
    
                                                    </div>
                                                </div>
                                                <div class="card-body p-1 p-sm-4 {{ is_null($swotAnalysis) ? 'blur-content' : '' }}">
    
    
                                                </div>
                                            </div>
                                        </div>
    
                                        </div>
                                        </div> -->
    
    
                            <!--<div class="col-xxl-12">-->
                            <!--    <div class="row">-->
                            <!--        <div class="col-6">-->
                            <!--            <div class="card deta-card">-->
                            <!--                <div class="card-header  d-block">-->
                            <!--                    <div class="d-flex justify-content-between align-items-center">-->
                            <!--                        <div>-->
                            <!--                            <h5 class="mb-0">{{ __('Team Members') }}-->
                            <!--                                ({{ count($project->users) }})-->
                            <!--                            </h5>-->
                            <!--                        </div>-->
                            <!--                    </div>-->
                            <!--                </div>-->
                            <!--                <div class="card-body top-10-scroll">-->
                            <!--                    @foreach ($project->users as $user)-->
                            <!--                        <ul class="list-group list-group-flush" style="width: 100%;">-->
                            <!--                            <li class="list-group-item d-block px-0">-->
                            <!--                                <div class="row align-items-center justify-content-between">-->
                            <!--                                    <div class="col-sm-auto mb-3 mb-sm-0">-->
                            <!--                                        <div class="d-flex align-items-center px-2">-->
                            <!--                                            <a href="#" class=" text-start">-->
                            <!--                                                <img alt="image" data-bs-toggle="tooltip"-->
                            <!--                                                    data-bs-placement="top"-->
                            <!--                                                    title="{{ $user->name }}"-->
                            <!--                                                    src="https://app.fikrahub.com/public/images/user.jpeg"-->
                            <!--                                                    class="rounded-circle " width="40px"-->
                            <!--                                                    height="40px">-->
                            <!--                                            </a>-->
                            <!--                                            <div class="px-2">-->
                            <!--                                                <h5 class="m-0">{{ $user->name }}</h5>-->
                            <!--                                                {{-- <small class="text-muted">{{ $user->email }}<span-->
                            <!--                                                    class="text-primary "> --->
                            <!--                                                    {{ (int) count($project->user_done_tasks($user->id)) }}/{{ (int) count($project->user_tasks($user->id)) }}</span></small> --}}-->
                            <!--                                            </div>-->
                            <!--                                        </div>-->
                            <!--                                    </div>-->
                            <!--                                    <div class="col-sm-auto text-sm-end d-flex align-items-center">-->
                            <!--                                        @auth('web')-->
                            <!--                                            @if ($user->id != Auth::id())-->
                            <!--                                                @can('team member remove')-->
                            <!--                                                    <form id="delete-user-{{ $user->id }}"-->
                            <!--                                                        action="{{ route('projects.user.delete', [$project->id, $user->id]) }}"-->
                            <!--                                                        method="POST" style="display: none;"-->
                            <!--                                                        class="d-inline-flex">-->
                            <!--                                                        <a href="#"-->
                            <!--                                                            class="action-btn btn-danger mx-1  btn btn-sm d-inline-flex align-items-center bs-pass-para show_confirm"-->
                            <!--                                                            data-confirm="{{ __('Are You Sure?') }}"-->
                            <!--                                                            data-text="{{ __('This action can not be undone. Do you want to continue?') }}"-->
                            <!--                                                            data-confirm-yes="delete-user-{{ $user->id }}"-->
                            <!--                                                            data-toggle="tooltip"-->
                            <!--                                                            title="{{ __('Delete') }}"><i-->
                            <!--                                                                class="ti ti-trash"></i></a>-->
    
                            <!--                                                        @csrf-->
                            <!--                                                        @method('DELETE')-->
                            <!--                                                    </form>-->
                            <!--                                                @endcan-->
                            <!--                                            @endif-->
                            <!--                                        @endauth-->
                            <!--                                    </div>-->
                            <!--                                </div>-->
                            <!--                            </li>-->
                            <!--                        </ul>-->
                            <!--                    @endforeach-->
                            <!--                </div>-->
                            <!--            </div>-->
                            <!--        </div>-->
    
                            <!--        <div class="col-6">-->
                            <!--            <div class="card">-->
                            <!--                <div class="card-header">-->
                            <!--                    <div class="d-flex justify-content-between align-items-center">-->
                            <!--                        <div>-->
                            <!--                            <h5 class="mb-0">{{ __('Files') }}</h5>-->
                            <!--                        </div>-->
                            <!--                    </div>-->
                            <!--                </div>-->
                            <!--                <div class="card-body p-1 p-sm-4">-->
    
                            <!--                    <div class="author-box-name form-control-label mb-4">-->
    
                            <!--                    </div>-->
                            <!--                    <div class="col-md-12 dropzone browse-file" id="dropzonewidget">-->
                            <!--                        <div class="dz-message" data-dz-message>-->
                            <!--                            <span>{{ __('Drop files here to upload') }}</span>-->
                            <!--                        </div>-->
                            <!--                    </div>-->
                            <!--                </div>-->
                            <!--            </div>-->
                            <!--        </div>-->
                            <!--    </div>-->
    
                            <!--</div>-->
                        @endif
    
                        {{-- @if ($project)
                        <!--<div class="col-md-12">-->
                        <!--    <div class="card deta-card">-->
                        <!--        <div class="card-header">-->
                        <!--            <div class="d-flex justify-content-between align-items-center">-->
                        <!--                <div>-->
                        <!--                    <h5 class="mb-0">{{ __('Activity') }}</h5>-->
                        <!--                </div>-->
                        <!--            </div>-->
                        <!--        </div>-->
                        <!--        <div class="card-body p-3">-->
                        <!--            <div class="timeline timeline-one-side top-10-scroll" data-timeline-content="axis"-->
                        <!--                data-timeline-axis-style="dashed" style="max-height: 300px;">-->
                        <!--                @foreach ($project->activities as $activity)-->
                        <!--                    <div class="timeline-block px-2 pt-3">-->
                        <!--                        @if ($activity->log_type == 'Upload File')-->
                        <!--                            <span-->
                        <!--                                class="timeline-step timeline-step-sm border border-primary text-white">-->
                        <!--                                <i class="fas fa-file text-dark"></i></span>-->
                        <!--                        @elseif($activity->log_type == 'Create Milestone')-->
                        <!--                            <span class="timeline-step timeline-step-sm border border-info text-white">-->
                        <!--                                <i class="fas fa-cubes text-dark"></i></span>-->
                        <!--                        @elseif($activity->log_type == 'Create Task')-->
                        <!--                            <span-->
                        <!--                                class="timeline-step timeline-step-sm border border-success text-white">-->
                        <!--                                <i class="fas fa-tasks text-dark"></i></span>-->
                        <!--                        @elseif($activity->log_type == 'Create Bug')-->
                        <!--                            <span-->
                        <!--                                class="timeline-step timeline-step-sm border border-warning text-white">-->
                        <!--                                <i class="fas fa-bug text-dark"></i></span>-->
                        <!--                        @elseif($activity->log_type == 'Move' || $activity->log_type == 'Move Bug')-->
                        <!--                            <span-->
                        <!--                                class="timeline-step timeline-step-sm border round border-danger text-white">-->
                        <!--                                <i class="fas fa-align-justify text-dark"></i></span>-->
                        <!--                        @elseif($activity->log_type == 'Create Invoice')-->
                        <!--                            <span-->
                        <!--                                class="timeline-step timeline-step-sm border border-bg-dark text-white">-->
                        <!--                                <i class="fas fa-file-invoice text-dark"></i></span>-->
                        <!--                        @elseif($activity->log_type == 'Invite User')-->
                        <!--                            <span class="timeline-step timeline-step-sm border border-success"> <i-->
                        <!--                                    class="fas fa-plus text-dark"></i></span>-->
                        <!--                        @elseif($activity->log_type == 'Share with Client')-->
                        <!--                            <span class="timeline-step timeline-step-sm border border-info text-white">-->
                        <!--                                <i class="fas fa-share text-dark"></i></span>-->
                        <!--                        @elseif($activity->log_type == 'Create Timesheet')-->
                        <!--                            <span-->
                        <!--                                class="timeline-step timeline-step-sm border border-success text-white">-->
                        <!--                                <i class="fas fa-clock-o text-dark"></i></span>-->
                        <!--                        @endif-->
                        <!--                        <div class=" row last_notification_text">-->
                        <!--                            <span class="col-1 m-0 h6 text-sm"> <span>{{ $activity->log_type }}-->
                        <!--                                </span> </span>-->
                        <!--                            <br>-->
                        <!--                            <span class="col text-start text-sm h6"> {!! $activity->getRemark() !!} </span>-->
                        <!--                            <div class="col-auto text-end notification_time_main">-->
                        <!--                                <p class="text-muted">{{ $activity->created_at->diffForHumans() }}-->
                        <!--                                </p>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
    
                        <!--                    </div>-->
                        <!--                @endforeach-->
                        <!--            </div>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                    @endif --}}
    
                    </div>
                    <!-- [ sample-page ] end -->
                </div>
                <!-- [ Main Content ] end -->
            </div>
        </div>
        <div id="marketing" style="display: none;">
            <div class="row">
                <div class="col-12 mt-3">
                    <div class="card">
                        <div class="card-header justify-between">
                            <h3 class="card-title text-heading">{{ __('Documents') }}</h3>
                            <div class="btn-list">
                                <a href="{{ route('dashboard.user.generator.index', ['idea_id' => $project->id]) }}"
                                    class="btn-sm btn btn-primary items-center" style="width: 200px; background: #fa690e;">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="!me-2" width="18" height="18"
                                        viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                        stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                        <path d="M12 5l0 14" />
                                        <path d="M5 12l14 0" />
                                    </svg>
                                    {{ __('New Documents') }}
                                </a>
                            </div>
                        </div>
                        <div class="card-table table-responsive">
                            <table class="table table-vcenter">
                                <tbody>
                                    @php
                                        $documents = Auth::user()->openai()->where('project_id', $project->id)->orderBy('created_at', 'desc')->get();
                                    @endphp
                                    @if ($documents->count() > 0)
                                        @foreach ($documents as $entry)
                                            @if ($entry->generator != null)
                                                <tr>
                                                    <td class="w-1 !pe-0">
                                                        <span class="avatar w-[43px] h-[43px] [&_svg]:w-[20px] [&_svg]:h-[20px]"
                                                            style="background: {{ $entry->generator->color }}">
                                                            @if ($entry->generator->image !== 'none')
                                                                {!! html_entity_decode($entry->generator->image) !!}
                                                            @endif
                                                        </span>
                                                    </td>
                                                    <td class="td-truncate">
                                                        <a href="{{route('dashboard.user.openai.documents.single', $entry->slug)}}?idea_id={{ $project->id }}"
                                                            class="block text-truncate text-heading hover:no-underline">
                                                            <span class="font-medium">
                                                                {{ $entry->title }}
                                                            </span>
                                                        </a>
                                                    </td>
                                                    <td class="text-nowrap">
                                                        <span class="text-heading">{{ __('in Workbook') }}</span>
                                                        <br>
                                                        <span
                                                            class="italic text-muted opacity-80 dark:!text-white dark:!opacity-50">{{ $entry->created_at->format('M d, Y') }}</span>
                                                    </td>
                                                    <td class="text-nowrap">
                                                        <a  onclick="return deleteProjectDocument('{{ route('dashboard.user.openai.projects.delete-document', ['slug' => $entry->slug, 'id' => $project->id]) }}');"
    
                                                            class="btn p-0 border w-[36px] h-[36px] hover:bg-red-600 hover:text-white"
                                                            title="{{ __('Delete') }}">
                                                            <svg width="10" height="10" viewBox="0 0 10 10" fill="currentColor"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M9.08789 1.74609L5.80664 5L9.08789 8.25391L8.26758 9.07422L4.98633 5.82031L1.73242 9.07422L0.912109 8.25391L4.16602 5L0.912109 1.74609L1.73242 0.925781L4.98633 4.17969L8.26758 0.925781L9.08789 1.74609Z" />
                                                            </svg>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr class="text-heading text-center py-5">
                                            <td class="w-1 !pe-0">
                                            {{ __('You have no documents. Create one to get started.') }}
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-3">
                    <div id="storytelling-body" class="col-12 {{ is_null($storytelling) ? 'blur-content' : '' }}">
                                <div class="card">
                                    <div class="card-header  d-block">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div>
                                                <h5 class="mb-0">{{ __('Storytelling') }}</h5>
                                            </div>
                                            <!-- Add this inside your Blade template where you show project details -->
                                            <div>
                                                <button onclick="regenerateProjectInfo({{ $project->id }}, 'storytelling')"
                                                    class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                                <button onclick="collapseByType('storytelling', 'minius')"
                                                    id="storytelling-minius" class="btn btn-success btn-sm"><i
                                                        class="ti ti-minus"></i></button>
                                                <button onclick="collapseByType('storytelling', 'plus')"
                                                    id="storytelling-plus" style="display: none"
                                                    class="btn btn-success btn-sm"><i class="ti ti-plus"></i></button>
                                                <button
                                                    onclick="deleteProjectInfo({{ $project->id }}, 'storytelling', '#storytelling-body')"
                                                    class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body p-1 p-sm-4" id="storytelling-content">
    
                                        <!-- Tabs -->
                                        <div class="tab-container">
                                            <div class="tab active" data-target="#brandWheel">{{ __('Brand Wheel') }}</div>
                                            <div class="tab" data-target="#startupNaming">{{ __('Startup Naming') }}</div>
                                            <div class="tab" data-target="#elevatorPitch">{{ __('Elevator Pitch') }}</div>
                                        </div>
    
    
                                        <!-- Tab Content -->
                                        <div class="tab-content" id="brandWheel" style="display: block;">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>{{ __('Element') }}</th>
                                                        <th>{{ __('Description') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>{{ __('Mission') }}</td>
                                                        <td class="position-relative">
                                                            @if ($storytelling)
                                                                @if ($storytelling->mission)
                                                                    <div class="edit-popup position-absolute postion-edit-5"
                                                                        data-id="{{ $storytelling->id }}"
                                                                        data-type="storytelling" data-key="mission"
                                                                        data-name="Mission"
                                                                        data-value="{{ $storytelling->mission }}"
                                                                         role="button"><i
                                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                                @endif
                                                            @endif
                                                            <p
                                                                id="storytelling-mission">{{ $storytelling->mission ?? 'Not generated.' }}</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ __('Vision') }}</td>
                                                        <td class="position-relative">
                                                            @if ($storytelling)
                                                                @if ($storytelling->vision)
                                                                    <div class="edit-popup position-absolute postion-edit-5"
                                                                        data-id="{{ $storytelling->id }}"
                                                                        data-type="storytelling" data-key="vision"
                                                                        data-name="Vision"
                                                                        data-value="{{ $storytelling->vision }}"
                                                                         role="button"><i
                                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                                @endif
                                                            @endif
                                                            <p
                                                                id="storytelling-vision">{{ $storytelling->vision ?? 'Not generated.' }}</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ __('Brand Positioning') }}</td>
                                                        <td class="position-relative">
                                                            @if ($storytelling)
                                                                @if ($storytelling->brand_positioning)
                                                                    <div class="edit-popup position-absolute postion-edit-5"
                                                                        data-id="{{ $storytelling->id }}"
                                                                        data-type="storytelling" data-key="brand_positioning"
                                                                        data-name="Brand Positioning"
                                                                        data-value="{{ $storytelling->brand_positioning }}"
                                                                         role="button"><i
                                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                                @endif
                                                            @endif
                                                            <p
                                                                id="storytelling-brand_positioning">{{ $storytelling->brand_positioning ?? 'Not generated.' }}</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ __('Brand Values') }}</td>
                                                        <td class="position-relative">
                                                            @if ($storytelling)
                                                                @if ($storytelling->brand_values)
                                                                    <div class="edit-popup position-absolute postion-edit-5"
                                                                        data-id="{{ $storytelling->id }}"
                                                                        data-type="storytelling" data-key="brand_values"
                                                                        data-name="Brand Values"
                                                                        data-value="{{ $storytelling->brand_values }}"
                                                                         role="button"><i
                                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                                @endif
                                                            @endif
                                                            <p
                                                                id="storytelling-brand_values">{{ $storytelling->brand_values ?? 'Not generated.' }}</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>{{ __('Personality') }}</td>
                                                        <td class="position-relative">
                                                            @if ($storytelling)
                                                                @if ($storytelling->personality)
                                                                    <div class="edit-popup position-absolute postion-edit-5"
                                                                        data-id="{{ $storytelling->id }}"
                                                                        data-type="storytelling" data-key="personality"
                                                                        data-name="Personality"
                                                                        data-value="{{ $storytelling->personality }}"
                                                                         role="button"><i
                                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                                @endif
                                                            @endif
                                                            <p
                                                                id="storytelling-personality">{{ $storytelling->personality ?? 'Not generated.' }}</p>
                                                        </td>
                                                    </tr>
                                                    <!-- More rows as needed -->
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-content" id="startupNaming" style="display: none;">
                                            @php
                                                $suggestedNames = isset($storytelling->suggested_names) ? preg_split('/\s(?=\d)/', $storytelling->suggested_names) : [];
                                            @endphp
    
                                            @if (!empty($suggestedNames))
                                                @foreach ($suggestedNames as $index => $suggestedName)
                                                    <div class="name-square">{{ trim($suggestedName) }}</div>
                                                @endforeach
                                            @else
                                                Not generated.
                                            @endif
                                        </div>
    
    
    
                                        <div class="tab-content position-relative" id="elevatorPitch" style="display: none;">
                                            @if ($storytelling)
                                                @if ($storytelling->elevator_pitch)
                                                    <div class="edit-popup position-absolute position-edit-5-0"
                                                        data-id="{{ $storytelling->id }}" data-type="storytelling"
                                                        data-key="elevator_pitch" data-name="Elevator Pitch"
                                                        data-value="{{ $storytelling->elevator_pitch }}"
                                                         role="button"><i
                                                            class="ti ti-pencil-alt text-xs"></i></div>
                                                @endif
                                            @endif
                                            <p><i id="storytelling-elevator_pitch">
                                                    {{ $storytelling->elevator_pitch ?? 'Not generated.' }}</i>
                                            </p>
                                        </div>
    
    
    
                                    </div>
                                </div>
                            </div>
                            
                </div>
            </div>
        </div>
        <div id="launch" style="display: none;">
            <div class="row">
                <div class="col-12 mt-3">
                    <div id="landing-page-body" class="col-12 {{ $landingPage->isEmpty() ? 'blur-content' : '' }}">
                        <div class="card">
                            <div class="card-header  d-block">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <h5 class="mb-0">{{ __('Landing Page') }}</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-1 p-sm-4" id="landingPage-content">
                                <div class="row">
                                    @foreach($landingPage as $page)
                                        <div class="col-md-6 col-lg-4">
                                            <div class="card position-relative mb-5">
                                                <div class="card-body mt-5 m-[15px] h-[300px] image-background d-flex align-items-center justify-center position-relative">
                                                    @if ($page->preview_url)
                                                        <iframe src="{{ $page->preview_url }}" class="image-iframe w-full h-full" frameborder="0"></iframe>
                                                    @else
                                                        <img src="https://app.fikrahub.com/public/images/fh.jpg"
                                                            style="width:100%;">
                                                    @endif
                                                </div>
                                                <div class="card-footer d-flex align-items-center justify-center">
                                                    <div class="text-center">
                                                        <a type="button" class="btn btn-primary" 
                                                            href="https://websites.fikrahub.com/login?email={{ Auth::user()->email }}&idea_id={{ $project->id }}&lara_id={{ $page->lara_id }}&landing_page_id={{ $page->id }}"
                                                            
                                                        >{{ __('Edit') }}</a>
                                                    </div>
                                                    <div class="text-center mx-1">
                                                        <a type="button" class="btn btn-primary" 
                                                            href="{{ $page->preview_url }}"
                                                            target="_blank"
                                                        >{{ __('Preview') }}</a>
                                                    </div>
                                                    <div class="text-center">
                                                        <a type="button" class="btn btn-danger" 
                                                            onclick="deleteLandingPage({{ $page->id }})"
                                                        >{{ __('Delete') }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-modal="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content" style="border: #333 0.5px solid">
                    <div class="modal-header">
                        <h5 class="modal-title" id="popup-title">Edit</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-floating">
                            <textarea class="form-control" placeholder="Leave a comment here" id="popup-value" style="height: 100px"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="d-none" data-value="{{ app()->getLocale() }}" id='save-locale' />
                        <input class="d-none" data-value="" id='save-data' />
                        <button type="button" class="btn btn-secondary hide-popup" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary hide-popup save" id="popup-save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Your existing content -->
<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>
@endsection

@push('script')
    <script>
        // Get all elements with the class 'generate'
        var generateButtons = document.getElementsByClassName('generate');

        // Function to handle the click event
        var handleClick = function(event) {
            event.preventDefault(); // Prevent the default form submission

            // Show the mesh-loader
            document.getElementById('fullPageLoader').style.display = 'block';

            // Delay the form submission to allow the loader to show
            setTimeout(function() {
                event.target.form.submit();
            }, 100); // You can adjust this delay as needed
        };

        // Add the event listener to each button
        for (var i = 0; i < generateButtons.length; i++) {
            generateButtons[i].addEventListener('click', handleClick);
        }
    </script>

    <script></script>



    <script>
        $(window).on('load', function() {
            if (location.href.includes('store') || location.href.includes('openai')) {
                window.history.pushState("", "", location.origin + '/dashboard/user/ideas/' + '{{ $project->id }}');
            }
                $('.tooltiptext').css('opacity', 1);
            setTimeout(() => {
                if ($('.tooltiptext').html() != '{{ __('Generating...') }}') {
                    $('.tooltiptext').css('opacity', 0);
                }
            }, 10000)
            localStorage.setItem('is_edit_show', 'hide');
            var queryString = location.search;
            var params = new URLSearchParams(queryString);
            var tab = params.get('tab');
            if (tab == 'marketing' || localStorage.getItem('show_tab') == 'marketing') {
                $('#tab-concept').removeClass('active');
                $('#tab-launch').removeClass('active');
                $('#tab-marketing').addClass('active');
                $('#marketing').show();
                $('#concept').hide();
                $('#launch').hide();
                $('.tab-marketing').show();
                $('.tab-concept').hide();
                $('.tab-launch').hide();
                localStorage.removeItem('show_tab');
            } else if(tab == 'launch') {
                $('#tab-marketing').removeClass('active');
                $('#tab-concept').removeClass('active');
                $('#tab-launch').addClass('active');
                $('#concept').hide();
                $('#marketing').hide();
                $('#launch').show();
                $('.tab-marketing').hide();
                $('.tab-concept').hide();
                $('.tab-launch').show();
            } else {
                $('#tab-marketing').removeClass('active');
                $('#tab-launch').removeClass('active');
                $('#tab-concept').addClass('active');
                $('#concept').show();
                $('#marketing').hide();
                $('#launch').hide();
                $('.tab-marketing').hide();
                $('.tab-concept').show();
                $('.tab-launch').hide();
            }
            let arr = ['swot', 'leanCanvas', 'competitiveAnalysis', 'userPersonas', 'storytelling', 'userStories'];
            arr.forEach((type) => {
                if (localStorage.getItem(type + '_collapse_' + {{ $project->id }}) == 'hide') {
                    $('#' + type + '-minius').hide();
                    $('#' + type + '-plus').show();
                    $('#' + type + '-content').hide();
                } else {
                    $('#' + type + '-minius').show();
                    $('#' + type + '-plus').hide();
                    $('#' + type + '-content').show();
                }
            });
        });
        
        function toggleEditButton() {
            if (localStorage.getItem('is_edit_show') == 'show') {
                localStorage.setItem('is_edit_show', 'hide');
                $('.bg-edit').css('background', 'transparent');
                $('.edit-popup').hide();
            } else {
                localStorage.setItem('is_edit_show', 'show');
                $('.bg-edit').css('background', '#333333');
                $('.edit-popup').show();
            }
        }

        function collapseByType(type, btn_name) {
            if (btn_name == 'minius') {
                $('#' + type + '-minius').hide();
                $('#' + type + '-plus').show();
                $('#' + type + '-content').hide();
                localStorage.setItem(type + '_collapse_' + {{ $project->id }}, 'hide');
            } else {
                $('#' + type + '-minius').show();
                $('#' + type + '-plus').hide();
                $('#' + type + '-content').show();
                localStorage.setItem(type + '_collapse_' + {{ $project->id }}, 'show');
            }
        };
        
        function addQueryStringToCurrentUrl(query) {
            // Get the current URL
            const currentUrl = location.href;
        
            // Check if the current URL already has a query string
            const separator = currentUrl.includes('?') ? '&' : '?';
        
            // Concatenate the query string to the current URL
            const modifiedUrl = `${currentUrl}${separator}query=${encodeURIComponent(query)}`;
        
            // Push the modified URL to the browser history without reloading the page
            history.pushState({ path: modifiedUrl }, '', modifiedUrl);
        }
        
        function deleteProjectDocument(url) {
            let link = "{{ route('dashboard.user.ideas.show', ':project') }}";
                link = link.replace(':project', {{ $project->id }}) + "?tab=marketing";
            $.ajax({
                url: url,
                type: 'GET',
                success: function(response) {
                    if (response.type == 'success') {
                        location.href = link;
                    }
                },
                error: function(response) {
                }
            });
        };

        function generateProjectInfo(url) {
            $(document).ajaxSend(function() {
                $("#overlay").fadeIn(300);
            });
            $.ajax({
                url: url,
                type: 'POST',
                success: function(response) {
                    if (response.success) {
                        location.reload();
                    }
                },
                error: function(response) {
                    response = response.responseJSON;
                }
            }).done(function() {
                setTimeout(function() {
                    $("#overlay").fadeOut(300);
                }, 500);
            });
            $('#fullPageLoader').hide();
        };

        function regenerateProjectInfo(id, type) {
            $('#add-new-floating').hide();
            $('.tooltiptext').css('opacity', 1);
            $('.tooltiptext').html('{{ __('Generating...') }}');
            $('.spinner_button svg').hide();
            $('.spinner-border').show();
            let data = {
                id: id,
                type: type
            };
            $.ajax({
                url: '{{ route('projects.regenerate-relate-information') }}',
                type: 'POST',
                data: data,
                success: function(response) {
                    if (response.is_success) {
                        $('#add-new-floating').show();
                        location.reload();
                    }
                },
                error: function(response) {
                    response = response.responseJSON;
                }
            });
            
        };
        
        function deleteLandingPage(id) {
            let data = {
                id: id
            };
            $.ajax({
                url: '{{ route('projects.delete-landing-page') }}',
                type: 'POST',
                data: data,
                success: function(response) {
                    if (response.is_success) {
                        toastr.success('Deleted Success');
                        if (location.href.includes('launch')) {
                            location.reload();
                        } else {
                            location.href = location.href + '?tab=launch';
                        }
                    } else {
                        toastr.error('Something Wents Wrong.');
                    }
                },
                error: function(response) {
                    toastr.error('Something Wents Wrong.');
                }
            });
        }

        function deleteProjectInfo(id, type, body_selector) {
            $(document).ajaxSend(function() {
                $("#overlay").fadeIn(300);
            });
            let data = {
                id: id,
                type: type
            };
            $.ajax({
                url: '{{ route('projects.delete-relate-information') }}',
                type: 'POST',
                data: data,
                success: function(response) {
                    if (response.is_success) {
                        toastr.success('Delete Success');
                        location.reload();
                        $(body_selector).hide();
                    } else {
                        toastr.error('Something Wents Wrong.');
                    }
                },
                error: function(response) {
                    $('.loader-wrapper').addClass('d-none')
                    toastr.error('Something Wents Wrong.');
                }
            }).done(function() {
                setTimeout(function() {
                    $("#overlay").fadeOut(300);
                }, 500);
            });
        }

        $(document).ready(function() {
            $('#tab-marketing').on('click', function() {
                $('#tab-marketing').addClass('active');
                $('#tab-concept').removeClass('active');
                $('#tab-launch').removeClass('active');
                $('#concept').hide();
                $('#marketing').show();
                $('#launch').hide();
                $('.tab-marketing').show();
                $('.tab-concept').hide();
                $('.tab-launch').hide();
            });
            $('#tab-concept').on('click', function() {
                $('#tab-concept').addClass('active');
                $('#tab-marketing').removeClass('active');
                $('#tab-launch').removeClass('active');
                $('#concept').show();
                $('#marketing').hide();
                $('#launch').hide();
                $('.tab-marketing').hide();
                $('.tab-concept').show();
                $('.tab-launch').hide();
            });
            $('#tab-launch').on('click', function() {
                $('#tab-marketing').removeClass('active');
                $('#tab-concept').removeClass('active');
                $('#tab-launch').addClass('active');
                $('#concept').hide();
                $('#marketing').hide();
                $('#launch').show();
                $('.tab-marketing').hide();
                $('.tab-concept').hide();
                $('.tab-launch').show();
            });
            
            $('.hide-popup').on('click', function() {
                let buttonName = $(this).html();
                if (buttonName == 'Save changes') {
                    let type = localStorage.getItem('type');
                    let name = localStorage.getItem('name');
                    let key = localStorage.getItem('key');
                    let value = $('#popup-value').val();
                    let id = localStorage.getItem('id');

                    let data = {
                        type: type,
                        id: id,
                        key: key,
                        value: value,
                    };

                    if ($('#save-locale').data('value') == 'ar') {
                        data.key += '_abr';
                    }
                    $.ajax({
                        url: '{{ route('projects.update-relate-information') }}',
                        type: 'POST',
                        data: data,
                        success: function(response) {
                            if (response.is_success) {
                                toastr.success(response.message);
                                if (type == 'userStories' || type == 'userPersonas' || type ==
                                    'competitiveAnalysis') {
                                    $('#' + type + '-' + key + '-' + id).html(value);
                                } else {
                                    $('#' + type + '-' + key).html(value);
                                }
                            } else {
                                toastr.error('Something Wents Wrong.');
                            }
                        },
                        error: function(response) {
                            toastr.error('Something Wents Wrong.');
                        }
                    })
                }
                $('#exampleModalCenter').removeClass('show');
                $('#exampleModalCenter').css('display', 'none');
            });
            $('.edit-popup').on('click', function() {
                let type = $(this).attr("data-type");
                let key = $(this).attr("data-key");
                let name = $(this).attr("data-name");
                let value = $(this).attr("data-value");
                let id = $(this).attr("data-id");
                localStorage.setItem('type', type);
                localStorage.setItem('key', key);
                localStorage.setItem('name', name);
                localStorage.setItem('value', value);
                localStorage.setItem('id', id);
                $('#popup-title').html('Edit ' + name);
                $('#popup-value').val(value);
                $('#exampleModalCenter').addClass('show');
                $('#exampleModalCenter').css('display', 'block');
            });
            $('.show-anwser').on('click', function() {
                let target = $(this).data('target');
                if ($(target + ' div').hasClass('show')) {
                    $(target + ' div').removeClass('show');
                } else {
                    $(target + ' div').addClass('show');
                }
            });
            var type = '{{ $project->type }}';
            if (type == 'template') {
                $('.pro_type').addClass('d-none');
            } else {
                $('.pro_type').removeClass('d-none');
            }
        });
    </script>
    <script>
        $('.cp_link').on('click', function() {
            var value = $(this).attr('data-link');
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(value).select();
            document.execCommand("copy");
            $temp.remove();
            toastrs('success', '{{ __('Link Copy on Clipboard') }}', 'success')
        });
    </script>

    <script>
        // Add event listeners once the DOM is fully loaded
        document.addEventListener('DOMContentLoaded', () => {
            // Get all tabs and tab content nodes
            const tabs = document.querySelectorAll('.tab');
            const tabContents = document.querySelectorAll('.tab-content');

            // Function to clear all active states and hide all content
            function clearActiveState() {
                tabs.forEach(tab => {
                    tab.classList.remove('active');
                });
                tabContents.forEach(content => {
                    content.style.display = 'none';
                });
            }

            // Add click event listeners to tabs
            tabs.forEach(tab => {
                tab.addEventListener('click', () => {
                    clearActiveState();
                    tab.classList.add('active'); // Make the clicked tab active
                    const activeTabContent = document.querySelector(tab.dataset.target);
                    activeTabContent.style.display = 'block'; // Show the clicked tab content
                });
            });

            // Initialize the first tab and content as active
            if (tabs.length > 0) {
                clearActiveState();
                tabs[0].classList.add('active');
                tabContents[0].style.display = 'block';
            }
        });
    </script>

    <script>
        // Add event listeners once the DOM is fully loaded
        document.addEventListener('DOMContentLoaded', () => {
            // Get all tabs and tab content nodes
            const tabs = document.querySelectorAll('.tab2');
            const tabContents = document.querySelectorAll('.tab-content');

            // Function to clear all active states and hide all content
            function clearActiveState() {
                tabs.forEach(tab => {
                    tab.classList.remove('active');
                });
                tabContents.forEach(content => {
                    content.style.display = 'none';
                });
            }

            // Add click event listeners to tabs
            tabs.forEach(tab => {
                tab.addEventListener('click', () => {
                    clearActiveState();
                    tab.classList.add('active'); // Make the clicked tab active
                    const activeTabContent = document.querySelector(tab.dataset.target);
                    activeTabContent.style.display = 'block'; // Show the clicked tab content
                });
            });

            // Initialize the first tab and content as active
            if (tabs.length > 0) {
                clearActiveState();
                tabs[0].classList.add('active');
                tabContents[0].style.display = 'block';
            }
        });
    </script>

    <script>
        var tabs = $('.tabs');
        var selector = $('.tabs').find('a').length;
        //var selector = $(".tabs").find(".selector");
        var activeItem = tabs.find('.active');
        var activeWidth = activeItem.innerWidth();
        $(".selector").css({
            "left": activeItem.position.left + "px",
            "width": activeWidth + "px"
        });

        $(".tabs").on("click", "a", function(e) {
            e.preventDefault();
            $('.tabs a').removeClass("active");
            $(this).addClass('active');
            var activeWidth = $(this).innerWidth();
            var itemPos = $(this).position();
            $(".selector").css({
                "left": itemPos.left + "px",
                "width": activeWidth + "px"
            });
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var dropdowns = document.querySelectorAll('.dropdown');
            dropdowns.forEach(function(dropdown) {
                dropdown.addEventListener('click', function(event) {
                    event.stopPropagation();
                    this.classList.toggle('active');
                });
            });

            // Close the dropdown if clicked outside
            window.addEventListener('click', function() {
                dropdowns.forEach(function(dropdown) {
                    dropdown.classList.remove('active');
                });
            });
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            var projectId = {{ $project->id }}; // Replace with the actual project ID

            // function checkProjectCount(projectId) {
            //     fetch(`/projects/${projectId}/check-count`, {
            //             method: 'POST',
            //             headers: {
            //                 'Content-Type': 'application/json',
            //                 'X-CSRF-TOKEN': csrfToken // Include CSRF token
            //             },
            //             body: JSON.stringify({
            //                 projectId: projectId
            //             })
            //         })
            //         .then(response => response.json())
            //         .then(data => {
            //             document.querySelector('.counter.research').textContent = data.count + '/3';
            //         })
            //         .catch(error => console.error('Error:', error));
            // }

            // // Call the function automatically when the page loads
            // checkProjectCount(projectId);
        });
    </script>
@endpush
