<option value="en-GB" {{$setting->openai_default_language == 'en-GB' ? 'selected' : null}}>{{__('English')}}</option>
<option value="ar-AE" {{$setting->openai_default_language == 'ar-AE' ? 'selected' : null}}>{{__('Arabic')}}</option>
