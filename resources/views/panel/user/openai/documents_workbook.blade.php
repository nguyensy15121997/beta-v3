@extends('panel.layout.app')
@section('title', __('Workbook'))

@section('content')
    <div class="page-header">
        <div class="container-xl">
            <div class="row g-2 items-center">
                <div class="col">
    				<a id='back-to-idea-board' style="cursor: pointer;" class="page-pretitle flex items-center">
    					<svg class="!me-2 rtl:-scale-x-100" width="8" height="10" viewBox="0 0 6 10" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    						<path d="M4.45536 9.45539C4.52679 9.45539 4.60714 9.41968 4.66071 9.36611L5.10714 8.91968C5.16071 8.86611 5.19643 8.78575 5.19643 8.71432C5.19643 8.64289 5.16071 8.56254 5.10714 8.50896L1.59821 5.00004L5.10714 1.49111C5.16071 1.43753 5.19643 1.35718 5.19643 1.28575C5.19643 1.20539 5.16071 1.13396 5.10714 1.08039L4.66071 0.633963C4.60714 0.580392 4.52679 0.544678 4.45536 0.544678C4.38393 0.544678 4.30357 0.580392 4.25 0.633963L0.0892856 4.79468C0.0357141 4.84825 0 4.92861 0 5.00004C0 5.07146 0.0357141 5.15182 0.0892856 5.20539L4.25 9.36611C4.30357 9.41968 4.38393 9.45539 4.45536 9.45539Z"/>
    					</svg>
    					{{__('Back to dashboard')}}
    				</a>
                    <h2 class="page-title mb-2">
                        {{ __('Edit') ." ". $workbook->title }} 
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- Page body -->
    <div class="page-body pt-6 max-md:pt-3">
        <div class="container-xl">
            <div class="row">
                <div class="col-12"></div>
                <div class="col-lg-8 mx-auto">
                    @if ($workbook->generator->type == 'code')
                        <div>
                        @else
                            <div
                                class="border-solid border-t border-r-0 border-b-0 border-l-0 border-[var(--tblr-border-color)] pt-[30px] mt-[15px] max-lg:mt-0 max-lg:pt-0 max-lg:border-t-0">
                    @endif
                    @if (view()->exists('panel.admin.custom.user.openai.documents_workbook_textarea'))
                        @include('panel.admin.custom.user.openai.documents_workbook_textarea')
                    @else
                        @include('panel.user.openai.documents_workbook_textarea')
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('script')
    <script src="/assets/libs/tinymce/tinymce.min.js" defer></script>
    <script src="/assets/js/panel/workbook.js"></script>
    <script>
        $(document).on('click', '#back-to-idea-board', function() {
            var queryString = location.search;
            var params = new URLSearchParams(queryString);
            var ideaId = params.get('idea_id');
            if (ideaId) {
                let url = "{{ route('dashboard.user.ideas.show', ':project') }}";
                url = url.replace(':project', ideaId) + "?tab=marketing";
                location.href = url;
            }
        });
    </script>
    @if ($openai->type == 'code')
        <link rel="stylesheet" href="/assets/libs/prism/prism.css">
        <script src="/assets/libs/prism/prism.js"></script>
        <script>
            window.Prism = window.Prism || {};
            window.Prism.manual = true;
            document.addEventListener('DOMContentLoaded', (event) => {
                "use strict";

                const codeLang = document.querySelector('#code_lang');
                const codePre = document.querySelector('#code-pre');
                const codeOutput = codePre?.querySelector('#code-output');

                if (!codeOutput) return;

                codePre.classList.add(`language-${codeLang && codeLang.value !== '' ? codeLang.value : 'javascript'}`);

                // saving for copy
                window.codeRaw = codeOutput.innerText;

                Prism.highlightElement(codeOutput);
            });
        </script>
    @endif
@endsection
