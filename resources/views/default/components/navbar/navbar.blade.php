<a
    class="lqd-skip-link pointer-events-none fixed start-7 top-7 z-[90] rounded-md bg-background px-3 py-1 text-lg opacity-0 shadow-xl focus-visible:pointer-events-auto focus-visible:opacity-100 focus-visible:outline-primary"
    href="#lqd-titlebar"
>
    {{ __('Skip to content') }}
</a>

<button
    class="lqd-navbar-expander size-6 fixed start-[--navbar-width] top-[calc(var(--header-height)/2)] z-[999] inline-flex -translate-x-1/2 -translate-y-1/2 cursor-pointer items-center justify-center rounded-full border-0 bg-foreground/10 p-0 text-heading-foreground backdrop-blur-sm transition-all hover:bg-heading-foreground hover:text-heading-background group-[.navbar-shrinked]/body:!start-[80px] group-[.navbar-shrinked]/body:rotate-180 max-lg:hidden"
    x-init
    @click.prevent="$store.navbarShrink.toggle()"
>
    <x-tabler-chevron-left class="w-4" />
</button>

<aside
    class="lqd-navbar max-lg:rounded-b-5 z-[99] w-[--navbar-width] shrink-0 overflow-hidden rounded-ee-navbar-ee rounded-es-navbar-es rounded-se-navbar-se rounded-ss-navbar-ss border-e border-navbar-border bg-navbar-background text-navbar font-medium text-navbar-foreground transition-all max-lg:invisible max-lg:absolute max-lg:left-0 max-lg:top-[65px] max-lg:z-[99] max-lg:max-h-[calc(85vh-2rem)] max-lg:min-h-0 max-lg:w-full max-lg:origin-top max-lg:-translate-y-2 max-lg:scale-95 max-lg:overflow-y-auto max-lg:bg-background max-lg:p-0 max-lg:opacity-0 max-lg:shadow-xl lg:sticky lg:top-0 lg:h-screen max-lg:[&.lqd-is-active]:visible max-lg:[&.lqd-is-active]:translate-y-0 max-lg:[&.lqd-is-active]:scale-100 max-lg:[&.lqd-is-active]:opacity-100"
    x-init
    :class="{ 'lqd-is-active': !$store.mobileNav.navCollapse }"
>
    <div class="lqd-navbar-inner -me-navbar-me h-full overflow-y-auto overscroll-contain pe-navbar-pe ps-navbar-ps">
        <div
            class="lqd-navbar-logo relative flex min-h-[--header-height] max-w-full items-center pe-navbar-link-pe ps-navbar-link-ps group-[.navbar-shrinked]/body:w-full group-[.navbar-shrinked]/body:justify-center group-[.navbar-shrinked]/body:px-0 group-[.navbar-shrinked]/body:text-center max-lg:hidden">
            <a
                class="block px-0"
                href="{{ LaravelLocalization::localizeUrl(route('dashboard.index')) }}"
            >
                @if (isset($setting->logo_dashboard))
                    <img
                        class="h-auto w-full group-[.navbar-shrinked]/body:hidden"
                        src="{{ custom_theme_url($setting->logo_dashboard_path, true) }}"
                        @if (isset($setting->logo_dashboard_2x_path)) srcset="/{{ $setting->logo_dashboard_2x_path }} 2x" @endif
                        alt="{{ $setting->site_name }}"
                    >
                @else
                    <img
                        class="h-auto w-full group-[.navbar-shrinked]/body:hidden dark:hidden"
                        src="{{ custom_theme_url($setting->logo_path, true) }}"
                        @if (isset($setting->logo_2x_path)) srcset="/{{ $setting->logo_2x_path }} 2x" @endif
                        alt="{{ $setting->site_name }}"
                    >
                    <img
                        class="hidden h-auto w-full group-[.navbar-shrinked]/body:hidden dark:block"
                        src="{{ custom_theme_url($setting->logo_dark_path, true) }}"
                        @if (isset($setting->logo_dark_2x_path)) srcset="/{{ $setting->logo_dark_2x_path }} 2x" @endif
                        alt="{{ $setting->site_name }}"
                    >
                @endif

                <!-- collapsed -->
                <img
                    class="max-w-10 mx-auto hidden h-auto w-full group-[.navbar-shrinked]/body:block dark:!hidden"
                    src="{{ custom_theme_url($setting->logo_collapsed_path, true) }}"
                    @if (isset($setting->logo_collapsed_2x_path)) srcset="/{{ $setting->logo_collapsed_2x_path }} 2x" @endif
                    alt="{{ $setting->site_name }}"
                >
                <img
                    class="max-w-10 mx-auto hidden h-auto w-full group-[.theme-dark.navbar-shrinked]/body:block"
                    src="{{ custom_theme_url($setting->logo_collapsed_dark_path, true) }}"
                    @if (isset($setting->logo_collapsed_dark_2x_path)) srcset="/{{ $setting->logo_collapsed_dark_2x_path }} 2x" @endif
                    alt="{{ $setting->site_name }}"
                >

            </a>
        </div>
        <nav
            class="lqd-navbar-nav"
            id="navbar-menu"
        >
            <ul class="lqd-navbar-ul">
                <x-navbar.item>
                    <x-navbar.label>
                        {{ __('User') }}
                    </x-navbar.label>
                </x-navbar.item>
                @if (!empty(getProjectInfo()))
                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Overview') }}"
                            href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.show', ['project' => getProjectInfo()->id])) }}"
                            active-condition="{{ activeRouteBulk('dashboard.user.ideas.show', 'dashboard.user.openai.projects.show') }}"
                            icon="tabler-layout-2"
                        />
                    </x-navbar.item>
                    <x-navbar.item has-dropdown>
                    <x-navbar.link
                        label="{{ __('Concept') }}"
                        icon="tabler-message-plus"
                        active-condition="{{ activeRouteBulk('dashboard.user.ideas.concept.*') }}"
                        dropdown-trigger
                    />
                    <x-navbar.dropdown.dropdown
                        open="{{ activeRouteBulk('dashboard.user.ideas.concept.*') }}"
                    >
    
                        <x-navbar.dropdown.item>
                            <x-navbar.dropdown.link
                                label="{{ __('Viability Analysis') }}"
                                href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.viability-analysis', ['project' => getProjectInfo()->id])) }}"
                            >
                            </x-navbar.dropdown.link>
                        </x-navbar.dropdown.item>
    
                        <x-navbar.dropdown.item>
                            <x-navbar.dropdown.link
                                label="{{ __('SWOT Analysis') }}"
                                href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.swot-analysis', ['project' => getProjectInfo()->id])) }}"
                            >
                            </x-navbar.dropdown.link>
                        </x-navbar.dropdown.item>
    
                        <x-navbar.dropdown.item>
                            <x-navbar.dropdown.link
                                label="{{ __('Lean Canvas') }}"
                                href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.lean-canvas', ['project' => getProjectInfo()->id])) }}"
                            >
                            </x-navbar.dropdown.link>
                        </x-navbar.dropdown.item>
    
                        <x-navbar.dropdown.item>
                            <x-navbar.dropdown.link
                                label="{{ __('User Personas') }}"
                                href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.user-personas', ['project' => getProjectInfo()->id])) }}"
                            >
                            </x-navbar.dropdown.link>
                        </x-navbar.dropdown.item>
    
                        <x-navbar.dropdown.item>
                            <x-navbar.dropdown.link
                                label="{{ __('User Stories') }}"
                                href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.user-stories', ['project' => getProjectInfo()->id])) }}"
                            >
                            </x-navbar.dropdown.link>
                        </x-navbar.dropdown.item>
    
                        <x-navbar.dropdown.item>
                            <x-navbar.dropdown.link
                                label="{{ __('Interview Questions') }}"
                                href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.customer-interview-questions', ['project' => getProjectInfo()->id])) }}"
                            >
                            </x-navbar.dropdown.link>
                        </x-navbar.dropdown.item>
    
                        <x-navbar.dropdown.item>
                            <x-navbar.dropdown.link
                                label="{{ __('Market Size and Trends') }}"
                                href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.market-size-trends', ['project' => getProjectInfo()->id])) }}"
                            >
                            </x-navbar.dropdown.link>
                        </x-navbar.dropdown.item>
    
                    </x-navbar.dropdown.dropdown>
                </x-navbar.item>
                    <x-navbar.item has-dropdown>
                        <x-navbar.link
                            label="{{ __('Marketing') }}"
                            icon="tabler-speakerphone"
                            active-condition="{{ activeRouteBulk('dashboard.user.ideas.marketing.*') }}"
                            dropdown-trigger
                        />
                        
                        <x-navbar.dropdown.dropdown
                            open="{{ activeRouteBulk('dashboard.user.ideas.marketing.*') }}"
                        >
        
                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Marketing Overview') }}"
                                    href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.marketing.marketing-overview', ['project' => getProjectInfo()->id])) }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>
        
                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Brand Wheel') }}"
                                    href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.marketing.brand-wheel', ['project' => getProjectInfo()->id])) }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>
    
                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Brand Identity') }}"
                                    href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.marketing.brand-identity', ['project' => getProjectInfo()->id])) }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>
        
                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Marketing Content') }}"
                                    href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.marketing.marketing-content', ['project' => getProjectInfo()->id])) }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>
                            
                        </x-navbar.dropdown.dropdown>
                        
                </x-navbar.item>
                    <x-navbar.item has-dropdown>
                        <x-navbar.link
                            label="{{ __('Launch') }}"
                            icon="tabler-rocket"
                            active-condition="{{ activeRouteBulk('dashboard.user.ideas.launch.*') }}"
                            dropdown-trigger
                        />
                        <x-navbar.dropdown.dropdown
                            open="{{ activeRouteBulk('dashboard.user.ideas.launch.*') }}"
                        >
        
                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Landing Pages') }}"
                                    href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.launch.landing-pages', ['project' => getProjectInfo()->id])) }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>
        
                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Tasks Management') }}"
                                    href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.launch.tasks-management', ['project' => getProjectInfo()->id])) }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>
                            
                        </x-navbar.dropdown.dropdown>
                                
                    </x-navbar.item>
                @else
                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Overview') }}"
                            icon="tabler-layout-2"
                        />
                    </x-navbar.item>
                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Concept') }}"
                            icon="tabler-message-plus"
                        />
                    </x-navbar.item>
                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Marketing') }}"
                            icon="tabler-speakerphone"
                        />
                    </x-navbar.item>
                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Launch') }}"
                            icon="tabler-rocket"
                        />
                    </x-navbar.item>
                @endif

                

                {{-- Admin menu items --}}
                @if (Auth::user()->type == 'admin')
                    <x-navbar.item>
                        <x-navbar.divider />
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.label>
                            {{ __('Admin') }}
                        </x-navbar.label>
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Dashboard') }}"
                            href="dashboard.admin.index"
                            icon="tabler-layout-2"
                        />
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Marketplace') }}"
                            href="dashboard.admin.marketplace.index"
                            icon="tabler-building-store"
                            new
                        />
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Themes') }}"
                            href="dashboard.admin.themes.index"
                            icon="tabler-palette"
                            new
                        />
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('User Management') }}"
                            href="dashboard.admin.users.index"
                            active-condition="{{ activeRoute('dashboard.admin.users.*') }}"
                            icon="tabler-users"
                        />
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Google Adsense') }}"
                            href="dashboard.admin.ads.index"
                            active-condition="{{ activeRoute('dashboard.admin.ads.*') }}"
                            icon="tabler-ad-circle"
                        />
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Support Requests') }}"
                            href="dashboard.support.list"
                            active-condition="{{ activeRoute('dashboard.support.*') }}"
                            icon="tabler-lifebuoy"
                        />
                    </x-navbar.item>

                    <x-navbar.item has-dropdown>
                        <x-navbar.link
                            label="{{ __('Templates') }}"
                            href="dashboard.admin.openai.list"
                            icon="tabler-list-details"
                            active-condition="{{ activeRouteBulk('dashboard.admin.openai.list', 'dashboard.admin.openai.custom.*', 'dashboard.admin.openai.categories.*') }}"
                            dropdown-trigger
                        />
                        <x-navbar.dropdown.dropdown
                            open="{{ activeRouteBulk('dashboard.admin.openai.list', 'dashboard.admin.openai.custom.*', 'dashboard.admin.openai.categories.*', 'dashboard.email-templates.*') }}"
                        >

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Built-in Templates') }}"
                                    href="dashboard.admin.openai.list"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Custom Templates') }}"
                                    href="dashboard.admin.openai.custom.list"
                                    active-condition="{{ activeRouteBulk('dashboard.admin.openai.custom.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('AI Writer Categories') }}"
                                    href="dashboard.admin.openai.categories.list"
                                    active-condition="{{ activeRouteBulk('dashboard.admin.openai.categories.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                        </x-navbar.dropdown.dropdown>
                    </x-navbar.item>

                    <x-navbar.item has-dropdown>
                        <x-navbar.link
                            label="{{ __('Chat Settings') }}"
                            href=""
                            icon="tabler-message-circle"
                            active-condition="{{ activeRouteBulk('dashboard.admin.chatbot.*', 'dashboard.admin.openai.chat.list', 'dashboard.admin.openai.chat.addOrUpdate', 'dashboard.admin.openai.chat.category', 'dashboard.admin.openai.chat.addOrUpdateCategory') }}"
                            dropdown-trigger
                        />
                        <x-navbar.dropdown.dropdown
                            open="{{ activeRouteBulk('dashboard.admin.chatbot.*', 'dashboard.admin.openai.chat.list', 'dashboard.admin.openai.chat.addOrUpdate', 'dashboard.admin.openai.chat.category', 'dashboard.admin.openai.chat.addOrUpdateCategory') }}"
                        >

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Chat Categories') }}"
                                    href="dashboard.admin.openai.chat.category"
                                    active-condition="{{ activeRoute('dashboard.admin.openai.chat.category.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Chat Templates') }}"
                                    href="dashboard.admin.openai.chat.list"
                                    active-condition="{{ activeRoute('dashboard.admin.chat.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Chatbot Training') }}"
                                    href="dashboard.admin.chatbot.index"
                                    active-condition="{{ activeRoute('dashboard.admin.chatbot') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Floating Chat Settings') }}"
                                    href="dashboard.admin.chatbot.setting"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                        </x-navbar.dropdown.dropdown>
                    </x-navbar.item>

                    <x-navbar.item has-dropdown>
                        <x-navbar.link
                            label="{{ __('Frontend') }}"
                            href="dashboard.admin.frontend.settings"
                            icon="tabler-device-laptop"
                            active-condition="{{ activeRouteBulk('dashboard.admin.testimonials.*', 'dashboard.admin.frontend.authsettings', 'dashboard.admin.frontend.settings', 'dashboard.admin.frontend.faq.*', 'dashboard.admin.frontend.tools.*', 'dashboard.admin.frontend.tools.*', 'dashboard.admin.frontend.future.*', 'dashboard.admin.frontend.whois.*', 'dashboard.admin.frontend.generatorlist.*', 'dashboard.admin.clients.*', 'dashboard.admin.howitWorks.*', 'dashboard.admin.whois.*', 'dashboard.admin.frontend.menusettings', 'dashboard.admin.frontend.sectionsettings') }}"
                            dropdown-trigger
                        />
                        <x-navbar.dropdown.dropdown
                            open="{{ activeRouteBulk('dashboard.admin.testimonials.*', 'dashboard.admin.frontend.authsettings', 'dashboard.admin.frontend.settings', 'dashboard.admin.frontend.faq.*', 'dashboard.admin.frontend.tools.*', 'dashboard.admin.frontend.tools.*', 'dashboard.admin.frontend.future.*', 'dashboard.admin.frontend.whois.*', 'dashboard.admin.frontend.generatorlist.*', 'dashboard.admin.clients.*', 'dashboard.admin.howitWorks.*', 'dashboard.admin.whois.*', 'dashboard.admin.frontend.menusettings', 'dashboard.admin.frontend.sectionsettings') }}"
                        >

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Frontend Settings') }}"
                                    href="dashboard.admin.frontend.settings"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Frontend Section Settings') }}"
                                    href="dashboard.admin.frontend.sectionsettings"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Menu') }}"
                                    href="dashboard.admin.frontend.menusettings"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Auth Settings') }}"
                                    href="dashboard.admin.frontend.authsettings"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('F.A.Q') }}"
                                    href="dashboard.admin.frontend.faq.index"
                                    active-condition="{{ activeRoute('dashboard.admin.frontend.faq.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Tools Section') }}"
                                    href="dashboard.admin.frontend.tools.index"
                                    active-condition="{{ activeRoute('dashboard.admin.frontend.tools.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Features Section') }}"
                                    href="dashboard.admin.frontend.future.index"
                                    active-condition="{{ activeRoute('dashboard.admin.frontend.future.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Testimonials Section') }}"
                                    href="dashboard.admin.testimonials.index"
                                    active-condition="{{ activeRoute('dashboard.admin.testimonials.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Clients Section') }}"
                                    href="dashboard.admin.clients.index"
                                    active-condition="{{ activeRoute('dashboard.admin.clients.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('How it Works Section') }}"
                                    href="dashboard.admin.howitWorks.index"
                                    active-condition="{{ activeRoute('dashboard.admin.howitWorks.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Who Can Use Section') }}"
                                    href="dashboard.admin.frontend.whois.index"
                                    active-condition="{{ activeRoute('dashboard.admin.frontend.whois.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Generators List Section') }}"
                                    href="dashboard.admin.frontend.generatorlist.index"
                                    active-condition="{{ activeRoute('dashboard.admin.frontend.generatorlist.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                        </x-navbar.dropdown.dropdown>
                    </x-navbar.item>

                    <x-navbar.item has-dropdown>
                        <x-navbar.link
                            label="{{ __('Finance') }}"
                            href="dashboard.admin.finance.plans.index"
                            icon="tabler-wallet"
                            active-condition="{{ activeRouteBulk('dashboard.admin.finance.*', 'dashboard.admin.bank.transactions.list') }}"
                            dropdown-trigger
                        />
                        <x-navbar.dropdown.dropdown open="{{ activeRouteBulk('dashboard.admin.finance.*', 'dashboard.admin.bank.transactions.list') }}">

                            @if (bankActive())
                                <x-navbar.dropdown.item>
                                    <x-navbar.dropdown.link
                                        label="{{ __('Bank Transactions') }}"
                                        href="dashboard.admin.bank.transactions.list"
                                        badge="{{ countBankTansactions() }}"
                                    />
                                </x-navbar.dropdown.item>
                            @endif

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Membership Plans') }}"
                                    href="dashboard.admin.finance.plans.index"
                                    active-condition="{{ activeRoute('dashboard.admin.finance.plans.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Payment Gateways') }}"
                                    href="dashboard.admin.finance.paymentGateways.index"
                                    active-condition="{{ activeRoute('dashboard.admin.finance.paymentGateways.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Trial Features') }}"
                                    href="dashboard.admin.finance.free.feature"
                                    active-condition="{{ activeRoute('dashboard.admin.finance.free.*') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            @if ($setting->mobile_payment_active)
                                <x-navbar.dropdown.item>
                                    <x-navbar.dropdown.link
                                        label="{{ __('Mobile Payment') }}"
                                        href="dashboard.admin.finance.mobile.index"
                                        active-condition="{{ activeRoute('dashboard.admin.finance.mobile.index') }}"
                                    >
                                    </x-navbar.dropdown.link>
                                </x-navbar.dropdown.item>
                            @endif

                        </x-navbar.dropdown.dropdown>
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Pages') }}"
                            href="dashboard.page.list"
                            icon="tabler-file-description"
                            active-condition="{{ activeRoute('dashboard.page.*') }}"
                        />
                    </x-navbar.item>

                    {{-- <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('ChatBot') }}"
                            href="dashboard.chatbot.index"
                            icon="tabler-message-2-code"
                            active-condition="{{ activeRoute('dashboard.chatbot.*') }}"
                            new
                        />
                    </x-navbar.item> --}}

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Blog') }}"
                            href="dashboard.blog.list"
                            active-condition="{{ activeRoute('dashboard.blog.*') }}"
                            icon="tabler-pencil"
                        />
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Affiliates') }}"
                            href="dashboard.admin.affiliates.index"
                            active-condition="{{ activeRoute('dashboard.admin.affiliates.*') }}"
                            icon="tabler-brand-mastercard"
                        />
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Coupons') }}"
                            href="dashboard.admin.coupons.index"
                            active-condition="{{ activeRoute('dashboard.admin.coupons.*') }}"
                            icon="tabler-ticket"
                        />
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Email Templates') }}"
                            href="dashboard.email-templates.index"
                            active-condition="{{ activeRoute('dashboard.email-templates.*') }}"
                            icon="tabler-mail"
                        />
                    </x-navbar.item>

                    <x-navbar.item has-dropdown>
                        <x-navbar.link
                            label="{{ __('API Integration') }}"
                            href="dashboard.admin.settings.general"
                            icon="tabler-api"
                            active-condition="{{ activeRouteBulk('dashboard.admin.settings.*', 'elseyyid.translations.home', 'elseyyid.translations.lang') }}"
                            dropdown-trigger
                        />
                        <x-navbar.dropdown.dropdown open="{{ activeRouteBulk('dashboard.admin.settings.*', 'elseyyid.translations.home', 'elseyyid.translations.lang') }}">

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('OpenAI') }}"
                                    onclick="{{ $app_is_demo ? 'return toastr.info(\'This feature is disabled in Demo version.\')' : '' }}"
                                    href="{{ $app_is_demo ? '#' : 'dashboard.admin.settings.openai' }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Anthropic') }}"
                                    onclick="{{ $app_is_demo ? 'return toastr.info(\'This feature is disabled in Demo version.\')' : '' }}"
                                    href="{{ $app_is_demo ? '#' : 'dashboard.admin.settings.anthropic' }}"
                                    badge="{{ trans('Beta') }}"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Gemini') }}"
                                    onclick="{{ $app_is_demo ? 'return toastr.info(\'This feature is disabled in Demo version.\')' : '' }}"
                                    href="{{ $app_is_demo ? '#' : 'dashboard.admin.settings.gemini' }}"
                                    {{--                                    badge="{{ trans('Beta') }}" --}}
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('StableDiffusion') }}"
                                    href="dashboard.admin.settings.stablediffusion"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            @php
                                try {
                                    $files = File::files(resource_path('views/default/components/navbar/extapinavbars'));
                                } catch (\Throwable $th) {
                                    $files = [];
                                }
                            @endphp
                            @foreach ($files as $file)
                                @php
                                    $filenameWithoutExtension = substr($file->getFilename(), 0, -10);
                                @endphp
                                @include("components.navbar.extapinavbars.{$filenameWithoutExtension}")
                            @endforeach
                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Unsplash') }}"
                                    href="dashboard.admin.settings.unsplashapi"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Pexels') }}"
                                    href="dashboard.admin.settings.pexelsapi"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Pixabay') }}"
                                    href="dashboard.admin.settings.pixabayapi"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Serper') }}"
                                    href="dashboard.admin.settings.serperapi"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('TTS') }}"
                                    href="dashboard.admin.settings.tts"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                        </x-navbar.dropdown.dropdown>
                    </x-navbar.item>

                    <x-navbar.item has-dropdown>
                        <x-navbar.link
                            label="{{ __('Settings') }}"
                            href="dashboard.admin.settings.general"
                            icon="tabler-device-laptop"
                            active-condition="{{ activeRouteBulk('dashboard.admin.settings.*', 'elseyyid.translations.home', 'elseyyid.translations.lang') }}"
                            dropdown-trigger
                        />
                        <x-navbar.dropdown.dropdown open="{{ activeRouteBulk('dashboard.admin.settings.*', 'elseyyid.translations.home', 'elseyyid.translations.lang') }}">

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('General') }}"
                                    href="dashboard.admin.settings.general"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Invoice') }}"
                                    href="dashboard.admin.settings.invoice"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            @php
                                try {
                                    $files = File::files(resource_path('views/default/components/navbar/extsettingnavbars'));
                                } catch (\Throwable $th) {
                                    $files = [];
                                }
                            @endphp
                            @foreach ($files as $file)
                                @php
                                    $filenameWithoutExtension = substr($file->getFilename(), 0, -10);
                                @endphp
                                @include("components.navbar.extsettingnavbars.{$filenameWithoutExtension}")
                            @endforeach

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Affiliate') }}"
                                    href="dashboard.admin.settings.affiliate"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Thumbnail System') }}"
                                    href="dashboard.admin.settings.thumbnail"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('SMTP') }}"
                                    href="dashboard.admin.settings.smtp"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('GDPR') }}"
                                    href="dashboard.admin.settings.gdpr"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Privacy Policy and Terms') }}"
                                    href="dashboard.admin.settings.privacy"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Languages') }}"
                                    href="elseyyid.translations.home"
                                    active-condition="{{ activeRoute('elseyyid.translations.home') }} {{ activeRoute('elseyyid.translations.lang') }}"
                                    localize-href
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                            <x-navbar.dropdown.item>
                                <x-navbar.dropdown.link
                                    label="{{ __('Storage') }}"
                                    href="dashboard.admin.settings.storage"
                                >
                                </x-navbar.dropdown.link>
                            </x-navbar.dropdown.item>

                        </x-navbar.dropdown.dropdown>
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('Site Health') }}"
                            href="dashboard.admin.health.index"
                            icon="tabler-activity-heartbeat"
                        >
                        </x-navbar.link>
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            label="{{ __('License') }}"
                            href="dashboard.admin.license.index"
                            icon="tabler-checklist"
                        >
                        </x-navbar.link>
                    </x-navbar.item>

                    <x-navbar.item>
                        <x-navbar.link
                            class="nav-link--update"
                            label="{{ __('Update') }}"
                            href="dashboard.admin.update.index"
                            icon="tabler-refresh"
                        >
                        </x-navbar.link>
                    </x-navbar.item>

                    @if ($app_is_not_demo)
                        <x-navbar.item>
                            <x-navbar.link
                                label="{{ __('Premium Support') }}"
                                href="#"
                                icon="tabler-diamond"
                                trigger-type="modal"
                            >
                                <x-slot:modal>
                                    @includeIf('premium-support.index')
                                </x-slot:modal>
                            </x-navbar.link>
                        </x-navbar.item>
                    @endif
                @endif

                <x-navbar.item>
                    <x-navbar.divider />
                </x-navbar.item>

                <x-navbar.item class="group-[&.navbar-shrinked]/body:hidden">
                    <x-navbar.label>
                        {{ __('Credits') }}
                    </x-navbar.label>
                </x-navbar.item>

                <x-navbar.item class="pb-navbar-link-pb pe-navbar-link-pe ps-navbar-link-ps pt-navbar-link-pt group-[&.navbar-shrinked]/body:hidden">
                    <div class="lqd-remaining-credit flex flex-col gap-2 text-2xs">
                        <div class="lqd-legend flex items-center gap-2.5">
                            <span class="lqd-legend-md size-2.5 lqd-legend-box shrink-0 grow-0 rounded-full bg-primary"></span>
                            <span class="lqd-legend-label">
                                {{ __('Ideas') }}
                            </span>
                            <span class="ms-auto font-medium">
                                {{ Auth::user()->projects()->count() .' / ' . number_format((int) Auth::user()->max_ideas)  }}
                            </span>
                        </div>
                        
                        <div class="lqd-progress flex overflow-hidden rounded-full h-2" style="background-color: rgba(0, 0, 0, 0.05)">
                            <div class="lqd-progress-bar shrink-0 grow-0 basis-auto bg-primary" 
                            style="width: {{ (100 * (int) Auth::user()->projects()->count()) / (int) Auth::user()->max_ideas }}%"></div>
                        </div>
                    </div>
                </x-navbar.item>

                @if ($setting->feature_affilates)
                    <x-navbar.item class="group-[&.navbar-shrinked]/body:hidden">
                        <x-navbar.divider />
                    </x-navbar.item>

                    <x-navbar.item class="group-[&.navbar-shrinked]/body:hidden">
                        <x-navbar.label>
                            {{ __('Affiliation') }}
                        </x-navbar.label>
                    </x-navbar.item>

                    <x-navbar.item class="pb-navbar-link-pb pe-navbar-link-pe ps-navbar-link-ps pt-navbar-link-pt group-[&.navbar-shrinked]/body:hidden">
                        <div
                            class="lqd-navbar-affiliation inline-block w-full rounded-xl border border-navbar-divider px-8 py-4 text-center text-2xs leading-tight transition-border">
                            <p class="m-0 mb-2 text-[20px] not-italic">🎁</p>
                            <p class="mb-4">{{ __('Invite your friend and get') }}
                                {{ $setting->affiliate_commission_percentage }}%
                                {{ __('on all their purchases.') }}
                            </p>
                            <x-button
                                class="text-3xs"
                                href="{{ route('dashboard.user.affiliates.index') }}"
                                variant="ghost-shadow"
                            >
                                {{ __('Invite') }}
                            </x-button>
                        </div>
                    </x-navbar.item>
                @endif
                <x-navbar.item>
                    <x-navbar.divider />
                </x-navbar.item>
                
                <x-navbar.item class="p-btn-1 pb-navbar-link-pb pe-navbar-link-pe ps-navbar-link-ps pt-navbar-link-pt group-[&.navbar-shrinked]/body:hidden"
                >
                    <div
                        class="lqd-navbar-affiliation inline-block w-full rounded-xl focus:text-neutral-900  border border-navbar-divider px-8 py-4 text-center text-2xs leading-tight transition-border"
                        style="cursor: pointer"
                        id="toggle-idea-popup">
                        <div class="flex items-center w-full"
                        >
                          @if (getProjectInfo())
                            <span class="truncate whitespace-nowrap">{{ getProjectInfo()->name }}</span
                          >
                          @endif
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            class="ml-auto h-4 w-4"
                          >
                            <path d="m7 15 5 5 5-5"></path>
                            <path d="m7 9 5-5 5 5"></path>
                          </svg>
                        </div>
                    </div>
                    <div
                      id="idea-popup"
                      data-radix-popper-content-wrapper=""
                      dir="ltr"
                      class="truncate hidden"
                      style="
                        position: fixed;    
                        left: 5px;
                        bottom: 75px;
                        width: 240px;
                        transform: translate(7.5px, 7px);
                        min-width: max-content;
                        --radix-popper-transform-origin: 50% 414px;
                        will-change: transform;
                        z-index: 50;
                        --radix-popper-available-width: 768px;
                        --radix-popper-available-height: 733px;
                        --radix-popper-anchor-width: 239px;
                        --radix-popper-anchor-height: 50px;
                      "
                    >
                      <div
                        data-side="top"
                        data-align="center"
                        role="menu"
                        aria-orientation="vertical"
                        data-state="open"
                        data-radix-menu-content=""
                        dir="ltr"
                        id="radix-:R2iumlaH1:"
                        aria-labelledby="radix-:R2iumla:"
                        class="z-50 min-w-[8rem] overflow-hidden rounded-md border border-neutral-200 bg-white p-1 text-neutral-950 shadow-md data-[state=open]:animate-in data-[state=closed]:animate-out data-[state=closed]:fade-out-0 data-[state=open]:fade-in-0 data-[state=closed]:zoom-out-95 data-[state=open]:zoom-in-95 data-[side=bottom]:slide-in-from-top-2 data-[side=left]:slide-in-from-right-2 data-[side=right]:slide-in-from-left-2 data-[side=top]:slide-in-from-bottom-2 dark:border-neutral-800 dark:bg-neutral-950 dark:text-neutral-50 w-[240px]"
                        tabindex="-1"
                        data-orientation="vertical"
                        style="
                          outline: none;
                          --radix-dropdown-menu-content-transform-origin: var(
                            --radix-popper-transform-origin
                          );
                          --radix-dropdown-menu-content-available-width: var(
                            --radix-popper-available-width
                          );
                          --radix-dropdown-menu-content-available-height: var(
                            --radix-popper-available-height
                          );
                          --radix-dropdown-menu-trigger-width: var(--radix-popper-anchor-width);
                          --radix-dropdown-menu-trigger-height: var(--radix-popper-anchor-height);
                          pointer-events: auto;
                        "
                      >
                        
                        <div class="px-2 py-1.5 font-medium text-xs text-muted-foreground">
                          {{ __('Your Ideas') }}
                        </div>
                        <div class="max-h-[200px] overflow-y-auto">
                            @php
                                $ideas = Auth::user()->projects()->orderBy('created_at', 'desc')->paginate(10);
                                $idea_id = request()->input('idea_id');
                            @endphp
                            @if ($ideas->count() > 0)
                                @foreach ($ideas as $entry)
                              <div
                                class="relative truncate whitespace-nowrap flex cursor-default select-none items-center rounded-sm px-2 py-1.5 text-sm outline-none transition-colors hover:bg-neutral-100 focus:text-neutral-900 data-[disabled]:pointer-events-none data-[disabled]:opacity-50 p-8 group false"
                                style="width: 230px; cursor: pointer"
                                onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.show', ['project' => $entry->id])) }}')"
                              >
                                 @if (!empty(getProjectInfo()) && getProjectInfo()->id == $entry->id)
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      stroke="currentColor"
                                      stroke-width="2"
                                      stroke-linecap="round"
                                      stroke-linejoin="round"
                                      class="mr-2 h-4 w-4 shrink-0 opacity-100"
                                    >
                                      <polyline points="20 6 9 17 4 12"></polyline></svg
                                    >
                                 @else 
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="24"
                                      height="24"
                                      style="visibility: hidden"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      stroke="currentColor"
                                      stroke-width="2"
                                      stroke-linecap="round"
                                      stroke-linejoin="round"
                                      class="mr-2 h-4 w-4 shrink-0 opacity-100"
                                    >
                                      <polyline points="20 6 9 17 4 12"></polyline></svg
                                    >
                                 @endif
                                <span class="truncate">{{ $entry->name }}</span>
                                <div class="ml-auto flex items-center">
                                    @if (!empty(getProjectInfo()) && getProjectInfo()->id != $entry->id)
                                        <button
                                        class="inline-flex items-center justify-center rounded-md text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 w-6 h-6 invisible group-hover:visible text-neutral-400 false false group"
                                        type="button"
                                        aria-haspopup="dialog"
                                        aria-expanded="false"
                                        aria-controls="radix-:r20b:"
                                        data-state="closed"
                                        onclick="
                                        event.preventDefault();
                                        return deleteIdea('{{ route('dashboard.user.openai.projects.destroy', $entry->id) }}');"
                                      >
                                        <svg
                                          xmlns="http://www.w3.org/2000/svg"
                                          width="24"
                                          height="24"
                                          viewBox="0 0 24 24"
                                          fill="none"
                                          stroke="currentColor"
                                          stroke-width="2"
                                          stroke-linecap="round"
                                          stroke-linejoin="round"
                                          class="w-4"
                                        >
                                          <path d="M3 6h18"></path>
                                          <path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"></path>
                                          <path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"></path>
                                        </svg>
                                      </button>
                                  @endif
                                </div>
                              </div>
                                @endforeach
                            @endif
                        </div>
                        <div
                          role="separator"
                          aria-orientation="horizontal"
                          class="-mx-1 my-1 h-px bg-neutral-100 dark:bg-neutral-800"
                        ></div>
                        <div
                          role="menuitem"
                          class="relative flex cursor-pointer select-none focus:text-neutral-900 items-center rounded-sm px-2 py-1.5 text-sm outline-none transition-colors focus:bg-neutral-100 data-[disabled]:pointer-events-none data-[disabled]:opacity-50 dark:focus:bg-neutral-800 dark:focus:text-neutral-50"
                          tabindex="-1"
                          data-orientation="vertical"
                          data-radix-collection-item=""
                          style="cursor: pointer"
                          data-bs-toggle="modal"
                          data-bs-target="#creatSubmitIdeaModal"
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            class="mr-2 h-4 w-4"
                          >
                            <circle cx="12" cy="12" r="10"></circle>
                            <path d="M8 12h8"></path>
                            <path d="M12 8v8"></path></svg
                          >{{ __('New Idea') }}
                        </div>
                      </div>
                    </div>

                </x-navbar.item>
                
            </ul>
        </nav>
    </div>
</aside>
<div class="modal fade" style="display: none" id="creatSubmitIdeaModal" tabindex="-1" aria-labelledby="creatSubmitIdeaLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form id="ideaForm" action="{{ LaravelLocalization::localizeUrl( route('dashboard.user.openai.projects.store')) }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="creatSubmitIdeaLabel">{{ __('Submit a New Idea') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="step1">
                        <label for="name" class="form-label">{{ __('Idea Title') }}</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('Give your idea a title') }}" required>
                    </div>
                    <div id="step2" style="display:none;">
                        <label class="form-label">{{ __('Idea Description') }}</label>
                        <textarea class="form-control" id="description" name="description" rows="12" placeholder="{{ __('Comprehensively explain your idea') }}" maxlength="1000" required="required"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="prevBtn" style="display:none;" onclick="prevStep()">{{ __('Previous') }}</button>
                    <button type="button" class="btn btn-primary" id="nextBtn" onclick="nextStep()">{{ __('Next') }}</button>
                    <button type="submit" class="btn btn-primary" id="submitBtn" style="display:none;">{{ __('Submit') }}</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('Cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
