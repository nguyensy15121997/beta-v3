@extends('panel.layout.app')
@section('title', __('Analytics'))
@section('titlebar_actions')
@endsection
@section('content')
<!-- Page body -->
<div class="page-body pt-5">
	<div class="container-xl">
	    <div class="row row-deck row-cards max-xl:[--tblr-gutter-y:1.5rem]">
			<div class="col-sm-6 col-xl-3">
				<div class="card card-sm">
					<div class="card-body">
						<div class="row align-items-center">
							<div class="col-auto">
								<span class="avatar bg-white dark:!bg-[rgba(255,255,255,0.05)]">
									<svg class="svgusermanage" xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 24 24" stroke="var(--lqd-vertical-nav-link-color)" stroke-width="1.5" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path d="M9 7m-4 0a4 4 0 1 0 8 0a4 4 0 1 0 -8 0"></path>
                                        <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                        <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
                                    </svg>
								</span>
							</div>
							<div class="col">
								<p class="font-weight-medium mb-1">
									{{__('Users')}}
								</p>
								<h3 class="text-[20px] mb-0 flex items-center">
									{{ $data['user_count'] }}
                                </h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-xl-3">
				<div class="card card-sm">
					<div class="card-body">
						<div class="row align-items-center">
							<div class="col-auto">
								<span class="avatar bg-white dark:!bg-[rgba(255,255,255,0.05)]">
								    <svg class="svgblog" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke="var(--lqd-vertical-nav-link-color)" stroke-width="1.5" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                        <path d="M4 20h4l10.5 -10.5a1.5 1.5 0 0 0 -4 -4l-10.5 10.5v4"></path>
                                        <path d="M13.5 6.5l4 4"></path>
                                    </svg>
								</span>
							</div>
							<div class="col">
								<p class="font-weight-medium mb-1">
									{{__('Total Ideas')}}
								</p>
								<h3 class="text-[20px] mb-0 flex items-center">
									{{ $data['idea_count'] }}
                                </h3>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="col-sm-6 col-xl-3">
				<div class="card card-sm">
					<div class="card-body">
						<div class="row align-items-center">
							<div class="col-auto flex items-center">
								<span class="avatar bg-white dark:!bg-[rgba(255,255,255,0.05)]">
    							    <svg class="svgaichat" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke="var(--lqd-vertical-nav-link-color)" stroke-width="1.5" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path d="M3 20l1.3 -3.9a9 8 0 1 1 3.4 2.9l-4.7 1"></path>
                                        <path d="M12 12l0 .01"></path>
                                        <path d="M8 12l0 .01"></path>
                                        <path d="M16 12l0 .01"></path>
                                    </svg>
                                </span>
							</div>
							<div class="col">
								<p class="font-weight-medium mb-1">
									{{__('Fikra Experts Chats')}}
								</p>
								<h3 class="text-[20px] mb-0 flex items-center">
									{{ $data['chat_count'] }}
								</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-xl-3"></div>
			<div class="col-md-6">
				<div class="card"">
					<div class="card-header">
						<h3 class="card-title text-heading">{{__('Concept Tools')}}</h3>
					</div>
					<div class="card-table table-responsive grow">
						<table class="table table-vcenter">
							<thead>
								<tr>
									<th>{{__('Tools Name')}}</th>
									<th>{{__('Total')}}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ __('SWOT Analysis') }}</td>
									<td>{{ $data['swot_analyses_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('Lean Canvas') }}</td>
									<td>{{ $data['lean_canvases'] }}</td>
								</tr>
								<tr>
									<td>{{ __('User Personas') }}</td>
									<td>{{ $data['user_personas_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('User Stories') }}</td>
									<td>{{ $data['user_stories_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('Competitive Analysis') }}</td>
									<td>{{ $data['competitive_analysis_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('Storytelling') }}</td>
									<td>{{ $data['storytelling_count'] }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title text-heading">{{__('Marketing Tools')}}</h3>
					</div>
					<div class="card-table table-responsive grow">
						<table class="table table-vcenter">
							<thead>
								<tr>
									<th>{{ __('Marketing Name') }}</th>
									<th>{{ __('Total') }}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ __('Article Generator') }}</td>
									<td>{{ $data['article_count'] }}</td>
								</tr>
								<tr>
									<td>{{ __('Blog Post Ideas') }}</td>
									<td>{{ $data['blog_idea_count'] }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
    <script>
        
    </script>
@endsection
