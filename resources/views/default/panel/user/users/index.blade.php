@extends('panel.layout.app')
@section('title', __('Users'))
@section('titlebar_actions')
@endsection
@section('content')
<!-- Page body -->
<div class="page-body pt-5">
	<div class="container-xl">
            <div class="card">
                <div id="table-default" class="card-table table-responsive">
                    <table id="userTable" class="table">
                        <thead>
                            <tr>
                                <th><button class="table-sort" data-sort="sort-name">{{ __('Name') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-email">{{ __('Email') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-ideas">{{ __('Ideas') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-remaining-words">{{ __('Remaining Words') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-date">{{ __('Created At') }}</button></th>
                                <th><button class="table-sort" data-sort="sort-last-login">{{ __('Last Login') }}</th>
                            </tr>
                        </thead>
                        <tbody class="table-tbody align-middle text-heading">
                            @foreach ($data as $entry)
                                <tr>
                                    <td class="sort-name">{{ $entry->fullName() }}</td>
                                    <td class="sort-email">{{ $entry->email }}</td>
                                    <td class="sort-ideas">{{ $entry->projects()->count() }}</td>
                                    <td class="sort-remaining-words">{{ $entry->remaining_words }}</td>
                                    <td class="sort-date" data-date="{{ strtotime($entry->created_at) }}">
                                        <p class="m-0">{{ date('j.n.Y', strtotime($entry->created_at)) }}</p>
                                        <p class="m-0 text-muted">{{ date('H:i:s', strtotime($entry->created_at)) }}
                                        </p>
                                    </td>
                                    <td class="sort-last-login" data-last-login="{{ strtotime($entry->last_login ?? $entry->created_at) }}">
                                        <p class="m-0">{{ date('j.n.Y', strtotime($entry->last_login ?? $entry->created_at)) }}</p>
                                        <p class="m-0 text-muted">{{ date('H:i:s', strtotime($entry->last_login ?? $entry->created_at)) }}
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                    <div
                        class="py-2 flex items-center border-solid border-t border-r-0 border-b-0 border-l-0 border-[--tblr-border-color] px-[--tblr-card-cap-padding-x] py-[--tblr-card-cap-padding-y] [&_.rounded-md]:rounded-full">
                        <div class="m-0 ms-auto p-0">{{ $data->links() }}</div>
                    </div>
                </div>
            </div>
	</div>
</div>
@endsection
@push('script')
    <script src="{{ custom_theme_url('/assets/js/panel/user-analytic.js') }}"></script>
@endpush

