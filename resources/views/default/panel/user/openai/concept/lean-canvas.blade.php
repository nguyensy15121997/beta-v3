<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <form action="{{ route('projects.generate-lean-canvas', ['project' => $project->id]) }}" method="POST"
        id="generate-leancanvas-form" style="display: none;">
        @csrf
    </form>


    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row draggable h-full" id="0">
                @if ($project)
                    <div id="leanCanvas-body" class="mt-3 draggable-item col-12 {{ is_null($leanCanvas) ? 'blur-content loading' : '' }}">
                        <div class="overlay">
                            <div class="cv-spinner">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div class="{{ is_null($leanCanvas) ? 'blur-content-card' : '' }} mt-0">
                            <div class="card-header d-block px-2rem">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center justify-center">
                                        <h2 class="mb-0">{{ __('Lean Canvas') }}</h2>
                                        <img src="{{ url('').'/images/info-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                                    </div>
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'leanCanvas')"
                                            class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-1 p-sm-4" id="leanCanvas-content">
                                <main class="lean-canvas">
                                    <section class="canvas-block position-relative" id="problem">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->problem)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="problem" data-name="Problem"
                                                    data-value="{{ $leanCanvas->problem }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/problem-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Problem') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-problem">
                                            {!! $leanCanvas->problem ?? 'Not generated.' !!}
                                        </p>
                                    </section>
                                    <section class="canvas-block position-relative" id="solution">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->solution)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="solution" data-name="Solution"
                                                    data-value="{{ $leanCanvas->solution }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/solution-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Solution') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-solution">{!! $leanCanvas->solution ?? 'Not generated.' !!}
                                        </p>
                                    </section>
                                    <section class="canvas-block position-relative" id="unique-value-prop">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->unique_value_proposition)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="unique_value_proposition" data-name="Unique Value Prop."
                                                    data-value="{{ $leanCanvas->unique_value_proposition }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/problem-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Value Proposition') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-unique_value_proposition">
                                            {!! $leanCanvas->unique_value_proposition ?? 'Not generated.' !!}</p>
                                    </section>
                                    <section class="canvas-block position-relative" id="unfair-advantage">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->unfair_advantage)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="unfair_advantage" data-name="Unfair Advantage"
                                                    data-value="{{ $leanCanvas->unfair_advantage }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/unfair-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Unfair Advantage') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-unfair_advantage">
                                            {!! $leanCanvas->unfair_advantage ?? 'Not generated.' !!}</p>
                                    </section>
                                    <section class="canvas-block position-relative" id="customer-segments">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->customer_segments)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="customer_segments" data-name="Customer Segments"
                                                    data-value="{{ $leanCanvas->customer_segments }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/customer-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Customer Segments') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-customer_segments">
                                            {!! $leanCanvas->customer_segments ?? 'Not generated.' !!}</p>
                                    </section>
                                    <section class="canvas-block position-relative" id="existing-alternatives">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->existing_alternatives)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="existing_alternatives" data-name="Existing Alternatives"
                                                    data-value="{{ $leanCanvas->existing_alternatives }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/existing-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Existing Alternatives') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-existing_alternatives">
                                            {!! $leanCanvas->existing_alternatives ?? 'Not generated.' !!}</p>
                                    </section>
                                    <section class="canvas-block position-relative" id="key-metrics">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->key_metrics)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="key_metrics" data-name="Key Metrics"
                                                    data-value="{{ $leanCanvas->key_metrics }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/metrics-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Key Metrics') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-key_metrics">
                                            {!! $leanCanvas->key_metrics ?? 'Not generated.' !!}
                                        </p>
                                    </section>
                                    <section class="canvas-block position-relative" id="channels">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->channels)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="channels" data-name="Channels"
                                                    data-value="{{ $leanCanvas->channels }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/channel-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Channels') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-channels">{!! $leanCanvas->channels ?? 'Not generated.' !!}
                                        </p>
                                    </section>
                                    <section class="canvas-block position-relative" id="cost-structure">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->cost_structure)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="cost_structure" data-name="Cost Structure"
                                                    data-value="{{ $leanCanvas->cost_structure }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/cost-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Cost Structure') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-cost_structure">
                                            {!! $leanCanvas->cost_structure ?? 'Not generated.' !!}</p>
                                    </section>
                                    <section class="canvas-block position-relative" id="revenue-streams">
                                        @if ($leanCanvas)
                                            @if ($leanCanvas->revenue_streams)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $leanCanvas->id }}" data-type="leanCanvas"
                                                    data-key="revenue_streams" data-name="Revenue Streams"
                                                    data-value="{{ $leanCanvas->revenue_streams }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/revenue-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Revenue Streams') }}</h2>
                                        <p class="white-space-pre-line" id="leanCanvas-revenue_streams">
                                            {!! $leanCanvas->revenue_streams ?? 'Not generated.' !!}</p>
                                    </section>
                                </main>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>