<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>

    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row draggable h-full" id="0">
                @if ($project)
                    <div id="industryOverview-body" class="mt-3 draggable-item col-12 {{ is_null($industryOverview) ? 'blur-content loading' : '' }}">
                        <div class="overlay">
                            <div class="cv-spinner">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div class="{{ is_null($industryOverview) ? 'blur-content-card' : '' }} mt-0">
                            <div class="card-header d-block px-2rem">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <h2 class="mb-0">{{ __('Industry Overview') }}</h2>
                                    </div>
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'industryOverview')"
                                            class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                        </button>
                                    </div>
                                    <!--<div>-->
                                    <!--    <button onclick="regenerateProjectInfo({{ $project->id }}, 'industryOverview')"-->
                                    <!--        class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>-->
                                    <!--    <button onclick="collapseByType('industryOverview', 'minius')"-->
                                    <!--        id="industryOverview-minius" class="btn btn-success btn-sm"><i-->
                                    <!--            class="ti ti-minus"></i></button>-->
                                    <!--    <button onclick="collapseByType('industryOverview', 'plus')" id="industryOverview-plus"-->
                                    <!--        style="display: none" class="btn btn-success btn-sm"><i-->
                                    <!--            class="ti ti-plus"></i></button>-->
                                    <!--    <button-->
                                    <!--        onclick="deleteProjectInfo({{ $project->id }}, 'industryOverview', '#industryOverview-body')"-->
                                    <!--        class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>-->
                                    <!--</div>-->

                                </div>
                            </div>
                            @if (!empty($industryOverview))
                                <div class="card-body p-1 p-sm-4" id="industryOverview-content">
                                    <div class="w-full grid grid-cols-3 gap-4  false">
                                        <div class="flex flex-col items-start justify-start bg-industry-box rounded-lg  p-4">
                                            <div class="text-lg sm:text-2xl font-medium text-left text-neutral-900 mb-2 position-relative">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="value_relate_highlight_1" data-name="value"
                                                            data-value="{{ $industryOverview->value_relate_highlight_1 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="industryOverview-value_relate_highlight_1">{{ $industryOverview->value_relate_highlight_1 }}</p>
                                            </div>
                                            <div class="w-full text-sm sm:text-base text-muted-foreground false position-relative">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="highlight_name_1" data-name="name"
                                                            data-value="{{ $industryOverview->highlight_name_1 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="industryOverview-highlight_name_1">{{ $industryOverview->highlight_name_1 }}</p>
                                            </div>
                                        </div>
                                        <div class="flex flex-col items-start justify-start bg-industry-box rounded-lg  p-4">
                                            <div class="text-lg sm:text-2xl font-medium text-left text-neutral-900 mb-2 position-relative">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="value_relate_highlight_2" data-name="value"
                                                            data-value="{{ $industryOverview->value_relate_highlight_2 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="industryOverview-value_relate_highlight_2">{{ $industryOverview->value_relate_highlight_2 }}</p>
                                            </div>
                                            <div class="w-full text-sm sm:text-base text-muted-foreground false position-relative">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="highlight_name_2" data-name="name"
                                                            data-value="{{ $industryOverview->highlight_name_2 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="industryOverview-highlight_name_2">{{ $industryOverview->highlight_name_2 }}</p>
                                            </div>
                                        </div>
                                        <div class="flex flex-col items-start justify-start bg-industry-box rounded-lg  p-4">
                                            <div class="text-lg sm:text-2xl font-medium text-left text-neutral-900 mb-2 position-relative">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="value_relate_highlight_3" data-name="value"
                                                            data-value="{{ $industryOverview->value_relate_highlight_3 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="industryOverview-value_relate_highlight_3">{{ $industryOverview->value_relate_highlight_3 }}</p>
                                            </div>
                                            <div class="w-full text-sm sm:text-base text-muted-foreground false position-relative">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="highlight_name_3" data-name="name"
                                                            data-value="{{ $industryOverview->highlight_name_3 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="industryOverview-highlight_name_3">{{ $industryOverview->highlight_name_3 }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="mt-3">{{ __('Industry Overview') }}</h2>
                                    <div class="position-relative w-full">
                                        <div class="edit-popup position-absolute position-edit-5-0"
                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                            data-key="description" data-name="description"
                                            data-value="{{ $industryOverview->description }}"
                                             role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                        </div>
                                        <p id="industryOverview-description">{{ $industryOverview->description }}</p>
                                    </div>
                                    <h3 class="mt-3">{{ __('Market Trends') }}</h3>
                                    <ul class="list-circles list-outside leading-3 mt-2 px-3 tight" data-tight="true">
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="market_trends_title_1" data-name="title"
                                                    data-value="{{ $industryOverview->market_trends_title_1 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-market_trends_title_1">{{ $industryOverview->market_trends_title_1 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="market_trends_description_1" data-name="description"
                                                            data-value="{{ $industryOverview->market_trends_description_1 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-market_trends_description_1">{{ $industryOverview->market_trends_description_1 }}</span>
                                            </div>
                                        </li>
                                        
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="market_trends_title_2" data-name="title"
                                                    data-value="{{ $industryOverview->market_trends_title_2 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-market_trends_title_2">{{ $industryOverview->market_trends_title_2 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="market_trends_description_2" data-name="description"
                                                            data-value="{{ $industryOverview->market_trends_description_2 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-market_trends_description_2">{{ $industryOverview->market_trends_description_2 }}</span>
                                            </div>
                                        </li>
                                        
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="market_trends_title_3" data-name="title"
                                                    data-value="{{ $industryOverview->market_trends_title_3 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-market_trends_title_3">{{ $industryOverview->market_trends_title_3 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="market_trends_description_3" data-name="description"
                                                            data-value="{{ $industryOverview->market_trends_description_3 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-market_trends_description_3">{{ $industryOverview->market_trends_description_3 }}</span>
                                            </div>
                                        </li>
                                        
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="market_trends_title_4" data-name="title"
                                                    data-value="{{ $industryOverview->market_trends_title_4 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-market_trends_title_4">{{ $industryOverview->market_trends_title_4 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="market_trends_description_4" data-name="description"
                                                            data-value="{{ $industryOverview->market_trends_description_4 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-market_trends_description_4">{{ $industryOverview->market_trends_description_4 }}</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <h3 class="mt-3">{{ __('Competitive Landscape') }}</h3>
                                    <ul class="list-circles list-outside leading-3 mt-2 px-3 tight" data-tight="true">
                                        
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="competitive_landscape_title_1" data-name="title"
                                                    data-value="{{ $industryOverview->competitive_landscape_title_1 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-competitive_landscape_title_1">{{ $industryOverview->competitive_landscape_title_1 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="competitive_landscape_description_1" data-name="description"
                                                            data-value="{{ $industryOverview->competitive_landscape_description_1 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-competitive_landscape_description_1">{{ $industryOverview->competitive_landscape_description_1 }}</span>
                                            </div>
                                        </li>
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="competitive_landscape_title_2" data-name="title"
                                                    data-value="{{ $industryOverview->competitive_landscape_title_2 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-competitive_landscape_title_2">{{ $industryOverview->competitive_landscape_title_2 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="competitive_landscape_description_2" data-name="description"
                                                            data-value="{{ $industryOverview->competitive_landscape_description_2 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-competitive_landscape_description_2">{{ $industryOverview->competitive_landscape_description_2 }}</span>
                                            </div>
                                        </li>
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="competitive_landscape_title_3" data-name="title"
                                                    data-value="{{ $industryOverview->competitive_landscape_title_3 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-competitive_landscape_title_3">{{ $industryOverview->competitive_landscape_title_3 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="competitive_landscape_description_3" data-name="description"
                                                            data-value="{{ $industryOverview->competitive_landscape_description_3 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-competitive_landscape_description_3">{{ $industryOverview->competitive_landscape_description_3 }}</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <h3 class="mt-3">{{ __('Challenges') }}</h3>
                                    <ul class="list-circles list-outside leading-3 mt-2 px-3 tight" data-tight="true">
                                        
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="challenges_title_1" data-name="title"
                                                    data-value="{{ $industryOverview->challenges_title_1 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-challenges_title_1">{{ $industryOverview->challenges_title_1 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="challenges_description_1" data-name="description"
                                                            data-value="{{ $industryOverview->challenges_description_1 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-challenges_description_1">{{ $industryOverview->challenges_description_1 }}</span>
                                            </div>
                                        </li>
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="challenges_title_2" data-name="description"
                                                    data-value="{{ $industryOverview->challenges_title_2 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-challenges_title_2">{{ $industryOverview->challenges_title_2 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="challenges_description_2" data-name="description"
                                                            data-value="{{ $industryOverview->challenges_description_2 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-challenges_description_2">{{ $industryOverview->challenges_description_2 }}</span>
                                            </div>
                                        </li>
                                        <li class="leading-normal mt-2">
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                    data-key="challenges_title_3" data-name="title"
                                                    data-value="{{ $industryOverview->challenges_title_3 }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <strong id="industryOverview-challenges_title_3">{{ $industryOverview->challenges_title_3 }}
                                                </strong>:
                                            </div>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                                            data-key="challenges_description_3" data-name="description"
                                                            data-value="{{ $industryOverview->challenges_description_3 }}"
                                                             role="button"><i
                                                                class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <span id="industryOverview-challenges_description_3">{{ $industryOverview->challenges_description_3 }}</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="position-relative w-full">
                                        <div class="edit-popup position-absolute position-edit-5-0"
                                            data-id="{{ $industryOverview->id }}" data-type="industryOverview"
                                            data-key="epilogue" data-name="epilogue"
                                            data-value="{{ $industryOverview->epilogue }}"
                                             role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                        </div>
                                        <p id="industryOverview-epilogue">{{ $industryOverview->epilogue }}</p>
                                    </div>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>