<!--<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">-->
<!--    <button-->
<!--        onclick="toggleEditButton()"-->
<!--        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"-->
<!--        type="button">-->
<!--        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">-->
<!--        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>-->
<!--    </svg>-->
<!--    </button>-->
<!--</div>-->
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>

    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row h-full" id="0">
                @if ($project)
                    <div id="marketSize-body" class="mt-3 col-12 {{ is_null($marketSize) ? 'blur-content loading' : '' }}">
                        <div class="overlay">
                            <div class="cv-spinner">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div class="{{ is_null($marketSize) ? 'blur-content-card' : '' }} mt-0">
                            <div class="card-header d-block px-2rem">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center justify-center">
                                        <h2 class="mb-0">{{ __('Market Size and Trends') }}</h2>
                                        <img src="{{ url('').'/images/info-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                                    </div>
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'marketSize')"
                                            class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                        </button>
                                    </div>
                                    <!--<div>-->
                                    <!--    <button onclick="regenerateProjectInfo({{ $project->id }}, 'marketSize')"-->
                                    <!--        class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>-->
                                    <!--    <button onclick="collapseByType('marketSize', 'minius')"-->
                                    <!--        id="marketSize-minius" class="btn btn-success btn-sm"><i-->
                                    <!--            class="ti ti-minus"></i></button>-->
                                    <!--    <button onclick="collapseByType('marketSize', 'plus')" id="marketSize-plus"-->
                                    <!--        style="display: none" class="btn btn-success btn-sm"><i-->
                                    <!--            class="ti ti-plus"></i></button>-->
                                    <!--    <button-->
                                    <!--        onclick="deleteProjectInfo({{ $project->id }}, 'marketSize', '#marketSize-body')"-->
                                    <!--        class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>-->
                                    <!--</div>-->

                                </div>
                            </div>
                            @if (!empty($marketSize))
                                <div class="card p-1 p-sm-4" id="marketSize-content">
                                   <h3 class="mb-3">{{ __('Market Size') }}</h3>
                                   <p class="mb-3">
                                       {{ $marketSize->description }}
                                   </p>
                                   <div class="row">
                                       <div class="col-12 col-sm-4 d-flex flex-column justify-content-center align-items-center">
                                           <div class="d-flex flex-column justify-content-center align-items-center">
                                               <img src="{{ url('').'/images/som-bg.png' }}" class="mx-2 w-2/4" alt="info-icon">
                                               <h1 class="absolute" style="color: white">{{ $marketSize->total_addressable_market_value }}</h1>
                                           </div>
                                       </div>
                                       <div class="col-12 col-sm-4 d-flex lg-show mb-4 flex-column justify-content-center align-items-center">
                                           <div class="mt-3 text-center">
                                               <h1>{{ __('SOM') }}</h1>
                                               <p>{{ $marketSize->total_addressable_market_title }}</p>
                                           </div>
                                       </div>
                                       <div class="col-12 col-sm-4 d-flex flex-column justify-content-center align-items-center">
                                           <div class="d-flex flex-column justify-content-center align-items-center">
                                               <img src="{{ url('').'/images/sam-bg.png' }}" class="mx-2 w-3/5" alt="info-icon">
                                               <h1 class="absolute">{{ $marketSize->total_servicable_market_value }}</h1>
                                           </div>
                                       </div>
                                       <div class="col-12 col-sm-4 d-flex lg-show mb-4 flex-column justify-content-center align-items-center">
                                           <div class="mt-3 text-center">
                                               <h1>{{ __('SAM') }}</h1>
                                               <p>{{ $marketSize->total_servicable_market_title }}</p>
                                           </div>
                                       </div>
                                       <div class="col-12 col-sm-4 d-flex flex-column justify-content-center align-items-center">
                                           <div class="d-flex flex-column justify-content-center align-items-center">
                                               <img src="{{ url('').'/images/tam-bg.png' }}" class="mx-2 w-4/5" alt="info-icon">
                                               <h1 class="absolute">{{ $marketSize->total_obtainable_market_value }}</h1>
                                           </div>
                                       </div>
                                       <div class="col-12 col-sm-4 d-flex lg-show mb-4 flex-column justify-content-center align-items-center">
                                           <div class="mt-3 text-center">
                                               <h1>{{ __('TAM') }}</h1>
                                               <p>{{ $marketSize->total_obtainable_market_title }}</p>
                                           </div>
                                       </div>
                                       <div class="col-12 col-sm-4 d-flex sm-none flex-column justify-content-center align-items-center">
                                           <div class="mt-3 text-center">
                                               <h1>{{ __('SOM') }}</h1>
                                               <p>{{ $marketSize->total_addressable_market_title }}</p>
                                           </div>
                                       </div>
                                       <div class="col-12 col-sm-4 d-flex sm-none flex-column justify-content-center align-items-center">
                                           <div class="mt-3 text-center">
                                               <h1>{{ __('SAM') }}</h1>
                                               <p>{{ $marketSize->total_servicable_market_title }}</p>
                                           </div>
                                       </div>
                                       <div class="col-12 col-sm-4 d-flex sm-none flex-column justify-content-center align-items-center">
                                           <div class="mt-3 text-center">
                                               <h1>{{ __('TAM') }}</h1>
                                               <p>{{ $marketSize->total_obtainable_market_title }}</p>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               
                                <div class="card p-1 p-sm-4" id="marketSize-content">
                                   <h3 class="mb-3">{{ __('Market Trends') }}</h3>
                                   <p class="mb-3">
                                       {{ $marketSize->description_2 }}
                                   </p>
                                   
                                   <div class="mt-4">
                                        <div class="d-flex align-items-center mb-3">
                                            <h4 class="mb-0">{{ __('1. Virtual Reality in Tourism') }}</h4>
                                            <x-tabler-chevron-down class="ml-2 chevron-down" data-type="virtual" />
                                            <x-tabler-chevron-up class="ml-2 chevron-up" data-type="virtual" style="display: none" />
                                        </div>
                                       <p class="mb-3">{{ $marketSize->virtual_reality_in_tourism_description }}</p>
                                       <div class="row" id="list-virtual">
                                           <div class="col-12 col-sm-6">
                                               <h4 class="mb-3" style="color: #666666">{{ __('Key points') }}</h4>
                                               <ul style="color: #666666">
                                                   <li>
                                                       {{ $marketSize->virtual_reality_in_tourism_key_point_1 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->virtual_reality_in_tourism_key_point_2 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->virtual_reality_in_tourism_key_point_3 }}
                                                   </li>
                                               </ul>
                                           </div>
                                           <div class="col-12 col-sm-6">
                                               <h4 class="mb-3" style="color: #666666">{{ __('How to Leverage') }}</h4>
                                               <ul style="color: #666666">
                                                   <li>
                                                       {{ $marketSize->virtual_reality_in_tourism_how_to_leverage_1 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->virtual_reality_in_tourism_how_to_leverage_1 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->virtual_reality_in_tourism_how_to_leverage_1 }}
                                                   </li>
                                               </ul>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="my-3 border-bottom" style="border: 1px #B9B9B9 solid"></div>
                                   <div class="">
                                        <div class="d-flex align-items-center mb-3">
                                            <h4 class="mb-0">{{ __('2. Personalized Travel Experiences') }}</h4>
                                            <x-tabler-chevron-down class="ml-2 chevron-down" data-type="personalized" />
                                            <x-tabler-chevron-up class="ml-2 chevron-up" data-type="personalized" style="display: none" />
                                        </div>
                                       <p class="mb-3">{{ $marketSize->personalized_travel_experiences_description }}</p>
                                       <div class="row" id="list-personalized">
                                           <div class="col-12 col-sm-6">
                                               <h4 class="mb-3" style="color: #666666">{{ __('Key points') }}</h4>
                                               <ul style="color: #666666">
                                                   <li>
                                                       {{ $marketSize->personalized_travel_experiences_key_point_1 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->personalized_travel_experiences_key_point_2 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->personalized_travel_experiences_key_point_3 }}
                                                   </li>
                                               </ul>
                                           </div>
                                           <div class="col-12 col-sm-6">
                                               <h4 class="mb-3" style="color: #666666">{{ __('How to Leverage') }}</h4>
                                               <ul style="color: #666666">
                                                   <li>
                                                       {{ $marketSize->personalized_travel_experiences_how_to_leverage_1 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->personalized_travel_experiences_how_to_leverage_2 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->personalized_travel_experiences_how_to_leverage_3 }}
                                                   </li>
                                               </ul>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="my-3 border-bottom" style="border: 1px #B9B9B9 solid"></div>
                                   <div>
                                        <div class="d-flex align-items-center mb-3">
                                            <h4 class="mb-0">{{ __('3. Local culture immersion') }}</h4>
                                            <x-tabler-chevron-down class="ml-2 chevron-down" data-type="local" />
                                            <x-tabler-chevron-up class="ml-2 chevron-up" data-type="local" style="display: none" />
                                        </div>
                                       <p class="mb-3">{{ $marketSize->local_culture_immersion_description }}</p>
                                       <div class="row" id="list-local">
                                           <div class="col-12 col-sm-6">
                                               <h4 class="mb-3" style="color: #666666">{{ __('Key points') }}</h4>
                                               <ul style="color: #666666">
                                                   <li>
                                                       {{ $marketSize->local_culture_immersion_key_point_1 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->local_culture_immersion_key_point_2 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->local_culture_immersion_key_point_3 }}
                                                   </li>
                                               </ul>
                                           </div>
                                           <div class="col-12 col-sm-6">
                                               <h4 class="mb-3" style="color: #666666">{{ __('How to Leverage') }}</h4>
                                               <ul style="color: #666666">
                                                   <li>
                                                       {{ $marketSize->local_culture_immersion_how_to_leverage_1 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->local_culture_immersion_how_to_leverage_2 }}
                                                   </li>
                                                   <li>
                                                       {{ $marketSize->local_culture_immersion_how_to_leverage_3 }}
                                                   </li>
                                               </ul>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="mt-3 border-bottom" style="border: 1px #B9B9B9 solid"></div>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>