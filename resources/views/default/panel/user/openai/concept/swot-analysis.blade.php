<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>

    <form action="{{ route('projects.generate-swot', ['project' => $project->id]) }}" method="POST"
        id="generate-swot-form" style="display: none;">
        @csrf
    </form>


    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row h-full" id="0">
                @if ($project)
                    <div id="swot-body" class="mt-3 col-12 {{ is_null($swotAnalysis) ? 'blur-content loading' : '' }}">
                        <div class="overlay">
                            <div class="cv-spinner">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div class="mt-0 {{ is_null($swotAnalysis) ? 'blur-content-card' : '' }}">
                            <div class="card-header d-block px-2rem">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center justify-center">
                                        <h2 class="mb-0">{{ __('SWOT Analysis') }}</h2>
                                        <img src="{{ url('').'/images/info-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                                    </div>
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'swotAnalysis')"
                                            class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-1 p-sm-4" id="swot-content">
                                <div class="swot-container">
                                    <!-- Strengths -->
                                    <div class="swot-section position-relative box-strengths">
                                        @if ($swotAnalysis)
                                            @if ($swotAnalysis->strengths)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $swotAnalysis->id }}" data-type="swotAnalysis"
                                                    data-key="strengths" data-name="Strengths"
                                                    data-value="{{ $swotAnalysis->strengths }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <div class="d-flex align-items-center justify-center">
                                            <img src="{{ url('').'/images/s-icon.png' }}" width="30" height="18" class="mr-2" alt="s-icon">
                                            <div class="swot-title">{{ __('Strengths') }}</div>
                                        </div>
                                        <div class="swot-content white-space-pre-line" id="swotAnalysis-strengths">
                                            {!! $swotAnalysis->strengths ?? 'Not generated.' !!}</div>
                                        <div class="absolute" style="right: -8px;bottom: 0px;">
                                            <img src="{{ url('').'/images/s-pie.png' }}" width="80" height="80" class="mr-2" alt="s-pie">
                                        </div>
                                    </div>

                                    <!-- Weaknesses -->
                                    <div class="swot-section position-relative box-weaknesses">
                                        @if ($swotAnalysis)
                                            @if ($swotAnalysis->weaknesses)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $swotAnalysis->id }}" data-type="swotAnalysis"
                                                    data-key="weaknesses" data-name="Weaknesses"
                                                    data-value="{{ $swotAnalysis->weaknesses }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <div class="d-flex align-items-center justify-center">
                                            <img src="{{ url('').'/images/w-icon.png' }}" width="30" height="18" class="mr-2" alt="w-icon">
                                            <div class="swot-title">{{ __('Weaknesses') }}</div>
                                        </div>
                                        <div class="swot-content white-space-pre-line" id="swotAnalysis-weaknesses">
                                            {!! $swotAnalysis->weaknesses ?? 'Not generated.' !!}</div>
                                        <div class="absolute" style="left: -1px;bottom: 0px;">
                                            <img src="{{ url('').'/images/w-pie.png' }}" width="80" height="80" class="mr-2" alt="w-pie">
                                        </div>
                                    </div>

                                    <!-- Opportunities -->
                                    <div class="swot-section position-relative box-opportunities">
                                        @if ($swotAnalysis)
                                            @if ($swotAnalysis->opportunities)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $swotAnalysis->id }}" data-type="swotAnalysis"
                                                    data-key="opportunities" data-name="Opportunities"
                                                    data-value="{{ $swotAnalysis->weaknesses }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <div class="d-flex align-items-center justify-center">
                                            <img src="{{ url('').'/images/o-icon.png' }}" width="30" height="18" class="mr-2" alt="o-icon">
                                            <div class="swot-title">{{ __('Opportunities') }}</div>
                                        </div>
                                        <div class="swot-content white-space-pre-line" id="swotAnalysis-opportunities">
                                            {!! $swotAnalysis->opportunities ?? 'Not generated.' !!}</div>
                                        <div class="absolute" style="right: -8px;top: -1px;">
                                            <img src="{{ url('').'/images/o-pie.png' }}" width="80" height="80" class="mr-2" alt="o-pie">
                                        </div>
                                    </div>

                                    <!-- Threats -->
                                    <div class="swot-section position-relative box-threats">
                                        @if ($swotAnalysis)
                                            @if ($swotAnalysis->threats)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $swotAnalysis->id }}" data-type="swotAnalysis"
                                                    data-key="threats" data-name="Threats"
                                                    data-value="{{ $swotAnalysis->threats }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <div class="d-flex align-items-center justify-center">
                                            <img src="{{ url('').'/images/t-icon.png' }}" width="30" height="18" class="mr-2" alt="t-icon">
                                            <div class="swot-title">{{ __('Threats') }}</div>
                                        </div>
                                        <div class="swot-content white-space-pre-line" id="swotAnalysis-threats">
                                            {!! $swotAnalysis->threats ?? 'Not generated.' !!}</div>
                                        <div class="absolute" style="left: -1px;top: -1px;">
                                            <img src="{{ url('').'/images/t-pie.png' }}" width="80" height="80" class="mr-2" alt="t-pie">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>