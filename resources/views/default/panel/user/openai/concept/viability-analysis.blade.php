<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
        
    <form action="{{ route('projects.generate-viability-analysis', ['project' => $project->id]) }}" method="POST"
        id="generate-viability-analysis" style="display: none;">
        @csrf
    </form>


    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row h-full" id="0">
                @if ($project)
                    <div id="viabilityAnalysis-body" class="mt-3 col-12 {{ is_null($viabilityAnalysis) ? 'blur-content loading' : '' }}">
                        <div class="overlay">
                            <div class="cv-spinner">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div class="{{ is_null($viabilityAnalysis) ? 'blur-content-card' : '' }} mt-0">
                            <div class="card-header d-block">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <h2 class="mb-0">{{ __('Viability Analysis') }}</h2>
                                    </div>
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'viabilityAnalysis')"
                                            class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                            @if (!empty($viabilityAnalysis))
                                @php
                                    $percent = $viabilityAnalysis->percent;
                                    $img = 'chart-bg.png';
                                    if ($percent != 0 || $percent % 5 == 0) {
                                        $img = 'chart-bg-' . $percent . '.png';
                                    }
                                    if ($percent % 5 != 0) {
                                        $remainder = $percent % 5;
                                        if ($remainder == 1 || $remainder == 2) {
                                            $percent = $percent - $remainder;
                                        }
                                        if ($remainder > 2) {
                                            $percent = $percent + 5 - $remainder;
                                        }
                                        $img = 'chart-bg-' . $percent . '.png';
                                    }
                                @endphp
                                <div class="card p-1 p-sm-4" id="viabilityAnalysis-content">
                                    <div
                                      class="row"
                                    >
                                      <div class="col-12 col-sm-5 px-3 relative d-flex justify-content-center">
                                        <img src="{{ url('').'/images/'.$img }}" class="w-full h-full" alt="s-icon">
                                        <p class="absolute" style="top: 37%">{{ __('Viability Percentage') }}</p>
                                        <h1 class="absolute" style="top: 47%;font-size: 4rem;">{{ $viabilityAnalysis->percent }} %</h1>
                                        <h2 class="absolute" style="top: 63%">{{ $viabilityAnalysis->percent > 50 ? __('High viability') : __('Low viability') }}</h2>
                                      </div>
                                      <div class="col-12 col-sm-7 d-flex align-items-center">
                                        <div>
                                            <h1 style="font-size: 3rem;" class="mb-3">"{{ $project->name }}"</h1>
                                            <img src="{{ url('').'/images/o-icon.png' }}" width="30" height="18" class="mt-3" alt="o-icon">
                                            <h3 class="mt-3">{{ $viabilityAnalysis->percent > 50 ? __('High viability') : __('Low viability') }}</h3>
                                            <p class="mt-3">{{ $viabilityAnalysis->percent > 50 
                                                ? __('High viability indicates a strong likelihood of success for a business idea or product in a particular market. It suggests that the market presents favorable conditions for your offering to be profitable and sustainable.') 
                                                : __('Low viability indicates a weak likelihood of success for a business idea or product in a particular market. It suggests that the market presents unfavorable conditions, making it difficult for the offering to be profitable and sustainable. Factors contributing to low viability might include strong competition, limited demand, regulatory challenges, or high operational costs.') }}
                                            </p>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="my-4 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                    <div class="position-relative w-full">
                                        <div class="edit-popup position-absolute position-edit-5-0"
                                            data-id="{{ $viabilityAnalysis->id }}" data-type="viabilityAnalysis"
                                            data-key="viability_analysis_content" data-name="market size"
                                            data-value="{{ $viabilityAnalysis->viability_analysis_content }}"
                                             role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                        </div>
                                        <p id="viabilityAnalysis-viability_analysis_content" class="white-space-pre-line">
                                            {!! $viabilityAnalysis->viability_analysis_content !!}
                                        </p>
                                    </div>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>