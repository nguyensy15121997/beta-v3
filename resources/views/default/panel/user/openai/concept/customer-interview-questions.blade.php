<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>

    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row h-full" id="0">
                @if ($project)
                    <div id="customerInterview-body" class="mt-3 col-12 {{ is_null($customerInterview) ? 'blur-content loading' : '' }}">
                        <div class="overlay">
                            <div class="cv-spinner">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div class="{{ is_null($customerInterview) ? 'blur-content-card' : '' }} mt-0">
                            <div class="card-header d-block px-2rem">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center justify-center">
                                        <h2 class="mb-0">{{ __('Interview Questions') }}</h2>
                                        <img src="{{ url('').'/images/info-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                                    </div>
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'customerInterview')"
                                            class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-1 p-sm-4" id="customerInterview-content">
                                <div class="card p-4">
                                    <div class="d-flex">
                                        <div class="w-5rem relative">
                                            <div class="line"></div>
                                            <div class="w-full d-flex align-items-center mb-3 justify-between">
                                                <div class="dot-orange"></div>
                                                <img src="{{ url('').'/images/disclaimer-icon.png' }}" width="20" height="20" alt="info-icon">
                                            </div>
                                        </div>
                                        <div class="w-full">
                                            <div class="d-flex align-items-center mb-3">
                                                <h2 class="mb-0 ml-5">{{ __('Disclaimers') }}</h2>
                                                <x-tabler-chevron-down class="ml-2 chevron-down" data-type="disclaimers" />
                                                <x-tabler-chevron-up class="ml-2 chevron-up" data-type="disclaimers" style="display: none" />
                                            </div>
                                            <div class="ml-5 w-6/7" id="list-disclaimers">
                                                <h3 style="color: #666666">{{ __('Purpose:') }}</h3>
                                                <p>{{ __('The purpose of the interview is to help us gain a better understanding of...') }}</p>
                                                <h3 style="color: #666666">{{ __('Expectations:') }}</h3>
                                                <ul class="ml-5">
                                                    <li>{{ __('Throughout the interview if there’s anything you don’t feel comfortable responding to, feel free to let me know.') }}</li>
                                                    <li>{{ __('There are no right or wrong answers. The goal of this interview is to hear your unique perspective about your experiences.') }}</li>
                                                    <li>{{ __('I’ll be taking notes throughout the interview, so if I am looking away from the camera, it means I’m busy typing.') }}</li>
                                                </ul>
                                                <h3 style="color: #666666; margin-top: 0.5rem">{{ __('Disclaimer:') }}</h3>
                                                <p>{{ __('Quick reminder, this session is recorded for referencing later. Can you please confirm you’re happy for us move ahead.') }}</p>
                                                <div class="my-4 border-bottom" style="border: 1px #B9B9B9 solid"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="w-5rem relative">
                                            <div class="line"></div>
                                            <div class="w-full d-flex align-items-center mb-3 justify-between">
                                                <div class="dot-orange"></div>
                                                <img src="{{ url('').'/images/demographic-icon.png' }}" width="20" height="20" alt="info-icon">
                                            </div>
                                        </div>
                                        <div class="w-full">
                                            <div class="d-flex align-items-center mb-3">
                                                <h2 class="mb-0 ml-5">{{ __('Demographic') }}</h2>
                                                <x-tabler-chevron-down class="ml-2 chevron-down" data-type="demographic" />
                                                <x-tabler-chevron-up class="ml-2 chevron-up" data-type="demographic" style="display: none" />
                                            </div>
                                            <ul class="ml-5 w-6/7" id="list-demographic">
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_11)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_11" data-name="Question 11"
                                                                data-value="{{ $customerInterview->question_11 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_11">
                                                        {{ $customerInterview->question_11 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_12)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_12" data-name="Question 12"
                                                                data-value="{{ $customerInterview->question_12 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_12">
                                                        {{ $customerInterview->question_12 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_13)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_13" data-name="Question 13"
                                                                data-value="{{ $customerInterview->question_13 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_13">
                                                        {{ $customerInterview->question_13 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <div class="my-4 border-bottom" style="border: 1px #B9B9B9 solid"></div>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="w-5rem relative">
                                            <div class="line"></div>
                                            <div class="w-full d-flex align-items-center mb-3 justify-between">
                                                <div class="dot-orange"></div>
                                                <img src="{{ url('').'/images/depth-icon.png' }}" width="20" height="20" alt="info-icon">
                                            </div>
                                        </div>
                                        <div class="w-full">
                                            <div class="d-flex align-items-center mb-3">
                                                <h2 class="mb-0 ml-5">{{ __('In-Depth') }}</h2>
                                                <x-tabler-chevron-down class="ml-2 chevron-down" data-type="depth" />
                                                <x-tabler-chevron-up class="ml-2 chevron-up" class="chevron-up" data-type="depth" style="display: none" />
                                            </div>
                                            <ul class="ml-5 w-6/7" id="list-depth">
                                                
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_1)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_1" data-name="Question 1"
                                                                data-value="{{ $customerInterview->question_1 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_1">
                                                        {{ $customerInterview->question_1 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_2)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_2" data-name="Question 2"
                                                                data-value="{{ $customerInterview->question_2 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_2">
                                                        {{ $customerInterview->question_2 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_3)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_3" data-name="Question 3"
                                                                data-value="{{ $customerInterview->question_3 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_3">
                                                        {{ $customerInterview->question_3 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_4)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_4" data-name="Question 4"
                                                                data-value="{{ $customerInterview->question_4 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_4">
                                                        {{ $customerInterview->question_4 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_5)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_5" data-name="Question 5"
                                                                data-value="{{ $customerInterview->question_5 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_5">
                                                        {{ $customerInterview->question_5 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_6)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_6" data-name="Question 6"
                                                                data-value="{{ $customerInterview->question_6 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="$customerInterview-question_6">
                                                        {{ $customerInterview->question_6 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_7)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_7" data-name="Question 7"
                                                                data-value="{{ $customerInterview->question_7 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_7">
                                                        {{ $customerInterview->question_7 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_8)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_8" data-name="Question 8"
                                                                data-value="{{ $customerInterview->question_8 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_8">
                                                        {{ $customerInterview->question_8 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_9)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_9" data-name="Question 9"
                                                                data-value="{{ $customerInterview->question_9 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_9">
                                                        {{ $customerInterview->question_9 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_10)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_10" data-name="Question 10"
                                                                data-value="{{ $customerInterview->question_10 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_10">
                                                        {{ $customerInterview->question_10 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <div class="my-4 border-bottom" style="border: 1px #B9B9B9 solid"></div>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="w-5rem relative">
                                            <div class="w-full d-flex align-items-center mb-3 justify-between">
                                                <div class="dot-orange"></div>
                                                <img src="{{ url('').'/images/wrap-up-icon.png' }}" width="20" height="20" alt="info-icon">
                                            </div>
                                        </div>
                                        <div class="w-full">
                                            <div class="d-flex align-items-center mb-3">
                                                <h2 class="mb-0 ml-5">{{ __('Wrap-Up') }}</h2>
                                                <x-tabler-chevron-down class="ml-2 chevron-down" data-type="wrap" />
                                                <x-tabler-chevron-up class="ml-2 chevron-up" class="chevron-up" data-type="wrap" style="display: none" />
                                            </div>
                                            <ul class="ml-5 w-6/7" id="list-wrap">
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_14)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_14" data-name="Question 14"
                                                                data-value="{{ $customerInterview->question_14 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_14">
                                                        {{ $customerInterview->question_14 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_15)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_15" data-name="Question 15"
                                                                data-value="{{ $customerInterview->question_15 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_15">
                                                        {{ $customerInterview->question_15 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <li class="position-relative question-container">
                                                    @if ($customerInterview)
                                                        @if ($customerInterview->question_16)
                                                            <div class="edit-popup position-absolute edit-position-ci"
                                                                data-id="{{ $customerInterview->id }}" data-type="customerInterview"
                                                                data-key="question_16" data-name="Question 16"
                                                                data-value="{{ $customerInterview->question_16 }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-sm"></i></div>
                                                        @endif
                                                    @endif
                                                    <p class="px-3" id="customerInterview-question_16">
                                                        {{ $customerInterview->question_16 ?? __('Not generated.') }}
                                                    </p>
                                                </li>
                                                <div class="my-4 border-bottom" style="border: 1px #B9B9B9 solid"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>