<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>

    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row h-full" id="0">
                @if ($project)
                    <div id="userStories-body" class="mt-3 col-12 {{ $userStories->isEmpty() ? 'blur-content loading' : '' }}">
                        <div class="overlay">
                            <div class="cv-spinner">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div class="{{ $userStories->isEmpty() ? 'blur-content-card' : '' }} mt-0">
                            <div class="card-header d-block px-2rem">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <h2 class="mb-0">{{ __('User Stories') }}</h2>
                                    </div>
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'userStories')"
                                            class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="card-body p-1 p-sm-4" id="userStories-content">
                                <div class="card p-4">
                                    @foreach ($userStories as $user)
                                        <div class="card">
                                            <div class="row">
                                                <div class="col-sm-3 col-5 p-4 border-box-right" style="">
                                                    <img src="{{ url('').'/images/user-story-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                                    <h3 class="my-2">{{ __('User story') }}</h3>
                                                    <div class="relative">
                                                        @if ($user->user_story)
                                                            <div class="edit-popup position-absolute postion-edit-0-15"
                                                                data-id="{{ $user->id }}" data-type="userStories"
                                                                data-key="user_story" data-name="User Story"
                                                                data-value="{{ $user->user_story }}"
                                                                 role="button"><i
                                                                    class="ti ti-pencil-alt text-xm"></i></div>
                                                        @endif
                                                        <span class="white-space-pre-line" id="userStories-user_story-{{ $user->id }}">
                                                            {{ $user->user_story ?? 'Not generated.' }}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-9 col-7 p-4">
                                                    <div class="">
                                                        <img src="{{ url('').'/images/criteria-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                                        <h3 class="my-2">{{ __('Acceptance Criteria') }}</h3>
                                                        <div class="relative">
                                                            @if ($user->acceptance_criterea)
                                                                <div class="edit-popup position-absolute postion-edit-0-15"
                                                                    data-id="{{ $user->id }}" data-type="userStories"
                                                                    data-key="acceptance_criterea"
                                                                    data-name="Acceptance Criterea"
                                                                    data-value="{{ $user->acceptance_criterea }}"
                                                                     role="button"><i
                                                                        class="ti ti-pencil-alt text-xm"></i></div>
                                                            @endif
                                                            <span class="white-space-pre-line" id="userStories-acceptance_criterea-{{ $user->id }}">
                                                                {!! $user->acceptance_criterea ?? 'Not generated.' !!}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="card shadow d-flex flex-row" id="create-one-user-stories" onclick='return createUserStories("{{ route('projects.generate-one-user-stories', ['project' => $project->id]) }}");' style="cursor: pointer;border: 1px #666666;border-style: dashed; min-height: 180px;">
                                        <div class="overlay">
                                            <div class="cv-spinner">
                                                <span class="spinner"></span>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-col align-items-center justify-center w-full">
                                            <p class="underline">{{ __('Add User Story') }}</p>
                                            <p>{{ __('Or') }}</p>
                                            <div class="d-flex align-items-center">
                                                <img src="{{ url('').'/images/add-icon.png' }}"
                                                class="mx-2" width=20 height=20  alt="Avatar 1">
                                                <div style="color: #666666;">{{ __('Add persona with AI') }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>