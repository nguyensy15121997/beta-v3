<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>

    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row h-full" id="0">
                @if ($project)
                    <div id="userPersonas-body" class="mt-3 col-12 {{ $userPersonas->isEmpty() ? 'blur-content loading' : '' }}">
                                <div class="overlay">
                                    <div class="cv-spinner">
                                        <span class="spinner"></span>
                                    </div>
                                </div>
                                <div class="{{ $userPersonas->isEmpty() ? 'blur-content-card' : '' }} mt-0">
                                    <div class="card-header d-block px-2rem">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div>
                                                <h2 class="mb-0">{{ __('User Personas') }}</h2>
                                            </div>
                                            <div>
                                                <button onclick="regenerateProjectInfo({{ $project->id }}, 'userPersonas')"
                                                    class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                                </button>
                                            </div>
                                            <!-- Add this inside your Blade template where you show project details -->
    
                                            <!--<div>-->
                                            <!--    <button onclick="regenerateProjectInfo({{ $project->id }}, 'userPersonas')"-->
                                            <!--        class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>-->
                                            <!--    <button onclick="collapseByType('userPersonas', 'minius')"-->
                                            <!--        id="userPersonas-minius" class="btn btn-success btn-sm"><i-->
                                            <!--            class="ti ti-minus"></i></button>-->
                                            <!--    <button onclick="collapseByType('userPersonas', 'plus')"-->
                                            <!--        id="userPersonas-plus" style="display: none"-->
                                            <!--        class="btn btn-success btn-sm"><i class="ti ti-plus"></i></button>-->
                                            <!--    <button-->
                                            <!--        onclick="deleteProjectInfo({{ $project->id }}, 'userPersonas', '#userPersonas-body')"-->
                                            <!--        class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>-->
                                            <!--</div>-->
                                        </div>
                                    </div>
                                    <div class="card-body p-1 p-sm-4" id="userPersonas-content">
                                        <div>
                                            @if ($userPersonas->count() > 0)
                                                @foreach ($userPersonas as $user)
                                                    <div class="card shadow"> 
                                                        <div class="line-personas"></div>
                                                        <div class="card-body row">
                                                            <div class="col-6 row">
                                                                <div class="col-5">
                                                                    @if (isset($user->url))
                                                                    <img src="{{ url('').'/testimonialAvatar/test/'.$user->url }}"
                                                                        class="card-img w-full h-full mx-auto" alt="Avatar 1">
                                                                    @endif
                                                                    <div class="name-absolute fs-5">
                                                                        {{ $user->name }}, {{ $user->age }}
                                                                    </div>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="d-flex">
                                                                        <img src="{{ url('').'/images/job-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                                                                        <div>{{ $user->job }}</div>
                                                                    </div>
                                                                    <div class="d-flex mt-3">
                                                                        <img src="{{ url('').'/images/married-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                                                                        <div>{{ $user->marriage }}</div>
                                                                    </div>
                                                                    <div class="d-flex flex-wrap mt-3 mx-2">
                                                                        @php
                                                                            if (app()->getLocale() == 'en') {
                                                                                $names = isset($user->personality_trait) ? preg_split('/,\s*/', $user->personality_trait) : [];
                                                                            } else {
                                                                                $names = explode("، ", $user->personality_trait);
                                                                            }
                                                                        @endphp
                                    
                                                                        @if (!empty($names))
                                                                            @foreach ($names as $index => $name)
                                                                                <div class="trait-square">{{ trim($name) }}</div>
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                    <ul class="list-group list-group-flush mt-3 mx-2">
                                                                        <li class="list-group-item fs-5 relative" style="font-size: 0.915rem !important;"><b>{{ __('Bio') }}:</b>
                                                                            @if ($user->description)
                                                                                <div class="edit-popup position-absolute postion-edit-5"
                                                                                    data-id="{{ $user->id }}"
                                                                                    data-type="userPersonas" data-key="description"
                                                                                    data-name="Description"
                                                                                    data-value="{{ $user->description }}"
                                                                                     role="button"><i
                                                                                        class="ti ti-pencil-alt text-xs"></i></div>
                                                                            @endif
                                                                            <span id="userPersonas-description-{{ $user->id }}">
                                                                                {{ $user->description }}
                                                                            </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <ul class="list-group list-group-flush">
                                                                    <li class="fs-5 mb-3 relative" style="font-size: 0.915rem !important;"><b>{{ __('Goals') }}:</b>
                                                                        @if ($user->goals)
                                                                            <div class="edit-popup position-absolute postion-edit-5-15"
                                                                                data-id="{{ $user->id }}"
                                                                                data-type="userPersonas" data-key="goals"
                                                                                data-name="Goals" data-value="{{ $user->goals }}"
                                                                                 role="button"><i
                                                                                    class="ti ti-pencil-alt text-xs"></i></div>
                                                                        @endif
                                                                        <span id="userPersonas-goals-{{ $user->id }}">
                                                                            {{ $user->goals }}
                                                                        </span>
                                                                    </li>
                                                                    <li class="fs-5 mb-3 relative" style="font-size: 0.915rem !important;"><b>{{ __('Needs') }}:</b>
                                                                        @if ($user->needs)
                                                                            <div class="edit-popup position-absolute postion-edit-5-15"
                                                                                data-id="{{ $user->id }}"
                                                                                data-type="userPersonas" data-key="needs"
                                                                                data-name="Needs" data-value="{{ $user->needs }}"
                                                                                 role="button"><i
                                                                                    class="ti ti-pencil-alt text-xs"></i></div>
                                                                        @endif
                                                                        <span id="userPersonas-needs-{{ $user->id }}">
                                                                            {{ $user->needs }}
                                                                        </span>
                                                                    </li>
                                                                    <li class="fs-5 mb-3 relative" style="font-size: 0.915rem !important;"><b>{{ __('Pains') }}:</b>
                                                                        @if ($user->pains)
                                                                            <div class="edit-popup position-absolute postion-edit-5-15"
                                                                                data-id="{{ $user->id }}"
                                                                                data-type="userPersonas" data-key="pains"
                                                                                data-name="Pains" data-value="{{ $user->pains }}"
                                                                                 role="button"><i
                                                                                    class="ti ti-pencil-alt text-xs"></i></div>
                                                                        @endif
                                                                        <span id="userPersonas-pains-{{ $user->id }}">
                                                                            {{ $user->pains }}
                                                                        </span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                            <div class="card shadow d-flex flex-row" id="create-one-user-personas" onclick='return createUserPersonas("{{ route('projects.generate-one-user-personas', ['project' => $project->id]) }}");' style="cursor: pointer;border: 1px #666666;border-style: dashed;">
                                                <div class="overlay">
                                                    <div class="cv-spinner">
                                                        <span class="spinner"></span>
                                                    </div>
                                                </div>
                                                <div class="card-img p-3" style="width: 20.83333%">
                                                    <img src="{{ url('').'/images/card-add.png' }}"
                                                        class="card-img w-full h-full mx-auto" alt="Avatar 1">
                                                </div>
                                                <div class="d-flex align-items-center justify-center w-full">
                                                    <div class="d-flex align-items-center">
                                                        <img src="{{ url('').'/images/add-icon.png' }}"
                                                        class="mx-2" width=20 height=20  alt="Avatar 1">
                                                        <div>{{ __('Add persona with AI') }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>