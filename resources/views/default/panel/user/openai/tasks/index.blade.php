@php
    $permissions = ['move task', 'create task', 'show task', 'edit task', 'delete task'];
@endphp
<div class="card overflow-hidden">
    <div class="container-kanban">
        <div class="kanban-board min-750" style="min-height: 750px" @if(isset($permissions) && in_array('move task',$permissions)) data-plugin="dragula" @endif data-containers='{{json_encode($stageClass)}}'>
            @foreach($stages as $stage)
                <div class="kanban-col px-0">
                    <div class="card-list card-list-flush card-list-flush-{{$stage->name}}">
                        <div class="card-list-title row align-items-center mb-3">
                            <div class="col">
                                <h6 class="mb-0">{{ __($stage->name) }}</h6>
                            </div>

                            @if((isset($permissions) && in_array('create task',$permissions)))
                                <div class="col text-right">
                                    <div class="actions d-flex justify-content-end">
                                            <a class="action-item" data-bs-toggle="modal" data-bs-target="#createTaskModel{{$stage->id}}">
                                                <x-tabler-plus class="size-5" />
                                            </a>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="card-list-body task-list-items" id="task-list-{{ $stage->id }}" data-status="{{$stage->id}}">
                            @foreach($stage->tasks as $taskDetail)
                                <div class="site-launch card card-progress @if(isset($permissions) && in_array('move task',$permissions)) draggable-item @endif border shadow-none" id="{{$taskDetail->id}}" style="{{ !empty($taskDetail->priority_color) ? 'border-left: 2px solid '.$taskDetail->priority_color.' !important' :'' }};">
                                    <div class="card-body">
                                        <div class="row align-items-center mb-2" onclick="viewTask('{{'#editTaskModel' . $stage->id}}', {{ $taskDetail }}, {{ $project->id }});">
                                            <div class="col-6">
                                                <span class="badge badge-pill badge-xs badge-{{\App\Models\ProjectTask::$priority_color[$taskDetail->priority]}}">{{ __(\App\Models\ProjectTask::$priority[$taskDetail->priority]) }}</span>

                                            </div>
                                            <div class="col-6 text-right">
                                                @if(str_replace('%','',$taskDetail->taskProgress()['percentage']) > 0)<span class="text-sm">{{ $taskDetail->taskProgress()['percentage'] }}</span>@endif


                                                @if(isset($permissions) && (in_array('show task',$permissions) || in_array('edit task',$permissions) || in_array('delete task',$permissions)))
                                                    <div class="dropdown action-item">
                                                      <a href="#" onclick="event.stopPropagation();" class="action-item" data-bs-toggle="dropdown"><x-tabler-dots class="size-5" /></a>
                                                      <div class="dropdown-menu">
                                                        <a href="#" class="dropdown-item hover:bg-primary"
                                                        onclick="event.stopPropagation(); updateDataEdit('{{'#editTaskModel' . $stage->id}}', {{ $taskDetail }}, {{ $project->id }});">{{ __('Edit') }}</a>
                                                        <a class="dropdown-item hover:bg-primary" href="{{ route('dashboard.user.openai.projects.tasks.destroy',[$project->id,$taskDetail->id]) }}">{{ __('Delete') }}</a>
                                                      </div>
                                                    </div>

                                                @endif
                                            </div>
                                        </div>
                                        <a class="h6 task-name-break" href="#">{{ $taskDetail->title }}</a>
                                        <div class="row align-items-center">
                                            <div class="col-12">
                                                <div class="actions d-inline-block">
                                                    @if(count($taskDetail->taskFiles) > 0)
                                                        <div class="action-item mr-2"><i class="fas fa-paperclip mr-2"></i>{{ count($taskDetail->taskFiles) }}</div>@endif
                                                    @if(count($taskDetail->comments) > 0)
                                                        <div class="action-item mr-2"><i class="fas fa-comment-alt mr-2"></i>{{ count($taskDetail->comments) }}</div>@endif
                                                    @if($taskDetail->checklist->count() > 0)
                                                        <div class="action-item mr-2"><i class="fas fa-tasks mr-2"></i>{{ $taskDetail->countTaskChecklist() }}</div>@endif
                                                </div>
                                            </div>
                                            <div class="col-5">@if(!empty($taskDetail->end_date) && $taskDetail->end_date != '0000-00-00')<small style="color: rgb(132, 146, 166);" @if(strtotime($taskDetail->end_date) < time())@endif>{{ date('d M Y', strtotime($taskDetail->end_date)) }}</small>@endif</div>
                                            <div class="col-7 text-right">
                                                @if($users = $taskDetail->users())
                                                    <div class="avatar-group">
                                                        @foreach($users as $key => $user)
                                                            @if($key<3)
                                                                <a href="" class="avatar rounded-circle avatar-sm">
                                                                     @if($user->avatar)
                                                                        <img  src="{{$logo_path.$user->avatar}}" title="{{ $user->name }}"  >

                                                                      @else
                                                                       <img {{ $user->img_avatar }} title="{{ $user->name }}">
                                                                       @endif
                                                                </a>
                                                            @else
                                                                @break
                                                            @endif
                                                        @endforeach
                                                        @if(count($users) > 3)
                                                            <a href="#" class="avatar rounded-circle avatar-sm">
                                                                <img avatar="+ {{ count($users)-3 }}">
                                                            </a>
                                                        @endif
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <span class="empty-container" data-placeholder="{{__('Empty')}}"></span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>