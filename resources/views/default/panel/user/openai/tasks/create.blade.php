<form action="" method="post">
    <div class="row">
    
        <div class="col-8">
            <div class="form-group">
                <label class="form-label">{{ __('Task Name') }}</label>
                <input class="form-control" id="title" type="text" name="title">
            </div>
        </div>
        <div class="col-4">
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="form-label">{{ __('Task Description') }}</label>
                <textarea class="form-control" id="description" name="description"></textarea>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label class="form-label">{{ __('Estimated Hours') }}</label>
                <input class="form-control" id="estimated_hrs" type="number" min=0 name="estimated_hrs">
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label class="form-label">{{ __('Priority') }}</label>
                <small class="form-text text-muted mb-2 mt-0">{{ __('Set Priority of your task') }}</small>
                <select class="form-control" name="priority" id="priority" required>
                    @foreach (\App\Models\ProjectTask::$priority as $key => $val)
                        <option value="{{ $key }}">{{ __($val) }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <x-forms.input
                    class:container="grow"
                    id="start_date"
                    label="{{ __('Start Date') }}"
                    type="date"
                    name="start_date"
                    size="lg"
                />
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <x-forms.input
                    class:container="grow"
                    id="end_date"
                    label="{{ __('End Date') }}"
                    type="date"
                    name="end_date"
                    size="lg"
                />
            </div>
        </div>
    </div>
    
    <div class="text-right">
        <x-button type="submit">
            {{ __('Save') }}
        </x-button>
    </div>
<form/>
