@extends('panel.layout.app', ['layout_wide' => true])

@section('additional_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/fontawesome.min.css"
        integrity="sha512-W5OxdLWuf3G9SMWFKJLHLct/Ljy7CmSxaABQLV2WIfAQPQZyLSDW/bKrw71Nx7mZKN5zcL+r8pRCZw+5bIoHHw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://unpkg.com/@icon/themify-icons/themify-icons.css">
    <link href="{{ custom_theme_url('assets/css/tabler.min.css') }}" rel="stylesheet" />
    <link href="{{ custom_theme_url('assets/css/test.css') }}" rel="stylesheet" />
    <link href="{{ custom_theme_url('assets/css/app.css') }}" rel="stylesheet" />
    <link href="{{ custom_theme_url('/assets/css/site-light.min.css') }}" rel="stylesheet" />
    <link href="{{ custom_theme_url('/assets/css/site-light.css') }}" rel="stylesheet" />
    <link href="{{ custom_theme_url('/assets/css/site-dark.css') }}" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Libre+Baskerville:ital,wght@0,400;0,700;1,400&family=Tajawal:wght@200;300;400;500;700;800;900&display=swap');
    </style>

    <style>
        .position-right {
            right: 0;
        }
        .lang_ar .position-right {
            right: unset;
            left: 0;
        }
        .float-edit-botton {
            bottom: 40px !important;
        }
        .float-plus-botton.bottom-20 {
            bottom: 7rem;
        }
        .modal-backdrop {
            z-index: 40 !important;
        }
        #chat_form .w-1\/4 {
            display: none;
        }
        #prompt {
            border: none !important;
        }
        .\[\&_\.lqd-chat-user-bubble_\.chat-content-container\]\:bg-\[\#9A34CD\] .lqd-chat-user-bubble .chat-content-container {
            background: #e5610b !important;
        }
        .lqd-page-content-container {
            background: #f6f5f5;
        }
        .lqd-header {
            background: #f6f5f5;
            border-bottom: 0px;
        }
        .container-overview {
            background: white;
            border-radius: 20px;
        }
        
        .theme-dark .swot-content, .theme-dark .swot-title, .theme-dark h3, .theme-dark p {
            color: white !important;
        }
        
        .theme-dark .container-overview, .theme-dark #marketingOverview-content .row, .theme-dark #marketingOverview-content, .theme-dark #marketingOverview-body .py-1 {
            background: #292b2e!important;
        }
        
        .theme-dark .overlay {
            background: rgb(41, 43, 46, 0.7);
        } 
        
        .theme-dark .lqd-header, .theme-dark .lqd-page-content-container, .theme-dark .lqd-page-content-wrap,
        .theme-dark #generate-success, .theme-dark #under-generating, .theme-dark #close-generate-success, .theme-dark .lqd-navbar-inner, .theme-dark .lqd-page-footer {
            background: #161b22!important;
        }
        
        .h-70px {
            max-height: 70px;
        }
        .libre-baskerville-regular {
          font-family: "Libre Baskerville", serif;
          font-weight: 400;
          font-style: normal;
        }
        
        .libre-baskerville-bold {
          font-family: "Libre Baskerville", serif;
          font-weight: 700;
          font-style: normal;
        }
        
        .libre-baskerville-regular-italic {
          font-family: "Libre Baskerville", serif;
          font-weight: 400;
          font-style: italic;
        }

        .box-action {
            right: 10px;
            display: none;
        }
        
        .lang_ar .box-action {
            left: 10px;
            right: unset;
        }

        .card-document:hover .box-action {
            display: block;
            transaction: display 0.5s ease;
        }
        .border-box-right {
            border-right: 1px rgba(0, 0, 0, 0.05) solid;
            border-radius: 12px;     
        }
        
        .lang_ar .border-box-right {
            border-right: unset;
            border-left: 1px rgba(0, 0, 0, 0.05) solid;
            border-radius: 12px;
        }
        
        #overview .border-box-right {
            border-right: 1px #B9B9B9 dashed;
        }
        
        .lang_ar #overview .border-box-right {
            border-left: 1px #B9B9B9 dashed;
        }
        
        .border-right {
            border-radius: 0 .5rem .5rem 0;;
        }
        .border-left {
            border-radius: .5rem 0 0 .5rem;;
        }
        
        .lang_ar .border-left {
            border-radius: 0 .5rem .5rem 0;;
        }
        .lang_ar .border-right {
            border-radius: .5rem 0 0 .5rem;;
        }
        
        #overview .card {
            border-style: dashed;
            border-color: #B9B9B9;
        }
        
        #overview .card-document {
            border-style: solid;
        }
        
        #overview .card-landing-page {
            border-style: solid;
        }
        
        .h-17rem {
            height: 20rem;
        }
        #overview .h-17rem{
            height: 2.5rem;
        }
        
        .lang_ar .text-left {
            text-align: right;
        }
        .h-1\/5 {
            height: 20%;
        }
        .h-1\/2 {
            height: 50%;
            overflow: hidden;
        }
        .sm-none {
            display: none!important;
        }
        .lg-show {
            display: flex!important;
        }
        
        @media only screen and (max-width: 576px) {
            .sm-mt-6 {
                margin-top: 1.5rem;
            }
        }
        
        @media only screen and (min-width: 576px) {
            .sm-none {
                display: flex!important;
            }
            .lg-show {
                display: none!important;
            }
        }
        
        .chevron-down, .chevron-up {
            cursor: pointer;
        }
        .dot-orange {
            height: 16px;
            width: 16px;
            background: #fa690f;
            border-radius: 50%;
        }
        
        .stories-absolute {
            padding-right: 20px;
            position: absolute;
            left: 20px;
            top: 10px;
            font-weight: 500;
            color: white;
        }
        
        .lang_ar .stories-absolute {
            padding-right: unset;
            padding-left: 20px;
            right: 20px;
            left: unset;
        }
        
        .line {
            position: absolute;
            left: 8px;
            top: 15px;
            height: 100%;
            width: 1px;
            background-color: #fa690f;
            margin-bottom: 5px;
        }
        
        .block-line {
            height: 1px;
            width: 100%;
            background-color: #fa690f;
            margin-bottom: 5px;
        }
        
        
        .line-personas {
            position: absolute;
            left: 47%;
            height: 100%;
            width: 0.5px;
            background-color: #666666;
            margin-bottom: 5px;
        }
        
        .lang_ar .line {
            left: unset;
            right: 8px;
        }
        
        .lang_ar .line-personas {
            left: 53%;
        }
        
        .w-5rem {
            width: 6rem;
        }
        
        .ml-5 {
            margin-left: 2rem;
        }
        
        .lang_ar .ml-5 {
            margin-left: unset;
            margin-right: 2rem;
        }
        
        .px-2rem {
            padding: 0 2rem;
        }
    
        .regenerate-animation {
          animation-name: slideInRight;
          animation-duration: 0.5s;
        }
        
        @keyframes slideInRight {
          from {
            transform: translateX(80px);
            opacity: 0;
          }
          to {
            transform: translateX(0);
            opacity: 1;
          }
        }
        @keyframes rotateSlowly {
          0% {
            transform: rotate(0deg);
          }
          100% {
            transform: rotate(-360deg);
          }
        }
        
        .rotate-slowly {
          animation: rotateSlowly 1.2s linear infinite;
        }
        .card-hover {
            cursor: pointer;
        }
        .card-hover:hover .group-hover\:opacity-100 {
            opacity: 1;
        }
        .card-hover:hover .group-hover\:translate-x-\[3px\] {
            transform: matrix(1, 0, 0, 1, 3, 0);
        }
        .bg-gradient-to-b {
            background-image: linear-gradient(to bottom, rgba(48, 33, 64, 0), rgba(64, 64, 64, 0.7));
        }
        @media only screen and (min-width: 1024px) {
            .lg\:w-\[200px\] {
                width: 200px;
            }
            .lg\:h-\[200px\] {
                height: 200px;
            }
            .lg\:text-8xl {
                font-size: 6rem !important;
                line-height: 1 !important;
            }
        }
        div.max-w-4xl {
            width: 56rem;
        }
        .w-2\/4 {
            width: 50%;
        }
        .h-2\/4 {
            height: 50%;
        }
        .w-4\/6 {
            width: 66.66%;
        }
        .w-2\/6 {
            width: 33.33%;
        }
        .h-2\/6 {
            height: 33.33%;
        }
        .w-90 {
            width: 90%;
        }
        .w-\[calc\(50\%-32px\)\] {
            width: calc(50% - 32px);
        }
        .-left-1\/2 {
            left: -50%;
        }
        .-rotate-2{
            transform: matrix(0.999391, -0.0348995, 0.0348995, 0.999391, 0, 0)
        }
        .rotate-2 {
            transform: matrix(0.999391, 0.0348995, -0.0348995, 0.999391, 0, 0)
        }
        .w-10 {
            width: 2.5rem;
        }
        .h-\[110\%\] {
            height: 110%;
        }
        .-ml-8 {
            margin-left: -2rem;
        }
        .-mr-8 {
            margin-right: -2rem;
        }
        div.left-1\/2 {
            left: 70%;
        }
        div.w-1\/2 {
            width: 40%;
        }
        .top-\[-5\%\] {
            top: -5%;
        }
        .bg-slate-900 {
            background-color: rgb(15 23 42);
        }
        div.bg-gray-50 {
            background-color: rgb(249 250 251)!important;
        }
        div.bg-gray-100 {
            background-color: rgb(243 244 246)!important;
        }
        div.bg-gray-150 {
            background-color: rgb(229 231 235)!important;
        }
        div.bg-gray-200 {
            background-color: rgb(209 213 219)!important;
        }
        div.bg-gray-250 {
            background-color: rgb(156 163 175)!important;
        }
        div.text-sm {
            font-size: .875rem;
            line-height: 1.25rem;
        }
        .list-circles {
            list-style-type: circle;
        }
        .bg-industry-box {
            background: rgb(249 243 233);
        }
        div.font-medium, span.font-medium {
            font-weight: 600;
        }
        span.text-sm {
            font-size: .75rem;
            line-height: 1.1rem;
        }
        .edit-position-ci {
            right: 0px;
            top: -5px;
        }
        .lang_ar .edit-position-ci {
            right: unset;
            left: 0px;
            top: -5px;
        }
        .question-container {
            margin-bottom: 20px;
        }
        #customerInterview-body ul {
            list-style: circle;
        }
        #customerInterview-body .px-3 {
            font-weight: 500;
            color: #666666;
        }
        @media only screen and (max-width: 576px) {
            .px-2rem {
                padding: 0;
            }
        }
        @media only screen and (max-width: 768px) {
            div.max-w-4xl {
                width: unset;
            }
            #customerInterview-body .px-3 {
                font-size: 14px;
            }
            #customerInterview-body ul {
                list-style: none;
            }
        }
        .marketing-tab .icon svg {
            fill: white;
        }

        .marketing-tab.pro-feature .icon svg {
            fill: none;
        }
        
        /* CSS cho overlay */
        .overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(255, 255, 255, 0.7);
            z-index: 39;
            display: none;
        }
        
        #overview .overlay {
            height: 300px;
        }

        #overview .blur-content {
            height: 300px;
        }
        
        /* CSS cho spinner */
        .loading-spinner {
            /* Định dạng spinner theo ý muốn */
        }
        .loading {
            position: relative;
        }
        
        /* CSS để hiển thị loading khi cần */
        .loading .overlay {
            display: block; /* Hiển thị overlay khi thẻ card có lớp 'loading' */
        }
    
        .draggable {
          cursor: move;
        }
        
        .justify-content-right {
            justify-content: right;
        }
        .white-space-pre-line {
            white-space: pre-line;
        }
        .theme-dark .card.shadow {
            box-shadow: none!important;
        }
        .image-background {
            margin-top: 0px !important;
            background-image: url('/assets/img/browser_mockup.png');
            background-size: cover;
            background-repeat: no-repeat;
            min-height: 300px;
        }
        .image-iframe {
            min-height: 240px;
        }
        
        @media only screen and (min-width: 1200px) {
            .image-background {
                min-height: 400px;
            }
            .image-iframe {
                min-height: 330px;
            }
        }
        h6 .show-anwser:hover {
            transform: unset;
        }
        .table-vcenter.table {
            table-layout: unset;
        }
        tbody,td,tfoot,th,thead,tr {
            border-color: var(--tblr-border-color)
        }
        
        tr:last-child td {
            border-bottom: none
        }
        
        .table-vcenter>:not(caption)>*>* {
            padding: 1.1rem
        }
        
        .table thead th {
            padding-top: 1.75em;
            padding-bottom: 1.05em;
            border-color: var(--tblr-border-color);
            background: none;
            font-weight: var(--tblr-font-weight-medium)
        }
        .edit-popup {
            display: none;
        }
        .bg-edit {
            background-color: transparent;
        }
        .bottom-40 {
            bottom: 9rem;
        }
        .lang_ar .card-list-flush-Done {
            border-right: 1px solid #eff2f7 !important;
        }
        .lang_ar th, .lang_ar button, .lang_ar p, .lang_ar td {
            text-align: right !important;
            direction: rtl;
        }
        .lang_ar ul {
            padding-inline-start: 0px;
        }
        .lang_ar .position-popup-100 {
            margin-right: -150px;
        }
        .lang_ar .show-anwser {
            justify-content: right !important;
        }
        .lang_ar .edit-popup {
            left: 5px;
        }
        .lang_ar .position-edit-15 {
            left: 15px;
            right: unset;
        }
        .lang_ar .postion-edit-5 {
            left: 5px;
            right: unset;
        }
        .postion-edit-0-15 {
            top: 0px;
            right: 15px;
        }
        .lang_ar .postion-edit-0-15 {
            left: 15px;
            right: unset;
        }
        .lang_ar .postion-edit-10 {
            left: 10px;
            right: unset;
        }
        .lang_ar .postion-edit-5-15 {
            left: -15px;
            right: unset;
        } 
        
        .position-edit-0-0 {
            top: 0px;
            right: 0px;
        }
        
        .pr-2 {
            padding-right: 20px;
        }
        
        .lang_ar .pr-2 {
            padding-right: unset;
            padding-left: 20px;
        }
        
        .lang_ar .position-edit-0-0 {
            left: 0px;
            right: unset;
        }
        
        .lang_ar .position-edit-5-0 {
            left: -5px;
            right: unset;
        }
        .position-edit-5-0 {
            top: 0px;
            right: -10px;
        }
        .postion-edit-10 {
            top: 10px;
            right: 10px;
        }
        .postion-edit-5 {
            z-index: 999;
            top: 5px;
            right: 5px;
        }
        .postion-edit-5-15 {
            top: 5px;
            right: -15px;
        }
        .position-edit-15 {
            top: 15px;
            right: 15px;
        }
        #overlay {
            position: absolute;
            top: 0;
            z-index: 100;
            width: 100%;
            height: 100%;
            display: none;
            background: rgba(0, 0, 0, 0.6);
        }

        .cv-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .spinner {
            width: 40px;
            height: 40px;
            border: 4px #ddd solid;
            border-top: 4px rgb(28, 166, 133) solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        }
        
        .loading .card-body {
            display: none !important;
        }

        @keyframes sp-anime {
            100% {
                transform: rotate(360deg);
            }
        }

        .is-hide {
            display: none;
        }

        .btn-success {
            --bs-btn-bg: #21a29a !important;
            border: none;
        }

        .btn-success:hover {
            --bs-btn-bg: #2a2a2a !important;
            border: none;
        }

        .btn-danger {
            --bs-btn-bg: #fa690e !important;
            border: none;
        }

        .btn-danger:hover {
            --bs-btn-bg: #2a2a2a !important;
            border: none;
        }

        #swot-plus @keyframes gradientAnimation {
            0% {
                background-position: 0% 50%;
            }

            100% {
                background-position: 100% 50%;
            }
        }

        body.theme-10 .bg-primary {
            background: linear-gradient(45deg, #14a299, #36c291, #14a299, #36c291, #14a299) !important;
            background-size: 200% 200% !important;
            animation: gradientAnimation 10s linear infinite !important;
        }

        .card-faq {
            border: none;
        }

        .card-faq-header {
            display: block !important;
            border: none;
        }

        .card-faq-header button {
            width: 100%;
            padding: 10px 20px;
            font-size: 1.2em;
            font-weight: bold;
            color: #333;
            display: flex;
            justify-content: left;
            align-items: center
        }

        .card-faq-body {
            border-top: 1px solid #eee;
            display: none;
        }

        .card-faq-body.show {
            display: block;
        }

        /* Style for the name squares */
        #startupNaming>div {
            width: 330px;
            /* Adjust the width to your preference */
            height: 100px;
            /* Set the height equal to the width for squares */
            background-color: #077784;
            /* Background color for the squares */
            color: #fff;
            /* Text color */
            align-items: center;
            justify-content: center;
            text-align: center;
            /* Center text vertically */
            margin: 5px;
            /* Add spacing between squares */
            display: flex;
            padding: 10px;
            /* Display as inline-block to appear in a row */
            border-radius: 5px;
            /* Rounded corners (optional) */
            font-size: 14px;
            /* Font size for the text */
            font-weight: bold;
            /* Bold text (optional) */
        }

        /* Style for the tab content container (adjust as needed) */
        .tab-content {
            margin-top: 10px;
            /* Add space between tabs and content */
        }

        /* Style for the "Not generated" message (adjust as needed) */
        .tab-content:empty::before {
            content: "Not generated.";
            font-weight: bold;
            color: #d35400;
            /* Custom color for the message */
            display: block;
            text-align: center;
            margin-top: 10px;
            /* Adjust spacing */
        }

        /* Tab container */
        .tab-container {
            display: flex;
            margin-bottom: 20px;
        }

        /* Individual tab */
        .tab {
            padding: 10px 20px;
            cursor: pointer;
            border: 1px solid #cccccc;
            border-bottom: none;
            background-color: #f9f9f9;
            margin-right: 8px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            transition: background-color 0.2s ease-in-out;
        }

        /* Active tab */
        /* Tabs container */
        .tab-container {
            display: flex;
            margin-bottom: 1rem;
            /* Space below the tabs */
        }

        /* Individual tab */
        .tab {
            flex: 1;
            /* Each tab takes up equal width */
            text-align: center;
            /* Center the text inside the tab */
            padding: 0.75rem 1rem;
            cursor: pointer;
            border: 1px solid #ccc;
            /* Light grey border */
            background-color: #f9f9f9;
            /* Light grey background */
            transition: background-color 0.3s, border-color 0.3s;
        }

        /* Active tab */
        .tab.active {
            background-color: #fa690e;
            /* Orange background */
            color: white;
            /* White text */
            border-color: #fa690e;
            /* Orange border */
        }

        /* Non-active tabs hover effect */
        .tab:not(.active):hover {
            background-color: #f3f3f3;
            /* Slightly darker grey on hover */
        }

        /* Tab content container */
        .tab-content {
            display: none;
            /* Hide all tab content by default */
            padding: 1rem;
            border-top: none;
            /* Remove top border */
        }

        /* Display the active tab content */
        .tab-content.active {
            display: block;
        }
        
        .theme-dark .tab-header, .theme-dark .tab {
            color: rgb(97, 104, 118);
        }
        
        .theme-dark #storytelling-body .tab.active {
            color: white;
        }
        
        .theme-dark .canvas-block {
            border: 1px rgba(255, 255, 255, 0.04) solid;
        }
        
        .theme-dark .card-faq-header button {
            color: #f8fafc;
        }
        
        .theme-dark .table tbody tr:hover {
            background-color: rgb(97, 104, 118);
        }

        .tab-header {
            font-size: 13px;
            padding: 9px 6px;
            flex: 1;
            text-align: center;
            cursor: pointer;
            border: 1px solid #ccc;
            background-color: #f9f9f9;
            transition: background-color 0.3s, border-color 0.3s;
        }
        .tab-header.active {
            background-color: #fa690e;
            color: white;
            border-color: #fa690e;
        }

        .table {
            width: 100%;
            table-layout: fixed;
            /* This makes the table layout fixed */
            border-collapse: collapse;
        }

        .table th,
        .table td {
            padding: 10px;
            overflow-wrap: break-word;
            /* Ensure that words can be broken and wrapped */
            word-wrap: break-word;
            /* Legacy property for older browsers */
            white-space: normal;
            /* Ensures that the whitespace is handled normally */
        }

        /* Table header */
        .table thead th {
            background-color: #f2f2f2;
            /* Light grey header */
            padding: 1rem;
            text-align: left;
        }

        /* Table body */
        .table tbody td {
            padding: 1rem;
            text-align: left;
            border-top: 1px solid #e0e0e0;
            /* Light grey row borders */
        }

        /* Hover effect for rows */
        .table tbody tr:hover {
            background-color: #f9f9f9;
        }
        
        .blur-content-card {
            /*opacity: 0.1;*/
            
        }
        .blur-content {
            height: 80vh;
        }
        
        .blur-content .loading-spinner {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            border: 8px solid #f3f3f3;
            border-top: 8px solid #3498db;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            animation: spin 1s linear infinite; /* Sử dụng animation có tên là 'spin' */
        }
    
        @keyframes spin {
            0% { transform: rotate(0deg); } /* Điểm bắt đầu */
            100% { transform: rotate(360deg); } /* Điểm kết thúc */
        }

        .mesh-loader {
            position: fixed;
            /* Fixed position to cover the entire viewport */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255, 255, 255, 0.8);
            /* Semi-transparent background */
            z-index: 9999;
            /* High z-index to keep it above other content */
            display: flex;
            justify-content: center;
            /* Center horizontally */
            align-items: center;
            /* Center vertically */
        }

        .mesh-loader .circle {
            width: 25px;
            height: 25px;
            position: absolute;
            background: #fd661a;
            border-radius: 50%;
            margin: -12.5px;
            -webkit-animation: mesh 3s ease-in-out infinite;
            animation: mesh 3s ease-in-out infinite -1.5s;
        }

        .mesh-loader>div .circle:last-child {
            -webkit-animation-delay: 0s;
            animation-delay: 0s;
        }

        .mesh-loader>div {
            position: absolute;
            top: 50%;
            left: 50%;
        }

        .mesh-loader>div:last-child {
            -webkit-transform: rotate(90deg);
            transform: rotate(90deg);
        }

        @-webkit-keyframes mesh {
            0% {
                -webkit-transform-origin: 50% -100%;
                transform-origin: 50% -100%;
                -webkit-transform: rotate(0);
                transform: rotate(0);
            }

            50% {
                -webkit-transform-origin: 50% -100%;
                transform-origin: 50% -100%;
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }

            50.00001% {
                -webkit-transform-origin: 50% 200%;
                transform-origin: 50% 200%;
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform-origin: 50% 200%;
                transform-origin: 50% 200%;
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes mesh {
            0% {
                -webkit-transform-origin: 50% -100%;
                transform-origin: 50% -100%;
                -webkit-transform: rotate(0);
                transform: rotate(0);
            }

            50% {
                -webkit-transform-origin: 50% -100%;
                transform-origin: 50% -100%;
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }

            50.00001% {
                -webkit-transform-origin: 50% 200%;
                transform-origin: 50% 200%;
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform-origin: 50% 200%;
                transform-origin: 50% 200%;
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        
        .name-absolute {
            position: absolute;
            bottom: 0px;
            color: white;
            font-weight: 600;
            margin-left: 20px;
        }
        
        .trait-square {
            margin-top: 5px;
            padding: 5px;
            background: #fa690f;
            color: white;
            border-radius: 5px;
            margin-right: 5px;
        }
        
        .box-strengths { grid-area: strengths; }
        .box-weaknesses { grid-area: weaknesses; }
        .box-opportunities { grid-area: opportunities; }
        .box-threats { grid-area: threats; }

        .swot-container {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-template-areas:
            "strengths weaknesses"
            "opportunities threats";
            gap: 20px;
            max-width: 100%;
        }
        
        .lang_ar .swot-container {
            grid-template-areas:
            "weaknesses strengths"
            "threats opportunities";
            
        }
        
        @media only screen and (max-width: 600px) {
            .swot-container {
                grid-template-columns: repeat(1, 1fr);
            }
        }
        
        @media only screen and (max-width: 700px) {
            .lean-canvas {
                display: block !important;
            }
        }

        .swot-section {
            border-radius: 18px;
            padding: 40px 50px 40px 50px;
            color: #666666;
            border: 1px #979797 solid;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100%;
        }
        
        #overview .swot-section {
            padding: 25px 30px;
        }

        .strengths {
            background-color: #fa690e;
            /* Light blue */
        }

        .weaknesses {
            background-color: #21a29a;
            /* Light orange */
        }

        .opportunities {
            background-color: #404040;
            /* Light green */
        }

        .threats {
            background-color: #000;
            /* Light red */
        }

        .swot-title {
            font-size: 18px;
            color: black;
        }

        .swot-letter {
            font-size: 72px;
            font-weight: bold;
            opacity: 0.2;
            margin-top: -30px;
            margin-bottom: 30px;
        }

        .lean-canvas {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            grid-gap: 10px;
            max-width: 100%;
            margin: 20px auto;
            font-family: Arial, sans-serif;
        }

        .canvas-block {
            border: 1px solid #e9e9e9;
            padding: 8px;
            border-radius: 5px;
        }

        .container {
            width: 100%;
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));
            grid-gap: 0.75rem;
        }

        .box {
            position: relative;
            width: auto;
            border-radius: 10px !important;
            height: 125px;
            background: #fff;
            border-radius: 5px;
            cursor: pointer;
            box-shadow: 10px 10px 15px rgba(59, 38, 38, 0.25);
        }

        .box::before {
            border-radius: 10px !important;

            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 6px;
            height: 100%;
            background: var(--color);
            transition: 0.5s ease-in-out;
        }

        .box:hover::before {
            width: 100%;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
        }


        .box .content {
            position: relative;
            display: flex;
            align-items: center;
            height: 100%;
        }

        .box .content .icon {
            position: relative;
            min-width: 70px;
            display: flex;
            justify-content: center;
            align-items: center;
            font-size: 1.5em;
            color: var(--color);
            transition: 0.5s ease-in-out;
        }

        .box:hover .content .icon {
            color: #fff;
        }

        .box .content .text h3 {
            font-weight: 500;
            color: var(--color);
            transition: 0.5s ease-in-out;
        }

        .box .content .text p {
            font-size: 0.9em;
            color: #999;
            transition: 0.5s ease-in-out;
        }

        .box:hover .content .text h3,
        .box:hover .content .text p {
            color: #fff;
        }



        /* Assigning grid areas */
        #problem {
            grid-area: 1 / 1 / 1 / 1;
        }

        #solution {
            grid-area: 1 / 2 / 1 / 2;
        }

        #unique-value-prop {
            grid-area: 1 / 3 / 3 / 3;
        }

        #unfair-advantage {
            grid-area: 1 / 5 / 3 / 5;
        }

        #customer-segments {
            grid-area: 1 / 4 / 2 / 4;
        }

        #existing-alternatives {
            grid-area: 2 / 1 / 2 / 1;
        }

        #key-metrics {
            grid-area: 2 / 2 / 2 / 2;
        }

        #channels {
            grid-area: 2 / 4 / 2 / 4;
        }

        #revenue-streams {
            grid-area: 3 / 3 / 3 / 6;
        }

        #cost-structure {
            grid-area: 3 / 1 / 3 / 3;
        }
        
        #vision {
            grid-area: 1 / 1 / 1 / 1;
        }
        
        #mission {
            grid-area: 1 / 2 / 1 / 2;
        }
        
        #brand_positioning {
            grid-area: 1 / 3 / 1 / 3;
        }
        
        #personality {
            grid-area: 1 / 4 / 2 / 4;
        }
        
        #brand_values {
            grid-area: 2 / 5 / 2 / 1;
        }

        h2 {
            font-size: 18px !important;
            color: #333;
            margin-bottom: 10px;
        }
        
        #storytelling-content .lean-canvas {
            grid-template-columns: repeat(4, 1fr);
        }


        /* You might want to add media queries to ensure responsiveness */
        @media (max-width: 768px) {
            .lean-canvas {
                grid-template-columns: 1fr;
                grid-template-rows: auto;
                grid-template-areas:
                    "problem"
                    "solution"
                    "unique-value-prop"
                    "unfair-advantage"
                    "customer-segments"
                    "existing-alternatives"
                    "key-metrics"
                    "channels"
                    "cost-structure"
                    "revenue-streams";
            }

            .canvas-block {
                margin-bottom: 10px;
            }
        }


        .horizontal-menu {
            display: flex;
            justify-content: start;
            background-color: #fff;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            border-radius: 4px;
            border: 1px solid lightgrey;
            margin-top: 15px;
            margin-bottom: 15px;

        }

        .menu-item {
            position: relative;
            display: flex;
            align-items: center;
            padding: 18px 30px;
            border-right: 1px solid lightgrey;
            cursor: pointer;
            width: 500px;
            justify-content: space-between;
        }

        .menu-item:hover {
            background: #ff640f;
            transition: 0.4s;
        }

        .menu-item:hover .menu-text {
            color: #fff;
            transition: 0.4s;
        }

        .menu-item:hover .dropdown-icon {
            color: #fff;
            transition: 0.4s;
        }

        .menu-item:last-child {
            border-right: none;
        }

        .menu-text {
            margin-right: 5px;
            font-size: 15px;
            font-weight: 600;
        }

        .dropdown-icon {
            margin: 0 5px;
            width: 100px;
        }

        .counter {
            height: 30px;
            width: 30px;
            padding: 15px;
            background-color: #e9e9e9;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            font-weight: 500;
            font-size: 0.9em;
        }

        .pro-pill {
            background-color: #20a29b;
            color: #fff;
            font-size: 12px;
            padding: 3px 8px;
            border-radius: 10px;
            margin-left: 8px;
            display: inline-block;
        }
        
        .list-group-item.fs-5 {
            padding: 3px;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            top: 100%;
            left: 0;
            background-color: #f9f9f9;
            min-width: 100%;
            box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
            z-index: 1;
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
            transition: 0.6s;

        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }
        
        .pro-feature {
            color: #969696!important;
            pointer-events: none!important;
        }

        .menu-item:hover .dropdown-content {
            display: block;
        }

        .menu-item:hover .counter {
            background: #fff;
        }

        /* Optional: Add some styling for when the dropdown is active */
        .menu-item.active .dropdown-content {
            display: block;
        }

        /* Optional: Style for the dropdown icon when active */
        .menu-item.active .dropdown-icon {
            transform: rotate(180deg);
        }

        .wrapper {
            text-align: center;
            margin-bottom: 20px;
            margin-top: -50px;
        }

        .tabs {
            margin-top: 50px;
            font-size: 15px;
            padding: 0px;
            list-style: none;
            background: #fff;
            box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.1);
            display: inline-block;
            border-radius: 50px;
            position: relative;
        }

        .tabs a {
            text-decoration: none;
            color: #777;
            text-transform: uppercase;
            padding: 10px 20px;
            display: inline-block;
            position: relative;
            z-index: 1;
            transition-duration: 0.6s;
        }

        .tabs a.active {
            color: #fff;
        }

        .tabs a i {
            margin-right: 5px;
        }

        .tabs .selector {
            height: 100%;
            display: inline-block;
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: 1;
            border-radius: 50px;
            transition-duration: 0.6s;
            transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);

            background: #05abe0;
            background: -moz-linear-gradient(45deg, #05abe0 0%, #8200f4 100%);
            background: -webkit-linear-gradient(45deg, #05abe0 0%, #8200f4 100%);
            background: linear-gradient(45deg, #ff640f 0%, #b74304 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#05abe0', endColorstr='#8200f4', GradientType=1);
        }
        .card {
            margin-top: 15px;
        }
        .btn-tooltip {
          position: relative;
          display: inline-block;
          border-bottom: 1px dotted black;
        }
        
        .tooltiptext {
            visibility: visible;
            position: absolute;
            width: 120px;
            background-color: #555;
            color: #fff;
            text-align: center;
            padding: 5px 0;
            border-radius: 6px;
            z-index: 100;
            opacity: 0;
            transition: opacity 2s ease;
        }
        .btn-tooltip .tooltiptext::after {
            content: '';
            position: absolute;
            top: 50%;
            right: -10px; /* Điều chỉnh giá trị này để điều chỉnh vị trí của tam giác */
            border-width: 5px;
            border-style: solid;
            border-color: transparent transparent transparent #555; /* Thay đổi màu sắc tùy theo nhu cầu */
            transform: translateY(-50%);
        }
        
        .tooltip-left2 {
            top: 15px;
            bottom: auto;
            right: 105%;
        }
        
        .lang_ar .tooltip-left2 {
            right: unset;
            left: 105%;
        }
        
        .lang_ar .btn-tooltip .tooltiptext::after {
            top: 50%;
            right: unset;
            left: -9px; /* Điều chỉnh giá trị này để điều chỉnh vị trí của tam giác */
            transform: translateY(-50%);
            border-color: transparent #555 transparent transparent;
        }
        
        .btn-tooltip:hover .tooltiptext {
          visibility: visible;
        }
        .lqd-generators-container .grid {
            gap: unset;
        }
        .lqd-filter-list .lqd-btn.active {
            background-color: hsl(var(--foreground) / .05)!important;
        }
        .lqd-favorite-icon.h-4 {
            height: 1rem!important;
        }
        .lqd-favorite-icon.w-4 {
            width: 1rem!important;
        }
        .lqd-icon.mb-5 {
            margin-bottom: 10px!important;
        }
        @media only screen and (min-width: 1320px) {
            .lqd-generators-container .lqd-card {
                padding-left: 4rem!important;
                padding-right: 4rem!important;
            }
        }
        @media only screen and (min-width: 922px) {
            .lqd-generators-container .lqd-card {
                padding-left: 2.5rem!important;
                padding-right: 2.5rem!important;
            }
        }
        
        @media only screen and (max-width: 600px) {
            .card-table .td-truncate {
                min-width: 100px;
            }
        }
        .draggable-item {
            cursor: -webkit-grab;
            cursor: grab;
        }
        .col-12.gu-mirror {
            position: fixed !important;
            margin: 0 !important;
            z-index: 9999 !important;
            opacity: 1;
            transform: translate3d(-4px, -4px, 0) rotateZ(2deg);
            cursor: -webkit-grabbing;
            cursor: grabbing;
            box-shadow: 0 1rem 3rem rgba(18, 38, 63, 0.125);
        }
        .draggable-item.gu-transit {
            opacity: 0.2;
        }
        
        .gu-hide {
            display: none !important;
        }
        
        .gu-unselectable {
            -webkit-user-select: none !important;
            -moz-user-select: none !important;
            -ms-user-select: none !important;
            user-select: none !important;
        }
        
        .gu-transit {
            opacity: .1;
        }
        .mt-3 {
            margin-top: 15px;
        }
        
        .logo-wrapper {
            position: relative;
            overflow: hidden;
        }
        
        .overlay-logo {
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            transition: opacity 0.3s ease;
        }
        
        .selected-icon {
            width: 30px;
            height: 30px;
            background: white;
            border-radius: 50%;
            display: none;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: green;
            font-size: 24px;
            transition: opacity 0.3s ease;
        }
        .hover\:bg-black\/5:hover {
            background-color: rgba(0, 0, 0, .05);
        }
        .fs-20 {
            font-size: 20px !important;
        }
        .fs-30 {
            font-size: 30px !important;
        }
        .fs-1rem {
            font-size: 0.875rem !important;
        }
        .fs-18 {
            font-size: 1rem !important;
        }
        .fs-14 {
            font-size: 0.7rem !important;
        }
        .fs-15 {
            font-size: 0.75rem !important;
        }
        
        .custom-contaier {
            margin-left: 3rem;
            margin-right: 3rem;
            padding-right: 1rem;
            padding-left: 1rem;
            padding-top: 1rem;
        }
        
        @media only screen and (max-width: 500px) {
            .custom-contaier {
                margin-left: unset;
                margin-right: unset;
            }
        }
        
        @media only screen and (min-width: 1000px) {
            .fs-15 {
                font-size: 1rem !important;
            }
        }
        .move-center {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
        }
        
        .block-important {
            display: block !important;
        }
    </style>
@endsection
@section('pretitle', '')
@section('no_title', '')
@section('titlebar_actions', '')
@section('content')
    <div class="custom-contaier truncate mb-3">
        <p class="fs-30 font-bold" style=>{{ __($project->name) }}</p>
    </div>
    
    <div class="group float-button float-plus-botton fixed bottom-20 end-12 z-50 lg:block 1 lang_{{app()->getLocale()}}">
        <button
            class="btn-tooltip collapsed peer h-14 w-14 translate-x-0 transform-gpu rounded-full border-none !shadow-lg !outline-none" style="background: linear-gradient(to left, #00A39A 0%, #21E4D9 59%);"
            type="button">
            <img src="{{ url('').'/images/float-button-icon.png' }}" width="20" height="20" class="mx-auto" alt="info-icon">
        </button>
        <div class="!invisible absolute position-right !mb-4 !opacity-0  before:absolute before:inset-x-0 before:-bottom-4 before:h-4 group-hover:!visible block-important group-hover:translate-y-0 group-hover:!opacity-100 dark:bg-zinc-800"
            id="add-new-floating" style="width: 250px; bottom: 45px; display: block; background: transparent">
            <div class="mb-3">
                @include('panel.chatbot.widget-gtm-expert')
            </div>
            <div class="mb-3">
                @include('panel.chatbot.widget-finance-expert')
            </div>
            <div>
                @include('panel.chatbot.widget-product-expert')
            </div>
        </div>
    </div>
    @if (isset($type) && $type == 'swotAnalysis')
        @include('panel.user.openai.concept.swot-analysis')
    @elseif (isset($type) && $type == 'viabilityAnalysis')
        @include('panel.user.openai.concept.viability-analysis')
    @elseif (isset($type) && $type == 'leanCanvas')
        @include('panel.user.openai.concept.lean-canvas')
    @elseif (isset($type) && $type == 'userPersonas')
        @include('panel.user.openai.concept.user-personas')
    @elseif (isset($type) && $type == 'userStories')
        @include('panel.user.openai.concept.user-stories')
    @elseif (isset($type) && $type == 'customerInterviewQuestions')
        @include('panel.user.openai.concept.customer-interview-questions')
    @elseif (isset($type) && $type == 'marketSizeTrends')
        @include('panel.user.openai.concept.market-size-trends')
    @elseif (isset($type) && $type == 'industries')
        @include('panel.user.openai.concept.industries')
    @elseif (isset($type) && $type == 'marketingOverview')
        @include('panel.user.openai.marketing.marketing-overview')
    @elseif (isset($type) && $type == 'brandWheel')
        @include('panel.user.openai.marketing.brand-wheel')
    @elseif (isset($type) && $type == 'marketingContent')
        @include('panel.user.openai.marketing.marketing-content')
    @elseif (isset($type) && $type == 'landingPages')
        @include('panel.user.openai.launch.landing-pages')
    @elseif (isset($type) && $type == 'tasksManagement')
        @include('panel.user.openai.launch.tasks-management')
    @elseif (isset($type) && $type == 'brandIdentity')
        @include('panel.user.openai.marketing.brand-identity')
    @else
        @include('panel.user.openai.details')
    @endif
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="border: #333 0.5px solid">
            <div class="modal-header">
                <h5 class="modal-title" id="popup-title">Edit</h5>
            </div>
            <div class="modal-body">
                <div class="form-floating">
                    <textarea class="form-control" placeholder="Leave a comment here" id="popup-value" style="height: 100px"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <input class="d-none" data-value="{{ app()->getLocale() }}" 
                data-generate="{{ __('Generating...') }}" data-project-id="{{ $project->id }}" data-project-type="{{ $project->type }}"
                data-route-idea-show ="{{ route('dashboard.user.ideas.show', ':project') }}"
                data-route-regenerate ="{{ route('projects.regenerate-relate-information') }}"
                data-route-delete-landing ="{{ route('projects.delete-landing-page') }}"
                data-route-update ="{{ route('projects.update-relate-information') }}"
                data-route-delete ="{{ route('projects.delete-relate-information') }}"
                data-route-pregenerate-swot ="{{ route('projects.pregenerate-swot', ['project' => $project->id]) }}"
                data-route-pregenerate-lean-canvas ="{{ route('projects.pregenerate-lean-canvas', ['project' => $project->id]) }}"
                data-route-pregenerate-storytelling ="{{ route('projects.pregenerate-storytelling', ['project' => $project->id]) }}"
                data-route-pregenerate-user-personas ="{{ route('projects.pregenerate-user-personas', ['project' => $project->id]) }}"
                data-route-pregenerate-user-stories ="{{ route('projects.pregenerate-user-stories', ['project' => $project->id]) }}"
                data-route-pregenerate-competitive-analysis ="{{ route('projects.pregenerate-competitive-analysis', ['project' => $project->id]) }}"
                data-route-pregenerate-customer-interview ="{{ route('projects.pregenerate-customer-interview', ['project' => $project->id]) }}"
                
                data-route-pregenerate-industry-overview ="{{ route('projects.pregenerate-industry-overview', ['project' => $project->id]) }}"
                data-route-pregenerate-marketing-overview ="{{ route('projects.pregenerate-marketing-overview', ['project' => $project->id]) }}"
                data-route-pregenerate-market-size ="{{ route('projects.pregenerate-market-size', ['project' => $project->id]) }}"
                data-route-pregenerate-viability-analysis ="{{ route('projects.pregenerate-viability-analysis', ['project' => $project->id]) }}"
                data-route-pregenerate-brand-identity ="{{ route('projects.pregenerate-brand-identity', ['project' => $project->id]) }}"

                data-generate-logo="{{ route('projects.generate-logo', ['project' => $project->id]) }}"
                id='save-locale' />
                <input class="d-none" data-value="" id='save-data' />
                <button type="button" class="btn btn-secondary hide-popup" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary hide-popup save" id="popup-save">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="chatModalLabel" aria-hidden="true" style="z-index: 41 !important;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="chatModalLabel">{{ __('Validation') }}</h2>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    
@endpush
@push('script')
    <script src="{{ custom_theme_url('assets/libs/autosize/dist/autosize.min.js') }}"></script>
    <script src="{{ custom_theme_url('assets/js/panel/detail.js') }}"></script>
    <script src="{{ custom_theme_url('assets/js/panel/colorPick.js') }}"></script>
    <script src="{{ custom_theme_url('assets/libs/dragula/dist/dragula.min.js') }}"></script>
    <script src="{{ custom_theme_url('/assets/libs/fslightbox/fslightbox.js') }}"></script>
    <script>
        let category = @json(getCategoryBySlug('finance-expert-oDKLz'));
        let category_finance = @json(getCategoryBySlug('finance-expert-oDKLz'));
        let category_gtm = @json(getCategoryBySlug('gtm-expert-EunT9'));
        let category_product = @json(getCategoryBySlug('product-expert-TDrof'));
        let prompt_prefix = '';
        
    </script>
    <script src="{{ custom_theme_url('/assets/js/panel/openai_chatbot_idea.js?v=1.0.0') }}"></script>
    <script>
        $(document).ready(function() {
            $('.openChatButton').click(function() {
                var dataType = $(this).data('type');
                var urlTemplate = "{{ LaravelLocalization::localizeUrl(route('dashboard.user.chat.chat', 'PLACEHOLDER')) }}";
                var url = urlTemplate.replace('PLACEHOLDER', dataType);

                $.ajax({
                    url: url,
                    method: 'GET',
                    success: function(response) {
                        $('#chatModal .modal-body').html(decodeHTMLEntities(response.html));
                        $('#chatModal').modal('show');
                        if (dataType == 'gtm-expert-EunT9') {
                            category = category_gtm;
                        } else if (dataType == 'product-expert-TDrof') {
                            category = category_product;
                        }
                        prompt_prefix = category.prompt_prefix + " you will now play a character and respond as that character (You will never break character). Your name is " + category.human_name + " but do not introduce by yourself as well as greetings.";
                        initChat();
                        updateChatButtons();
                        scrollConversationAreaToBottom();
                    },
                    error: function() {}
                });
            });
            
            function decodeHTMLEntities(text) {
                var textArea = document.createElement('textarea');
                textArea.innerHTML = text;
                return textArea.value;
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#close-generate-success').on("click", function () {
                $('#generate-success').hide();
                localStorage.setItem('close_generate_' + $('#save-locale').data('project-id'), 'hide');
            });
            if ($('.blur-content').length == 0) {
                if (localStorage.getItem('close_generate_' + $('#save-locale').data('project-id')) != 'hide') {
                    localStorage.setItem('close_generate_' + $('#save-locale').data('project-id'), 'hide');
                    $('#generate-success').show();
                    $('#under-generating').hide();
                }
            } else {
                $('#under-generating .count-number').html(9 - $(".blur-content").length);
                let percent = $(".blur-content").length * 9;
                $('#under-generating .percent-count').css('transform', 'translateX(-' + percent + '%)');
                $('#generate-success').hide();
                $('#under-generating').show();
            }
            if ($('#swot-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-swot');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#leanCanvas-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-lean-canvas');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#userPersonas-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-user-personas');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#userStories-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-user-stories');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#storytelling-overview-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-storytelling');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#customerInterview-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-customer-interview');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#viabilityAnalysis-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-viability-analysis');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#marketingOverview-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-marketing-overview');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#marketSize-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-market-size');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#industryOverview-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-industry-overview');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            if ($('#brandIdentity-body').hasClass('blur-content')) {
                let url = $('#save-locale').data('route-pregenerate-brand-identity');
                checkEmptyConcept();
                preGenerateProjectInfo(url);
            }
            function checkEmptyConcept() {
                if ($('#swot-body').hasClass('blur-content') || $('#userStories-body').hasClass('blur-content') || $('#leanCanvas-body').hasClass('blur-content') || $('#userPersonas-body').hasClass('blur-content') || $('#storytelling-body').hasClass('blur-content') || $('#viabilityAnalysis-body').hasClass('blur-content') || $('#marketingOverview-body').hasClass('blur-content') || $('#marketSize-body').hasClass('blur-content') || $('#industryOverview-body').hasClass('blur-content')) {
                    $('#add-new-floating').hide();
                    $('.tooltiptext').css('opacity', 1);
                    $('.tooltiptext').html('{{ __('Generating...') }}');
                    $('.spinner_button svg').hide();
                    $('.spinner-border').show();
                } else {
                    $('#add-new-floating').show();
                    $('.tooltiptext').css('opacity', 0);
                    $('.tooltiptext').html('');
                    $('.spinner_button svg').show();
                    $('.spinner-border').hide();
                }
            }
            
            function preGenerateProjectInfo(url) {
                let data = {
                    locale : $('#save-locale').data('value')
                };
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    success: function(response) {
                        if (response.swotAnalysis) {
                            location.reload();
                            // updateContentSwot(response.swotAnalysis);
                        }
                        
                        if (response.userPersonas) {
                            location.reload();
                            // updateContentUserPersonas(response.userPersonas);
                        }
                        
                        if (response.userStories) {
                            location.reload();
                            // updateContentUserStories(response.userStories);
                        }
                        
                        if (response.leanCanvas) {
                            location.reload();
                            // updateContentLeanCanvas(response.leanCanvas);
                        }
                        
                        if (response.storytelling) {
                            location.reload();
                            // updateContentStorytelling(response.storytelling);
                        }
                        
                        if (response.customerInterview) {
                            location.reload();
                            // updateContentCustomerInterview(response.customerInterview);
                        }
                        
                        if (response.viabilityAnalysis) {
                            location.reload();
                            // updateContentViabilityAnalysis(response.viabilityAnalysis);
                        }
                        
                        if (response.marketingOverview) {
                            location.reload();
                            // updateContentMarketingOverview(response.marketingOverview);
                        }
                        
                        if (response.marketSize) {
                            location.reload();
                            // updateContentMarketSize(response.marketSize);
                        }
                        
                        if (response.industryOverview) {
                            location.reload();
                            // updateContentIndustryOverview(response.industryOverview);
                        }
                        
                        if (response.brandIdentity) {
                            location.reload();
                        }
                    },
                    error: function(response) {
                        
                    }
                });
                return false;
            }
            
            function updateContentSwot(data) {
                var html = `
                <div class="card mt-0">
                    <div class="card-header d-block">
                        <div class="d-flex justify-content-between align-items-center">
                            <div>
                                <h5 class="mb-0">SWOT Analysis</h5>
                            </div>
                            <div>
                                <button onclick="regenerateProjectInfo(${data.project_id}, 'swotAnalysis')" class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                <button onclick="collapseByType('swot', 'minius')" id="swot-minius" class="btn btn-success btn-sm"><i class="ti ti-minus"></i></button>
                                <button onclick="collapseByType('swot', 'plus')" id="swot-plus" style="display: none" class="btn btn-success btn-sm"><i class="ti ti-plus"></i></button>
                                <button onclick="deleteProjectInfo(${data.project_id}, 'swotAnalysis', '#swot-body')" class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-1 p-sm-4" id="swot-content">
                        <div class="swot-container">
                            <!-- Strengths -->
                            <div class="swot-section strengths position-relative">
                                <div 
                                onclick='showEditPopup(${data.id}, "swotAnalysis", "strengths", "Strengths", "swotAnalysis-strengths")'
                                class="edit-popup-pre position-absolute position-edit-15" data-id="${data.id}" data-type="swotAnalysis" data-key="strengths" data-name="Strengths" data-value="${data.strengths}" role="button"><i class="ti ti-pencil-alt text-xl"></i></div>
                                <div class="swot-title">Strengths</div>
                                <div class="swot-letter">S</div>
                                <div class="swot-content white-space-pre-line" id="swotAnalysis-strengths">${data.strengths}</div>
                            </div>
                            <!-- Weaknesses -->
                            <div class="swot-section weaknesses position-relative">
                                <div 
                                onclick='showEditPopup(${data.id}, "swotAnalysis", "weaknesses", "Weaknesses", "swotAnalysis-weaknesses")'
                                class="edit-popup-pre position-absolute position-edit-15" data-id="${data.id}" data-type="swotAnalysis" data-key="weaknesses" data-name="Weaknesses" data-value="${data.weaknesses}" role="button"  ><i class="ti ti-pencil-alt text-xl"></i></div>
                                <div class="swot-title">Weaknesses</div>
                                <div class="swot-letter">W</div>
                                <div class="swot-content white-space-pre-line" id="swotAnalysis-weaknesses">${data.weaknesses}</div>
                            </div>
                            <!-- Opportunities -->
                            <div class="swot-section opportunities position-relative">
                                <div 
                                onclick='showEditPopup(${data.id}, "swotAnalysis", "opportunities", "Opportunities", "swotAnalysis-opportunities")'
                                class="edit-popup-pre position-absolute position-edit-15" data-id="${data.id}" data-type="swotAnalysis" data-key="opportunities" data-name="Opportunities" data-value="${data.opportunities}" role="button"><i class="ti ti-pencil-alt text-xl"></i></div>
                                <div class="swot-title">Opportunities</div>
                                <div class="swot-letter">O</div>
                                <div class="swot-content white-space-pre-line" id="swotAnalysis-opportunities">${data.opportunities}</div>
                            </div>
                            <!-- Threats -->
                            <div class="swot-section threats position-relative">
                                <div 
                                onclick='showEditPopup(${data.id}, "swotAnalysis", "threats", "Threats", "swotAnalysis-threats")'
                                class="edit-popup-pre position-absolute position-edit-15" data-id="${data.id}" data-type="swotAnalysis" data-key="threats" data-name="Threats" data-value="${data.threats}" role="button"><i class="ti ti-pencil-alt text-xl"></i></div>
                                <div class="swot-title">Threats</div>
                                <div class="swot-letter">T</div>
                                <div class="swot-content white-space-pre-line" id="swotAnalysis-threats">${data.threats}</div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                $('#swot-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
            }
            
            function updateContentUserPersonas(data) {
                let html = `
                        <div class="card mt-0">
                            <div class="card-header d-block">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <h5 class="mb-0">{{ __('User Personas') }}</h5>
                                    </div>
                                    <!-- Add this inside your Blade template where you show project details -->
                    
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'userPersonas')" class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                        <button onclick="collapseByType('userPersonas', 'minius')" id="userPersonas-minius" class="btn btn-success btn-sm"><i class="ti ti-minus"></i></button>
                                        <button onclick="collapseByType('userPersonas', 'plus')" id="userPersonas-plus" style="display: none" class="btn btn-success btn-sm"><i class="ti ti-plus"></i></button>
                                        <button onclick="deleteProjectInfo({{ $project->id }}, 'userPersonas', '#userPersonas-body')" class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-1 p-sm-4" id="userPersonas-content">
                                <div class="swot-container">
                    `;
                    
                    data.forEach(user => {
                        html += `
                        <div class="card shadow"> 
                            <div class="card-body">
                                ${user.url ? `<img src="{{ url('') }}/testimonialAvatar/${user.url}" class="card-img-top avatar mx-auto mt-3" alt="Avatar ${user.id}">` : ''}
                                <h5 class="card-title text-center"></h5>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item fs-5 position-relative"><b>{{ __('Name') }}:</b>
                                        ${user.name ? `
                                        <div 
                                        onclick='showEditPopup(${user.id}, "userPersonas", "name", "Name", "${user.name}")'
                                        class="edit-popup position-absolute postion-edit-5-15" data-id="${user.id}" data-type="userPersonas" data-key="name" data-name="Name" data-value="${user.name}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                        <span id="userPersonas-name-${user.id}">${user.name ?? 'Not provided'}</span>
                                    </li>
                                    <li class="list-group-item fs-5"><b>{{ __('Age') }}:</b>
                                        ${user.age ? `
                                        <div 
                                        onclick='showEditPopup(${user.id}, "userPersonas", "age", "Age", "${user.age}")'
                                        class="edit-popup position-absolute postion-edit-5-15" data-id="${user.id}" data-type="userPersonas" data-key="age" data-name="Age" data-value="${user.age}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                        <span id="userPersonas-age-${user.id}">${user.age ?? 'Not provided'}</span>
                                    </li>
                                    <li class="list-group-item fs-5"><b>{{ __('Description') }}:</b>
                                        ${user.description ? `
                                        <div 
                                        onclick='showEditPopup(${user.id}, "userPersonas", "description", "Description", "${user.description}")'
                                        class="edit-popup position-absolute postion-edit-5-15" data-id="${user.id}" data-type="userPersonas" data-key="description" data-name="Description" data-value="${user.description}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                        <span id="userPersonas-description-${user.id}">${user.description ?? 'Not provided'}</span>
                                    </li>
                                    <li class="list-group-item fs-5"><b>{{ __('Goals') }}:</b>
                                        ${user.goals ? `<div 
                                        onclick='showEditPopup(${user.id}, "userPersonas", "goals", "Goals", "${user.goals}")'
                                        class="edit-popup position-absolute postion-edit-5-15" data-id="${user.id}" data-type="userPersonas" data-key="goals" data-name="Goals" data-value="${user.goals}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                        <span id="userPersonas-goals-${user.id}">${user.goals ?? 'Not provided'}</span>
                                    </li>
                                    <li class="list-group-item fs-5"><b>{{ __('Needs') }}:</b>
                                        ${user.needs ? `<div 
                                        onclick='showEditPopup(${user.id}, "userPersonas", "needs", "Needs", "${user.needs}")'
                                        class="edit-popup position-absolute postion-edit-5-15" data-id="${user.id}" data-type="userPersonas" data-key="needs" data-name="Needs" data-value="${user.needs}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                        <span id="userPersonas-needs-${user.id}">${user.needs ?? 'Not provided'}</span>
                                    </li>
                                    <li class="list-group-item fs-5"><b>{{ __('Pains') }}:</b>
                                        ${user.pains ? `<div 
                                        onclick='showEditPopup(${user.id}, "userPersonas", "pains", "Pains", "${user.pains}")'
                                        class="edit-popup position-absolute postion-edit-5-15" data-id="${user.id}" data-type="userPersonas" data-key="pains" data-name="Pains" data-value="${user.pains}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                        <span id="userPersonas-pains-${user.id}">${user.pains ?? 'Not provided'}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        `;
                    });
                    
                    html += `
                                </div>
                            </div>
                        </div>
                    `;
                $('#userPersonas-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
            }
            
            function generateSectionLeanCanvas(id, content, content_abr, data) {
                return `
                <section class="canvas-block position-relative" id="${id}">
                    <h2>${id.charAt(0).toUpperCase() + id.slice(1).replace(/-/g, ' ')}</h2>
                    <p class="white-space-pre-line" id="leanCanvas-${id}">${content ? content : 'Not generated.'}</p>
                    ${content ? `
                    <div class="edit-popup-pre position-absolute position-edit-15"
                        onclick='showEditPopup(${data.id}, "leanCanvas", "${id}", "${id.charAt(0).toUpperCase() + id.slice(1).replace(/-/g, ' ')}", "${content}")'
                        data-id="${data.id}" data-type="leanCanvas"
                        data-key="${id}" data-name="${id.charAt(0).toUpperCase() + id.slice(1).replace(/-/g, ' ')}"
                        data-value="${content}" role="button"><i class="ti ti-pencil-alt text-xl"></i></div>` : ''}
                </section>`;
            }
            
            function updateContentUserStories(data) {
                let html = `
                <div id="userStories-body" class="mt-3 draggable-item col-12 ${data.length === 0 ? 'blur-content' : ''}">
                    <div class="card mt-0">
                        <div class="card-header d-block">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h5 class="mb-0">{{ __('User Stories') }}</h5>
                                </div>
                                <!-- Add this inside your Blade template where you show project details -->
                
                                <div>
                                    <button onclick="regenerateProjectInfo({{ $project->id }}, 'userStories')" class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                    <button onclick="collapseByType('userStories', 'minius')" id="userStories-minius" class="btn btn-success btn-sm"><i class="ti ti-minus"></i></button>
                                    <button onclick="collapseByType('userStories', 'plus')" id="userStories-plus" style="display: none" class="btn btn-success btn-sm"><i class="ti ti-plus"></i></button>
                                    <button onclick="deleteProjectInfo({{ $project->id }}, 'userStories', '#userStories-body')" class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-1 p-sm-4" id="userStories-content">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>{{ __('User story') }}</th>
                                        <th>{{ __('Acceptance Criteria') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                `;
                
                data.forEach(user => {
                    html += `
                    <tr>
                        <td class="position-relative">
                            ${user.user_story ? `<div
                            onclick='showEditPopup(${user.id}, "userStories", "user_story", "User Story", "${user.user_story}")'
                            class="edit-popup position-absolute postion-edit-5" data-id="${user.id}" data-type="userStories" data-key="user_story" data-name="User Story" data-value="${user.user_story}" role="button"><i class="ti ti-pencil-alt text-xm"></i></div>` : ''}
                            <span class="white-space-pre-line">${user.user_story ?? 'Not generated.'}</span>
                        </td>
                        <td class="position-relative">
                            ${user.acceptance_criterea ? `<div 
                            onclick='showEditPopup(${user.id}, "userStories", "acceptance_criterea", "Acceptance Criteria", "userStories-acceptance_criterea-${user.id}")'
                            class="edit-popup position-absolute postion-edit-5" data-id="${user.id}" data-type="userStories" data-key="acceptance_criterea" data-name="Acceptance Criteria" data-value="${user.acceptance_criterea}" role="button"><i class="ti ti-pencil-alt text-xm"></i></div>` : ''}
                            <span id="userStories-acceptance_criterea-${user.id}" class="white-space-pre-line">${user.acceptance_criterea ?? 'Not generated.'}</span>
                        </td>
                    </tr>
                    `;
                });
                
                html += `
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                `;
                
                $('#userStories-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
            }
            
            function updateContentLeanCanvas(data) {
                var html = `
                <div class="card mt-0">
                    <div class="card-header d-block">
                        <div class="d-flex justify-content-between align-items-center">
                            <div>
                                <h5 class="mb-0">{{ __('Lean Canvas') }}</h5>
                            </div>
                            <div>
                                <button onclick="regenerateProjectInfo(${data.project_id}, 'leanCanvas')"
                                    class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                <button onclick="collapseByType('leanCanvas', 'minius')"
                                    id="leanCanvas-minius" class="btn btn-success btn-sm"><i
                                        class="ti ti-minus"></i></button>
                                <button onclick="collapseByType('leanCanvas', 'plus')" id="leanCanvas-plus"
                                    style="display: none" class="btn btn-success btn-sm"><i
                                        class="ti ti-plus"></i></button>
                                <button
                                    onclick="deleteProjectInfo(${data.project_id}, 'leanCanvas', '#leanCanvas-body')"
                                    class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-1 p-sm-4" id="leanCanvas-content">
                        <main class="lean-canvas">
                            ${generateSectionLeanCanvas("problem", data.problem, data.problem_abr, data)}
                            ${generateSectionLeanCanvas("solution", data.solution, data.solution_abr, data)}
                            ${generateSectionLeanCanvas("unique-value-prop", data.unique_value_proposition, data.unique_value_proposition_abr, data)}
                            ${generateSectionLeanCanvas("unfair-advantage", data.unfair_advantage, data.unfair_advantage_abr, data)}
                            ${generateSectionLeanCanvas("customer-segments", data.customer_segments, data.customer_segments_abr, data)}
                            ${generateSectionLeanCanvas("existing-alternatives", data.existing_alternatives, data.existing_alternatives_abr, data)}
                            ${generateSectionLeanCanvas("key-metrics", data.key_metrics, data.key_metrics_abr, data)}
                            ${generateSectionLeanCanvas("channels", data.channels, data.channels_abr, data)}
                            ${generateSectionLeanCanvas("cost-structure", data.cost_structure, data.cost_structure_abr, data)}
                            ${generateSectionLeanCanvas("revenue-streams", data.revenue_streams, data.revenue_streams_abr, data)}
                        </main>
                    </div>
                </div>
                `;
                $('#leanCanvas-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
            }
            
            function isNull(value) {
                return value === null || value === undefined;
            }
            
            function updateContentCustomerInterview(data) {
                var html = `
                    <div class="overlay">
                        <div class="cv-spinner">
                            <span class="spinner"></span>
                        </div>
                    </div>
                    <div class="card ${isNull(data.customerInterview) ? 'blur-content-card' : ''} mt-0">
                        <div class="card-header d-block">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h5 class="mb-0">{{ __('Customer Validation Interviews') }}</h5>
                                </div>
                                <div>
                                    <button onclick="regenerateProjectInfo(${data.project_id}, 'customerInterview')" class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                    <button onclick="collapseByType('customerInterview', 'minius')" id="customerInterview-minius" class="btn btn-success btn-sm"><i class="ti ti-minus"></i></button>
                                    <button onclick="collapseByType('customerInterview', 'plus')" id="customerInterview-plus" style="display: none" class="btn btn-success btn-sm"><i class="ti ti-plus"></i></button>
                                    <button onclick="deleteProjectInfo(${data.project_id}, 'customerInterview', '#customerInterview-body')" class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-1 p-sm-4" id="customerInterview-content">
                            <ul>
                                ${data ? 
                                    Object.keys(data).map(function(key) {
                                        if (key.includes('question')) {
                                            return `
                                                <li class="position-relative question-container">
                                                    ${data[key] ? 
                                                        `<div class="edit-popup position-absolute edit-position-ci" data-id="${data.id}" data-type="customerInterview" data-key="${key}" 
                                                        onclick='showEditPopup(${data.id}, "customerInterview", "${key}", "Question ${key.split('_')[1]}", "${data[key]}")'
                                                        data-name="Question ${key.split('_')[1]}" data-value="${data[key]}" role="button"><i class="ti ti-pencil-alt text-sm"></i></div>` 
                                                        : ''}
                                                    <p class="px-3" id="customerInterview-${key}">${data[key] ?? __('Not generated.')}</p>
                                                </li>
                                            `;
                                        }
                                    }).join('')
                                    : ''}
                            </ul>
                        </div>
                    </div>
                `;
                $('#customerInterview-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
            }
            
            function updateContentStorytelling(data) {
                let suggestedNamesHtml = '';
                if (data.suggested_names && data.suggested_names !== 'Not generated.') {
                    let suggestedNames = data.suggested_names.split(',').map(name => name.trim());
                    suggestedNamesHtml = suggestedNames.map(name => `<div class="name-square">${name}</div>`).join('');
                } else {
                    suggestedNamesHtml = 'Not generated.';
                }
                
                let html = `
                <div id="storytelling-body" class="mt-3 draggable-item col-12 ${!data ? 'blur-content' : ''}">
                    <div class="card mt-0">
                        <div class="card-header d-block">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h5 class="mb-0">{{ __('Storytelling') }}</h5>
                                </div>
                                <!-- Add this inside your Blade template where you show project details -->
                                <div>
                                    <button onclick="regenerateProjectInfo(${data.id}, 'storytelling')" class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>
                                    <button onclick="collapseByType('storytelling', 'minius')" id="storytelling-minius" class="btn btn-success btn-sm"><i class="ti ti-minus"></i></button>
                                    <button onclick="collapseByType('storytelling', 'plus')" id="storytelling-plus" style="display: none" class="btn btn-success btn-sm"><i class="ti ti-plus"></i></button>
                                    <button onclick="deleteProjectInfo(${data.id}, 'storytelling', '#storytelling-body')" class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-1 p-sm-4" id="storytelling-content">
                
                            <!-- Tabs -->
                            <div class="tab-container">
                                <div class="tab active" id="tab-brandWheel" data-target="#brandWheel">{{ __('Brand Wheel') }}</div>
                                <div class="tab" id="tab-startupNaming" data-target="#startupNaming">{{ __('Startup Naming') }}</div>
                                <div class="tab" id="tab-elevatorPitch" data-target="#elevatorPitch">{{ __('Elevator Pitch') }}</div>
                            </div>
                
                
                            <!-- Tab Content -->
                            <div class="tab-content" id="brandWheel" style="display: block;">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>{{ __('Element') }}</th>
                                            <th>{{ __('Description') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ __('Mission') }}</td>
                                            <td class="position-relative">
                                                ${data.mission && data.mission !== 'Not generated.' ? `<div
                                                onclick='showEditPopup(${data.id}, "storytelling", "mission", "Mission", "storytelling-mission")'
                                                class="edit-popup position-absolute postion-edit-5" data-id="${data.id}" data-type="storytelling" data-key="mission" data-name="Mission" data-value="${data.mission}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                                <p id="storytelling-mission">${data.mission ?? 'Not generated.'}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('Vision') }}</td>
                                            <td class="position-relative">
                                                ${data.vision && data.vision !== 'Not generated.' ? `<div
                                                onclick='showEditPopup(${data.id}, "storytelling", "vision", "Vision", "storytelling-vision")'
                                                class="edit-popup position-absolute postion-edit-5" data-id="${data.id}" data-type="storytelling" data-key="vision" data-name="Vision" data-value="${data.vision}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                                <p id="storytelling-vision">${data.vision ?? 'Not generated.'}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('Brand Positioning') }}</td>
                                            <td class="position-relative">
                                                ${data.brand_positioning && data.brand_positioning !== 'Not generated.' ? `<div
                                                onclick='showEditPopup(${data.id}, "storytelling", "brand_positioning", "Brand Positioning", "storytelling-brand_positioning")'
                                                class="edit-popup position-absolute postion-edit-5" data-id="${data.id}" data-type="storytelling" data-key="brand_positioning" data-name="Brand Positioning" data-value="${data.brand_positioning}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                                <p id="storytelling-brand_positioning">${data.brand_positioning ?? 'Not generated.'}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('Brand Values') }}</td>
                                            <td class="position-relative">
                                                ${data.brand_values && data.brand_values !== 'Not generated.' ? `<div
                                                onclick='showEditPopup(${data.id}, "storytelling", "brand_values", "Brand Values", "storytelling-brand_values")'
                                                class="edit-popup position-absolute postion-edit-5" data-id="${data.id}" data-type="storytelling" data-key="brand_values" data-name="Brand Values" data-value="${data.brand_values}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                                <p id="storytelling-brand_values">${data.brand_values ?? 'Not generated.'}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('Personality') }}</td>
                                            <td class="position-relative">
                                                ${data.personality && data.personality !== 'Not generated.' ? `<div
                                                onclick='showEditPopup(${data.id}, "storytelling", "personality", "Personality", "storytelling-personality")'
                                                class="edit-popup position-absolute postion-edit-5" data-id="${data.id}" data-type="storytelling" data-key="personality" data-name="Personality" data-value="${data.personality}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                                <p id="storytelling-personality">${data.personality ?? 'Not generated.'}</p>
                                            </td>
                                        </tr>
                                        <!-- More rows as needed -->
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-content" id="startupNaming" style="display: none;">
                                ${suggestedNamesHtml}
                            </div>
                
                
                
                            <div class="tab-content position-relative" id="elevatorPitch" style="display: none;">
                                ${data.elevator_pitch && data.elevator_pitch !== 'Not generated.' ? `<div
                                onclick='showEditPopup(${data.id}, "storytelling", "elevator_pitch", "Elevator Pitch", "storytelling-elevator_pitch")'
                                class="edit-popup position-absolute position-edit-5-0" data-id="${data.id}" data-type="storytelling" data-key="elevator_pitch" data-name="Elevator Pitch" data-value="${data.elevator_pitch}" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>` : ''}
                                <p><i id="storytelling-elevator_pitch">${data.elevator_pitch ?? 'Not generated.'}</i></p>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                $('#storytelling-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
                
                
                $('#tab-startupNaming').on('click', function() {
                    if (!$('#tab-startupNaming').hasClass('active')) {
                        $('#tab-startupNaming').addClass('active');
                    }
                    $('#tab-brandWheel').removeClass('active');
                    $('#tab-elevatorPitch').removeClass('active');
                    $('#startupNaming').show();
                    $('#brandWheel').hide();
                    $('#elevatorPitch').hide();
                });
                $('#tab-brandWheel').on('click', function() {
                    if (!$('#tab-brandWheel').hasClass('active')) {
                        $('#tab-brandWheel').addClass('active');
                    }
                    $('#tab-startupNaming').removeClass('active');
                    $('#tab-elevatorPitch').removeClass('active');
                    $('#brandWheel').show();
                    $('#startupNaming').hide();
                    $('#elevatorPitch').hide();
                });
                $('#tab-elevatorPitch').on('click', function() {
                    if (!$('#tab-elevatorPitch').hasClass('active')) {
                        $('#tab-elevatorPitch').addClass('active');
                    }
                    $('#tab-startupNaming').removeClass('active');
                    $('#tab-brandWheel').removeClass('active');
                    $('#startupNaming').hide();
                    $('#brandWheel').hide();
                    $('#elevatorPitch').show();
                });
            }

            function updateContentViabilityAnalysis(data) {
                var html =
                    '<div class="overlay">' +
                    '<div class="cv-spinner">' +
                    '<span class="spinner"></span>' +
                    '</div>' +
                    '</div>' +
                    '<div class="' + (data ? '' : 'blur-content-card') + ' mt-0">' +
                    '<div class="card-header d-block">' +
                    '<div class="d-flex justify-content-between align-items-center">' +
                    '<div>' +
                    '<h5 class="mb-0">{{ __('Viability Analysis') }}</h5>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                
                if (data) {
                    html += '<div class="card-body p-1 p-sm-4" id="viabilityAnalysis-content">' +
                        '<div class="rounded-lg border border-neutral-200 mb-4 relative group" data-node-view-wrapper="" style="white-space: normal">' +
                        '<div class="">' +
                        '<div class="max-w-4xl m-auto columns-2 rounded-md items-center justify-center flex gap-4 lg:gap-8">' +
                        '<div class="w-[100px] lg:w-[200px] h-[100px] lg:h-[200px] flex">' +
                        '<div class="recharts-responsive-container" style="width: 100%; height: 100%; min-width: 0px">' +
                        '<div class="recharts-wrapper" role="region" style="position: relative; cursor: default; width: 100%; height: 100%; max-height: 200px; max-width: 200px">' +
                        '<svg cx="50%" cy="50%" class="recharts-surface" width="200" height="200" viewBox="0 0 200 200" style="width: 100%; height: 100%">' +
                        '<title></title>' +
                        '<desc></desc>' +
                        '<defs>' +
                        '<clipPath id="recharts5-clip">' +
                        '<rect x="5" y="5" height="190" width="190"></rect>' +
                        '</clipPath>' +
                        '</defs>' +
                        '<g class="recharts-layer recharts-pie" tabindex="0">' +
                        '<g class="recharts-layer recharts-pie-sector" tabindex="-1">' +
                        '<path cx="100" cy="155" name="A" fill="#54C65A" stroke="none" color="#54C65A" tabindex="-1" class="recharts-sector" d="M 0,155 A 100,100,0,0,1,50.00000000000002,68.39745962155612 L 75.00000000000001,111.69872981077806 A 50,50,0,0,0,50,155 Z" role="img"></path>' +
                        '</g>' +
                        '<g class="recharts-layer recharts-pie-sector" tabindex="-1">' +
                        '<path cx="100" cy="155" name="B" fill="#36BA3C" stroke="none" color="#36BA3C" tabindex="-1" class="recharts-sector" d="M 50.00000000000002,68.39745962155612 A 100,100,0,0,1,150,68.39745962155614 L 125,111.69872981077808 A 50,50,0,0,0,75.00000000000001,111.69872981077806 Z" role="img"></path>' +
                        '</g>' +
                        '<g class="recharts-layer recharts-pie-sector" tabindex="-1">' +
                        '<path cx="100" cy="155" name="C" fill="#009307" stroke="none" color="#009307" tabindex="-1" class="recharts-sector" d="M 150,68.39745962155614 A 100,100,0,0,1,200,155 L 150,155 A 50,50,0,0,0,125,111.69872981077808 Z" role="img"></path>' +
                        '</g>' +
                        '</g>' +
                        '<circle cx="100" cy="155" r="5" fill="#2D2D2D" stroke="none"></circle>' +
                        '<path d="M98.15937723657662 159.64888242944124L101.84062276342338 150.35111757055876 L22.51862617597905 124.32295394294351 L98.15937723657662 159.64888242944124" stroke="#none" fill="#2D2D2D"></path>' +
                        '</svg>' +
                        '</div>' +
                        '</div>' +
                        '<h1 class="text-4xl lg:text-8xl font-bold m-0">' + data.percent + '/100</h1>' +
                        '</div>' +
                        '</div>' +
                        '<h2 class="">{{ __('Viability Analysis') }}</h2>' +
                        '<div class="position-relative w-full">' +
                        '<div class="edit-popup position-absolute position-edit-5-0" data-id="' + data.id + '" data-type="viabilityAnalysis" data-key="viability_analysis_content" data-name="market size" data-value="' + data.viability_analysis_content + '" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>' +
                        '<p id="viabilityAnalysis-viability_analysis_content" class="white-space-pre-line">' + data.viability_analysis_content + '</p>' +
                        '</div>' +
                        '<h5 class="mt-4">{{ __('Source:') }}</h5>' +
                        '<ul class="list-circles list-outside mt-2 px-3 tight">' +
                        '<li class="mt-2">' +
                        '<div class="position-relative w-full">' +
                        '<div class="edit-popup position-absolute position-edit-5-0" data-id="' + data.id + '" data-type="viabilityAnalysis" data-key="source_1" data-name="source" data-value="' + data.source_1 + '" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>' +
                        '<p id="viabilityAnalysis-source_1">' + data.source_1 + '</p>' +
                        '</div>' +
                        '</li>' +
                        '<li class="mt-2">' +
                        '<div class="position-relative w-full">' +
                        '<div class="edit-popup position-absolute position-edit-5-0" data-id="' + data.id + '" data-type="viabilityAnalysis" data-key="source_2" data-name="source" data-value="' + data.source_2 + '" role="button"><i class="ti ti-pencil-alt text-xs"></i></div>' +
                        '<p id="viabilityAnalysis-source_2">' + data.source_2 + '</p>' +
                        '</div>' +
                        '</li>' +
                        '</ul>' +
                        '</div>';
                }
                
                $('#viabilityAnalysis-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
            }

            function updateContentMarketingOverview(data) {
                var html = `
                    
                `;
                $('#marketingOverview-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
            }

            function updateContentMarketSize(data) {
                var html = `
                    
                `;
                $('#marketSize-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
            }

            function updateContentIndustryOverview(data) {
                var html = `
                    
                `;
                $('#industryOverview-body').removeClass('blur-content').removeClass('loading').html(html);
                checkEmptyConcept();
                if (localStorage.getItem('is_edit_show') == 'show') {
                    $('.edit-popup-pre').show();
                } else {
                    $('.edit-popup-pre').hide();
                }
            }
            
            const containers = $('.draggable');
        
            // Lưu vị trí các phần tử vào local storage
            function savePositions() {
              const positions = {};
              containers.each(function() {
                const containerId = $(this).attr('id');
                const childIds = $(this).children().map(function() {
                  return this.id;
                }).get();
                positions[containerId] = childIds;
              });
              localStorage.setItem('positions-{{ $project->id }}', JSON.stringify(positions));
            }
        
            // Khôi phục vị trí từ local storage
            function restorePositions() {
              const positions = JSON.parse(localStorage.getItem('positions-{{ $project->id }}'));
              if (positions) {
                $.each(positions, function(containerId, childIds) {
                  const container = $('#' + containerId);
                  $.each(childIds, function(index, childId) {
                    $('#' + childId).appendTo(container);
                  });
                });
              }
            }
        
            // Khởi tạo Dragula cho các container
            const drake = dragula(containers.toArray(), {
              revertOnSpill: true // Cho phép phần tử trở lại vị trí ban đầu nếu không kéo được
            });
        
            // Lưu vị trí khi có sự thay đổi
            drake.on('dragend', function() {
              savePositions();
            });
        
            // Khôi phục vị trí từ local storage khi tài liệu được tải
            restorePositions();
          });
        var now = "{{__('Now')}}";

        /*For Task Move To Update Stage*/
        !function (a) {
            "use strict";
            var t = function () {
                this.$body = a("body")
            };
            t.prototype.init = function () {
                a('[data-plugin="dragula"]').each(function () {
                    var t = a(this).data("containers"), n = [];
                    if (t) for (var i = 0; i < t.length; i++) n.push(a("#" + t[i])[0]); else n = [a(this)[0]];
                    var r = a(this).data("handleclass");
                    r ? dragula(n, {
                        moves: function (a, t, n) {
                            return n.classList.contains(r)
                        }
                    }) : dragula(n).on('drop', function (el, target, source, sibling) {
                        var sort = [];
                        $("#" + target.id + " > div").each(function () {
                            sort[$(this).index()] = $(this).attr('id');
                        });

                        var id = el.id;
                        var old_stage = $("#" + source.id).data('status');
                        var new_stage = $("#" + target.id).data('status');
                        var project_id = '{{$project->id}}';

                        $("#" + source.id).parent().find('.count').text($("#" + source.id + " > div").length);
                        $("#" + target.id).parent().find('.count').text($("#" + target.id + " > div").length);
                        $.ajax({
                            url: '{{route('dashboard.user.openai.projects.tasks.update.order',[$project->id])}}',
                            type: 'PATCH',
                            data: {id: id, sort: sort, new_stage: new_stage, old_stage: old_stage, project_id: project_id},
                            success: function (data) {

                                // console.log(data);
                            }
                        });
                    });
                })
            }, a.Dragula = new t, a.Dragula.Constructor = t
        }(window.jQuery), function (a) {
            "use strict";
            a.Dragula.init()
        }(window.jQuery);

        $(document).ready(function () {
            /*Set assign_to Value*/
            $(document).on('click', '.add_usr', function () {
                var ids = [];
                $(this).toggleClass('selected');
                var crr_id = $(this).attr('data-id');
                $('#usr_txt_' + crr_id).html($('#usr_txt_' + crr_id).html() == 'Add' ? '{{__('Added')}}' : '{{__('Add')}}');
                if ($('#usr_icon_' + crr_id).hasClass('fa-plus')) {
                    $('#usr_icon_' + crr_id).removeClass('fa-plus');
                    $('#usr_icon_' + crr_id).addClass('fa-check');
                } else {
                    $('#usr_icon_' + crr_id).removeClass('fa-check');
                    $('#usr_icon_' + crr_id).addClass('fa-plus');
                }
                $('.selected').each(function () {
                    ids.push($(this).attr('data-id'));
                });
                $('input[name="assign_to"]').val(ids);
            });

            $(document).on("click", ".del_task", function () {
                var id = $(this);
                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'DELETE',
                    dataType: 'JSON',
                    success: function (data) {
                        $('#' + data.task_id).remove();
                        show_toastr('{{__('Success')}}', '{{ __("Task Deleted Successfully!")}}', 'success');
                    },
                });
            });

            /*For Task Comment*/
            $(document).on('click', '#comment_submit', function (e) {
                var curr = $(this);
                var comment = $.trim($("#form-comment textarea[name='comment']").val());
                if (comment != '') {
                    $.ajax({
                        url: $("#form-comment").data('action'),
                        data: {comment: comment},
                        type: 'POST',
                        success: function (data) {
                            data = JSON.parse(data);
                             var avatar = (data.user.avatar) ?  data.user.avatar + "'" : "avatar='" + data.user.img_avatar + "'";
                            var html = "<div class='list-group-item px-0'>" +
                                "                    <div class='row align-items-center'>" +
                                "                        <div class='col-auto'>" +
                                "                            <a href='#' class='avatar avatar-sm rounded-circle'>" +
                                "                                <img " + avatar + " alt='" + data.user.name + "'>" +
                                "                            </a>" +
                                "                        </div>" +
                                "                        <div class='col ml-n2'>" +
                                "                            <p class='d-block h6 text-sm font-weight-light mb-0 text-break'>" + data.comment + "</p>" +
                                "                            <small class='d-block'>" + now + "</small>" +
                                "                        </div>" +
                                "                        <div class='col-auto'><a href='#' class='delete-comment' data-url='" + data.deleteUrl + "'><i class='fas fa-trash-alt text-danger'></i></a></div>" +
                                "                    </div>" +
                                "                </div>";

                            $("#comments").prepend(html);
                            $("#form-comment textarea[name='comment']").val('');
                            load_task(curr.closest('.side-modal').attr('id'));
                            show_toastr('{{__('Success')}}', '{{ __("Comment Added Successfully!")}}', 'success');
                        },
                        error: function (data) {
                            show_toastr('{{__('Error')}}', '{{ __("Some Thing Is Wrong!")}}', 'error');
                        }
                    });
                } else {
                    show_toastr('{{__('Error')}}', '{{ __("Please write comment!")}}', 'error');
                }
            });

            $(document).on("click", ".delete-comment", function () {
                var btn = $(this);

                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'DELETE',
                    dataType: 'JSON',
                    success: function (data) {
                        load_task(btn.closest('.side-modal').attr('id'));
                        show_toastr('{{__('Success')}}', '{{ __("Comment Deleted Successfully!")}}', 'success');
                        btn.closest('.list-group-item').remove();
                    },
                    error: function (data) {
                        data = data.responseJSON;
                        if (data.message) {
                            show_toastr('{{__('Error')}}', data.message, 'error');
                        } else {
                            show_toastr('{{__('Error')}}', '{{ __("Some Thing Is Wrong!")}}', 'error');
                        }
                    }
                });
            });

            /*For Task Checklist*/
            $(document).on('click', '#checklist_submit', function () {
                var name = $("#form-checklist input[name=name]").val();
                if (name != '') {
                    $.ajax({
                        url: $("#form-checklist").data('action'),
                        data: {name: name},
                        type: 'POST',
                        success: function (data) {
                            data = JSON.parse(data);
                            load_task($('.side-modal').attr('id'));
                            show_toastr('{{__('Success')}}', '{{ __("Checklist Added Successfully!")}}', 'success');
                            var html = '<div class="card border shadow-none checklist-member">' +
                                '                    <div class="px-3 py-2 row align-items-center">' +
                                '                        <div class="col-10">' +
                                '                            <div class="custom-control custom-checkbox">' +
                                '                                <input type="checkbox" class="custom-control-input" id="check-item-' + data.id + '" value="' + data.id + '" data-url="' + data.updateUrl + '">' +
                                '                                <label class="custom-control-label h6 text-sm" for="check-item-' + data.id + '">' + data.name + '</label>' +
                                '                            </div>' +
                                '                        </div>' +
                                '                        <div class="col-auto card-meta d-inline-flex align-items-center ml-sm-auto">' +
                                '                            <a href="#" class="action-item delete-checklist" role="button" data-url="' + data.deleteUrl + '">' +
                                '                                <i class="fas fa-trash-alt text-danger"></i>' +
                                '                            </a>' +
                                '                        </div>' +
                                '                    </div>' +
                                '                </div>'

                            $("#checklist").append(html);
                            $("#form-checklist input[name=name]").val('');
                            $("#form-checklist").collapse('toggle');
                        },
                        error: function (data) {
                            data = data.responseJSON;
                            show_toastr('{{__('Error')}}', data.message, 'error');
                        }
                    });
                } else {
                    show_toastr('{{__('Error')}}', '{{ __("Please write checklist name!")}}', 'error');
                }
            });

            $(document).on("change", "#checklist input[type=checkbox]", function () {
                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {
                        load_task($('.side-modal').attr('id'));
                        show_toastr('{{__('Success')}}', '{{ __("Checklist Updated Successfully!")}}', 'success');
                    },
                    error: function (data) {
                        data = data.responseJSON;
                        if (data.message) {
                            show_toastr('{{__('Error')}}', data.message, 'error');
                        } else {
                            show_toastr('{{__('Error')}}', '{{ __("Some Thing Is Wrong!")}}', 'error');
                        }
                    }
                });
            });

            $(document).on("click", ".delete-checklist", function () {
                var btn = $(this);
                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'DELETE',
                    dataType: 'JSON',
                    success: function (data) {
                        load_task($('.side-modal').attr('id'));
                        show_toastr('{{__('Success')}}', '{{ __("Checklist Deleted Successfully!")}}', 'success');
                        btn.closest('.checklist-member').remove();
                    },
                    error: function (data) {
                        data = data.responseJSON;
                        if (data.message) {
                            show_toastr('{{__('Error')}}', data.message, 'error');
                        } else {
                            show_toastr('{{__('Error')}}', '{{ __("Some Thing Is Wrong!")}}', 'error');
                        }
                    }
                });
            });

            /*For Task Attachment*/
            $(document).on('click', '#file_submit', function () {
                var file_data = $("#task_attachment").prop("files")[0];
                if (file_data != '' && file_data != undefined) {
                    var formData = new FormData();
                    formData.append('file', file_data);

                    $.ajax({
                        url: $("#file_submit").data('action'),
                        type: 'POST',
                        data: formData,
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                           // alert(data);
                            $('#task_attachment').val('');
                            $('.attachment_text').html('{{__('Choose a file…')}}');
                            data = JSON.parse(data);
                            load_task(data.task_id);
                            show_toastr('{{__('Success')}}', '{{ __("File Added Successfully!")}}', 'success');

                            var delLink = '';
                            if (data.deleteUrl.length > 0) {
                                delLink = '<a href="#" class="action-item delete-comment-file" role="button" data-url="' + data.deleteUrl + '">' +
                                    '                                        <i class="fas fa-trash"></i>' +
                                    '                                    </a>';
                            }

                            var html = '<div class="card mb-3 border shadow-none task-file">' +
                                '                    <div class="px-3 py-3">' +
                                '                        <div class="row align-items-center">' +
                                '                            <div class="col-auto">' +
                                '                                <img src="{{asset('assets/img/icons/files')}}/' + data.extension + '.png" class="img-fluid" style="width: 40px;">' +
                                '                            </div>' +
                                '                            <div class="col ml-n2">' +
                                '                                <h6 class="text-sm mb-0">' +
                                '                                    <a href="#">' + data.name + '</a>' +
                                '                                </h6>' +
                                '                                <p class="card-text small text-muted">' + data.file_size + '</p>' +
                                '                            </div>' +
                                '                            <div class="col-auto actions">' +
                                '                                <a href="/' + data.file + '" download class="action-item" role="button">' +
                                '                                    <i class="fas fa-download"></i>' +
                                '                                </a>' +
                                delLink +
                                '                            </div>' +
                                '                        </div>' +
                                '                    </div>' +
                                '                </div>'

                            $("#comments-file").prepend(html);
                        },
                        error: function (data) {

                            data = data.responseJSON;
                            if (data) {
                                   show_toastr('{{__('Error')}}', 'File type and size must be match with Storage setting.', 'error');
                                $('#file-error').text(data.errors.file[0]).show();
                            } else {
                                show_toastr('{{__('Error')}}', '{{ __("Some Thing Is Wrong!")}}', 'error');
                            }
                        }
                    });
                } else {
                    show_toastr('{{__('Error')}}', '{{ __("Please select file!")}}', 'error');
                }
            });

            $(document).on("click", ".delete-comment-file", function () {
                var btn = $(this);
                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'DELETE',
                    dataType: 'JSON',
                    success: function (data) {
                        load_task(btn.closest('.side-modal').attr('id'));
                        show_toastr('{{__('Success')}}', '{{ __("File Deleted Successfully!")}}', 'success');
                        btn.closest('.task-file').remove();
                    },
                    error: function (data) {
                        data = data.responseJSON;
                        if (data.message) {
                            show_toastr('{{__('Error')}}', data.message, 'error');
                        } else {
                            show_toastr('{{__('Error')}}', '{{ __("Some Thing Is Wrong!")}}', 'error');
                        }
                    }
                });
            });

            /*For Favorite*/
            $(document).on('click', '#add_favourite', function () {
                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'POST',
                    success: function (data) {
                        if (data.fav == 1) {
                            $('#add_favourite').addClass('action-favorite');
                        } else if (data.fav == 0) {
                            $('#add_favourite').removeClass('action-favorite');
                        }
                    }
                });
            });

            /*For Complete*/
            $(document).on('change', '#complete_task', function () {
                $.ajax({
                    url: $(this).attr('data-url'),
                    type: 'POST',
                    success: function (data) {
                        if (data.com == 1) {
                            $("#complete_task").prop("checked", true);
                        } else if (data.com == 0) {
                            $("#complete_task").prop("checked", false);
                        }
                        $('#' + data.task).insertBefore($('#task-list-' + data.stage + ' .empty-container'));
                        load_task(data.task);
                    }
                });
            });

            /*Progress Move*/
            $(document).on('change', '#task_progress', function () {
                var progress = $(this).val();
                $('#t_percentage').html(progress);
                $.ajax({
                    url: $(this).attr('data-url'),
                    data: {progress: progress},
                    type: 'POST',
                    success: function (data) {
                        load_task(data.task_id);
                    }
                });
            });
        });

        function load_task(id) {
            $.ajax({
                dataType: 'html',
                success: function (data) {
                    $('#' + id).html('');
                    $('#' + id).html(data);
                }
            });
        }
    </script>
@endpush

@push('theme-script')
@endpush