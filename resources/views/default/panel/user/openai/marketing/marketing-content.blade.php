<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5 d-block">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <div class="">
        <h2 class="mb-0">{{ __('Marketing content') }}</h2>
    </div>
    <div class="row">
        <div class="col-12 mt-3">
            <div class="lqd-generators-container mt-3" id="lqd-generators-container">
                <div class="lqd-generators-list grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4" id="lqd-generators-list">
                    @foreach ($list as $item)
                        @if ($item->active != 1 || str()->startsWith($item->slug, 'ai_'))
                            @continue
                        @endif
                        <x-generator-item 
                        @class(['pro-feature'])
                        :$item />
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-table table-responsive">
                    <table class="table table-vcenter">
                        <div class="card-header justify-between">
                            <h3 class="card-title text-heading">{{ __('Documents') }}</h3>
                        </div>
                        <tbody>
                            @php
                                $documents = Auth::user()->openai()->where('project_id', $project->id)->orderBy('created_at', 'desc')->get();
                            @endphp
                            @if ($documents->count() > 0)
                                @foreach ($documents as $entry)
                                    @if ($entry->generator != null)
                                        <tr>
                                            <td class="w-1 !pe-0">
                                                <span class="avatar w-[43px] h-[43px] [&_svg]:w-[20px] [&_svg]:h-[20px]"
                                                    style="background: {{ $entry->generator->color }}">
                                                    @if ($entry->generator->image !== 'none')
                                                        {!! html_entity_decode($entry->generator->image) !!}
                                                    @endif
                                                </span>
                                            </td>
                                            <td class="td-truncate">
                                                <a href="{{route('dashboard.user.openai.documents.single', $entry->slug)}}?idea_id={{ $project->id }}"
                                                    class="block text-truncate text-heading hover:no-underline">
                                                    <span class="font-medium">
                                                        {{ $entry->title }}
                                                    </span>
                                                </a>
                                            </td>
                                            <td class="text-nowrap">
                                                <span class="text-heading">{{ __('in Workbook') }}</span>
                                                <br>
                                                <span
                                                    class="italic text-muted opacity-80 dark:!text-white dark:!opacity-50">{{ $entry->created_at->format('M d, Y') }}</span>
                                            </td>
                                            <td class="text-nowrap">
                                                <a  onclick="return deleteProjectDocument('{{ route('dashboard.user.openai.projects.delete-document', ['slug' => $entry->slug, 'id' => $project->id]) }}');"

                                                    class="btn p-0 border w-[36px] h-[36px] hover:bg-red-600 hover:text-white"
                                                    title="{{ __('Delete') }}">
                                                    <svg width="10" height="10" viewBox="0 0 10 10" fill="currentColor"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M9.08789 1.74609L5.80664 5L9.08789 8.25391L8.26758 9.07422L4.98633 5.82031L1.73242 9.07422L0.912109 8.25391L4.16602 5L0.912109 1.74609L1.73242 0.925781L4.98633 4.17969L8.26758 0.925781L9.08789 1.74609Z" />
                                                    </svg>
                                                </a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @else
                                <tr class="text-heading text-center py-5">
                                    <td class="w-1 !pe-0">
                                    {{ __('You have no documents. Create one to get started.') }}
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>