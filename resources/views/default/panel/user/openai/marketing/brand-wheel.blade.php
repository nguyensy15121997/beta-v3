<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <form action="{{ route('projects.generate-lean-canvas', ['project' => $project->id]) }}" method="POST"
        id="generate-storytelling-form" style="display: none;">
        @csrf
    </form>


    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row h-full" id="0">
                @if ($project)
                    <div id="storytelling-body" class="mt-3 col-12 {{ is_null($storytelling) ? 'blur-content loading' : '' }}">
                        <div class="overlay">
                            <div class="cv-spinner">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div class="{{ is_null($storytelling) ? 'blur-content-card' : '' }} mt-0">
                            <div class="card-header  d-block px-2rem">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center justify-center">
                                        <h2 class="mb-0">{{ __('Brand Wheel') }}</h2>
                                        <img src="{{ url('').'/images/info-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                                    </div>
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'storytelling')"
                                            class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="card-body p-1 p-sm-4" id="storytelling-content">
                                <main class="lean-canvas">
                                    <section class="canvas-block position-relative" id="vision">
                                        @if ($storytelling)
                                            @if ($storytelling->vision)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $storytelling->id }}" data-type="storytelling"
                                                    data-key="vision" data-name="vision"
                                                    data-value="{{ $storytelling->vision }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/vision-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Vision') }}</h2>
                                        <p class="white-space-pre-line" id="storytelling-vision">
                                            {!! $storytelling->vision ?? 'Not generated.' !!}
                                        </p>
                                    </section>
                                    <section class="canvas-block position-relative" id="mission">
                                        @if ($storytelling)
                                            @if ($storytelling->mission)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $storytelling->id }}" data-type="storytelling"
                                                    data-key="mission" data-name="mission"
                                                    data-value="{{ $storytelling->mission }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/mission-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Mission') }}</h2>
                                        <p class="white-space-pre-line" id="storytelling-mission">{!! $storytelling->mission ?? 'Not generated.' !!}
                                        </p>
                                    </section>
                                    <section class="canvas-block position-relative" id="brand_positioning">
                                        @if ($storytelling)
                                            @if ($storytelling->brand_positioning)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $storytelling->id }}" data-type="storytelling"
                                                    data-key="brand_positioning" data-name="brand positioning"
                                                    data-value="{{ $storytelling->brand_positioning }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/brand-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Brand Proposition') }}</h2>
                                        <p class="white-space-pre-line" id="storytelling-brand_positioning">
                                            {!! $storytelling->brand_positioning ?? 'Not generated.' !!}</p>
                                    </section>
                                    <section class="canvas-block position-relative" id="personality">
                                        @if ($storytelling)
                                            @if ($storytelling->personality)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $storytelling->id }}" data-type="storytelling"
                                                    data-key="personality" data-name="personality"
                                                    data-value="{{ $storytelling->personality }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/personality-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Personality') }}</h2>
                                        <p class="white-space-pre-line" id="storytelling-personality">
                                            {!! $storytelling->personality ?? 'Not generated.' !!}</p>
                                    </section>
                                    <section class="canvas-block position-relative" id="brand_values">
                                        @if ($storytelling)
                                            @if ($storytelling->brand_values)
                                                <div class="edit-popup position-absolute position-edit-15"
                                                    data-id="{{ $storytelling->id }}" data-type="storytelling"
                                                    data-key="brand_values" data-name="Customer Segments"
                                                    data-value="{{ $storytelling->brand_values }}"
                                                     role="button"><i
                                                        class="ti ti-pencil-alt text-xl"></i></div>
                                            @endif
                                        @endif
                                        <img src="{{ url('').'/images/elevator-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                        <h2 class="mb-0">{{ __('Brand Values') }}</h2>
                                        <p class="white-space-pre-line" id="storytelling-brand_values">
                                            {!! $storytelling->brand_values ?? 'Not generated.' !!}</p>
                                    </section>
                                </main>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>