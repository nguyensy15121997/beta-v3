<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <form action="{{ route('projects.generate-lean-canvas', ['project' => $project->id]) }}" method="POST"
        id="generate-leancanvas-form" style="display: none;">
        @csrf
    </form>


    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row  h-full" id="0">
                @if ($project)
                    <div id="brandIdentity-body" class="mt-3 col-12 {{ is_null($brandIdentity) ? 'blur-content loading' : '' }}">
                            <div class="overlay">
                                <div class="cv-spinner">
                                    <span class="spinner"></span>
                                </div>
                            </div>
                            <div class="{{ is_null($brandIdentity) ? '' : '' }} mt-0">  
                                <div class="card-header d-block px-2rem">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="d-flex align-items-center justify-center">
                                            <h2 class="mb-0">{{ __('Branding Identity') }}</h2>
                                            <img src="{{ url('').'/images/info-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                                        </div>
                                        <div>
                                            <button onclick="regenerateProjectInfo({{ $project->id }}, 'brandIdentity')"
                                                class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>    
                                @if (!empty($brandIdentity))
                                    <div class="card-body p-1 p-sm-4" id="brandIdentity-content">
                                        <h3 class="">{{ __('Colour Palette') }}</h3>
                                        <div class="grid grid-cols-5" style="gap: 0px">
                                            <div style="white-space: normal;">
                                                <div>
                                                    <button class="border-left text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_1 }};">
                                                    </button>
                                                </div>
                                            </div>
                                            <div style="white-space: normal;">
                                                <div>
                                                    <button class="text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_2 }}">
                                                    </button>
                                                </div>
                                            </div>
                                            <div style="white-space: normal;">
                                                <div>
                                                    <button class="text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_3 }}">
                                                    </button>
                                                </div>
                                            </div>
                                            <div style="white-space: normal;">
                                                <div>
                                                    <button class="text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_4 }}">
                                                    </button>
                                                </div>
                                            </div>
                                            <div style="white-space: normal;">
                                                <div>
                                                    <button class="border-right text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_1 }};">
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="my-4 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                        
                                        <h3 class="mt-4">{{ __('Typography') }}</h3>
                                        <div class="mx-3 row">
                                            <div class="col-12 col-sm-6 p-3">
                                                <div>
                                                    <div>{{ __('Headline') }}</div>
                                                    <h1>{{ __('Golos Text Display') }}</h1>
                                                </div>
                                                <div class="mt-3">
                                                    <div>{{ __('Subheadline') }}</div>
                                                    <h1>{{ __('Golos Text Bold') }}</h1>
                                                </div>
                                                <div class="mt-3">
                                                    <div>{{ __('Headline') }}</div>
                                                    <h1 style="font-weight: 400">{{ __('Golos Text Regular') }}</h1>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="p-3 card mt-0 w-full h-full">
                                                    <h1>{{ __('Hey There!') }}</h1>
                                                    <h3>{{ __('It’s so nice to meet you.') }}</h3>
                                                    <p>{{ __('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="mt-4 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                        
                                        <h3 class="mt-4">{{ __('Logo Idea') }}</h3>
                                        <div class="position-relative">
                                            <div class="edit-popup position-absolute position-edit-5-0"
                                                        data-id="{{ $brandIdentity->id }}" data-type="brandIdentity"
                                                        data-key="logo_idea" data-name="logo idea"
                                                        data-value="{{ $brandIdentity->logo_idea }}"
                                                         role="button"><i
                                                            class="ti ti-pencil-alt text-xs"></i>
                                            </div>
                                            <p id="brandIdentity-logo_idea">{{ $brandIdentity->logo_idea }}</p>
                                        </div>
                                        <div class="mt-4 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                        
                                        <h3 class="mt-4">{{ __('Imagery & Photography') }}</h3>
                                        <div class="position-relative">
                                            <div class="edit-popup position-absolute position-edit-5-0"
                                                        data-id="{{ $brandIdentity->id }}" data-type="brandIdentity"
                                                        data-key="imagery_photography" data-name="Imagery & Photography"
                                                        data-value="{{ $brandIdentity->imagery_photography }}"
                                                         role="button"><i
                                                            class="ti ti-pencil-alt text-xs"></i>
                                            </div>
                                            <p id="brandIdentity-imagery_photography">{{ $brandIdentity->imagery_photography }}</p>
                                        </div>
                                        <div class="mt-4 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                        <!--<h3 class="mt-4">{{ __('Brand Voice') }}</h3>-->
                                        <!--<div class="position-relative">-->
                                        <!--    <div class="edit-popup position-absolute position-edit-5-0"-->
                                        <!--                data-id="{{ $brandIdentity->id }}" data-type="brandIdentity"-->
                                        <!--                data-key="brand_voice" data-name="brand voice"-->
                                        <!--                data-value="{{ $brandIdentity->brand_voice }}"-->
                                        <!--                 role="button"><i-->
                                        <!--                    class="ti ti-pencil-alt text-xs"></i>-->
                                        <!--    </div>-->
                                        <!--    <p id="brandIdentity-brand_voice">{{ $brandIdentity->brand_voice }}</p>-->
                                        <!--</div>-->
                                        <!--<h3 class="mt-4">{{ __('Brand Values') }}</h3>-->
                                        <!--<div class="position-relative">-->
                                        <!--    <div class="edit-popup position-absolute position-edit-5-0"-->
                                        <!--                data-id="{{ $brandIdentity->id }}" data-type="brandIdentity"-->
                                        <!--                data-key="brand_values" data-name="brand values"-->
                                        <!--                data-value="{{ $brandIdentity->brand_values }}"-->
                                        <!--                 role="button"><i-->
                                        <!--                    class="ti ti-pencil-alt text-xs"></i>-->
                                        <!--    </div>-->
                                        <!--    <p id="brandIdentity-brand_values">{{ $brandIdentity->brand_values }}</p>-->
                                        <!--</div>-->
                                        <h3 class="mt-4">{{ __('Brand Personality Traits') }}</h3>
                                        <div class="position-relative">
                                            <div class="edit-popup position-absolute position-edit-5-0"
                                                        data-id="{{ $brandIdentity->id }}" data-type="brandIdentity"
                                                        data-key="brand_personality_traits" data-name="brand personality traits"
                                                        data-value="{{ $brandIdentity->brand_personality_traits }}"
                                                         role="button"><i
                                                            class="ti ti-pencil-alt text-xs"></i>
                                            </div>
                                            <p id="brandIdentity-brand_personality_traits">{{ $brandIdentity->brand_personality_traits }}</p>
                                        </div>
                                        <!--<h3 class="mt-4">{{ __('Customer Promise') }}</h3>-->
                                        <!--<div class="position-relative">-->
                                        <!--    <div class="edit-popup position-absolute position-edit-5-0"-->
                                        <!--                data-id="{{ $brandIdentity->id }}" data-type="brandIdentity"-->
                                        <!--                data-key="customer_promise" data-name="customer promise"-->
                                        <!--                data-value="{{ $brandIdentity->customer_promise }}"-->
                                        <!--                 role="button"><i-->
                                        <!--                    class="ti ti-pencil-alt text-xs"></i>-->
                                        <!--    </div>-->
                                        <!--    <p id="brandIdentity-customer_promise">{{ $brandIdentity->customer_promise }}</p>-->
                                        <!--</div>-->
                                    </div>
                                @endif
                                
                            </div>
                        </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>