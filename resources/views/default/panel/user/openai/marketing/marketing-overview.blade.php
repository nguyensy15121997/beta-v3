<div class="group float-button float-edit-botton fixed bottom-20 end-12 z-50 lg:block lang_{{app()->getLocale()}}">
    <button
        onclick="toggleEditButton()"
        class="d-flex align-items-center justify-center collapsed peer h-14 w-14 translate-x-0 transform-gpu overflow-hidden rounded-full border-none bg-edit p-0 !shadow-lg"
        type="button">
        <svg width="20" height="20" viewBox="0 0 15 14" fill="white" stroke="#333333" xmlns="http://www.w3.org/2000/svg">
        <path d="M8.71875 2.43988L11.9688 5.58995M10.75 11.4963H14M4.25 13.0714L12.7812 4.80248C12.9946 4.59564 13.1639 4.35009 13.2794 4.07984C13.3949 3.8096 13.4543 3.51995 13.4543 3.22744C13.4543 2.93493 13.3949 2.64528 13.2794 2.37504C13.1639 2.10479 12.9946 1.85924 12.7812 1.6524C12.5679 1.44557 12.3145 1.28149 12.0357 1.16955C11.7569 1.05761 11.458 1 11.1562 1C10.8545 1 10.5556 1.05761 10.2768 1.16955C9.99799 1.28149 9.74465 1.44557 9.53125 1.6524L1 9.92135V13.0714H4.25Z" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
    </button>
</div>
<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>

    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-sm-12">
            <div class="row h-full" id="0">
                @if ($project)
                    <div id="marketingOverview-body" class="mt-3 col-12 {{ is_null($marketingOverview) ? 'blur-content loading' : '' }}">
                        <div class="overlay">
                            <div class="cv-spinner">
                                <span class="spinner"></span>
                            </div>
                        </div>
                        <div class="{{ is_null($marketingOverview) ? 'blur-content-card' : '' }} mt-0">
                            <div class="card-header d-block">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center justify-center">
                                        <h2 class="mb-0">{{ __('Marketing Overview') }}</h2>
                                        <img src="{{ url('').'/images/info-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                                    </div>
                                    <div>
                                        <button onclick="regenerateProjectInfo({{ $project->id }}, 'marketingOverview')"
                                            class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                                        </button>
                                    </div>
                                    <!--<div>-->
                                    <!--    <button onclick="regenerateProjectInfo({{ $project->id }}, 'marketingOverview')"-->
                                    <!--        class="btn btn-success btn-sm"><i class="ti ti-reload"></i></button>-->
                                    <!--    <button onclick="collapseByType('marketingOverview', 'minius')"-->
                                    <!--        id="marketingOverview-minius" class="btn btn-success btn-sm"><i-->
                                    <!--            class="ti ti-minus"></i></button>-->
                                    <!--    <button onclick="collapseByType('marketingOverview', 'plus')" id="marketingOverview-plus"-->
                                    <!--        style="display: none" class="btn btn-success btn-sm"><i-->
                                    <!--            class="ti ti-plus"></i></button>-->
                                    <!--    <button-->
                                    <!--        onclick="deleteProjectInfo({{ $project->id }}, 'marketingOverview', '#marketingOverview-body')"-->
                                    <!--        class="btn btn-danger btn-sm"><i class="ti ti-close"></i></button>-->
                                    <!--</div>-->
    
                                </div>
                            </div>
                            @if (!empty($marketingOverview))
                                <div class="card">
                                    <h3 class="p-3 mb-0">{{ __('Business Funnel') }}</h3>
                                    <div
                                      class="m-4 mt-0 rounded-lg bg-white flex flex-col items-center justify-content-center relative group mb-4 p-2 lg:p-4 false"
                                      style="white-space: normal"
                                      id="marketingOverview-content"
                                    >
                                        <div class="row">
                                            <div
                                              class="flex col-12 col-lg-4 relative h-full justify-center "
                                            >
                                                <img src="{{ url('').'/images/business-funnel-bg.png' }}" class="w-full h-full" alt="s-icon">
                                                <div class="absolute text-center" style="top: 8%;width: 100%;">
                                                    <h4 style="color: white;">{{ __('AWARENESS') }}</h4>
                                                </div>
                                                <div class="absolute text-center" style="top: 28%;width: 100%;">
                                                    <h4 style="color: white;">{{ __('INTEREST') }}</h4>
                                                </div>
                                                <div class="absolute text-center" style="top: 48%;width: 100%;">
                                                    <h4 style="color: white;">{{ __('CONSIDERATION') }}</h4>
                                                </div>
                                                <div class="absolute text-center" style="top: 68%;width: 100%;">
                                                    <h4 style="color: white;">{{ __('CONVERTION') }}</h4>
                                                </div>
                                                <div class="absolute text-center" style="top: 88%;width: 100%;">
                                                    <h4 style="color: white;">{{ __('RETENTION') }}</h4>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-8 text-sm items-center text-center lg:text-left justify-center"
                                            >
                                              <div class="h-1/5 flex border rounded-xl w-full">
                                                  <div class="flex align-items-center justify-content-center rounded-xl" style="border-radius: .75rem .75rem 0 0;min-width: 30px; background: #B24B0A; color: white;">
                                                      1
                                                  </div>
                                                  <div class="px-3 h-full">
                                                      <div class="position-relative w-full text-left h-1/2 h-1/2 pt-1">
                                                        <strong>{{ __('Objective:') }}</strong>
                                                        <div
                                                          class="edit-popup position-absolute position-edit-0-0"
                                                          data-id="{{ $marketingOverview->id }}"
                                                          data-type="marketingOverview"
                                                          data-key="awareness_objective"
                                                          data-name="awareness objective"
                                                          data-value="{{ $marketingOverview->awareness_objective }}"
                                                          role="button"
                                                        >
                                                          <i class="ti ti-pencil-alt text-xs"></i>
                                                        </div>
                                                        <span class="pr-2" id="marketingOverview-awareness_objective"
                                                          >{{ $marketingOverview->awareness_objective }}</span
                                                        >
                                                      </div>
                                                      <div class="position-relative w-full text-left h-1/2 h-1/2">
                                                        <strong>{{ __('Tactics:') }}</strong>
                                                        <div
                                                          class="edit-popup position-absolute position-edit-0-0"
                                                          data-id="{{ $marketingOverview->id }}"
                                                          data-type="marketingOverview"
                                                          data-key="awareness_tatics"
                                                          data-name="awareness tatics"
                                                          data-value="{{ $marketingOverview->awareness_tatics }}"
                                                          role="button"
                                                        >
                                                          <i class="ti ti-pencil-alt text-xs"></i>
                                                        </div>
                                                        <span class="pr-2" id="marketingOverview-awareness_tatics"
                                                          >{{ $marketingOverview->awareness_tatics }}</span
                                                        >
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="h-1/5 flex border rounded-xl w-full">
                                                  <div class="flex align-items-center justify-content-center" style="min-width: 30px; background: #B24B0A; color: white;">
                                                      2
                                                  </div>
                                                <div class="px-3 h-full">
                                                    <div class="position-relative w-full text-left h-1/2 pt-1">
                                                      <strong>{{ __('Objective:') }}</strong>
                                                      <div
                                                        class="edit-popup position-absolute position-edit-0-0"
                                                        data-id="{{ $marketingOverview->id }}"
                                                        data-type="marketingOverview"
                                                        data-key="interest_objective"
                                                        data-name="interest objective"
                                                        data-value="{{ $marketingOverview->interest_objective }}"
                                                        role="button"
                                                      >
                                                        <i class="ti ti-pencil-alt text-xs"></i>
                                                      </div>
                                                      <span class="pr-2" id="marketingOverview-interest_objective"
                                                        >{{ $marketingOverview->interest_objective }}</span
                                                      >
                                                    </div>
                                                    <div class="position-relative w-full text-left h-1/2">
                                                      <strong>{{ __('Tactics:') }}</strong>
                                                      <div
                                                        class="edit-popup position-absolute position-edit-0-0"
                                                        data-id="{{ $marketingOverview->id }}"
                                                        data-type="marketingOverview"
                                                        data-key="interest_tatics"
                                                        data-name="interest tatics"
                                                        data-value="{{ $marketingOverview->interest_tatics }}"
                                                        role="button"
                                                      >
                                                        <i class="ti ti-pencil-alt text-xs"></i>
                                                      </div>
                                                      <span class="pr-2" id="marketingOverview-interest_tatics"
                                                        >{{ $marketingOverview->interest_tatics }}</span
                                                      >
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="h-1/5 flex border rounded-xl w-full">
                                                  <div class="flex align-items-center justify-content-center" style="min-width: 30px; background: #B24B0A; color: white;">
                                                      3
                                                  </div>
                                                  <div class="px-3 h-full">
                                                    <div class="position-relative w-full text-left h-1/2 pt-1">
                                                      <strong>{{ __('Objective:') }}</strong>
                                                      <div
                                                        class="edit-popup position-absolute position-edit-0-0"
                                                        data-id="{{ $marketingOverview->id }}"
                                                        data-type="marketingOverview"
                                                        data-key="consideration_objective"
                                                        data-name="consideration objective"
                                                        data-value="{{ $marketingOverview->consideration_objective }}"
                                                        role="button"
                                                      >
                                                        <i class="ti ti-pencil-alt text-xs"></i>
                                                      </div>
                                                      <span class="pr-2" id="marketingOverview-consideration_objective"
                                                        >{{ $marketingOverview->consideration_objective }}</span
                                                      >
                                                    </div>
                                                    <div class="position-relative w-full text-left h-1/2">
                                                      <strong>{{ __('Tactics:') }}</strong>
                                                      <div
                                                        class="edit-popup position-absolute position-edit-0-0"
                                                        data-id="{{ $marketingOverview->id }}"
                                                        data-type="marketingOverview"
                                                        data-key="consideration_tatics"
                                                        data-name="consideration tatics"
                                                        data-value="{{ $marketingOverview->consideration_tatics }}"
                                                        role="button"
                                                      >
                                                        <i class="ti ti-pencil-alt text-xs"></i>
                                                      </div>
                                                      <span class="pr-2" id="marketingOverview-consideration_tatics"
                                                        >{{ $marketingOverview->consideration_tatics }}</span
                                                      >
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="h-1/5 flex border rounded-xl w-full">
                                                  <div class="flex align-items-center justify-content-center" style="min-width: 30px; background: #B24B0A; color: white;">
                                                      4
                                                  </div>
                                                  <div class="px-3 h-full">
                                                    <div class="position-relative w-full text-left h-1/2 pt-1">
                                                      <strong>{{ __('Objective:') }}</strong>
                                                      <div
                                                        class="edit-popup position-absolute position-edit-0-0"
                                                        data-id="{{ $marketingOverview->id }}"
                                                        data-type="marketingOverview"
                                                        data-key="conversion_objective"
                                                        data-name="conversion objective"
                                                        data-value="{{ $marketingOverview->conversion_objective }}"
                                                        role="button"
                                                      >
                                                        <i class="ti ti-pencil-alt text-xs"></i>
                                                      </div>
                                                      <span class="pr-2" id="marketingOverview-conversion_objective"
                                                        >{{ $marketingOverview->conversion_objective }}</span
                                                      >
                                                    </div>
                                                    <div class="position-relative w-full text-left h-1/2">
                                                      <strong>{{ __('Tactics:') }}</strong>
                                                      <div
                                                        class="edit-popup position-absolute position-edit-0-0"
                                                        data-id="{{ $marketingOverview->id }}"
                                                        data-type="marketingOverview"
                                                        data-key="conversion_tatics"
                                                        data-name="conversion tatics"
                                                        data-value="{{ $marketingOverview->conversion_tatics }}"
                                                        role="button"
                                                      >
                                                        <i class="ti ti-pencil-alt text-xs"></i>
                                                      </div>
                                                      <span class="pr-2" id="marketingOverview-conversion_tatics"
                                                        >{{ $marketingOverview->conversion_tatics }}</span
                                                      >
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="h-1/5 flex border rounded-xl w-full">
                                                  <div class="flex align-items-center justify-content-center rounded-xl" style="border-radius: 0 0 .75rem .75rem;min-width: 30px; background: #B24B0A; color: white;">
                                                      5
                                                  </div>
                                                  <div class="px-3 h-full">
                                                    <div class="position-relative w-full text-left h-1/2 pt-1">
                                                      <strong>{{ __('Objective:') }}</strong>
                                                      <div
                                                        class="edit-popup position-absolute position-edit-0-0"
                                                        data-id="{{ $marketingOverview->id }}"
                                                        data-type="marketingOverview"
                                                        data-key="retention_objective"
                                                        data-name="retention objective"
                                                        data-value="{{ $marketingOverview->retention_objective }}"
                                                        role="button"
                                                      >
                                                        <i class="ti ti-pencil-alt text-xs"></i>
                                                      </div>
                                                      <span class="pr-2" id="marketingOverview-retention_objective"
                                                        >{{ $marketingOverview->retention_objective }}</span
                                                      >
                                                    </div>
                                                    <div class="position-relative w-full text-left h-1/2">
                                                      <strong>{{ __('Tactics:') }}</strong>
                                                      <div
                                                        class="edit-popup position-absolute position-edit-0-0"
                                                        data-id="{{ $marketingOverview->id }}"
                                                        data-type="marketingOverview"
                                                        data-key="retention_tatics"
                                                        data-name="retention tatics"
                                                        data-value="{{ $marketingOverview->retention_tatics }}"
                                                        role="button"
                                                      >
                                                        <i class="ti ti-pencil-alt text-xs"></i>
                                                      </div>
                                                      <span class="pr-2" id="marketingOverview-retention_tatics"
                                                        >{{ $marketingOverview->retention_tatics }}</span
                                                      >
                                                    </div>
                                                  </div>
                                              </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="p-4 pt-2">
                                        <div>
                                            <div class="my-2 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                            <img src="{{ url('').'/images/marketing-strategy-icon.png' }}" width="22" height="22" class="mb-1" alt="s-icon">
                                            <h3 class="">{{ __('Marketing Strategy Overview') }}</h3>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $marketingOverview->id }}" data-type="marketingOverview"
                                                    data-key="marketing_strategy_overview" data-name="marketing strategy overview"
                                                    data-value="{{ $marketingOverview->marketing_strategy_overview }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="marketingOverview-marketing_strategy_overview">{{ $marketingOverview->marketing_strategy_overview }}</p>
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <div class="my-2 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                            <img src="{{ url('').'/images/approach-icon.png' }}" width="22" height="22" class="mb-1" alt="s-icon">
                                            <h3>{{ __('Approach') }}</h3>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $marketingOverview->id }}" data-type="marketingOverview"
                                                    data-key="approach" data-name="approach"
                                                    data-value="{{ $marketingOverview->approach }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="marketingOverview-approach">{{ $marketingOverview->approach }}</p>
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <div class="my-2 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                            <img src="{{ url('').'/images/goals-icon.png' }}" width="22" height="22" class="mb-1" alt="s-icon">
                                            <h3>{{ __('Goals') }}</h3>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $marketingOverview->id }}" data-type="marketingOverview"
                                                    data-key="goals" data-name="goals"
                                                    data-value="{{ $marketingOverview->goals }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="marketingOverview-goals">{{ $marketingOverview->goals }}</p>
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <div class="my-2 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                            <img src="{{ url('').'/images/primary-icon.png' }}" width="22" height="22" class="mb-1" alt="s-icon">
                                            <h3>{{ __('Primary Channels') }}</h3>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $marketingOverview->id }}" data-type="marketingOverview"
                                                    data-key="primary_channels" data-name="primary channels"
                                                    data-value="{{ $marketingOverview->primary_channels }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="marketingOverview-primary_channels">{{ $marketingOverview->primary_channels }}</p>
                                            </div>
                                        </div>
                                        <div class="mt-4">
                                            <div class="my-2 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                                            <img src="{{ url('').'/images/budget-icon.png' }}" width="22" height="22" class="mb-1" alt="s-icon">
                                            <h3>{{ __('Budget') }}</h3>
                                            <div class="position-relative w-full">
                                                <div class="edit-popup position-absolute position-edit-5-0"
                                                    data-id="{{ $marketingOverview->id }}" data-type="marketingOverview"
                                                    data-key="budget" data-name="budget"
                                                    data-value="{{ $marketingOverview->budget }}"
                                                     role="button"><i class="ti ti-pencil-alt text-xs"></i>
                                                </div>
                                                <p id="marketingOverview-budget">{{ $marketingOverview->budget }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                @endif

            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>