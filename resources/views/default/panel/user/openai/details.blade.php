<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5" style="border-radius: 20px;">
  <form action="{{ route('projects.generate-competitive-analysis', ['project' => $project->id]) }}" method="POST"
      id="generate-competitive-form" style="display: none;">
      @csrf
  </form>
  <form action="{{ route('projects.generate-user-stories', ['project' => $project->id]) }}" method="POST"
      id="generate-stories-form" style="display: none;">
      @csrf
  </form>
  <form action="{{ route('projects.generate-user-personas', ['project' => $project->id]) }}" method="POST"
      id="generate-personas-form" style="display: none;">
      @csrf
  </form>
  <form action="{{ route('projects.generate-swot', ['project' => $project->id]) }}" method="POST"
      id="generate-swot-form" style="display: none;">
      @csrf
  </form>
   <form action="{{ route('projects.generate-customer-interview', ['project' => $project->id]) }}" method="POST"
      id="generate-customer-interview" style="display: none;">
      @csrf
  </form>
  <form action="{{ route('projects.generate-storytelling', ['project' => $project->id]) }}" method="POST"
      id="generate-storytelling-form" style="display: none;">
      @csrf
  </form>
  <form action="{{ route('projects.generate-lean-canvas', ['project' => $project->id]) }}" method="POST"
      id="generate-leancanvas-form" style="display: none;">
      @csrf
  </form>
  <form action="{{ route('projects.generate-logo', ['project' => $project->id]) }}" method="POST"
      id="generate-logo-form" style="display: none;">
      @csrf
  </form>
  
  <form action="{{ route('projects.generate-brand-identity', ['project' => $project->id]) }}" method="POST"
      id="generate-brand-identity" style="display: none;">
      @csrf
  </form>
  
  <form action="{{ route('projects.generate-industry-overview', ['project' => $project->id]) }}" method="POST"
      id="generate-industry-overview" style="display: none;">
      @csrf
  </form>
  
  <form action="{{ route('projects.generate-marketing-overview', ['project' => $project->id]) }}" method="POST"
      id="generate-marketing-overview" style="display: none;">
      @csrf
  </form>
  
  <form action="{{ route('projects.generate-market-size', ['project' => $project->id]) }}" method="POST"
      id="generate-market-size" style="display: none;">
      @csrf
  </form>
  
  <form action="{{ route('projects.generate-viability-analysis', ['project' => $project->id]) }}" method="POST"
      id="generate-viability-analysis" style="display: none;">
      @csrf
  </form>


  <!-- Tab Content -->
  <div id="concept">
      <div
        role="alert"
        class="relative text-sm rounded-lg p-3 [&amp;>svg~*]:pl-7 [&amp;>svg+div]:translate-y-[-3px] [&amp;>svg]:absolute [&amp;>svg]:left-4 [&amp;>svg]:top-4 [&amp;>svg]:text-neutral-950 dark:border-neutral-800 dark:[&amp;>svg]:text-neutral-50 bg-green-100 mb-4 flex-col w-full gap-3 items-center text-center sm:flex-row sm:text-left border-none flex"
        id="generate-success"
        style="display: none"
      >
        <div>
          <h5
            class="text-sm mb-0 font-medium leading-none flex items-center flex-col sm:flex-row space-y-2 sm:space-x-2 sm:space-y-0"
          >
            <span class="font-medium flex items-center space-x-2"
              ><svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
                class="w-4"
              >
                <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                <polyline points="22 4 12 14.01 9 11.01"></polyline></svg
              ><span>Done!</span></span
            >
          </h5>
        </div>
        <div class="ml-auto text-center shrink-0 items-center flex w-1/4">
          <span class="text-xs font-medium whitespace-nowrap mr-2">{{ __('9 of 9') }}</span>
          <div
            aria-valuemax="100"
            aria-valuemin="0"
            role="progressbar"
            data-state="indeterminate"
            data-max="100"
            class="relative overflow-hidden rounded-full dark:bg-neutral-800 h-2 w-full my-1 bg-white false"
          >
            <div
              data-state="indeterminate"
              data-max="100"
              class="h-full w-full flex-1 bg-neutral-900 rounded-full transition-all dark:bg-neutral-50"
              style="transform: translateX(0%); background: darkslategray;"
            ></div>
          </div>
        </div>
        <button
          class="inline-flex items-center justify-center rounded-md text-[15px] font-normal ring-offset-background focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 cursor disabled:opacity-50 hover:text-accent-foreground h-7 w-7 text-muted-foreground hover:bg-black/5 active:bg-black/10 transition-colors group"
          id="close-generate-success"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            stroke-width="2" 
            stroke-linecap="round"
            stroke-linejoin="round"
            class="w-4 h-4"
          >
            <path d="M18 6 6 18"></path>
            <path d="m6 6 12 12"></path>
          </svg>
        </button>
      </div>

      <div
        role="alert"
        class="relative text-sm rounded-lg p-3 [&amp;>svg~*]:pl-7 [&amp;>svg+div]:translate-y-[-3px] [&amp;>svg]:absolute [&amp;>svg]:left-4 [&amp;>svg]:top-4 [&amp;>svg]:text-neutral-950 dark:border-neutral-800 dark:[&amp;>svg]:text-neutral-50 bg-neutral-100 text-neutral-950 dark:bg-neutral-950 dark:text-neutral-50 mb-4 flex-col w-full gap-3 items-center text-center sm:flex-row sm:text-left border-none flex"
          id="under-generating"
          style="display: none"
      >
        <div>
          <h5
            class="text-sm mb-0 font-medium leading-none flex items-center flex-col sm:flex-row space-y-2 sm:space-x-2 sm:space-y-0"
          >
            <span class="font-medium flex items-center space-x-2"
              ><svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
                class="w-4 animate-spin shrink-0"
              >
                <path d="M21 12a9 9 0 1 1-6.219-8.56"></path></svg
              ><span>Generating</span></span
            ><span class="font-normal  mt-0"> {{ __('This can take a few minutes.') }} </span>
          </h5>
        </div>
        <div class="ml-auto text-center shrink-0 items-center flex w-1/4">
          <span class="text-xs font-medium whitespace-nowrap mr-2"><span class="count-number"> 13 </span> {{ __('of 9') }} </span>
          <div
            aria-valuemax="100"
            aria-valuemin="0"
            role="progressbar"
            data-state="indeterminate"
            data-max="100"
            class="relative overflow-hidden rounded-full dark:bg-neutral-800 h-2 w-full my-1 bg-white animate-pulse"
          >
            <div
              data-state="indeterminate"
              data-max="100"
              class="h-full w-full flex-1 bg-neutral-900 rounded-full transition-all dark:bg-neutral-50 percent-count"
              style="background: darkslategray;"
            ></div>
          </div>
        </div>
      </div>

      <div class="pt-3 pb-0 d-block">
          <div class="d-flex justify-content-between align-items-center">
              <div>
                    <div class="d-flex align-items-center pb-3">
                        <h2 class="mb-0">{{ __('Overview') }}</h2>
                        <img src="{{ url('').'/images/info-icon.png' }}" width="20" height="20" class="mx-2" alt="info-icon">
                    </div>
                  <p class="pb-3">{{ $project->description }}</p>
              </div>
              <div>
                  <button
                      class="btn btn-success btn-sm" style="background: #fa690f"><span id="regenerate" class="mr-2 regenerate-animation" style="display: none;"> {{ __('Regenerate ') }} </span> <i class="ti ti-reload"></i>
                  </button>
              </div>
          </div>
      </div>
      <div class="row" id="overview">
          <!-- [ sample-page ] start -->
          <div class="col-sm-12">
              <div class="h-full" id="0">
                  @if ($project)
                      <h3 class="mb-0">{{ __('Concept') }}</h3>
                      <div class="row">
                          <div id="swot-body" class="mt-6 col-12 col-md-6 {{ is_null($swotAnalysis) ? 'blur-content loading' : '' }}" style="min-height: 450px;">
                              <div class="overlay" style="height: 450px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>
                              <div class="card mt-0 card-hover {{ is_null($swotAnalysis) ? '' : '' }} relative " style="height: 450px; overflow: auto;" 
                               onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.swot-analysis', ['project' => getProjectInfo()->id])) }}')">
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('SWOT Analysis') }}
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                      </div>
                                  </div>
                                  @if (!empty($swotAnalysis))
                                  <div class="py-1 p-1 p-sm-4" id="swot-content">
                                  <div class="swot-container">
                                      <!-- Strengths -->
                                      <div class="swot-section position-relative box-strengths">
                                          <div class="swot-content fs-1rem" id="swotAnalysis-strengths">
                                              {!! truncateTextTo80($swotAnalysis->strengths) ?? 'Not generated.' !!}</div>
                                          <div class="absolute" style="right: -8px;bottom: 0px;">
                                              <img src="{{ url('').'/images/s-pie.png' }}" width="80" height="80" class="mr-2" alt="s-pie">
                                          </div>
                                          <div class="d-flex align-items-center justify-center mt-3">
                                              <img src="{{ url('').'/images/s-icon.png' }}" width="30" height="18" class="mr-2" alt="s-icon">
                                              <div class="swot-title fs-18 font-bold">{{ __('Strengths') }}</div>
                                          </div>
                                      </div>
  
                                      <!-- Weaknesses -->
                                      <div class="swot-section position-relative box-weaknesses">
                                          <div class="swot-content fs-1rem" id="swotAnalysis-weaknesses">
                                              {!! truncateTextTo80($swotAnalysis->weaknesses) ?? 'Not generated.' !!}</div>
                                          <div class="absolute" style="left: -1px;bottom: 0px;">
                                              <img src="{{ url('').'/images/w-pie.png' }}" width="80" height="80" class="mr-2" alt="w-pie">
                                          </div>
                                          <div class="d-flex align-items-center justify-center mt-3">
                                              <img src="{{ url('').'/images/w-icon.png' }}" width="30" height="18" class="mr-2" alt="w-icon">
                                              <div class="swot-title fs-18 font-bold">{{ __('Weaknesses') }}</div>
                                          </div>
                                      </div>
  
                                      <!-- Opportunities -->
                                      <div class="swot-section position-relative box-opportunities">
                                          <div class="d-flex align-items-center justify-center mb-3">
                                              <img src="{{ url('').'/images/o-icon.png' }}" width="30" height="18" class="mr-2" alt="o-icon">
                                              <div class="swot-title fs-18 font-bold">{{ __('Opportunities') }}</div>
                                          </div>
                                          <div class="swot-content fs-1rem" id="swotAnalysis-opportunities">
                                              {!! truncateTextTo80($swotAnalysis->opportunities) ?? 'Not generated.' !!}</div>
                                          <div class="absolute" style="right: -8px;top: -1px;">
                                              <img src="{{ url('').'/images/o-pie.png' }}" width="80" height="80" class="mr-2" alt="o-pie">
                                          </div>
                                      </div>
  
                                      <!-- Threats -->
                                      <div class="swot-section position-relative box-threats">
                                          <div class="d-flex align-items-center justify-center mb-3">
                                              <img src="{{ url('').'/images/t-icon.png' }}" width="30" height="18" class="mr-2" alt="t-icon">
                                              <div class="swot-title fs-18 font-bold">{{ __('Threats') }}</div>
                                          </div>
                                          <div class="swot-content fs-1rem" id="swotAnalysis-threats">
                                              {!! truncateTextTo80($swotAnalysis->threats) ?? 'Not generated.' !!}</div>
                                          <div class="absolute" style="left: -1px;top: -1px;">
                                              <img src="{{ url('').'/images/t-pie.png' }}" width="80" height="80" class="mr-2" alt="t-pie">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                                  @endif
                              </div>
                          </div>
                          <div id="leanCanvas-body" class="mt-6 col-12 col-md-6 {{ is_null($leanCanvas) ? 'blur-content loading' : '' }}" style="min-height: 450px;">
                              <div class="card  card-hover {{ is_null($leanCanvas) ? '' : '' }} mt-0" style="height: 450px; overflow: auto;"
                              onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.lean-canvas', ['project' => getProjectInfo()->id])) }}')">  
                              <div class="overlay" style="height: 450px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>                               
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('Lean Canvas') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.lean-canvas', ['project' => getProjectInfo()->id])) }}')" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                      </div>
                                  </div>
                                  @if (!empty($leanCanvas))
                                      <div class="py-1 p-2" id="leanCanvas-content">
                                          <main class="lean-canvas">
                                              <section class="canvas-block position-relative" id="problem">
                                                  <img src="{{ url('').'/images/problem-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Problem') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-problem">
                                                      {!! truncateTextTo50($leanCanvas->problem) ?? 'Not generated.' !!}
                                                  </p>
                                              </section>
                                              <section class="canvas-block position-relative" id="solution">
                                                  <img src="{{ url('').'/images/solution-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Solution') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-solution">
                                                      {!! truncateTextTo50($leanCanvas->solution) ?? 'Not generated.' !!}
                                                  </p>
                                              </section>
                                              <section class="canvas-block position-relative" id="unique-value-prop">
                                                  <img src="{{ url('').'/images/problem-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Value Proposition') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-unique_value_proposition">
                                                      {!! truncateTextTo100($leanCanvas->unique_value_proposition) ?? 'Not generated.' !!}</p>
                                              </section>
                                              <section class="canvas-block position-relative" id="unfair-advantage">
                                                  <img src="{{ url('').'/images/unfair-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Unfair Advantage') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-unfair_advantage">
                                                      {!! truncateTextTo100($leanCanvas->unfair_advantage) ?? 'Not generated.' !!}</p>
                                              </section>
                                              <section class="canvas-block position-relative" id="customer-segments">
                                                  <img src="{{ url('').'/images/customer-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Customer Segments') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-customer_segments">
                                                      {!! truncateTextTo50($leanCanvas->customer_segments) ?? 'Not generated.' !!}</p>
                                              </section>
                                              <section class="canvas-block position-relative" id="existing-alternatives">
                                                  <img src="{{ url('').'/images/existing-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Existing Alternatives') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-existing_alternatives">
                                                      {!! truncateTextTo50($leanCanvas->existing_alternatives) ?? 'Not generated.' !!}</p>
                                              </section>
                                              <section class="canvas-block position-relative" id="key-metrics">
                                                  <img src="{{ url('').'/images/metrics-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Key Metrics') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-key_metrics">
                                                      {!! truncateTextTo50($leanCanvas->key_metrics) ?? 'Not generated.' !!}
                                                  </p>
                                              </section>
                                              <section class="canvas-block position-relative" id="channels">
                                                  <img src="{{ url('').'/images/channel-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Channels') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-channels">
                                                      {!! truncateTextTo50($leanCanvas->channels) ?? 'Not generated.' !!}
                                                  </p>
                                              </section>
                                              <section class="canvas-block position-relative" id="cost-structure">
                                                  <img src="{{ url('').'/images/cost-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Cost Structure') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-cost_structure">
                                                      {!! truncateTextTo70($leanCanvas->cost_structure) ?? 'Not generated.' !!}</p>
                                              </section>
                                              <section class="canvas-block position-relative" id="revenue-streams">
                                                  <img src="{{ url('').'/images/revenue-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                                  <h3 class="mb-0">{{ __('Revenue Streams') }}</h3>
                                                  <p class="fs-1rem" id="leanCanvas-revenue_streams">
                                                      {!! truncateTextTo70($leanCanvas->revenue_streams) ?? 'Not generated.' !!}</p>
                                              </section>
                                          </main>
                                      </div>
                                  @endif
                                </div>
                          </div>
                          <div id="customerInterview-body" class="mt-6 col-12 col-md-12 {{ is_null($customerInterview) ? 'blur-content loading' : '' }}"  style="min-height: 300px;"
                          >
                              <div class="card card-hover {{ is_null($customerInterview) ? '' : '' }} mt-0" style="height: 300px; overflow: auto;"
                              onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.customer-interview-questions', ['project' => getProjectInfo()->id])) }}')" >     
                                  <div class="overlay" style="height: 300px;">
                                      <div class="cv-spinner">
                                          <span class="spinner"></span>
                                      </div>
                                  </div>                
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('Customer Validation Interviews') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                      </div>
                                  </div>
                                @if (!empty($customerInterview))
                                  <div class="py-1 p-2" id="customerInterview-content">
                                      <div class="card p-4">
                                          <div class="row">
                                              <div class="col-12 col-md-3 d-flex">
                                                  <div class="w-full">
                                                      <div class="d-flex align-items-center mb-3 w-full">
                                                          <img src="{{ url('').'/images/disclaimer-icon.png' }}" style="height: 20px;" width="20" height="20" alt="info-icon">
                                                          <h2 class="mb-0 ml-1 fs-18" style="text-wrap: nowrap;">{{ __('Disclaimers') }}</h2>
                                                          <div class="ml-2 block-line"></div>
                                                      </div>
                                                      <div class="ml-1 w-6/7 fs-1rem" id="list-disclaimers">
                                                          <p>{{ __('The purpose of the interview is to help us gain a better understanding of...') }}</p>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12 col-md-3 d-flex">
                                                  <div class="w-full">
                                                      <div class="d-flex align-items-center mb-3">
                                                          <img src="{{ url('').'/images/demographic-icon.png' }}" width="20" height="20" alt="info-icon">
                                                          <h2 class="mb-0 ml-1 fs-18">{{ __('Demographic') }}</h2>
                                                          <div class="ml-2 block-line"></div>
                                                      </div>
                                                      <ul class="ml-1 w-6/7" id="list-demographic">
                                                          <li class="position-relative question-container">
                                                              <p class="fs-1rem" id="customerInterview-question_11">
                                                                  {{ truncateTextTo50($customerInterview->question_11) ?? __('Not generated.') }}
                                                              </p>
                                                          </li>
                                                            <li class="position-relative question-container">
                                                                <p class="fs-1rem" id="customerInterview-question_12">
                                                                    {{ truncateTextTo50($customerInterview->question_12) ?? __('Not generated.') }}
                                                                </p>
                                                            </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                              <div class="col-12 col-md-3 d-flex">
                                                  <div class="w-full">
                                                      <div class="d-flex align-items-center mb-3">
                                                          <img src="{{ url('').'/images/depth-icon.png' }}" width="20" height="20" alt="info-icon">
                                                          <h2 class="mb-0 ml-1 fs-18" style="text-wrap: nowrap;">{{ __('In-Depth') }}</h2>
                                                          <div class="ml-2 block-line"></div>
                                                      </div>
                                                      <ul class="ml-1 w-6/7" id="list-depth">
                                                          <li class="position-relative question-container">
                                                              <p class="fs-1rem" id="customerInterview-question_1">
                                                                  {{ truncateTextTo50($customerInterview->question_1) ?? __('Not generated.') }}
                                                              </p>
                                                          </li>
                                                        <li class="position-relative question-container">
                                                            <p class="fs-1rem" id="customerInterview-question_2">
                                                                {{ truncateTextTo50($customerInterview->question_2) ?? __('Not generated.') }}
                                                            </p>
                                                        </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                              <div class="col-12 col-md-3 d-flex">
                                                  <div class="w-full">
                                                      <div class="d-flex align-items-center mb-3">
                                                          <img src="{{ url('').'/images/wrap-up-icon.png' }}" width="20" height="20" alt="info-icon">
                                                          <h2 class="mb-0 ml-1 fs-18" style="text-wrap: nowrap;">{{ __('Wrap-Up') }}</h2>
                                                      </div>
                                                      <ul class="ml-1 w-6/7" id="list-wrap">
                                                          <li class="position-relative question-container">
                                                              <p class="fs-1rem" id="customerInterview-question_14">
                                                                  {{ truncateTextTo50($customerInterview->question_14) ?? __('Not generated.') }}
                                                              </p>
                                                          </li>
                                                          <li class="position-relative question-container">
                                                              <p class="fs-1rem" id="customerInterview-question_15">
                                                                  {{ truncateTextTo50($customerInterview->question_15) ?? __('Not generated.') }}
                                                              </p>
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                @endif
                            </div>
                          </div>
                          <div id="userPersonas-body" class="mt-6 col-12 col-md-8 {{ $userPersonas->isEmpty() ? 'blur-content loading' : '' }}" style="min-height: 400px;">
                              <div class="overlay" style="height: 400px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>
                              <div class=" card-hover {{ $userPersonas->isEmpty() ? '' : '' }} card mt-0" style="height: 400px; overflow: auto;"
                              onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.user-personas', ['project' => getProjectInfo()->id])) }}')">
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('User Personas') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                      </div>
                                  </div>
                                  @if (!empty($userPersonas))
                                    <div class="py-1 p-2" id="userPersonas-content">
                                          <div class="row">
                                              @if ($userPersonas->count() > 0)
                                                  @foreach ($userPersonas as $key => $user)
                                                      @if ($key < 4)
                                                          <div class="col-6 col-lg-3">
                                                            <div class="card">
                                                              <div class="card-body">
                                                                  @if (isset($user->url))
                                                                  <img src="{{ url('').'/testimonialAvatar/test/'.$user->url }}"
                                                                      class="card-img w-full mx-auto" alt="Avatar 1" style="height: 180px">
                                                                  @endif
                                                                  <div class="fs-18 font-bold mt-1">
                                                                      {{ $user->name }}, {{ $user->age }}
                                                                  </div>
                                                                  <p class="fs-1rem">{{ truncateTextTo70($user->description) }}</p>
                                                              </div>
                                                          </div>
                                                          </div>
                                                      @endif
                                                  @endforeach
                                              @endif
                                          </div>
                                      </div>
                                  @endif
                              </div>
                          </div>
                          <div id="userStories-body" class="mt-6 col-12 col-md-4 {{ $userStories->isEmpty() ? 'blur-content loading' : '' }}" style="min-height: 400px;">
                              <div class="overlay" style="height: 400px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>
                              <div class="card-hover{{ $userStories->isEmpty() ? '' : '' }} card mt-0" style="height: 400px; overflow: auto;"
                              onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.user-stories', ['project' => getProjectInfo()->id])) }}')">
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('User Stories') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                      </div>
                                  </div>
                                  @if (!empty($userStories))
                                  <div class="p-2 py-1" id="userStories-content">
                                      <div class="">
                                          @foreach ($userStories as $key => $user)
                                              @if ($key < 2)
                                                <div class="card">
                                                    <div class="row">
                                                        <div class="col-5 border-box-right" style="">
                                                            <div class="p-2 py-3">
                                                                <img src="{{ url('').'/images/user-story-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                                                <h3 class="my-2">{{ __('User story') }}</h3>
                                                                <div class="relative">
                                                                    <span class="fs-16" id="userStories-user_story-{{ $user->id }}">
                                                                        {{ truncateTextTo50($user->user_story) ?? 'Not generated.' }}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-7">
                                                            <div class="p-2 py-3 mb-0">
                                                                <img src="{{ url('').'/images/criteria-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                                                <h3 class="my-2">{{ __('Acceptance Criteria') }}</h3>
                                                                <div class="relative">
                                                                    <span class="fs-16" id="userStories-acceptance_criterea-{{ $user->id }}">
                                                                        {!! truncateTextTo50($user->acceptance_criterea) ?? 'Not generated.' !!}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              @endif
                                          @endforeach
                                      </div>
                                  </div>
                                  @endif
                              </div>
                          </div>
                      </div>
                      <div class="my-4 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                      <h3 class="mb-0">{{ __('Marketing') }}</h3>
                      <div class="row">
                          <div id="storytelling-overview-body" class="mt-6 col-12 col-md-7 {{ is_null($storytelling) ? 'blur-content loading' : '' }}" style="min-height: 350px;">
                              <div class="overlay" style="height: 350px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>
                              <div class="card-hover card {{ is_null($storytelling) ? '' : '' }} mt-0" style="height: 350px; overflow: auto;"
                              onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.marketing.brand-wheel', ['project' => getProjectInfo()->id])) }}')">
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('Storytelling') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                      </div>
                                  </div>
                                  @if (!empty($storytelling))
                                  <div class="py-1 p-2" id="storytelling-content">
                                    <main class="lean-canvas">
                                      <section class="canvas-block position-relative" id="vision">
                                          <img src="{{ url('').'/images/vision-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                          <h2 class="mb-0">{{ __('Vision') }}</h2>
                                          <p class="" id="storytelling-vision">
                                              {!! truncateTextTo70($storytelling->vision) ?? 'Not generated.' !!}
                                          </p>
                                      </section>
                                      <section class="canvas-block position-relative" id="mission">
                                          <img src="{{ url('').'/images/mission-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                          <h2 class="mb-0">{{ __('Mission') }}</h2>
                                          <p class="" id="storytelling-mission">{!! truncateTextTo70($storytelling->mission) ?? 'Not generated.' !!}
                                          </p>
                                      </section>
                                      <section class="canvas-block position-relative" id="brand_positioning">
                                          <img src="{{ url('').'/images/brand-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                          <h2 class="mb-0">{{ __('Brand Proposition') }}</h2>
                                          <p class="" id="storytelling-brand_positioning">
                                              {!! truncateTextTo70($storytelling->brand_positioning) ?? 'Not generated.' !!}</p>
                                      </section>
                                      <section class="canvas-block position-relative" id="personality">
                                          <img src="{{ url('').'/images/personality-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                          <h2 class="mb-0">{{ __('Personality') }}</h2>
                                          <p class="" id="storytelling-personality">
                                              {!! truncateTextTo70($storytelling->personality) ?? 'Not generated.' !!}</p>
                                      </section>
                                      <section class="canvas-block position-relative" id="brand_values">
                                          <img src="{{ url('').'/images/elevator-icon.png' }}" width="20" height="20" class="mb-1" alt="s-icon">
                                          <h2 class="mb-0">{{ __('Brand Values') }}</h2>
                                          <p class="" id="storytelling-brand_values">
                                              {!! truncateTextTo100($storytelling->brand_values) ?? 'Not generated.' !!}</p>
                                      </section>
                                  </main>
                                  </div>
                                  @endif
                              </div>
                          </div>
                          <div id="brandIdentity-body" class="mt-6 col-12 col-md-5 {{ is_null($brandIdentity) ? 'blur-content loading' : '' }}" style="min-height: 350px;">
                              <div class="overlay" style="height: 350px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>
                              <div class="card-hover card {{ is_null($brandIdentity) ? '' : '' }} mt-0" style="height: 350px; overflow: auto;"
                               onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.marketing.brand-identity', ['project' => getProjectInfo()->id])) }}')">
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('Branding Identity') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                      </div>
                                  </div>
                                  @if (!empty($brandIdentity))
                                      <div class="py-1 p-1 p-sm-4" id="brandIdentity-content">
                                          <div class="grid grid-cols-5" style="gap: 0px">
                                              <div style="white-space: normal;">
                                                  <div>
                                                      <button class="border-left text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_1 }};">
                                                      </button>
                                                  </div>
                                              </div>
                                              <div style="white-space: normal;">
                                                  <div>
                                                      <button class="text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_2 }}">
                                                      </button>
                                                  </div>
                                              </div>
                                              <div style="white-space: normal;">
                                                  <div>
                                                      <button class="text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_3 }}">
                                                      </button>
                                                  </div>
                                              </div>
                                              <div style="white-space: normal;">
                                                  <div>
                                                      <button class="text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_4 }}">
                                                      </button>
                                                  </div>
                                              </div>
                                              <div style="white-space: normal;">
                                                  <div>
                                                      <button class="border-right text-[15px] font-normal ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 hover:bg-accent hover:text-accent-foreground active:bg-black/10 px-4 flex w-full h-17rem  group mb-1 items-center justify-center shadow-[inset_0_0_0_1px_rgba(0,0,0,0.1)] false group" type="button" aria-haspopup="dialog" aria-expanded="false" aria-controls="radix-:rg:" data-state="closed" style="background: {{ $brandIdentity->color_hex_1 }};">
                                                      </button>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="mt-4">
                                              <h1 class="libre-baskerville-bold">{{ __('Libre Baskerville Display') }}</h1>
                                          </div>
                                          <div class="mt-3">
                                              <h1>{{ __('Golos Text Bold') }}</h1>
                                          </div>
                                          <div class="mt-3">
                                              <h1 style="font-weight: 400">{{ __('Golos Text Regular') }}</h1>
                                          </div>
                                          <div class="d-flex flex-wrap mt-3">
                                                @php
                                                    if (app()->getLocale() == 'en') {
                                                        $names = isset($brandIdentity->brand_personality_traits) ? preg_split('/,\s*/', $brandIdentity->brand_personality_traits) : [];
                                                    } else {
                                                        $names = explode("، ", $brandIdentity->brand_personality_traits);
                                                    }
                                                @endphp
            
                                                @if (!empty($names))
                                                    @foreach ($names as $index => $name)
                                                        <div class="trait-square">{{ trim($name) }}</div>
                                                    @endforeach
                                                @endif
                                          </div>
                                      </div>
                                  @endif
                              </div>
                          </div>
                          <div id="viabilityAnalysis-body" class="mt-6 col-12 col-md-6 col-lg-4 {{ is_null($viabilityAnalysis) ? 'blur-content loading' : '' }}" style="min-height: 450px;">
                              <div class="overlay" style="height: 450px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>
                              <div class="card card-hover{{ is_null($viabilityAnalysis) ? '' : '' }} mt-0" style="height: 450px; overflow: auto;"
                              onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.viability-analysis', ['project' => getProjectInfo()->id])) }}')">
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('Viability Analysis') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
  
                                      </div>
                                  </div>
                                  @if (!empty($viabilityAnalysis))
                                  @php
                                      $percent = $viabilityAnalysis->percent;
                                      $img = 'chart-bg.png';
                                      if ($percent != 0 || $percent % 5 == 0) {
                                          $img = 'chart-bg-' . $percent . '.png';
                                      }
                                      if ($percent % 5 != 0) {
                                          $remainder = $percent % 5;
                                          if ($remainder == 1 || $remainder == 2) {
                                              $percent = $percent - $remainder;
                                          }
                                          if ($remainder > 2) {
                                              $percent = $percent + 5 - $remainder;
                                          }
                                          $img = 'chart-bg-' . $percent . '.png';
                                      }
                                  @endphp
                                  <div class="py-1 p-2" id="viabilityAnalysis-content">
                                      <div
                                        class="row"
                                      >
                                        <div class="col-12 px-3 relative d-flex justify-content-center">
                                          <img src="{{ url('').'/images/'.$img }}" class="w-full h-full" alt="s-icon">
                                          <p class="absolute" style="top: 35%">{{ __('Viability Percentage') }}</p>
                                          <h1 class="absolute" style="top: 47%; font-size: 3.5rem;">{{ $viabilityAnalysis->percent }} %</h1>
                                          <h2 class="absolute" style="top: 65%; ">{{ $viabilityAnalysis->percent > 50 ? __('High viability') : __('Low viability') }}</h2>
                                        </div>
                                      </div>
                                  </div>
                                  @endif
                                  
                              </div>
                          </div>
                          <div id="marketingOverview-body" class="mt-6 col-12 col-md-6 col-lg-8 {{ is_null($marketingOverview) ? 'blur-content loading' : '' }}" style="min-height: 450px;">
                              <div class="overlay" style="height: 450px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>
                              <div class="card card-hover {{ is_null($marketingOverview) ? '' : '' }} mt-0" style="height: 450px; overflow: auto;"
                               onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.marketing.marketing-overview', ['project' => getProjectInfo()->id])) }}')">
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('Marketing Overview') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
  
                                      </div>
                                  </div>
                                  @if (!empty($marketingOverview))
                                  <div class="py-1 p-2">
                                      <div
                                        class="m-4 mt-0 rounded-lg bg-white flex flex-col items-center justify-content-center relative group mb-4 p-2 lg:p-4 false"
                                        style="white-space: normal"
                                        id="marketingOverview-content"
                                      >
                                          
                                                  <!--<img src="{{ url('').'/images/business-funnel-bg.png' }}" class="w-full h-full" alt="s-icon">-->
                                                  <!--<div class="absolute text-center" style="top: 8%;width: 100%;">-->
                                                  <!--    <h4 style="color: white;" class="fs-14">{{ __('AWARENESS') }}</h4>-->
                                                  <!--</div>-->
                                                  <!--<div class="absolute text-center" style="top: 28%;width: 100%;">-->
                                                  <!--    <h4 style="color: white;" class="fs-14">{{ __('INTEREST') }}</h4>-->
                                                  <!--</div>-->
                                                  <!--<div class="absolute text-center" style="top: 48%;width: 100%;">-->
                                                  <!--    <h4 style="color: white;" class="fs-14">{{ __('CONSIDERATION') }}</h4>-->
                                                  <!--</div>-->
                                                  <!--<div class="absolute text-center" style="top: 68%;width: 100%;">-->
                                                  <!--    <h4 style="color: white;" class="fs-14">{{ __('CONVERTION') }}</h4>-->
                                                  <!--</div>-->
                                                  <!--<div class="absolute text-center" style="top: 88%;width: 100%;">-->
                                                  <!--    <h4 style="color: white;" class="fs-14">{{ __('RETENTION') }}</h4>-->
                                                  <!--</div>-->
                                              <div class="row border">
                                                  <div class="col-4 relative">
                                                      <img src="{{ url('').'/images/top-bg.png' }}" class="w-full h-full" alt="s-icon">
                                                      <div class="absolute text-center" style="top: 50%;left: 50%;transform: translate(-50%, -50%);">
                                                          <h4 style="color: white;" class="fs-14">{{ __('AWARENESS') }}</h4>
                                                      </div>
                                                  </div>
                                                  <div class="col-8">
                                                    <div class="flex rounded-xl h-full w-full">
                                                        <div class="px-3 h-full d-flex flex-col justify-center">
                                                            <div class="position-relative w-full text-left">
                                                              <strong class="fs-16">{{ __('Objective:') }}</strong>
                                                              <span class="fs-16 pr-2" id="marketingOverview-awareness_objective"
                                                                >{{ truncateTextTo50($marketingOverview->awareness_objective) }}</span
                                                              >
                                                            </div>
                                                            <div class="position-relative w-full text-left pt-2">
                                                              <strong class="fs-16">{{ __('Tactics:') }}</strong>
                                                              <span class="fs-16 pr-2" id="marketingOverview-awareness_tatics"
                                                                >{{ truncateTextTo50($marketingOverview->awareness_tatics) }}</span
                                                              >
                                                            </div>
                                                        </div>
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="row border">
                                                  <div class="col-4 relative">
                                                      <img src="{{ url('').'/images/interest-bg.png' }}" class="w-full h-full" alt="s-icon">
                                                      <div class="absolute text-center" style="top: 50%;left: 50%;transform: translate(-50%, -50%);">
                                                          <h4 style="color: white;" class="fs-14">{{ __('INTEREST') }}</h4>
                                                      </div>
                                                  </div>
                                                  <div class="col-8">
                                                    <div class="flex rounded-xl h-full w-full">
                                                      <div class="px-3 h-full d-flex flex-col justify-center">
                                                          <div class="position-relative w-full text-left">
                                                            <strong class="fs-16">{{ __('Objective:') }}</strong>
                                                            <span class="fs-16 pr-2" id="marketingOverview-interest_objective"
                                                              >{{ truncateTextTo50($marketingOverview->interest_objective) }}</span
                                                            >
                                                          </div>
                                                          <div class="position-relative w-full text-left pt-2">
                                                            <strong class="fs-16">{{ __('Tactics:') }}</strong>
                                                            <span class="fs-16 pr-2" id="marketingOverview-interest_tatics"
                                                              >{{ truncateTextTo50($marketingOverview->interest_tatics) }}</span
                                                            >
                                                          </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="row border">
                                                  <div class="col-4 relative">
                                                      <img src="{{ url('').'/images/consider-bg.png' }}" class="w-full h-full" alt="s-icon">
                                                      <div class="absolute text-center" style="top: 50%;left: 50%;transform: translate(-50%, -50%);">
                                                          <h4 style="color: white;" class="fs-14">{{ __('CONSIDERATION') }}</h4>
                                                      </div>
                                                  </div>
                                                  <div class="col-8">
                                                    <div class="flex rounded-xl h-full w-full">
                                                      <div class="px-3 h-full d-flex flex-col justify-center">
                                                          <div class="position-relative w-full text-left">
                                                            <strong class="fs-16">{{ __('Objective:') }}</strong>
                                                            <span class="fs-16 pr-2" id="marketingOverview-consideration_objective"
                                                              >{{ truncateTextTo50($marketingOverview->consideration_objective) }}</span
                                                            >
                                                          </div>
                                                          <div class="position-relative w-full text-left pt-2">
                                                            <strong class="fs-16">{{ __('Tactics:') }}</strong>
                                                            <span class="fs-16 pr-2" id="marketingOverview-consideration_tatics"
                                                              >{{ truncateTextTo50($marketingOverview->consideration_tatics) }}</span
                                                            >
                                                          </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="row border">
                                                  <div class="col-4 relative">
                                                      <img src="{{ url('').'/images/convertion-bg.png' }}" class="w-full h-full" alt="s-icon">
                                                      <div class="absolute text-center" style="top: 50%;left: 50%;transform: translate(-50%, -50%);">
                                                          <h4 style="color: white;" class="fs-14">{{ __('CONVERTION') }}</h4>
                                                      </div>
                                                  </div>
                                                  <div class="col-8">
                                                    <div class="flex rounded-xl h-full w-full">
                                                      <div class="px-3 h-full d-flex flex-col justify-center">
                                                          <div class="position-relative w-full text-left">
                                                            <strong class="fs-16">{{ __('Objective:') }}</strong>
                                                            <span class="fs-16 pr-2" id="marketingOverview-conversion_objective"
                                                              >{{ truncateTextTo50($marketingOverview->conversion_objective) }}</span
                                                            >
                                                          </div>
                                                          <div class="position-relative w-full text-left pt-2">
                                                            <strong class="fs-16">{{ __('Tactics:') }}</strong>
                                                            <span class="fs-16 pr-2" id="marketingOverview-conversion_tatics"
                                                              >{{ truncateTextTo50($marketingOverview->conversion_tatics) }}</span
                                                            >
                                                          </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="row border">
                                                  <div class="col-4 relative">
                                                      <img src="{{ url('').'/images/retention-bg.png' }}" class="w-full h-full" alt="s-icon">
                                                      <div class="absolute text-center" style="top: 50%;left: 50%;transform: translate(-50%, -50%);">
                                                          <h4 style="color: white;" class="fs-14">{{ __('RETENTION') }}</h4>
                                                      </div>
                                                  </div>
                                                  <div class="col-8">
                                                    <div class="flex rounded-xl h-full w-full">
                                                      <div class="px-3 h-full d-flex flex-col justify-center">
                                                          <div class="position-relative w-full text-left">
                                                            <strong class="fs-16">{{ __('Objective:') }}</strong>
                                                            <span class="fs-16 pr-2" id="marketingOverview-retention_objective"
                                                              >{{ truncateTextTo50($marketingOverview->retention_objective) }}</span
                                                            >
                                                          </div>
                                                          <div class="position-relative w-full text-left pt-2">
                                                            <strong class="fs-16">{{ __('Tactics:') }}</strong>
                                                            <span class="fs-16 pr-2" id="marketingOverview-retention_tatics"
                                                              >{{ truncateTextTo50($marketingOverview->retention_tatics) }}</span
                                                            >
                                                          </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                              </div>
                                      </div>
                                  </div>
                                  @endif
                                  
                              </div>
                          </div>
                          <div id="marketSize-body" class="mt-6 col-12 col-md-5 {{ is_null($marketSize) ? 'blur-content loading' : '' }}" style="min-height: 400px;">
                              <div class="overlay" style="height: 400px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>
                              <div class="card card-hover {{ is_null($marketSize) ? '' : '' }} mt-0" style="height: 400px; overflow: auto;"
                               onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.concept.market-size-trends', ['project' => getProjectInfo()->id])) }}')">
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('Market Size and Trends') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                      </div>
                                  </div>
                                  @if (!empty($marketSize))
                                     <div class="py-1 px-4" id="marketSize-content">
                                         <div class="row">
                                             <div class="col-4 col-sm-4 d-flex flex-column justify-content-center align-items-center">
                                                 <div class="d-flex flex-column justify-content-center align-items-center">
                                                     <img src="{{ url('').'/images/som-bg.png' }}" class="mx-2 w-2/4" alt="info-icon">
                                                     <h4 class="absolute fs-15" style="color: white">{{ $marketSize->total_addressable_market_value }}</h4>
                                                 </div>
                                             </div>
                                             <div class="col-4 col-sm-4 d-flex flex-column justify-content-center align-items-center">
                                                 <div class="d-flex flex-column justify-content-center align-items-center">
                                                     <img src="{{ url('').'/images/sam-bg.png' }}" class="mx-2 w-3/5" alt="info-icon">
                                                     <h4 class="absolute fs-15">{{ $marketSize->total_servicable_market_value }}</h4>
                                                 </div>
                                             </div>
                                             <div class="col-4 col-sm-4 d-flex flex-column justify-content-center align-items-center">
                                                 <div class="d-flex flex-column justify-content-center align-items-center">
                                                     <img src="{{ url('').'/images/tam-bg.png' }}" class="mx-2 w-4/5" alt="info-icon">
                                                     <h4 class="absolute fs-15">{{ $marketSize->total_obtainable_market_value }}</h4>
                                                 </div>
                                             </div>
                                             <div class="col-4 col-sm-4 d-flex flex-column justify-content-center align-items-center">
                                                 <div class="mt-2 text-center">
                                                     <h1 class="fs-18">{{ __('SOM') }}</h1>
                                                 </div>
                                             </div>
                                             <div class="col-4 col-sm-4 d-flex flex-column justify-content-center align-items-center">
                                                 <div class="mt-2 text-center">
                                                     <h1 class="fs-18">{{ __('SAM') }}</h1>
                                                 </div>
                                             </div>
                                             <div class="col-4 col-sm-4 d-flex flex-column justify-content-center align-items-center">
                                                 <div class="mt-2 text-center">
                                                     <h1 class="fs-18">{{ __('TAM') }}</h1>
                                                 </div>
                                             </div>
                                         </div>
                                         <p class="mb-3">
                                             {{ truncateTextTo150($marketSize->description) }}
                                         </p>
                                         <div class="my-3 border-bottom" style="border: 1px #B9B9B9 solid"></div>
                                         <h5 class="">{{ __('1. Virtual Reality in Tourism') }}</h5>
                                         <h5 class="">{{ __('2. Personalized Travel Experiences') }}</h5>
                                         <h5 class="">{{ __('3. Local culture immersion') }}</h5>
                                     </div>
                                  @endif
                                  
                              </div>
                          </div>
                          @php
                            $documents = Auth::user()->openai()->where('project_id', $project->id)->orderBy('created_at', 'desc')->get();
                          @endphp
                          @if (count($documents) > 0)
                            <div id="document-body" class="mt-6 col-12 col-md-7">
                              <div class="overlay" style="height: 400px;">
                                  <div class="cv-spinner">
                                      <span class="spinner"></span>
                                  </div>
                              </div>
                              <div class="card card-hover mt-0" style="height: 400px; overflow: auto;"
                               onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.marketing.marketing-content', ['project' => getProjectInfo()->id])) }}')">
                                  <div class="pt-3 pb-0 px-4 d-block">
                                      <div class="d-flex justify-content-between align-items-center">
                                          <div class="d-flex align-items-center">
                                              <h2 class="mb-0">{{ __('Documents') }} 
                                              </h2>
                                          </div>
                                          <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                      </div>
                                  </div>
                                  <div class="py-1 p-2 px-3">
                                    <div class="row">
                                        @foreach ($documents as $entry)
                                            @if ($entry->generator != null)
                                                <div class="col-6 col-lg-4">
                                                    <div class="card text-center p-3 py-4 card-document">
                                                        <div class="absolute top-[10px] box-action">
                                                            <a  href="{{route('dashboard.user.openai.documents.single', $entry->slug)}}?idea_id={{ $project->id }}"
                                                                class="btn p-0 border w-[10px] h-[10px]"
                                                                title="{{ __('Edit') }}"
                                                                style="background: #FA690E;"
                                                                >
                                                                <x-tabler-edit class="size-4" style="color: white;" />
                                                            </a>
                                                            <a 
                                                                class="btn p-0 border w-[10px] h-[10px]"
                                                                title="{{ __('Delete') }}"
                                                                style="background: #FA690E;"
                                                                >
                                                                <x-tabler-trash class="size-4" style="color: white;" />
                                                            </a>
                                                        </div>
                                                        <div class="email-icon d-flex justify-center">
                                                            <x-tabler-script  style="color: #FA690E"/>
                                                        </div>
                                                        <h4 class="mt-2 font-bold">{{ $entry->title }}</h4>
                                                        <p class="email-date mb-2" style="color: #666666;">{{ $entry->created_at->format('M d, Y') }}</p>
                                                        <div class="email-body text-left">
                                                            <p class="email-subject" style="color: #666666;">{{ truncateTextTo300($entry->response) }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                  </div>
                              </div>
                          </div>
                          @endif
                      </div>
                      @if (count($landingPage) > 0)
                          <div class="my-4 border-bottom" style="border: 0.5px #B9B9B9 solid"></div>
                          <h3 class="mb-0">{{ __('Launch') }}</h3>
                          <div class="row">
                                <div id="" class="mt-6 col-12">
                                  <div class="overlay" style="height: 500px;">
                                      <div class="cv-spinner">
                                          <span class="spinner"></span>
                                      </div>
                                  </div>
                                  <div class="card card-hover mt-0" style="height: 500px; overflow: auto;" onclick="gotoIdeaItem('{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.launch.landing-pages', ['project' => getProjectInfo()->id])) }}')">
                                      <div class="pt-3 pb-0 px-4 d-block">
                                          <div class="d-flex justify-content-between align-items-center">
                                              <div class="d-flex align-items-center">
                                                  <h2 class="mb-0">{{ __('Landing Pages') }} 
                                                  </h2>
                                              </div>
                                              <img class="card-hover" src="{{ url('').'/images/expand-icon.png' }}" width="20" height="20" alt="Avatar 1">
                                          </div>
                                      </div>
                                    <div class="card-body p-1 p-sm-4" id="landingPage-content" style="zoom: 0.8">
                                        <div class="row">
                                            @foreach($landingPage as $key => $page)
                                                @if ($key < 2)
                                                    <div class="col-md-4 col-12">
                                                        <div class="card position-relative card-landing-page">
                                                            <div class="card-body m-[15px] h-[300px] image-background d-flex align-items-center justify-center position-relative">
                                                                @if ($page->preview_url)
                                                                    <iframe src="{{ $page->preview_url }}" class="image-iframe w-full h-full" frameborder="0"></iframe>
                                                                @else
                                                                    <img src="https://app.fikrahub.com/public/images/fh.jpg"
                                                                        style="width:100%;">
                                                                @endif
                                                            </div>
                                                            <div class="card-footer d-flex align-items-center justify-center">
                                                                <div class="text-center">
                                                                    <a type="button" class="btn btn-primary" 
                                                                        href="https://websites.fikrahub.com/login?email={{ Auth::user()->email }}&idea_id={{ $project->id }}&lara_id={{ $page->lara_id }}&landing_page_id={{ $page->id }}"
                                                                        
                                                                    >{{ __('Edit') }}</a>
                                                                </div>
                                                                <div class="text-center mx-1">
                                                                    <a type="button" class="btn btn-primary" 
                                                                        href="{{ $page->preview_url }}"
                                                                        target="_blank"
                                                                    >{{ __('Preview') }}</a>
                                                                </div>
                                                                <div class="text-center">
                                                                    <a type="button" class="btn btn-danger" 
                                                                        onclick="deleteLandingPage({{ $page->id }})"
                                                                    >{{ __('Delete') }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                  </div>
                              </div>
                          </div>
                      @else 
                        <div class="mb-3"></div>
                      @endif
                  @endif
                  
              </div>
              <!-- [ sample-page ] end -->
          </div>
          <!-- [ Main Content ] end -->
      </div>
  </div>
  <div id="tasks" style="display: none;">
  </div>
</div>