<div class="group float-button float-plus-botton fixed bottom-10 end-12 z-50 lg:block 1 lang_{{app()->getLocale()}}">
    <button
        class="btn-tooltip collapsed peer h-14 w-14 translate-x-0 transform-gpu rounded-full border-none bg-transparent p-0 !shadow-lg !outline-none"
        type="button">
        <span
            class="spinner_button before:animate-spin-grow relative mb-1 inline-flex h-full w-full items-center justify-center overflow-hidden rounded-full before:absolute before:left-0 before:top-0 before:h-full before:w-full before:rounded-full">
            <svg class="relative transition-transform duration-300 group-hover:rotate-[135deg]"
                xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 24 24" stroke-width="3"
                stroke="#fff" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path d="M12 5l0 14"></path>
                <path d="M5 12l14 0"></path>
            </svg>
            <div class="spinner-border text-blue p-1" style="display: none"></div>
        </span>
        <span class="tooltiptext tooltip-left2">{{ __('Generate') }}</span>
    </button>
    <div class="position-popup-100 text-heading !invisible absolute bottom-full end-0 !mb-4 min-w-[145px] translate-y-2 rounded-md bg-[--tblr-body-bg] text-sm font-medium !opacity-0 shadow-[0_3px_12px_rgba(0,0,0,0.08)] transition-all before:absolute before:inset-x-0 before:-bottom-4 before:h-4 group-hover:!visible group-hover:translate-y-0 group-hover:!opacity-100 dark:bg-zinc-800"
        id="add-new-floating" style="width: 250px">
        <span
            class="text-heading block border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 !px-4 !py-2 text-opacity-50 last:border-0 dark:!border-white dark:!border-opacity-5">{{ __('Items:') }}</span>
        <ul class="m-0 list-none p-0"
        >
            <li
                class="border-b border-l-0 border-r-0 border-t-0 border-solid !border-black !border-opacity-5 last:border-0 dark:!border-white dark:!border-opacity-5">
                <a class="{{ activeRoute('dashboard.user.openai.list') }} d-flex items-center relative block !px-4 !py-2 text-inherit before:absolute before:inset-0 before:bg-current before:text-current before:opacity-0 before:transition-opacity hover:no-underline hover:before:opacity-5 [&.active]:before:opacity-5"
                    @if ($landingPage->count() >= 6)
                    onclick="toastr.error('{{ __("You can only create a maximum of 6 landing pages.") }}');"
                    @else 
                    href="https://websites.fikrahub.com/login?email={{ Auth::user()->email }}&idea_id={{ $project->id }}"
                    @endif
                    >
                    {{ __('Generate Landing Page') }}
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="relative lang_{{app()->getLocale()}} container container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12 mt-3">
            <div id="landing-page-body" class="col-12 " style="{{ $landingPage->isEmpty() ? 'min-height: 65vh' : '' }}">
                <div class="">
                    <div class="card-header  d-block px-2rem">
                        <div class="d-flex justify-content-between align-items-center">
                            <div>
                                <h2 class="mb-0">{{ __('Landing Page') }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-1 p-sm-4" id="landingPage-content" style="{{ $landingPage->isEmpty() ? 'min-height: 55vh' : '' }}">
                        <div class="row">
                            @foreach($landingPage as $page)
                                <div class="col-md-6 col-lg-4">
                                    <div class="card position-relative mb-5">
                                        <div class="card-body mt-5 m-[15px] h-[300px] image-background d-flex align-items-center justify-center position-relative">
                                            @if ($page->preview_url)
                                                <iframe src="{{ $page->preview_url }}" class="image-iframe w-full h-full" frameborder="0"></iframe>
                                            @else
                                                <img src="https://app.fikrahub.com/public/images/fh.jpg"
                                                    style="width:100%;">
                                            @endif
                                        </div>
                                        <div class="card-footer d-flex align-items-center justify-center">
                                            <div class="text-center">
                                                <a type="button" class="btn btn-primary" 
                                                    href="https://websites.fikrahub.com/login?email={{ Auth::user()->email }}&idea_id={{ $project->id }}&lara_id={{ $page->lara_id }}&landing_page_id={{ $page->id }}"
                                                    
                                                >{{ __('Edit') }}</a>
                                            </div>
                                            <div class="text-center mx-1">
                                                <a type="button" class="btn btn-primary" 
                                                    href="{{ $page->preview_url }}"
                                                    target="_blank"
                                                >{{ __('Preview') }}</a>
                                            </div>
                                            <div class="text-center">
                                                <a type="button" class="btn btn-danger" 
                                                    onclick="deleteLandingPage({{ $page->id }})"
                                                >{{ __('Delete') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>