<div class="relative lang_{{app()->getLocale()}} custom-contaier container-overview mb-5">
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-3 site-launch">
            @include('panel.user.openai.tasks.index')
        </div>
    </div>
    @foreach($stages as $stage)
    <!-- Modal -->
    <div class="modal fade" id="createTaskModel{{$stage->id}}" tabindex="-1" aria-labelledby="createTaskModelLabel{{$stage->id}}"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="{{ LaravelLocalization::localizeUrl(route('dashboard.user.openai.projects.tasks.store', ['pid' => $project->id, 'sid' => $stage->id])) }}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="popup-title">{{ __('Create Task') }}</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="form-group">
                                    <label class="form-label">{{ __('Task Name') }}</label>
                                    <input class="form-control" id="title" type="text" name="title" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="form-group">
                                    <label class="form-label">{{ __('Task Description') }}</label>
                                    <textarea class="form-control" id="description" name="description" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="form-label">{{ __('Estimated Hours') }}</label>
                                    <input class="form-control" id="estimated_hrs" type="number" min=0 name="estimated_hrs" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="form-label">{{ __('Priority') }}</label>
                                    <select class="form-control" name="priority" id="priority" required>
                                        @foreach (\App\Models\ProjectTask::$priority as $key => $val)
                                            <option value="{{ $key }}">{{ __($val) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-6">
                                <div class="form-group">
                                    <x-forms.input
                                        class:container="grow"
                                        id="start_date"
                                        label="{{ __('Start Date') }}"
                                        type="date"
                                        name="start_date"
                                        size="lg"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <x-forms.input
                                        class:container="grow"
                                        id="end_date"
                                        label="{{ __('End Date') }}"
                                        type="date"
                                        name="end_date"
                                        size="lg"
                                        required
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" 
                            data-bs-dismiss="modal"
                            type="button"
                            aria-label="Close">Close</button>
                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    
    <!-- Modal -->
    <div class="modal fade" id="editTaskModel{{$stage->id}}" tabindex="-1" aria-labelledby="editTaskModelLabel{{$stage->id}}"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="popup-title-update">{{ __('Update Task') }}</h5>
                        <h5 class="modal-title" id="popup-title-view" style="display:none">{{ __('Task Details') }}</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="form-group">
                                    <label class="form-label">{{ __('Task Name') }}</label>
                                    <input class="form-control" id="title" type="text" name="title" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="form-group">
                                    <label class="form-label">{{ __('Task Description') }}</label>
                                    <textarea class="form-control" id="description" name="description" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="form-label">{{ __('Estimated Hours') }}</label>
                                    <input class="form-control" id="estimated_hrs" type="number" min=0 name="estimated_hrs" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="form-label">{{ __('Priority') }}</label>
                                    <select class="form-control" name="priority" id="priority" required>
                                        @foreach (\App\Models\ProjectTask::$priority as $key => $val)
                                            <option value="{{ $key }}">{{ __($val) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-6">
                                <div class="form-group">
                                    <x-forms.input
                                        class:container="grow"
                                        id="start_date"
                                        label="{{ __('Start Date') }}"
                                        type="date"
                                        name="start_date"
                                        size="lg"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <x-forms.input
                                        class:container="grow"
                                        id="end_date"
                                        label="{{ __('End Date') }}"
                                        type="date"
                                        name="end_date"
                                        size="lg"
                                        required
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" 
                            data-bs-dismiss="modal"
                            type="button"
                            aria-label="Close">Close</button>
                        <button type="submit" class="save btn btn-primary">{{ __('Save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach
</div>

<!-- Your existing content -->


<div class="mesh-loader" id="fullPageLoader" style="display:none;">
    <div class="set-one">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="set-two">
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
</div>