@extends('panel.layout.app', ['layout_wide' => true])

@section('no_title', '')
@section('titlebar_actions', '')
@section('content')
    <div class="modal fade" id="creatSubmitIdeaModal" tabindex="-1" aria-labelledby="creatSubmitIdeaLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <form id="ideaForm" action="{{ LaravelLocalization::localizeUrl( route('dashboard.user.openai.projects.store')) }}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="creatSubmitIdeaLabel">{{ __('Submit a New Idea') }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div id="step1">
                            <label for="name" class="form-label">{{ __('Idea Title') }}</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('Give your idea a title') }}" required>
                        </div>
                        <div id="step2" style="display:none;">
                            <label class="form-label">{{ __('Idea Description') }}</label>
                            <textarea class="form-control" id="description" name="description" rows="12" placeholder="{{ __('Comprehensively explain your idea') }}" maxlength="1000" required="required"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="prevBtn" style="display:none;" onclick="prevStep()">{{ __('Previous') }}</button>
                        <button type="button" class="btn btn-primary" id="nextBtn" onclick="nextStep()">{{ __('Next') }}</button>
                        <button type="submit" class="btn btn-primary" id="submitBtn" style="display:none;">{{ __('Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function() {
            $('#creatSubmitIdeaModal').modal('show');
        });
    </script>
@endpush

