<option value="ar-AE" {{$setting->openai_default_language == 'ar-AE' ? 'selected' : null}}>{{__('Arabic')}}</option>
<option value="en-US" {{$setting->openai_default_language == 'en-US' ? 'selected' : null}}>{{__('English')}}</option>
