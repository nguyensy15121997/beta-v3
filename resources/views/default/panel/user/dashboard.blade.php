@extends('panel.layout.app')
@section('title', __('Dashboard'))
@section('titlebar_title')
    {{ __('Welcome') }}, {{ auth()->user()->name }}!
@endsection
@section('additional_css')
    <style>
        @media only screen and (max-width: 600px) {
            .card-table .td-truncate {
                min-width: 100px;
            }
        }
    </style>
@endsection
@section('titlebar_actions')
    <div class="col-auto">
        <div class="btn-list">
            <a @if ($app_is_demo) onclick="return toastr.info('This feature is disabled in Demo version.')" @else data-bs-toggle="modal" data-bs-target="#creatSubmitIdeaModal" @endif
                class="btn btn-primary items-center">
                <svg xmlns="http://www.w3.org/2000/svg" class="!me-2" width="18" height="18"
                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                    stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <path d="M12 5l0 14" />
                    <path d="M5 12l14 0" />
                </svg>
                {{ __('Submit a New Idea') }}
            </a>
        </div>
    </div>
@endsection

@section('content')
    <!-- Create Submit Idea Modal -->
    <div class="modal fade" id="creatSubmitIdeaModal" tabindex="-1" aria-labelledby="creatSubmitIdeaLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ LaravelLocalization::localizeUrl( route('dashboard.user.openai.projects.store')) }}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="creatSubmitIdeaLabel">{{ __('Submit a New Idea') }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <label for="name" class="form-label">{{ __('Idea Title') }}</label>
                        <input type="text" class="form-control" name="name" placeholder="{{ __('Give your idea a title') }}" required>
                        <br>
                        <label class="form-label">{{ __('Idea Description') }}</label>
                        <textarea class="form-control" id="description" name="description" rows="12" placeholder="{{ __('Comprehensively explain your idea') }}"
                            maxlength="1000" required="required"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                            data-bs-dismiss="modal">{{ __('Cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Page body -->
    <div class="page-body pt-5">
        <div class="container-xl">
            <div class="row row-deck row-cards">
<!--                <div class="col-12">
                    @if (view()->exists('panel.admin.custom.user.payment.subscriptionStatus'))
                        @include('panel.admin.custom.user.payment.subscriptionStatus')
                    @else
                        @include('panel.user.finance.subscriptionStatus')
                    @endif
                </div>
-->

                @if ($ongoingPayments != null)
                    <div class="col-12">
                        @include('panel.user.finance.ongoingPayments')
                    </div>
                @endif

                <div class="col-12">
                    <div class="card">
                        <div class="card-header justify-between">
                            <h3 class="card-title text-heading">{{ __('Ideas') }}</h3>
                            <div class="btn-list">
                                
                            </div>
                        </div>
                        <div class="card-table table-responsive">
                            <table class="table table-vcenter">
                                <tbody>
                                    @php
                                        $ideas = Auth::user()->projects()->orderBy('created_at', 'desc')->paginate(10);
                                    @endphp
                                    @if ($ideas->count() > 0)
                                        @foreach ($ideas as $entry)
                                            <tr class="hover:bg-black hover:bg-opacity-[0.03] link">
                                                <td class="w-1 !pe-0">
                                                    <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.show', $entry->id)) }}"
                                                        class="block text-truncate text-heading hover:no-underline">
                                                        <span class="avatar w-[43px] h-[43px] [&amp;_svg]:w-[20px] [&amp;_svg]:h-[20px]" style="background: #A3D6C2">
                                                            <svg xmlns="http://www.w3.org/2000/svg" height="48" viewBox="0 96 960 960" width="48"><path d="M140 936q-24.75 0-42.375-17.625T80 876V216l67 67 66-67 67 67 67-67 66 67 67-67 67 67 66-67 67 67 67-67 66 67 67-67v660q0 24.75-17.625 42.375T820 936H140Zm0-60h310V596H140v280Zm370 0h310V766H510v110Zm0-170h310V596H510v110ZM140 536h680V416H140v120Z"></path></svg>
                                                        </span>
                                                    </a>
                                                </td>
                                                <td class="td-truncate">
                                                    <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.show', $entry->id)) }}"
                                                        class="block text-truncate text-heading hover:no-underline">
                                                        <span class="font-medium">{{ $entry->name }}</span>
                                                        <br>
                                                        <span
                                                            class="italic text-muted opacity-80 dark:!text-white dark:!opacity-50">{{ $entry->description }}</span>
                                                    </a>
                                                </td>
                                                <td class="text-nowrap">
                                                    <a href="{{ LaravelLocalization::localizeUrl(route('dashboard.user.ideas.show', $entry->id)) }}"
                                                        class="block text-truncate text-heading hover:no-underline">
                                                        <span class="text-heading">{{ __('Submited On') }}</span>
                                                        <br>
                                                        <span class="italic text-muted opacity-80 dark:!text-white dark:!opacity-50">{{ $entry->created_at->format('M d, Y') }}</span>
                                                    </a>
                                                </td>
                                                <td class="text-nowrap">
                                                    <a  data-url="{{ route('dashboard.user.openai.projects.destroy', $entry->id) }}"
                                                        class="btn p-0 border w-[36px] h-[36px] hover:bg-red-600 hover:text-white"
                                                        title="{{ __('Delete') }}"
                                                        onclick="
                                                        event.preventDefault();
                                                        return deleteIdea('{{ route('dashboard.user.openai.projects.destroy', $entry->id) }}');"
                                                        >
                                                        <svg width="10" height="10" viewBox="0 0 10 10" fill="currentColor"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M9.08789 1.74609L5.80664 5L9.08789 8.25391L8.26758 9.07422L4.98633 5.82031L1.73242 9.07422L0.912109 8.25391L4.16602 5L0.912109 1.74609L1.73242 0.925781L4.98633 4.17969L8.26758 0.925781L9.08789 1.74609Z" />
                                                        </svg>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-heading py-5">
                                            <td class="w-1 !pe-0">
                                            {{ __('You have no ideas. Submit one to get started.') }}
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        {{ $ideas->links('pagination::bootstrap-5') }}
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        // $('#delete_idea').on('click', function(event) {
        //     var url = this.data('url');
        //     $.ajax({
        //         url: url,
        //         type: 'DELETE',
        //         success: function(response) {
        //             toastr.success(response.message); 
        //             location.reload();
        //         },
        //         error: function(error) {toastr.error("{{ __('Error delete project:') }}", error);}
        //     })
        // });
        
        function goToDetailPage(id) {
            let url = "{{ route('dashboard.user.ideas.show', ':project') }}";
            url = url.replace(':project', id);
            location.href = url;
        }
    </script>
@endsection