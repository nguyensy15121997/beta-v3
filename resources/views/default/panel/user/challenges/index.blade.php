@extends('panel.layout.app')
@section('title', __('Challenges'))
@section('titlebar_actions')
    <div class="modal fade" id="creatSubmitIdeaModal" tabindex="-1" aria-labelledby="creatSubmitIdeaLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form onsubmit="return challengeSave();" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="creatSubmitIdeaLabel">{{ __('Submit a New Challenge') }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <label for="name" class="form-label">{{ __('Challenge Title') }}</label>
                        <input id="title" type="text" class="form-control" name="title" placeholder="{{ __('Give your challenge a title') }}" required>
                        <br>
                        <label class="form-label">{{ __('Challenge Description') }}</label>
                        <textarea class="form-control" id="description" name="description" rows="12" placeholder="{{ __('Comprehensively explain your challenge') }}"
                            maxlength="1000" required="required"></textarea>
                        <br>
                        <label class="form-label">{{__('Image')}}</label>
                        <input type="file" class="form-control" id="image" name="image" accept="image/*" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                            data-bs-dismiss="modal">{{ __('Cancel') }}</button>
                        <button id="submit_challenge" type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="btn-list">
        <a @if ($app_is_demo) onclick="return toastr.info('This feature is disabled in Demo version.')" @else data-bs-toggle="modal" data-bs-target="#creatSubmitIdeaModal" @endif
            class="btn btn-primary items-center">
            <svg xmlns="http://www.w3.org/2000/svg" class="!me-2" width="18" height="18"
                viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                <path d="M12 5l0 14" />
                <path d="M5 12l14 0" />
            </svg>
            {{ __('Submit a New Challenge') }}
        </a>
    </div>
@endsection
@section('content')
<!-- Page body -->
<div class="lqd-generators-container pt-5" id="lqd-generators-container">
    <div class="lqd-generators-list grid grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5" id="lqd-generators-list">
        @foreach ($challenges as $item)
            <x-card
                    class:body="static"
                    @class([
                        'lqd-generator-item border group relative w-full rounded-xl',
                    ])
                    size="none"
                    roundness="{{ Theme::getSetting('defaultVariations.card.roundness', 'default') === 'default' ? 'none' : Theme::getSetting('defaultVariations.card.roundness', 'default') }}"
                    x-data
                >
                    <x-button
                            class="border bg-red-100 absolute end-2 top-1"
                            hover-variant="danger"
                            size="none"
                            variant="ghost-shadow"
                            onclick="return deleteChallenge('{{ route('dashboard.user.challenge.destroy', $item->id) }}');"
                            title="{{ __('Delete') }}"
                        >
                        <x-tabler-x class="size-6" />
                    </x-button>
                    <img
                        class="w-full object-cover object-center shadow-lg"
                        style="height: 150px; border-radius: 10px 10px 0px 0px;"
                        src="{{ custom_theme_url($item->image, true) }}"
                        alt="{{ $item->title }}"
                    >
                    <div class="card-body relative p-5 cursor-pointer" onclick="goToChallengeDetails('{{ route('dashboard.user.challenge.show', $item->id) }}')">
                        <h4 class="relative mb-3.5 inline-block text-lg">
                            {{ __($item->title) }}
                        </h4>
                        <p class="opacity-85 m-0">
                            {{ __($item->description) }}
                        </p>
                    </div>
                </x-card>
        @endforeach
    </div>
</div>
@endsection
@push('script')
    <script src="{{ custom_theme_url('/assets/js/panel/challenges.js') }}"></script>
@endpush

