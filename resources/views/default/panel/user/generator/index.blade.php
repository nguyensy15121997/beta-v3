@extends('panel.layout.app', [
    'disable_header' => true,
    'disable_titlebar' => true,
    'disable_navbar' => true,
    'disable_footer' => true,
    'disable_floating_menu' => true,
    'disable_mobile_bottom_menu' => true,
    'disable_tblr' => true,
])
@section('additional_css')
    <style>
        .pro-feature {
            color: #969696!important;
            pointer-events: none!important;
        }

        .pro-pill {
            background-color: #20a29b;
            color: #fff;
            font-size: 12px;
            padding: 3px 8px;
            border-radius: 10px;
            margin-left: 8px;
            display: inline-block;
        }
        .lang_ar.group\/generator.lqd-generator-sidebar-collapsed .group-\[\&\.lqd-generator-sidebar-collapsed\]\/generator\:-translate-x-\[calc\(100\%-35px\)\] {
                --tw-translate-x: calc((100% - 35px)) !important;
        }
        .lang_ar.group\/generator.lqd-generator-sidebar-collapsed .group-\[\&\.lqd-generator-sidebar-collapsed\]\/generator\:opacity-100 {
            --tw-translate-x: 50%;
        }
    </style>
@endsection

@section('content')
    <div
        class="lang_{{ app()->getLocale() }} lqd-generator-v2 group/generator [--editor-bb-h:40px] [--editor-tb-h:50px] [--sidebar-w:min(440px,90vw)]"
        :class="{ 'lqd-generator-sidebar-collapsed': sideNavCollapsed }"
        x-data="generatorV2"
    >
        @include('panel.user.generator.components.sidebar')
        @include('panel.user.generator.components.editor')
    </div>
@endsection
