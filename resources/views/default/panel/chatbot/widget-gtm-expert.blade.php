<div class="d-flex align-items-center justify-end">
    <div class="mx-3" style="padding: 5px; border-radius: 5px; color: white;background: #979797;">{{ __('GTM Expert') }}</div>
    <button
        class="openChatButton size-16 group relative cursor-pointer overflow-hidden rounded-full border-none bg-white p-0 text-black shadow-lg dark:!bg-[#1a1d23] dark:!text-white"
        data-type="gtm-expert-EunT9"
        type="button"
    >
        <img
            class="h-full w-full overflow-hidden rounded-full object-cover object-center transition-all group-[&.lqd-is-active]:-translate-y-2 group-[&.lqd-is-active]:scale-90 group-[&.lqd-is-active]:opacity-0"
            src="/upload/images/chatbot/GcYz-gtm-expert-avatar.webp"
            alt=""
        >
        <span
            class="absolute start-0 top-0 inline-flex h-full w-full translate-y-2 scale-90 items-center justify-center opacity-0 transition-all group-[&.lqd-is-active]:translate-y-0 group-[&.lqd-is-active]:scale-100 group-[&.lqd-is-active]:!opacity-100"
        >
            <x-tabler-chevron-down class="size-6" />
        </span>
    </button>
</div>