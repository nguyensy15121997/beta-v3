@extends('panel.layout.app', ['layout_wide' => true])

@section('content')
    <div class="text-center max-lg:px-1 pt-8">
        <div class="flex-grow">
            <a
                class="navbar-brand"
                href="{{ route('index') }}"
            >
                @if (isset($setting->logo_dashboard))
                    <img
                        class="group-[.navbar-shrinked]/body:hidden"
                        src="{{ custom_theme_url($setting->logo_dashboard_path, true) }}"
                        @if (isset($setting->logo_dashboard_2x_path)) srcset="/{{ $setting->logo_dashboard_2x_path }} 2x" @endif
                        alt="{{ $setting->site_name }}"
                    >
                @else
                    <img
                        class="group-[.navbar-shrinked]/body:hidden dark:hidden"
                        src="{{ custom_theme_url($setting->logo_path, true) }}"
                        @if (isset($setting->logo_2x_path)) srcset="/{{ $setting->logo_2x_path }} 2x" @endif
                        alt="{{ $setting->site_name }}"
                    >
                    <img
                        class="hidden group-[.navbar-shrinked]/body:hidden dark:block"
                        src="{{ custom_theme_url($setting->logo_dark_path, true) }}"
                        @if (isset($setting->logo_dark_2x_path)) srcset="/{{ $setting->logo_dark_2x_path }} 2x" @endif
                        alt="{{ $setting->site_name }}"
                    >
                @endif
            </a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row items-stretch justify-center min-h-[100vh] max-lg:pt-10 max-lg:pb-10">
            <div class="flex flex-col justify-center col-12 col-sm-9 col-lg-8">
                <div class="row">
                    <div class="mx-auto col-12 col-sm-9 col-lg-6 px-6">
                        @yield('form')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
