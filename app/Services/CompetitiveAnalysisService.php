<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Already imported

class CompetitiveAnalysisService
{
    public function generateAndSaveCompetitiveAnalysisService($project)
    {
        for ($i = 0; $i < 2; $i++) {
            $projectDescription = $project->description;
            // Use an appropriate API key management strategy
            $apiKey = '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'; // Make sure to secure this
        
            $data = [
                'model' => 'gpt-3.5-turbo-1106',
                'temperature' => 0.3,
                'messages' => [
                    ['role' => 'system', 'content' => 'Generate competitive analysis on the project description.'],
                    ['role' => 'user', 'content' => "Project Description: {$projectDescription}. Please create 5 competitive analysis based on this. I need a complete competitive analysis including Name, Location, Strengths & Features, Weaknesses, Areas of Opportunity, Name_abr, Location_abr, Strengths & Features_abr, Weaknesses_abr, Areas of Opportunity_abr. The fields Name_abr, Location_abr, Strengths & Features_abr, Weaknesses_abr, Areas of Opportunity_abr return Arabic. The fields Name, Location, Strengths & Features, Weaknesses, Areas of Opportunity return English. There is only one line break between data lines, please note this!. The data of the fields is not too long. I would like the all field to be shorter."]
                ],
                'max_tokens' => 2000
            ];
    
            $response = $this->sendRequestToOpenAI($data, $apiKey);
    
            if ($response) {
                $competitiveAnalysisContent = $response['choices'][0]['message']['content'];
                
                $sections = preg_split("/\n\n+/", $competitiveAnalysisContent);
                if ($this->isAnyFieldNullArray($sections) && $i != 1) {
                    continue;
                } else {
                    foreach ($sections as $section) {
                        if (stristr($section, 'Name:')) {
                            $competitiveAnalysisParts = $this->processCompetitiveAnalysisContent($section);
                            if (!empty($competitiveAnalysisParts['name'])) {
                                // Insert Lean Canvas data into the database
                                DB::table('competitive_analysis')->insert([
                                    'project_id' => $project->id,
                                    'name_abr' => $competitiveAnalysisParts['name_abr'],
                                    'location_abr' => $competitiveAnalysisParts['location_abr'],
                                    'strengths_features_abr' => $competitiveAnalysisParts['strengths_features_abr'],
                                    'weaknesses_abr' => $competitiveAnalysisParts['weaknesses_abr'],
                                    'areas_opportunity_abr' => $competitiveAnalysisParts['areas_opportunity_abr'],
                                    'name' => $competitiveAnalysisParts['name'],
                                    'location' => $competitiveAnalysisParts['location'],
                                    'strengths_features' => $competitiveAnalysisParts['strengths_features'],
                                    'weaknesses' => $competitiveAnalysisParts['weaknesses'],
                                    'areas_opportunity' => $competitiveAnalysisParts['areas_opportunity'],
                                    // Add other sections if needed
                                    'created_at' => now(),
                                    'updated_at' => now()
                                ]);
                        }
                        }
                    }
                    break;
                }
                
            }
        }
    }
    
    function isAnyFieldNullArray($array): bool {
        foreach ($array as $object) {
            if (stristr($object, 'Name:')) {
                $competitiveAnalysisParts = $this->processCompetitiveAnalysisContent($object);
                foreach ($competitiveAnalysisParts as $value) {
                    if (empty($value)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return json_decode($response, true);
        }
    }

    private function processCompetitiveAnalysisContent($content)
    {
        // Initialize an array to hold each part of the Lean Canvas
        $competitiveAnalysisParts = [
            'name' => '',
            'location' => '',
            'strengths_features' => '',
            'weaknesses' => '',
            'areas_opportunity' => '',
            'name_abr' => '',
            'location_abr' => '',
            'strengths_features_abr' => '',
            'weaknesses_abr' => '',
            'areas_opportunity_abr' => '',
            // Add other sections if needed
        ];

        // Split the content into sections and process
        $sections = preg_split("/\n+/", $content);

        foreach ($sections as $section) {
            // Check which part of Lean Canvas the section belongs to and add it to the respective key
            if (stristr($section, 'Name:')) {
                $competitiveAnalysisParts['name'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Location:')) {
                $competitiveAnalysisParts['location'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Strengths & Features:')) {
                $competitiveAnalysisParts['strengths_features'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Weaknesses:')) {
                $competitiveAnalysisParts['weaknesses'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Areas of Opportunity:')) {
                $competitiveAnalysisParts['areas_opportunity'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Name_abr:')) {
                $competitiveAnalysisParts['name_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Location_abr:')) {
                $competitiveAnalysisParts['location_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Strengths & Features_abr:')) {
                $competitiveAnalysisParts['strengths_features_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Weaknesses_abr:')) {
                $competitiveAnalysisParts['weaknesses_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Areas of Opportunity_abr:')) {
                $competitiveAnalysisParts['areas_opportunity_abr'] = $this->cleanSection($section);
            }
            // Add other sections if the response includes them
        }

        return $competitiveAnalysisParts;
    }

private function cleanSection($section)
{
    // Remove anything before a ":"
    $section = preg_replace('/^[^\n]*:\s*/', '', $section);
    
    return trim($section);
}

}
?>