<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Already imported

class LeanCanvasService
{
    public function generateAndSaveLeanCanvas($project)
    {
        
        $projectDescription = $project->description;
        // Use an appropriate API key management strategy
        $apiKey = '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'; // Make sure to secure this

        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
        ['role' => 'system', 'content' => 'You are a helpful assistant. Generate a detailed Lean Canvas for a project.'],
        ['role' => 'user', 'content' => "
                Here's a project description: {$projectDescription}. Based on this,  I need a complete Lean Canvas analysis
                        - Problem
                        - Solution 
                        - Unique Value Proposition
                        - Unfair Advantage 
                        - Customer Segments 
                        - Existing Alternatives 
                        - Key Metrics 
                        - Channels 
                        - Cost Structure 
                        - Revenue Streams
                        
                        For the Arabic version, ensure to include:
                        - Problem_abr
                        - Solution_abr
                        - Unique Value Proposition_abr
                        - Unfair Advantage_abr
                        - Customer Segments_abr
                        - Existing Alternatives_abr
                        - Key Metrics_abr
                        - Channels_abr
                        - Cost Structure_abr
                        - Revenue Streams_abr
                        
                        Example:
                        
                        Problem: Ahmed struggles to find efficient banking solutions that cater to his business needs.
                        Solution: He plans to develop a user-friendly fintech platform tailored for entrepreneurs.
                        Unique Value Proposition: Ahmed's platform offers seamless integration of banking services and financial management tools.
                        Unfair Advantage: His extensive network in the fintech industry gives him access to valuable insights and partnerships.
                        Customer Segments: Ahmed targets small and medium-sized businesses seeking modern banking solutions.
                        Existing Alternatives: Traditional banking services lack the agility and customization Ahmed requires.
                        Key Metrics: Ahmed will track user engagement, transaction volume, and customer satisfaction.
                        Channels: He will leverage online marketing, business networks, and industry events to reach potential customers.
                        Cost Structure: Development costs, marketing expenses, and ongoing maintenance constitute the primary expenses.
                        Revenue Streams: Revenue will be generated through subscription fees, transaction fees, and premium features.
                        
                        Problem_abr: أحمد يواجه صعوبة في العثور على حلول مصرفية فعالة التكلفة ومناسبة لاحتياجات عمله الخاصة، حيث يجد أن الخدمات المصرفية التقليدية غالبًا ما تكون بطيئة وغير ملائمة للعملاء الصغار والمتوسطين.
                        Solution_abr: لحل هذه المشكلة، يخطط أحمد لتطوير منصة تكنولوجيا مالية مبتكرة توفر خدمات مصرفية سريعة وسهلة الاستخدام، مع توفير أدوات إدارة الأموال المخصصة لرجال الأعمال.
                        Unique Value Proposition_abr: تتميز منصة أحمد بتكاملها السلس بين الخدمات المصرفية وأدوات إدارة الأموال، مما يوفر للعملاء تجربة فريدة ومتكاملة.
                        Unfair Advantage_abr: إحدى ميزات أحمد غير العادلة هي شبكته القوية في صناعة التكنولوجيا المالية، مما يمكنه من الوصول إلى شراكات استراتيجية وتوجيهات مهمة تعزز من قدرته على تقديم منتج مبتكر ومطابق لاحتياجات السوق.
                        Customer Segments_abr: يستهدف أحمد رجال الأعمال والشركات الناشئة والمتوسطة التي تبحث عن حلول مصرفية مبتكرة وملائمة لاحتياجاتها الفريدة.
                        Existing Alternatives_abr: البدائل القائمة تشمل البنوك التقليدية ومقدمي الخدمات المالية عبر الإنترنت، ولكنها غالبًا ما تفتقر إلى المرونة والتكامل الذي يقدمه منتج أحمد.
                        Key Metrics_abr: يعتزم أحمد تتبع معدل التحويل وعدد المستخدمين النشطين والعائد على الاستثمار كمقاييس رئيسية لأداء منصته.
                        Channels_abr: سيستخدم أحمد قنوات التسويق الرقمي والشبكات الاجتماعية والترويج المباشر للتواصل مع جمهوره المستهدف.
                        Cost Structure_abr: تشمل هيكلة التكلفة تكاليف التطوير والتسويق والدعم الفني والصيانة للمنصة.
                        Revenue Streams_abr: من المتوقع أن تتضمن مصادر الإيرادات الرسوم الشهرية للاشتراك والعمولات على المعاملات والخدمات الإضافية المدفوعة.
                        
                        Just break the line with /n, do not add any special characters.
                "]
        ],
            'max_tokens' => 3000
        ];

       for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);

            if ($response && isset($response['choices'][0]['message']['content'])) {
                $leanCanvasContent = $response['choices'][0]['message']['content'];
                $leanCanvasParts = $this->processLeanCanvasContent($leanCanvasContent);
                
                if ($this->isAnyFieldNullObject($leanCanvasParts)) {
                    continue;
                }
                
                // Insert Lean Canvas data into the database
                DB::table('lean_canvases')->insert([
                    'project_id' => $project->id,
                    'created_at' => now(),
                    'updated_at' => now()
                ] + $leanCanvasParts);
                
                break;
            }
        }
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }

    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return json_decode($response, true);
        }
    }

    private function processLeanCanvasContent($content)
    {
        // Initialize an array to hold each part of the Lean Canvas
        $leanCanvasParts = [
            'problem' => '',
            'solution' => '',
            'unique_value_proposition' => '',
            'unfair_advantage' => '',
            'customer_segments' => '',
            'existing_alternatives' => '',
            'key_metrics' => '',
            'channels' => '',
            'cost_structure' => '',
            'revenue_streams' => '',
            'problem_abr' => '',
            'solution_abr' => '',
            'unique_value_proposition_abr' => '',
            'unfair_advantage_abr' => '',
            'customer_segments_abr' => '',
            'existing_alternatives_abr' => '',
            'key_metrics_abr' => '',
            'channels_abr' => '',
            'cost_structure_abr' => '',
            'revenue_streams_abr' => '',
            // Add other sections if needed
        ];

        // Split the content into sections and process
        $sections = preg_split("/\n\n+/", $content);
        foreach ($sections as $key => $section) {
            // Check which part of Lean Canvas the section belongs to and add it to the respective key
            if (stristr($section, 'Problem:')) {
                $leanCanvasParts['problem'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Solution:')) {
                $leanCanvasParts['solution'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Unique Value Proposition:')) {
                $leanCanvasParts['unique_value_proposition'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Unfair Advantage:')) {
                $leanCanvasParts['unfair_advantage'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Customer Segments:')) {
                $leanCanvasParts['customer_segments'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Existing Alternatives:')) {
                $leanCanvasParts['existing_alternatives'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Key Metrics:')) {
                $leanCanvasParts['key_metrics'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Channels:')) {
                $leanCanvasParts['channels'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Cost Structure:')) {
                $leanCanvasParts['cost_structure'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Revenue Streams:')) {
                $leanCanvasParts['revenue_streams'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Problem_abr:')) {
                $leanCanvasParts['problem_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Solution_abr:')) {
                $leanCanvasParts['solution_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Unique Value Proposition_abr:')) {
                $leanCanvasParts['unique_value_proposition_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Unfair Advantage_abr:')) {
                $leanCanvasParts['unfair_advantage_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Customer Segments_abr:')) {
                $leanCanvasParts['customer_segments_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Existing Alternatives_abr:')) {
                $leanCanvasParts['existing_alternatives_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Key Metrics_abr:')) {
                $leanCanvasParts['key_metrics_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Channels_abr:')) {
                $leanCanvasParts['channels_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Cost Structure_abr:')) {
                $leanCanvasParts['cost_structure_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Revenue Streams_abr:')) {
                $leanCanvasParts['revenue_streams_abr'] = $this->cleanSection($section);
            }
            // Add other sections if the response includes them
        }

        return $leanCanvasParts;
    }

private function cleanSection($section)
{
    // Remove anything before a ":"
    $section = preg_replace('/^[^\n]*:\s*/', '', $section);
    
    return trim($section);
}

}
?>