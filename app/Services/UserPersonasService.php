<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Already imported
use Illuminate\Support\Facades\Log;

class UserPersonasService
{
    public function generateAndSaveUserPersonasService($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this
    
        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
                [
                    'role' => 'system',
                    'content' => 'Generate user personas based on the project description.'
                ],
                [
                    'role' => 'user',
                    'content' => "
                        Generate 4 user personas based on the project description below:
                        
                        Project Description: {$projectDescription}
                        
                        Please create 4 user personas based on this. Each user persona should include:
                        - Name
                        - Age
                        - Gender
                        - personality_trait
                        - job
                        - marriage
                        - Description
                        - Goals
                        - Needs
                        - Pains
                        
                        For the Arabic version, ensure to include:
                        - Name_abr
                        - personality_trait_abr
                        - job_abr
                        - marriage_abr
                        - Description_abr
                        - Goals_abr
                        - Needs_abr
                        - Pains_abr
                        
                        Please make sure there is only one line break between each field.
                        
                        Example:
                        
                        Name: John Doe
                        Age: 30
                        Gender: Male
                        personality_trait: Outgoing, Passionate, Fun, Short-Tempered
                        job: Developer
                        marriage: Married
                        Description: John is an ardent software developer who finds joy in unraveling complex coding challenges.
                        Goals: John's aspiration is to ascend to the pinnacle of programming proficiency, mastering diverse languages.
                        Needs: He craves access to a plethora of coding resources and a nurturing community to foster growth.
                        Pains: John grapples with a perpetual scarcity of time to devote to his personal coding endeavors.
                        
                        Name_abr: جون دو
                        personality_trait_abr: صادق، عاطفي، مرح، سريع الغضب
                        job_abr: مطور
                        marriage_abr: متزوج
                        Description_abr: جون هو مطور برمجيات متحمس يجد السعادة في حل التحديات البرمجية المعقدة.
                        Goals_abr: هدف جون هو الصعود إلى قمة الإتقان البرمجي، واحتراف لغات متعددة.
                        Needs_abr: يتوق إلى الوصول إلى مجموعة واسعة من الموارد البرمجية ومجتمع داعم لتعزيز النمو.
                        Pains_abr: يُعاني جون من نقص مستمر في الوقت لتفريغ طاقته في مشاريعه الشخصية في البرمجة.
                        
                        Just break the line with /n, do not add any special characters.
                        Send me 4 user personas.
                        "
                ]
            ],
            'max_tokens' => 4000
        ];
        for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
        
            if (!$response) {
                // Handle error or retry
                continue;
            }
        
            $userPersonasContent = $response['choices'][0]['message']['content'];
            $sections = preg_split("/\n\n+/", $userPersonasContent);
            if (count($sections) !== 8) {
                Log::info('continue ' . $i);
                continue;
            }
            
            for ($j = 0; $j < 4; $j++) {
                $index = $j * 2;
                $userPersonasParts = $this->processUserPersonasContent($sections[$index], $sections[$index + 1]);
                if ($this->isAnyFieldNullObject($userPersonasParts)) {
                    Log::info('Skipped a user persona due to incomplete data');
                    continue;
                }
                $this->makeUserPersonas($project, $userPersonasParts);
            }
            break;
        }
        return true;
    }
    
    public function generateAndSaveOneUserPersonasService($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this
    
        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
                [
                    'role' => 'system',
                    'content' => 'Generate user personas based on the project description.'
                ],
                [
                    'role' => 'user',
                    'content' => "
                        Generate 1 user personas based on the project description below:
                        
                        Project Description: {$projectDescription}
                        
                        Please create 1 user personas based on this. Each user persona should include:
                        - Name
                        - Age
                        - Gender
                        - personality_trait
                        - job
                        - marriage
                        - Description
                        - Goals
                        - Needs
                        - Pains
                        
                        For the Arabic version, ensure to include:
                        - Name_abr
                        - personality_trait_abr
                        - job_abr
                        - marriage_abr
                        - Description_abr
                        - Goals_abr
                        - Needs_abr
                        - Pains_abr
                        
                        Please make sure there is only one line break between each field.
                        
                        Example:
                        
                        Name: John Doe
                        Age: 30
                        Gender: Male
                        personality_trait: Outgoing, Passionate, Fun, Short-Tempered
                        job: Developer
                        marriage: Married
                        Description: John is an ardent software developer who finds joy in unraveling complex coding challenges.
                        Goals: John's aspiration is to ascend to the pinnacle of programming proficiency, mastering diverse languages.
                        Needs: He craves access to a plethora of coding resources and a nurturing community to foster growth.
                        Pains: John grapples with a perpetual scarcity of time to devote to his personal coding endeavors.
                        
                        Name_abr: جون دو
                        personality_trait_abr: صادق، عاطفي، مرح، سريع الغضب
                        job_abr: مطور
                        marriage_abr: متزوج
                        Description_abr: جون هو مطور برمجيات متحمس يجد السعادة في حل التحديات البرمجية المعقدة.
                        Goals_abr: هدف جون هو الصعود إلى قمة الإتقان البرمجي، واحتراف لغات متعددة.
                        Needs_abr: يتوق إلى الوصول إلى مجموعة واسعة من الموارد البرمجية ومجتمع داعم لتعزيز النمو.
                        Pains_abr: يُعاني جون من نقص مستمر في الوقت لتفريغ طاقته في مشاريعه الشخصية في البرمجة.
                        
                        Just break the line with /n, do not add any special characters.
                        Send me 1 user personas.
                        "
                ]
            ],
            'max_tokens' => 4000
        ];
        for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
        
            if (!$response) {
                // Handle error or retry
                continue;
            }
        
            $userPersonasContent = $response['choices'][0]['message']['content'];
            $sections = preg_split("/\n\n+/", $userPersonasContent);
            if (count($sections) !== 2) {
                Log::info('continue ' . $i);
                continue;
            }
            
            $userPersonasParts = $this->processUserPersonasContent($sections[0], $sections[1]);
            if ($this->isAnyFieldNullObject($userPersonasParts)) {
                Log::info('Skipped a user persona due to incomplete data');
                continue;
            }
            $this->makeUserPersonas($project, $userPersonasParts);
            break;
        }
        return true;
    }
    private function makeUserPersonas($project, $userPersonasParts)
    {
        DB::table('user_personas')->insert([
            'project_id' => $project->id,
            'name' => $userPersonasParts['name'] ?? $this->getDefaultData('name'),
            'age' => is_numeric($userPersonasParts['age']) ? $userPersonasParts['age'] : random_int(15, 60),
            'description' => $userPersonasParts['description'] ?? $this->getDefaultData('description'),
            'personality_trait' => $userPersonasParts['personality_trait'] ?? '',
            'job' => $userPersonasParts['job'] ?? '',
            'marriage' => $userPersonasParts['marriage'] ?? '',
            'personality_trait_abr' => $userPersonasParts['personality_trait_abr'] ?? '',
            'job_abr' => $userPersonasParts['job_abr'] ?? '',
            'marriage_abr' => $userPersonasParts['marriage_abr'] ?? '',
            'goals' => $userPersonasParts['goals'] ?? $this->getDefaultData('goals'),
            'needs' => $userPersonasParts['needs'] ?? $this->getDefaultData('needs'),
            'pains' => $userPersonasParts['pains'] ?? $this->getDefaultData('pains'),
            'name_abr' => $userPersonasParts['name_abr'] ?? $this->getDefaultData('name_abr'),
            'description_abr' => $userPersonasParts['description_abr'] ?? $this->getDefaultData('description_abr'),
            'goals_abr' => $userPersonasParts['goals_abr'] ?? $this->getDefaultData('goals_abr'),
            'needs_abr' => $userPersonasParts['needs_abr'] ?? $this->getDefaultData('needs_abr'),
            'pains_abr' => $userPersonasParts['pains_abr'] ?? $this->getDefaultData('pains_abr'),
            'url' => $this->generateImageUrl($userPersonasParts['gender']),
            // Add other sections if needed
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
    
    function getDefaultData($type) {
        $needs = [
            'Access to a diverse range of content and services catering to different interests and preferences.',
            'Offline access to essential features or content, particularly in areas with limited internet connectivity.',
            'Personal data protection measures and the ability to control and manage privacy settings.',
            'Multilingual support to cater to users from various linguistic backgrounds.',
            'Educational resources and learning opportunities to enhance skills or knowledge.',
            'Social connectivity features, such as friend lists, messaging, and community forums.',
            'Real-time updates and notifications to stay informed about relevant events or changes.',
            'Seamless transition between different devices and platforms while maintaining continuity of experience.',
            'Efficient and hassle-free returns or refunds process for purchased products or services.',
            'Access to reliable and trustworthy reviews or ratings to aid decision-making processes.'
        ];
        
        $needs_abr = [
            'الوصول إلى مجموعة متنوعة من المحتويات والخدمات التي تلبي مختلف الاهتمامات والتفضيلات.',
            'الوصول غير المتصل إلى الميزات أو المحتوى الأساسي، خاصة في المناطق ذات الاتصال الإنترنت محدود.',
            'تدابير حماية البيانات الشخصية والقدرة على التحكم وإدارة إعدادات الخصوصية.',
            'الدعم متعدد اللغات لتلبية احتياجات المستخدمين من خلفيات لغوية مختلفة.',
            'الموارد التعليمية وفرص التعلم لتعزيز المهارات أو المعرفة.',
            'ميزات الاتصال الاجتماعي، مثل قوائم الأصدقاء والرسائل ومنتديات المجتمع.',
            'التحديثات والإشعارات الفورية للبقاء على اطلاع على الأحداث أو التغييرات ذات الصلة.',
            'الانتقال السلس بين أجهزة ومنصات مختلفة مع الحفاظ على استمرارية الخبرة.',
            'عملية إعادة البضائع أو استرداد الأموال بكفاءة وبدون متاعب للمنتجات أو الخدمات المشتراة.',
            'الوصول إلى مراجعات أو تقييمات موثوقة وجديرة بالثقة لمساعدة عمليات اتخاذ القرار.'
        ];
        
        $user_goals = [
            'Access a diverse range of content and services catering to different interests and preferences.',
            'Have offline access to essential features or content, especially in areas with limited internet connectivity.',
            'Ensure personal data protection measures and the ability to control and manage privacy settings.',
            'Benefit from multilingual support to cater to users from various linguistic backgrounds.',
            'Access educational resources and learning opportunities to enhance skills or knowledge.',
            'Utilize social connectivity features, such as friend lists, messaging, and community forums.',
            'Receive real-time updates and notifications to stay informed about relevant events or changes.',
            'Experience seamless transition between different devices and platforms while maintaining continuity of experience.',
            'Enjoy an efficient and hassle-free returns or refunds process for purchased products or services.',
            'Access reliable and trustworthy reviews or ratings to aid decision-making processes.'
        ];
        
        $user_goals_abr = [
            'الوصول إلى مجموعة متنوعة من المحتويات والخدمات التي تلبي مختلف الاهتمامات والتفضيلات.',
            'الوصول غير المتصل إلى الميزات أو المحتوى الأساسي، خاصة في المناطق ذات الاتصال الإنترنت محدود.',
            'ضمان تدابير حماية البيانات الشخصية والقدرة على التحكم وإدارة إعدادات الخصوصية.',
            'الاستفادة من الدعم متعدد اللغات لتلبية احتياجات المستخدمين من خلفيات لغوية مختلفة.',
            'الوصول إلى الموارد التعليمية وفرص التعلم لتعزيز المهارات أو المعرفة.',
            'استخدام ميزات الاتصال الاجتماعي، مثل قوائم الأصدقاء والرسائل ومنتديات المجتمع.',
            'تلقي التحديثات والإشعارات في الوقت الفعلي للبقاء على اطلاع على الأحداث أو التغييرات ذات الصلة.',
            'تجربة الانتقال السلس بين أجهزة ومنصات مختلفة مع الحفاظ على استمرارية الخبرة.',
            'الاستمتاع بعملية إعادة البضائع أو استرداد الأموال بكفاءة وبدون متاعب للمنتجات أو الخدمات المشتراة.',
            'الوصول إلى مراجعات أو تقييمات موثوقة وجديرة بالثقة لمساعدة عمليات اتخاذ القرار.'
        ];

        $user_names = [
            'Ali Abdullah',
            'Fatima Ahmed',
            'Mohammed Khan',
            'Aisha Ali',
            'Omar Hassan',
            'Mariam Ibrahim',
            'Ahmed Mahmoud',
            'Layla Khalid',
            'Yusuf Hassan',
            'Noor Malik'
        ];
        
        $user_names_abr = [
            'علي عبدالله',
            'فاطمة أحمد',
            'محمد خان',
            'عائشة علي',
            'عمر حسن',
            'مريم إبراهيم',
            'أحمد محمود',
            'ليلى خالد',
            'يوسف حسن',
            'نور مالك'
        ];

        $user_descriptions = [
            'Ali Abdullah is a 40-year-old business consultant based in Riyadh, Saudi Arabia. With years of experience in the field, he specializes in advising companies on strategic planning and market analysis. In his free time, Ali enjoys reading, traveling, and spending time with his family.',
            'Fatima Ahmed, a 28-year-old software engineer from Dubai, UAE, is passionate about coding and creating innovative solutions. She works diligently to develop user-friendly applications that improve efficiency and productivity. Outside of work, Fatima enjoys hiking and photography.',
            'Mohammed Khan, a 35-year-old entrepreneur from Karachi, Pakistan, is the founder of a successful e-commerce platform. He is known for his innovative business strategies and commitment to customer satisfaction. In his leisure time, Mohammed enjoys playing soccer and attending cultural events.',
            'Aisha Ali, a 30-year-old marketing manager from Cairo, Egypt, excels in developing creative campaigns that resonate with target audiences. Her strong communication skills and strategic mindset have contributed to the success of numerous projects. Aisha is passionate about fitness and volunteers at local charities.',
            'Omar Hassan, a 32-year-old architect from Amman, Jordan, is recognized for his innovative designs and attention to detail. He combines traditional architectural elements with modern aesthetics to create unique spaces. Outside of work, Omar enjoys painting and exploring the outdoors.',
            'Mariam Ibrahim, a 25-year-old medical doctor from Casablanca, Morocco, is dedicated to providing quality healthcare to her patients. She is known for her compassionate bedside manner and commitment to continuous learning. In her free time, Mariam enjoys cooking Moroccan cuisine and practicing yoga.',
            'Ahmed Mahmoud, a 38-year-old financial analyst from Alexandria, Egypt, possesses a strong analytical mindset and expertise in financial modeling. He has a track record of delivering accurate forecasts and insightful recommendations to clients. Ahmed enjoys playing chess and attending classical music concerts.',
            'Layla Khalid, a 33-year-old fashion designer from Beirut, Lebanon, is renowned for her elegant and sophisticated designs. She draws inspiration from her Lebanese heritage and international travels to create timeless pieces. Layla is passionate about promoting sustainability in the fashion industry.',
            'Yusuf Hassan, a 45-year-old professor of literature from Baghdad, Iraq, is known for his deep knowledge of Arabic literature and poetry. He inspires his students through engaging lectures and encourages critical thinking. Outside of academia, Yusuf enjoys writing poetry and hiking in the countryside.',
            'Noor Malik, a 29-year-old environmental scientist from Muscat, Oman, is committed to protecting the natural world through research and advocacy. She specializes in studying the impact of climate change on marine ecosystems. Noor enjoys scuba diving and participating in beach cleanup initiatives.'
        ];
        
        $user_descriptions_abr = [
            'علي عبدالله، عمره 40 عامًا، مستشار أعمال مقره في الرياض، المملكة العربية السعودية. بعد سنوات من الخبرة في المجال، يختص في تقديم النصائح للشركات في التخطيط الاستراتيجي وتحليل السوق. في وقت فراغه، يستمتع علي بالقراءة والسفر وقضاء الوقت مع عائلته.',
            'فاطمة أحمد، عمرها 28 عامًا، مهندسة برمجيات من دبي، الإمارات العربية المتحدة. متحمسة للغاية للبرمجة وإنشاء حلول مبتكرة. تعمل بجد لتطوير تطبيقات سهلة الاستخدام تعزز الكفاءة والإنتاجية. خارج العمل، تستمتع فاطمة برياضة المشي لمسافات طويلة والتصوير الفوتوغرافي.',
            'محمد خان، عمره 35 عامًا، رائد أعمال من كراتشي، باكستان. مؤسس منصة تجارة إلكترونية ناجحة. معروف بإستراتيجياته الريادية المبتكرة والالتزام برضاء العملاء. في وقت فراغه، يستمتع محمد بلعب كرة القدم وحضور الفعاليات الثقافية.',
            'عائشة علي، عمرها 30 عامًا، مدير تسويق من القاهرة، مصر. تتميز بتطوير حملات إبداعية تلقى صدىًا لدى الجماهير المستهدفة. مهاراتها الاتصالية القوية وروحها الاستراتيجية ساهمت في نجاح العديد من المشاريع. تهوى عائشة ممارسة اللياقة البدنية والتطوع في الجمعيات الخيرية المحلية.',
            'عمر حسن، عمره 32 عامًا، مهندس معماري من عمان، الأردن. معروف بتصاميمه المبتكرة واهتمامه بالتفاصيل. يجمع بين العناصر المعمارية التقليدية والجماليات الحديثة لإنشاء مساحات فريدة. خارج العمل، يستمتع عمر بالرسم واستكشاف الهواء الطلق.',
            'مريم إبراهيم، عمرها 25 عامًا، طبيبة من الدار البيضاء، المغرب. ملتزمة بتقديم رعاية صحية عالية الجودة لمرضاها. معروفة بأسلوبها الدافئ والرحب والتزامها بالتعلم المستمر. في أوقات فراغها، تستمتع مريم بطهي المأكولات المغربية وممارسة اليوغا.',
            'أحمد محمود، عمره 38 عامًا، محلل مالي من الإسكندرية، مصر. يتمتع بعقلية تحليلية قوية وخبرة في التنبؤ المالي. لديه سجل حافل في تقديم توقعات دقيقة وتوصيات مفيدة للعملاء. يستمتع أحمد بلعب الشطرنج وحضور الحفلات الموسيقية الكلاسيكية.',
            'ليلى خالد، عمرها 33 عامًا، مصممة أزياء من بيروت، لبنان. مشهورة بتصميماتها الأنيقة والراقية. تستوحي ليلى إبداعها من تراثها اللبناني ورحلاتها الدولية لإنشاء قطع خالدة. تهوى ليلى تعزيز الاستدامة في صناعة الأزياء.',
            'يوسف حسن، عمره 45 عامًا، أستاذ أدب من بغداد، العراق. معروف بمعرفته العميقة بالأدب والشعر العربي. يلهم طلابه من خلال محاضراته المشوقة ويشجع التفكير النقدي. خارج الأكاديمية، يستمتع يوسف بكتابة الشعر والمشي في الريف.',
            'نور مالك، عمرها 29 عامًا، عالمة بيئة من مسقط، عمان. ملتزمة بحماية العالم الطبيعي من خلال البحث والتشجيع. تتخصص في دراسة تأثير التغيرات المناخية على النظم البيئية البحرية. تستمتع نور بالغوص والمشاركة في مبادرات تنظيف الشواطئ.'
        ];

        $user_pains = [
            'Difficulty keeping up with rapidly changing technology and business trends.',
            'Struggle to find reliable sources for industry news and market insights.',
            'Challenges in effectively managing time to handle multiple client projects.',
            'Difficulty maintaining a healthy work-life balance.',
            'Ensuring the security and confidentiality of client data.',
            'Trouble adapting to new software or tools required for work.',
            'Feeling overwhelmed by the volume of information available.',
            'Dealing with unexpected setbacks or obstacles in projects.',
            'Finding it hard to stay motivated and productive.',
            'Feeling isolated or lacking support in professional endeavors.',
        ];
        
        $user_pains_abr = [
            'صعوبة مواكبة التغييرات السريعة في التكنولوجيا واتجاهات الأعمال.',
            'صعوبة في العثور على مصادر موثوقة للأخبار الصناعية والرؤى السوقية.',
            'تحديات في إدارة الوقت بشكل فعال للتعامل مع عدة مشاريع للعملاء.',
            'صعوبة في الحفاظ على توازن صحي بين العمل والحياة الشخصية.',
            'ضمان أمان وسرية بيانات العملاء.',
            'مواجهة صعوبة في التكيف مع البرمجيات أو الأدوات الجديدة المطلوبة للعمل.',
            'الشعور بالتحدي بسبب حجم المعلومات المتاحة.',
            'التعامل مع العقبات غير المتوقعة أو المشاكل في المشاريع.',
            'الصعوبة في البقاء متحمسًا وإنتاجيًا.',
            'الشعور بالعزلة أو نقص الدعم في الجهود المهنية.',
        ];
        
        $number = rand(0, 9);
        
        if ($type == 'name') {
            return $user_names[$number];
        } elseif ($type == 'description') {
            return $user_descriptions[$number];
        } elseif ($type == 'goals') {
            return $user_goals[$number];
        } elseif ($type == 'needs') {
            return $needs[$number];
        } elseif ($type == 'pains') {
            return $user_pains[$number];
        } elseif ($type == 'name_abr') {
            return $user_names_abr[$number];
        } elseif ($type == 'description_abr') {
            return $user_descriptions_abr[$number];
        } elseif ($type == 'goals_abr') {
            return $user_goals_abr[$number];
        } elseif ($type == 'needs_abr') {
            return $needs_abr[$number];
        } elseif ($type == 'pains_abr') {
            return $user_pains_abr[$number];
        }
        return '';
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }
    
    private function generateImageUrl($gender)
    {
        $number = rand(1, 14);
        if (strtolower($gender) == 'female') {
            $gender = '';
        } else {
            $gender =  'male-';
        }
        return 'avatar'. '-' . $gender . $number . '.jpg';
    }

    // private function makeUserPersonas($project, $data, $data_abr)
    // {
    //     DB::table('user_personas')->insert([
    //                     'project_id' => $project->id,
    //                     'name' => $data['name'],
    //                     'age' => is_numeric($data['age']) ? $data['age'] : random_int(15, 60),
    //                     'description' => $data['description'],
    //                     'goals' => $data['goals'],
    //                     'needs' => $data['needs'],
    //                     'pains' => $data['pains'],
    //                     'name_abr' => $data_abr['name_abr'],
    //                     'description_abr' => $data_abr['description_abr'],
    //                     'goals_abr' => $data_abr['goals_abr'],
    //                     'needs_abr' => $data_abr['needs_abr'],
    //                     'pains_abr' => $data_abr['pains_abr'],
    //                     'url' => $this->generateImageUrl($data['gender']),
    //                     // Add other sections if needed
    //                     'created_at' => now(),
    //                     'updated_at' => now()
    //                 ]);
    // }

    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return json_decode($response, true);
        }
    }

    private function processUserPersonasContent($content, $content_abr)
    {
        // Initialize an array to hold each part of the Lean Canvas
        $userPersonasParts = [
            'name' => '',
            'age' => '',
            'description' => '',
            'personality_trait' => '',
            'job' => '',
            'marriage' => '',
            'goals' => '',
            'needs' => '',
            'pains' => '',
            'gender' => '',
            'name_abr' => '',
            'personality_trait_abr' => '',
            'job_abr' => '',
            'marriage_abr' => '',
            'description_abr' => '',
            'goals_abr' => '',
            'needs_abr' => '',
            'pains_abr' => '',
            // Add other sections if needed
        ];

        // Split the content into sections and process
        $sections = preg_split("/\n+/", $content);
        $sections_abr = preg_split("/\n+/", $content_abr);

        foreach ($sections as $section) {
            // Check which part of Lean Canvas the section belongs to and add it to the respective key
            if (stristr($section, 'Name:')) {
                $userPersonasParts['name'] = $this->cleanSection($section);
            } elseif (stristr($section, 'personality_trait:')) {
                $userPersonasParts['personality_trait'] = $this->cleanSection($section);
            } elseif (stristr($section, 'job:')) {
                $userPersonasParts['job'] = $this->cleanSection($section);
            } elseif (stristr($section, 'marriage:')) {
                $userPersonasParts['marriage'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Gender:')) {
                $userPersonasParts['gender'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Age:')) {
                $userPersonasParts['age'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Description:')) {
                $userPersonasParts['description'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Goals:')) {
                $userPersonasParts['goals'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Needs:')) {
                $userPersonasParts['needs'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Pains:')) {
                $userPersonasParts['pains'] = $this->cleanSection($section);
            }
        }
        
        foreach ($sections_abr as $section) {
            // Check which part of Lean Canvas the section belongs to and add it to the respective key
            if (stristr($section, 'Description_abr:')) {
                $userPersonasParts['description_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'personality_trait_abr:')) {
                $userPersonasParts['personality_trait_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'job_abr:')) {
                $userPersonasParts['job_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'marriage_abr:')) {
                $userPersonasParts['marriage_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Goals_abr:')) {
                $userPersonasParts['goals_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Needs_abr:')) {
                $userPersonasParts['needs_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Pains_abr:')) {
                $userPersonasParts['pains_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'name_abr:')) {
                $userPersonasParts['name_abr'] = $this->cleanSection($section);
            }
        }

        return $userPersonasParts;
    }

private function cleanSection($section)
{
    // Remove anything before a ":"
    $section = preg_replace('/^[^\n]*:\s*/', '', $section);
    
    return trim($section);
}

}
?>