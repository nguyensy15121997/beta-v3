<?php

namespace App\Services;

use App\Models\Storytelling; // Make sure this matches the namespace of your SwotAnalysis model
use Illuminate\Support\Facades\DB;

class StorytellingService
{
    public function generateAndSaveStorytelling($project)
    {
        for ($i = 0; $i < 2; $i++) {
            $projectDescription = $project->description;
            // Use an appropriate API key management strategy
            $apiKey = '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'; // Make sure to secure this
    
            $data = [
                'model' => 'gpt-3.5-turbo-1106', // Specify the GPT-4 model
                'temperature' => 0.3,
                'messages' => [
                    ['role' => 'system', 'content' => 'You are a helpful assistant. Please generate the elements I request based on my idea description.'],
                    ['role' => 'user', 'content' => "Project description: {$projectDescription}. Please create comprehensive details for the following storytelling elements first in English, then in Arabic. The elements are: 'Mission', 'Vision', 'Brand Positioning', 'Brand Values', 'Personality', 'Messaging', 'Suggested Names for the Idea', 'Elevator Pitch', 'Mission_abr', 'Vision_abr', 'Brand Positioning_abr', 'Brand Values_abr', 'Personality', 'Messaging_abr', 'Suggested Names for the Idea_abr', and 'Elevator Pitch_abr' . For 'Mission', 'Vision', 'Brand Positioning', 'Brand Values', 'Personality', 'Messaging', 'Suggested Names for the Idea', 'Elevator Pitch' return English. For 'Mission_abr', 'Vision_abr', 'Brand Positioning_abr', 'Brand Values_abr', 'Personality', 'Messaging_abr', 'Suggested Names for the Idea_abr', and 'Elevator Pitch_abr' return Arabic. Please return all the information I requested and the correct fields I requested."]
                ],
                'max_tokens' => 4096
            ];
    
            $response = $this->sendRequestToOpenAI($data, $apiKey);
    
           if ($response) {
                $storytellingContent = $response['choices'][0]['message']['content'];
    
                $englishParts = $this->processStorytellingContent($storytellingContent);
                if ($this->isAnyFieldNullObject($englishParts) && $i != 1) {
                    continue;
                } else {
                    // Insert storytelling data into the database
                    DB::table('storytelling')->insert([
                        'project_id' => $project->id,
                        'mission' => $englishParts['Mission'] ?? null,
                        'vision' => $englishParts['Vision'] ?? null,
                        'brand_positioning' => $englishParts['Brand Positioning'] ?? null,
                        'brand_values' => $englishParts['Brand Values'] ?? null,
                        'personality' => $englishParts['Personality'] ?? null,
                        'messaging' => $englishParts['Messaging'] ?? null,
                        'suggested_names' => $englishParts['Suggested Names for the Idea'] ?? null,
                        'elevator_pitch' => $englishParts['Elevator Pitch'] ?? null,
                        'mission_abr' => $englishParts['Mission_abr'] ?? null,
                        'vision_abr' => $englishParts['Vision_abr'] ?? null,
                        'brand_positioning_abr' => $englishParts['Brand Positioning_abr'] ?? null,
                        'brand_values_abr' => $englishParts['Brand Values_abr'] ?? null,
                        'personality_abr' => $englishParts['Personality_abr'] ?? null,
                        'messaging_abr' => $englishParts['Messaging_abr'] ?? null,
                        'suggested_names_abr' => $englishParts['Suggested Names for the Idea_abr'] ?? null,
                        'elevator_pitch_abr' => $englishParts['Elevator Pitch_abr'] ?? null,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]);
                    break;
                }
            }
        }
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }


    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json',
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
            $responseArray = json_decode($response, true);


        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return $responseArray;
        }
    }

    private function processStorytellingContent($content, $isArabic = false)
    {
        $storytellingParts = [
            'Mission' => '',
            'Vision' => '',
            'Brand Positioning' => '',
            'Brand Values' => '',
            'Personality' => '',
            'Messaging' => '',
            'Suggested Names for the Idea' => '',
            'Elevator Pitch' => '',
            'Mission_abr' => '',
            'Vision_abr' => '',
            'Brand Positioning_abr' => '',
            'Brand Values_abr' => '',
            'Personality_abr' => '',
            'Messaging_abr' => '',
            'Suggested Names for the Idea_abr' => '',
            'Elevator Pitch_abr' => ''
            // Add other sections if needed
        ];

        // Add '_abr' suffix for Arabic keys if processing Arabic content
        $suffix = $isArabic ? '_abr' : '';

        $sections = preg_split("/\n\n+/", $content);

        foreach ($sections as $section) {
            // foreach ($storytellingParts as $key => $value) {
            //     $keyWithSuffix = $key . $suffix;
            //     if (stristr($section, $keyWithSuffix . ':')) {
            //         $storytellingParts[$keyWithSuffix] = $this->cleanSection($section);
            //         break; // Stop the inner loop once the key is found and assigned
            //     }
            // }
            foreach ($sections as $section) {
                // Check which part of the Storytelling the section belongs to and add it to the respective key
                if (stristr($section, 'Mission:')) {
                    $storytellingParts['Mission'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Vision:')) {
                    $storytellingParts['Vision'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Brand Positioning:')) {
                    $storytellingParts['Brand Positioning'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Brand Values:')) {
                    $storytellingParts['Brand Values'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Personality:')) {
                    $storytellingParts['Personality'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Messaging:')) {
                    $storytellingParts['Messaging'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Suggested Names for the Idea:')) {
                    $storytellingParts['Suggested Names for the Idea'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Elevator Pitch:')) {
                    $storytellingParts['Elevator Pitch'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Mission_abr:')) {
                    $storytellingParts['Mission_abr'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Vision_abr:')) {
                    $storytellingParts['Vision_abr'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Brand Positioning_abr:')) {
                    $storytellingParts['Brand Positioning_abr'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Brand Values_abr:')) {
                    $storytellingParts['Brand Values_abr'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Personality_abr:')) {
                    $storytellingParts['Personality_abr'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Messaging_abr:')) {
                    $storytellingParts['Messaging_abr'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Suggested Names for the Idea_abr:')) {
                    $storytellingParts['Suggested Names for the Idea_abr'] = $this->cleanSection($section);
                } elseif (stristr($section, 'Elevator Pitch_abr:')) {
                    $storytellingParts['Elevator Pitch_abr'] = $this->cleanSection($section);
                }
                // Add other sections if the response includes them
            }
        }

        return $storytellingParts;
    }

private function cleanSection($section)
{
    // Remove anything before a ":" and remove "*"
    $section = preg_replace('/^[^\n]*:\s*/', '', $section);
    // $section = preg_replace('/\*/', '', $section);
    
    return trim($section);
}




}
