<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Add this line at the top of your controller to import the DB facade


class SwotAnalysisService
{
    public function generateAndSaveSwotAnalysis($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this

        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3, // Optional: control the randomness of the output
            'messages' => [
                ['role' => 'system', 'content' => 'You are a helpful assistant.'],
                ['role' => 'user', 'content' => "
                Here's a project description: {$projectDescription}. Based on this,  I need a complete SWOT analysis
                    - Strengths
                    - Weaknesses 
                    - Opportunities
                    - Threats
                    
                    For the Arabic version, ensure to include:
                    - Threats_abr
                    - Opportunities_abr
                    - Weaknesses_abr
                    - Strengths_abr
                    
                    Example:
                    
                    Strengths: ...
                    Weaknesses: ...
                    Opportunities: ...
                    Threats: ...
                    
                    Strengths_abr: ...
                    Weaknesses_abr: ...
                    Opportunities_abr: ...
                    Threats_abr: ...
                    
                    Just break the line with /n, do not add any special characters.
                    Send me Arabic and English and the returned results are not short. Please send me 4 different users.
            "]
            ],
            'max_tokens' => 4000 // You may want to adjust this
        ];
        
        $ch = curl_init('https://api.openai.com/v1/chat/completions'); // Updated endpoint for GPT-4
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);
        
        for ($i = 0; $i < 2; $i++) {
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
        
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $responseArray = json_decode($response, true);
            
                if (isset($responseArray['choices'][0]['message']['content'])) {
                    $swotAnalysisContent = $responseArray['choices'][0]['message']['content'];
                    
                    $sections = preg_split("/\n\n+/", $swotAnalysisContent);
                    if (count($sections) !== 8) {
                        \Log::info('continue ' . $i);
                        continue;
                    }
                    $swotParts = $this->processSwotContent($swotAnalysisContent);
            
                    if ($this->isAnyFieldNullObject($swotParts)) {
                        continue;
                    } else {
                        // Access the parts directly from the array
                        $strengths = $swotParts['strengths'];
                        $weaknesses = $swotParts['weaknesses'];
                        $opportunities = $swotParts['opportunities'];
                        $threats = $swotParts['threats'];
                        $strengths_abr = $swotParts['strengths_abr'];
                        $weaknesses_abr = $swotParts['weaknesses_abr'];
                        $opportunities_abr = $swotParts['opportunities_abr'];
                        $threats_abr = $swotParts['threats_abr'];
                
                        // Insert SWOT analysis data into the existing table
                        DB::table('swot_analyses')->insert([
                            'project_id' => $project->id,
                            'strengths' => $strengths,
                            'weaknesses' => $weaknesses,
                            'opportunities' => $opportunities,
                            'threats_abr' => $threats_abr,
                            'strengths_abr' => $strengths_abr,
                            'weaknesses_abr' => $weaknesses_abr,
                            'opportunities_abr' => $opportunities_abr,
                            'threats' => $threats,
                        ]);
                        break;
                    }
                }
            }
        }
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }

    public function processSwotContent($content)
    {
        // Initialize an array to hold each part of the SWOT analysis
        $swotParts = [
            'strengths' => '',
            'weaknesses' => '',
            'opportunities' => '',
            'threats' => '',
            'strengths_abr' => '',
            'weaknesses_abr' => '',
            'opportunities_abr' => '',
            'threats_abr' => ''
        ];

        // Split the content by double line breaks
        $sections = preg_split("/\n\n+/", $content);

        foreach ($sections as $section) {
            // Check which part of SWOT the section belongs to and add it to the respective key
            if (stristr($section, 'Strengths:')) {
                $swotParts['strengths'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Weaknesses:')) {
                $swotParts['weaknesses'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Opportunities:')) {
                $swotParts['opportunities'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Threats:')) {
                $swotParts['threats'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Strengths_abr:')) {
                $swotParts['strengths_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Weaknesses_abr:')) {
                $swotParts['weaknesses_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Opportunities_abr:')) {
                $swotParts['opportunities_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'Threats_abr:')) {
                $swotParts['threats_abr'] = $this->cleanSection($section);
            }
        }

        return $swotParts;
    }

    function cleanSection($section)
    {
        // Remove the title from each section and trim whitespace
        $section = preg_replace('/^[^\n]*:\s*/', '', $section);
        return $section;
    }
}
?>