<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Already imported
use Illuminate\Support\Facades\Log;

class BrandIdentityService
{
    public function generateAndSaveBrandIdentity($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this
    
        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
                [
                    'role' => 'system',
                    'content' => 'Generate branding identity based on the project description.'
                ],
                [
                    'role' => 'user',
                    'content' => "
                        Generate branding identity based on the project description below:
                        
                        Project Description: {$projectDescription}
                        
                        Please create branding identity based on this. Brand identity should include:
                        - color_name_1
                        - color_name_2
                        - color_name_3
                        - color_name_4
                        - color_name_5
                        - color_hex_1
                        - color_hex_2
                        - color_hex_3
                        - color_hex_4
                        - color_hex_5
                        - color_des
                        - logo_idea
                        - typography
                        - imagery_photography
                        - brand_voice
                        - brand_values
                        - brand_personality_traits
                        - customer_promise
                        
                        For the Arabic version, ensure to include:
                        - color_name_1_abr
                        - color_name_2_abr
                        - color_name_3_abr
                        - color_name_4_abr
                        - color_name_5_abr
                        - color_des_abr
                        - logo_idea_abr
                        - typography_abr
                        - imagery_photography_abr
                        - brand_voice_abr
                        - brand_values_abr
                        - brand_personality_traits_abr
                        - customer_promise_abr
                        
                        Example:
                        
                        color_name_1: Burnt Orange
                        color_name_2: Fern
                        color_name_3: Olive Green
                        color_name_4: Navy Blue
                        color_name_5: Wild Strawberry
                        color_hex_1: #FF7F50
                        color_hex_2: #228B22
                        color_hex_3: #a79111
                        color_hex_4: #1E90FF
                        color_hex_5: #FF1493
                        color_des: The color scheme is inspired by the vibrant landscapes of Saudi Arabia, reflecting a blend of modernity and tradition.
                        logo_idea: The logo for Swah features a stylized letter 'S' integrated with elements representing travel and exploration, symbolizing our innovative and personalized approach to travel planning.
                        typography:	We chose Lato as the main font for its modern and friendly appearance, complemented by Playfair Display as the secondary font to add elegance and sophistication to our brand identity.
                        imagery_photography: Our imagery showcases the beauty of Saudi Arabia's landscapes, rich culture, and immersive travel experiences, capturing the essence of adventure and exploration.
                        brand_voice: Swah's brand voice is informative, inviting, and tailored to inspire travelers to discover the hidden gems of Saudi Arabia.
                        brand_values: Innovation, Personalization, Cultural Integration, Exclusive Access.
                        brand_personality_traits: Innovative, Personalized, Adventurous, Cultural, Exclusive.
                        customer_promise: We promise to provide travelers with unforgettable and tailored experiences, showcasing the best of Saudi Arabia's hidden treasures.
                        
                        color_name_1_abr: برتقالي محروق
                        color_name_2_abr: السرخس
                        color_name_3_abr: أخضر زيتوني
                        color_name_4_abr: أزرق داكن
                        color_name_5_abr: الفراولة البرية
                        color_des_abr: نظام الألوان مستوحى من المناظر الطبيعية النابضة بالحياة في المملكة العربية السعودية، مما يعكس مزيجًا من الحداثة والتقاليد.
                        logo_idea_abr: يتميز شعار Swah بالحرف S' المتكامل مع العناصر التي تمثل السفر والاستكشاف، مما يرمز إلى نهجنا المبتكر والشخصي في التخطيط للسفر.
                        typography_abr: لقد اخترنا Lato كخط رئيسي لمظهره العصري والودي، واستكمله Playfair Display كخط ثانوي لإضافة الأناقة والرقي إلى هوية علامتنا التجارية.
                        imagery_photography_abr: تعرض صورنا جمال المناظر الطبيعية في المملكة العربية السعودية والثقافة الغنية وتجارب السفر الغامرة، مما يجسد جوهر المغامرة والاستكشاف.
                        brand_voice_abr: صوت العلامة التجارية سواه غني بالمعلومات، وجذاب، ومصمم لإلهام المسافرين لاكتشاف الجواهر الخفية في المملكة العربية السعودية.
                        brand_values_abr: الابتكار، التخصيص، التكامل الثقافي، الوصول الحصري.
                        brand_personality_traits_abr: مبتكرة، شخصية، مغامرة، ثقافية، حصرية.
                        customer_promise_abr: نعد بتزويد المسافرين بتجارب لا تُنسى ومصممة خصيصًا، ونعرض أفضل كنوز المملكة العربية السعودية المخفية.
                        
                        Just break the line with /n, do not add any special characters.
                        Please send me all the fields that I have provided above.
                        "
                ]
            ],
            'max_tokens' => 4000
        ];
        for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
        
            if (!$response) {
                // Handle error or retry
                continue;
            }
        
            $content = $response['choices'][0]['message']['content'];
            $sections = preg_split("/\n\n+/", $content);
            if (count($sections) !== 2) {
                Log::info('continue ' . $i);
                continue;
            }
            
            $parts = $this->processContent($sections[0], $sections[1]);

            if ($this->isAnyFieldNullObject($parts)) {
                Log::info('Skipped a part due to incomplete data');
                continue;
            }

            DB::table('brands_identity')->insert([
                'project_id' => $project->id,
                'created_at' => now(),
                'updated_at' => now()
            ] + $parts);
            break;
        }
        return true;
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }

    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return json_decode($response, true);
        }
    }

    private function processContent($content, $content_abr)
    {
        $parts = [
            'color_name_1' => '',
            'color_name_2' => '',
            'color_name_3' => '',
            'color_name_4' => '',
            'color_name_5' => '',
            'color_hex_1' => '',
            'color_hex_2' => '',
            'color_hex_3' => '',
            'color_hex_4' => '',
            'color_hex_5' => '',
            'color_des' => '',
            'logo_idea' => '',
            'typography' => '',
            'imagery_photography' => '',
            'brand_voice' => '',
            'brand_values' => '',
            'brand_personality_traits' => '',
            'customer_promise' => '',
            'color_name_1_abr' => '',
            'color_name_2_abr' => '',
            'color_name_3_abr' => '',
            'color_name_4_abr' => '',
            'color_name_5_abr' => '',
            'color_des_abr' => '',
            'logo_idea_abr' => '',
            'typography_abr' => '',
            'imagery_photography_abr' => '',
            'brand_voice_abr' => '',
            'brand_values_abr' => '',
            'brand_personality_traits_abr' => '',
            'customer_promise_abr' => '',
            // Add other sections if needed
        ];

        // Split the content into sections and process
        $sections = preg_split("/\n+/", $content);
        $sections_abr = preg_split("/\n+/", $content_abr);

        foreach ($sections as $section) {
            if (stristr($section, 'color_name_1:')) {
                $parts['color_name_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_name_2:')) {
                $parts['color_name_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_name_3:')) {
                $parts['color_name_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_name_4:')) {
                $parts['color_name_4'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_name_5:')) {
                $parts['color_name_5'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_hex_1:')) {
                $parts['color_hex_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_hex_2:')) {
                $parts['color_hex_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_hex_3:')) {
                $parts['color_hex_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_hex_4:')) {
                $parts['color_hex_4'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_hex_5:')) {
                $parts['color_hex_5'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_des:')) {
                $parts['color_des'] = $this->cleanSection($section);
            } elseif (stristr($section, 'logo_idea:')) {
                $parts['logo_idea'] = $this->cleanSection($section);
            } elseif (stristr($section, 'typography:')) {
                $parts['typography'] = $this->cleanSection($section);
            } elseif (stristr($section, 'imagery_photography:')) {
                $parts['imagery_photography'] = $this->cleanSection($section);
            } elseif (stristr($section, 'brand_voice:')) {
                $parts['brand_voice'] = $this->cleanSection($section);
            } elseif (stristr($section, 'brand_values:')) {
                $parts['brand_values'] = $this->cleanSection($section);
            } elseif (stristr($section, 'brand_personality_traits:')) {
                $parts['brand_personality_traits'] = $this->cleanSection($section);
            } elseif (stristr($section, 'customer_promise:')) {
                $parts['customer_promise'] = $this->cleanSection($section);
            }
        }
        
        foreach ($sections_abr as $section) {
            if (stristr($section, 'color_name_1_abr:')) {
                $parts['color_name_1_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_name_2_abr:')) {
                $parts['color_name_2_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_name_3_abr:')) {
                $parts['color_name_3_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_name_4_abr:')) {
                $parts['color_name_4_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_name_5_abr:')) {
                $parts['color_name_5_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'color_des_abr:')) {
                $parts['color_des_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'logo_idea_abr:')) {
                $parts['logo_idea_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'typography_abr:')) {
                $parts['typography_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'imagery_photography_abr:')) {
                $parts['imagery_photography_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'brand_voice_abr:')) {
                $parts['brand_voice_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'brand_values_abr:')) {
                $parts['brand_values_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'brand_personality_traits_abr:')) {
                $parts['brand_personality_traits_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'customer_promise_abr:')) {
                $parts['customer_promise_abr'] = $this->cleanSection($section);
            }
        }
        return $parts;
    }

    private function cleanSection($section)
    {
        // Remove anything before a ":"
        $section = preg_replace('/^[^\n]*:\s*/', '', $section);
        
        return trim($section);
    }

}
?>