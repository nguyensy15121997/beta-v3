<?php

namespace App\Services;

use App\Models\Storytelling; // Make sure this matches the namespace of your SwotAnalysis model
use Illuminate\Support\Facades\DB;

class CustomerInterviewService
{
    public function generateAndSaveCustomerInterview($project)
    {
        $projectDescription = $project->description;
        // Use an appropriate API key management strategy
        $apiKey = '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'; // Make sure to secure this

        $data = [
            'model' => 'gpt-3.5-turbo-1106', // Specify the GPT-4 model
            'temperature' => 0.3,
            'messages' => [
                ['role' => 'system', 'content' => 'You are a helpful assistant. Please generate the elements I request based on my idea description.'],
                [
                'role' => 'user',
                'content' => "
                    Generate 10 interview questions I could ask customers to validate my idea and ensure that I’ve thought of it properly from all aspects, based on the project description below:
                    
                    Project Description: {$projectDescription}
                    
                    Please create 10 interview questions in depth should include:
                    - question_1
                    - question_2
                    - question_3
                    - question_4
                    - question_5
                    - question_6
                    - question_7
                    - question_8
                    - question_9
                    - question_10
                    
                    For the Arabic version, ensure to include:
                    - question_1_ar
                    - question_2_ar
                    - question_3_ar
                    - question_4_ar
                    - question_5_ar
                    - question_6_ar
                    - question_7_ar
                    - question_8_ar
                    - question_9_ar
                    - question_10_ar
                    
                    Please create 3 interview questions demographic should include:
                    - question_11
                    - question_12
                    - question_13
                    
                    For the Arabic version, ensure to include:
                    - question_11_ar
                    - question_12_ar
                    - question_13_ar
                    
                    Please create 3 interview questions wrap up should include:
                    - question_14
                    - question_15
                    - question_16
                    
                    For the Arabic version, ensure to include:
                    - question_14_ar
                    - question_15_ar
                    - question_16_ar
                    
                    Please make sure there is only one line break between each field.
                    
                    Example:
                    
                    question_1: What do you understand by the term 'Big Cat'?
                    question_2: Have you ever encountered any challenges related to big cats?
                    question_3: How interested are you in learning more about big cats?
                    question_4: What specific aspects of big cats do you find most intriguing?
                    question_5: Are you aware of any existing resources or platforms related to big cats?
                    question_6: How do you prefer to consume information about big cats (e.g., articles, videos, documentaries)?
                    question_7: Would you be interested in participating in activities or events related to big cats?
                    question_8: What potential benefits do you see in learning more about big cats?
                    question_9: Are there any concerns or reservations you have about engaging with content about big cats?
                    question_10: How likely are you to recommend information about big cats to others?
                    question_11: Are you aware of any existing resources or platforms related to big cats?
                    question_12: How do you prefer to consume information about big cats (e.g., articles, videos, documentaries)?
                    question_13: Would you be interested in participating in activities or events related to big cats?
                    question_14: What potential benefits do you see in learning more about big cats?
                    question_15: Are there any concerns or reservations you have about engaging with content about big cats?
                    question_16: How likely are you to recommend information about big cats to others?

                    question_1_ar: ما هو فهمك لمصطلح 'بيغ كات'؟
                    question_2_ar: هل واجهت يومًا تحديات متعلقة بالقطط الكبيرة؟
                    question_3_ar: ما مدى اهتمامك بمعرفة المزيد عن القطط الكبيرة؟
                    question_4_ar: ما الجوانب المحددة للقطط الكبيرة التي تجدها مثيرة للاهتمام؟
                    question_5_ar: هل تعلم بوجود موارد أو منصات متعلقة بالقطط الكبيرة؟
                    question_6_ar: كيف تفضل استهلاك المعلومات حول القطط الكبيرة (مثل: المقالات، الفيديوهات، الوثائقيات)؟
                    question_7_ar: هل تكون مهتمًا بالمشاركة في الأنشطة أو الفعاليات المتعلقة بالقطط الكبيرة؟
                    question_8_ar: ما الفوائد المحتملة التي تراها في معرفة المزيد عن القطط الكبيرة؟
                    question_9_ar: هل لديك أي مخاوف أو تحفظات تتعلق بالتفاعل مع محتوى عن القطط الكبيرة؟
                    question_10_ar: مدى احتمالية توصيتك بمعلومات عن القطط الكبيرة للآخرين؟
                    question_11_ar: هل تعلم بوجود موارد أو منصات متعلقة بالقطط الكبيرة؟
                    question_12_ar: كيف تفضل استهلاك المعلومات حول القطط الكبيرة (مثل: المقالات، الفيديوهات، الوثائقيات)؟
                    question_13_ar: هل تكون مهتمًا بالمشاركة في الأنشطة أو الفعاليات المتعلقة بالقطط الكبيرة؟
                    question_14_ar: ما الفوائد المحتملة التي تراها في معرفة المزيد عن القطط الكبيرة؟
                    question_15_ar: هل لديك أي مخاوف أو تحفظات تتعلق بالتفاعل مع محتوى عن القطط الكبيرة؟
                    question_16_ar: مدى احتمالية توصيتك بمعلومات عن القطط الكبيرة للآخرين؟
                    
                    Just break the line with /n, do not add any special characters.
                    Send me 10 customer interview.
                    "
            ]
            ],
            'max_tokens' => 4096
        ];

        for ($i = 0; $i < 3; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
        
            if (!$response) {
                // Handle error or retry
                continue;
            }
        
            $customerInterviewContent = $response['choices'][0]['message']['content'];
            
            $cleanedData = preg_replace('/\n+/', '|', $customerInterviewContent);
            $dataArray = explode('|', $cleanedData);
            $sections = array_map('trim', $dataArray);
            if (count($sections) < 30) {
                Log::info('continue ' . $i);
                continue;
            }
            
            $parts = $this->processCustomerInterviewContent($sections);
            
            if ($this->isAnyFieldNullObject($parts)) {
                Log::info('Skipped a user persona due to incomplete data');
                continue;
            }
            // Insert customer interview data into the database
            DB::table('customer_interview')->insert([
                'project_id' => $project->id,
                'created_at' => now(),
                'updated_at' => now()
            ] + $parts);
            break;
        }
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }


    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json',
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
            $responseArray = json_decode($response, true);


        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return $responseArray;
        }
    }

    private function processCustomerInterviewContent($content)
    {
        $parts = [
            'question_1' => '',
            'question_2' => '',
            'question_3' => '',
            'question_4' => '',
            'question_5' => '',
            'question_6' => '',
            'question_7' => '',
            'question_8' => '',
            'question_9' => '',
            'question_10' => '',
            'question_11' => '',
            'question_12' => '',
            'question_13' => '',
            'question_14' => '',
            'question_15' => '',
            'question_16' => '',
            'question_1_ar' => '',
            'question_2_ar' => '',
            'question_3_ar' => '',
            'question_4_ar' => '',
            'question_5_ar' => '',
            'question_6_ar' => '',
            'question_7_ar' => '',
            'question_8_ar' => '',
            'question_9_ar' => '',
            'question_10_ar' => '',
            'question_11_ar' => '',
            'question_12_ar' => '',
            'question_13_ar' => '',
            'question_14_ar' => '',
            'question_15_ar' => '',
            'question_16_ar' => '',
            // Add other sections if needed
        ];
    
        // Sort the keys by length in descending order
        $keys = array_keys($parts);
        usort($keys, function($a, $b) {
            return strlen($b) - strlen($a);
        });
    
        foreach ($content as $section) {
            foreach ($keys as $key) {
                if (stristr($section, "$key:")) {
                    $parts[$key] = $this->cleanSection($section);
                    break;
                }
            }
        }
        return $parts;

        // $sections = preg_split("/\n+/", $content);
        // $sections_ar = preg_split("/\n+/", $content_ar);
        // foreach ($sections as $section) {
        //     // Check which part of the Storytelling the section belongs to and add it to the respective key
        //     if (stristr($section, 'question_1:')) {
        //         $storytellingParts['question_1'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_2:')) {
        //         $storytellingParts['question_2'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_3:')) {
        //         $storytellingParts['question_3'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_4:')) {
        //         $storytellingParts['question_4'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_5:')) {
        //         $storytellingParts['question_5'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_6:')) {
        //         $storytellingParts['question_6'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_7:')) {
        //         $storytellingParts['question_7'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_8:')) {
        //         $storytellingParts['question_8'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_9:')) {
        //         $storytellingParts['question_9'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_10:')) {
        //         $storytellingParts['question_10'] = $this->cleanSection($section);
        //     }
        // }
        
        // foreach ($sections_ar as $section) {
        //     // Check which part of the Storytelling the section belongs to and add it to the respective key
        //     if (stristr($section, 'question_1_ar:')) {
        //         $storytellingParts['question_1_ar'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_2_ar:')) {
        //         $storytellingParts['question_2_ar'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_3_ar:')) {
        //         $storytellingParts['question_3_ar'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_4_ar:')) {
        //         $storytellingParts['question_4_ar'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_5_ar:')) {
        //         $storytellingParts['question_5_ar'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_6_ar:')) {
        //         $storytellingParts['question_6_ar'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_7_ar:')) {
        //         $storytellingParts['question_7_ar'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_8_ar:')) {
        //         $storytellingParts['question_8_ar'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_9_ar:')) {
        //         $storytellingParts['question_9_ar'] = $this->cleanSection($section);
        //     } elseif (stristr($section, 'question_10_ar:')) {
        //         $storytellingParts['question_10_ar'] = $this->cleanSection($section);
        //     }
        // }

        // return $storytellingParts;
    }

private function cleanSection($section)
{
    // Remove anything before a ":" and remove "*"
    $section = preg_replace('/^[^\n]*:\s*/', '', $section);
    // $section = preg_replace('/\*/', '', $section);
    
    return trim($section);
}




}
