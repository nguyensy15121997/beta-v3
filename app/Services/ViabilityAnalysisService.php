<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Already imported
use Illuminate\Support\Facades\Log;

class ViabilityAnalysisService
{
    public function generateAndSaveViabilityAnalysis($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this
    
        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
                [
                    'role' => 'system',
                    'content' => 'Generate viability analysis based on the project description.'
                ],
                [
                    'role' => 'user',
                    'content' => "
                        Generate viability analysis based on the project description below:
                        
                        Project Description: {$projectDescription}
                        
                        Please create viability analysis based on this. Viability analysis should include:
                        - percent
                        - viability_analysis_content
                        - source_1
                        - source_2

                        For the Arabic version, ensure to include:
                        - viability_analysis_content_abr
                        - source_1_abr
                        - source_2_abr
                        
                        Example:
                        
                        percent: 12
                        viability_analysis_content: The viability of Swah as a business is promising due to several key factors. Firstly, the growing tourism industry in Saudi Arabia presents a significant opportunity for innovative travel solutions like Swah. According to a report by the World Travel and Tourism Council, the tourism sector in Saudi Arabia is expected to contribute significantly to the country's GDP in the coming years.

                            Furthermore, Swah's unique selling points of personalized travel itineraries and virtual reality tour experiences set it apart from traditional travel agencies. This differentiation allows Swah to target a niche market of experiential travelers seeking authentic cultural experiences.
                            
                            Additionally, the use of AI technology in creating personalized itineraries enables Swah to scale its operations efficiently and cater to a wide range of customer preferences. By leveraging AI, Swah can continuously improve its services based on user feedback and data analytics.
                            
                            Moreover, the exclusive access to hidden gems in Saudi Arabia offered by Swah enhances the overall customer experience and creates a competitive advantage in the market. By tapping into the local culture and providing insider tips, Swah can attract travelers looking for unique and off-the-beaten-path experiences.
                            
                            In conclusion, the combination of a growing tourism industry in Saudi Arabia, unique selling points, AI technology, and exclusive access to hidden gems positions Swah as a viable and promising business in the travel industry.
                        source_1: World Travel and Tourism Council report on Saudi Arabia tourism industry.
                        source_2: Industry reports on the use of AI in the travel sector.
                        
                        viability_analysis_content_abr: تعتبر جدوى سواه كعمل تجاري واعدة بسبب عدة عوامل رئيسية. أولاً، تقدم صناعة السياحة المتنامية في المملكة العربية السعودية فرصة كبيرة لحلول السفر المبتكرة مثل سواه. ووفقاً لتقرير صادر عن المجلس العالمي للسفر والسياحة، من المتوقع أن يساهم قطاع السياحة في المملكة العربية السعودية بشكل كبير في الناتج المحلي الإجمالي للبلاد في السنوات المقبلة.

                            علاوة على ذلك، فإن نقاط البيع الفريدة التي تتمتع بها سواه فيما يتعلق بمسارات السفر الشخصية وتجارب جولات الواقع الافتراضي تميزها عن وكالات السفر التقليدية. يسمح هذا التمييز لشركة Swah باستهداف سوق متخصصة من المسافرين التجريبيين الذين يبحثون عن تجارب ثقافية أصيلة.
                            
                            بالإضافة إلى ذلك، فإن استخدام تقنية الذكاء الاصطناعي في إنشاء مسارات مخصصة يمكّن Swah من توسيع نطاق عملياتها بكفاءة وتلبية مجموعة واسعة من تفضيلات العملاء. ومن خلال الاستفادة من الذكاء الاصطناعي، تستطيع Swah تحسين خدماتها بشكل مستمر بناءً على تعليقات المستخدمين وتحليلات البيانات.
                            
                            علاوة على ذلك، فإن الوصول الحصري إلى الجواهر المخفية في المملكة العربية السعودية الذي توفره شركة Swah يعزز تجربة العملاء الشاملة ويخلق ميزة تنافسية في السوق. من خلال الاستفادة من الثقافة المحلية وتقديم النصائح الداخلية، يمكن لـ Swah جذب المسافرين الباحثين عن تجارب فريدة وبعيدة عن المألوف.
                            
                            في الختام، فإن الجمع بين صناعة السياحة المتنامية في المملكة العربية السعودية، ونقاط البيع الفريدة، وتكنولوجيا الذكاء الاصطناعي، والوصول الحصري إلى الجواهر الخفية، يضع سواه كشركة قابلة للحياة وواعدة في صناعة السفر.
                        source_1_abr: تقرير المجلس العالمي للسفر والسياحة عن صناعة السياحة في المملكة العربية السعودية.
                        source_2_abr: تقارير الصناعة حول استخدام الذكاء الاصطناعي في قطاع السفر.
                        
                        Just break the line with /n, do not add any special characters.
                        Please send me only all the fields that I have provided above.
                        "
                ]
            ],
            'max_tokens' => 4000
        ];
        for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
        
            if (!$response) {
                // Handle error or retry
                continue;
            }
        
            $content = $response['choices'][0]['message']['content'];
            
            $parts = $this->processContent($content);

            if ($this->isAnyFieldNullObject($parts)) {
                Log::info('Skipped a part due to incomplete data');
                continue;
            }

            DB::table('viability_analysis')->insert([
                'project_id' => $project->id,
                'created_at' => now(),
                'updated_at' => now()
            ] + $parts);
            break;
        }
        return true;
    }
    
    function extract_field_content($data, $field_name) {
        $pattern = "/" . preg_quote($field_name, '/') . ": (.*?)\n\n/s";
        preg_match($pattern, $data, $matches);
        if (isset($matches[1])) {
            return $matches[1];
        }
        return null;
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }

    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return json_decode($response, true);
        }
    }

    private function processContent($input_data)
    {
        $parts = [
            'percent' => '',
            'viability_analysis_content' => '',
            'source_1' => '',
            'source_2' => '',
            'viability_analysis_content_abr' => '',
            'source_1_abr' => '',
            'source_2_abr' => '',
        ];
        
        foreach ($parts as $key => $value) {
            $start_pos = strpos($input_data, "$key:");
            if ($start_pos !== false) {
                if ($key == 'percent') {
                    $end_pos = strpos($input_data, "viability_analysis_content:");
                    if ($end_pos !== false) {
                        $parts['percent'] = $this->cleanSection(substr($input_data, $start_pos, $end_pos - $start_pos));
                    }
                }
                
                if ($key == 'viability_analysis_content') {
                    $end_pos = strpos($input_data, "source_1:");
                    if ($end_pos !== false) {
                        $parts['viability_analysis_content'] = $this->cleanSection(substr($input_data, $start_pos, $end_pos - $start_pos));
                    }
                }
                
                if ($key == 'source_1') {
                    $end_pos = strpos($input_data, "source_2:");
                    if ($end_pos !== false) {
                        $parts['source_1'] = $this->cleanSection(substr($input_data, $start_pos, $end_pos - $start_pos));
                    }
                }
                
                if ($key == 'source_2') {
                    $end_pos = strpos($input_data, "viability_analysis_content_abr:");
                    if ($end_pos !== false) {
                        $parts['source_2'] = $this->cleanSection(substr($input_data, $start_pos, $end_pos - $start_pos));
                    }
                }
                
                if ($key == 'viability_analysis_content_abr') {
                    $end_pos = strpos($input_data, "source_1_abr:");
                    if ($end_pos !== false) {
                        $parts['viability_analysis_content_abr'] = $this->cleanSection(substr($input_data, $start_pos, $end_pos - $start_pos));
                    }
                }
                
                if ($key == 'source_1_abr') {
                    $end_pos = strpos($input_data, "source_2_abr:");
                    if ($end_pos !== false) {
                        $parts['source_1_abr'] = $this->cleanSection(substr($input_data, $start_pos, $end_pos - $start_pos));
                    }
                }
                
                if ($key == 'source_2_abr') {
                    $parts['source_2_abr'] = $this->cleanSection(substr($input_data, $start_pos));
                }
            }
        }
        
        return $parts;
    }


    private function cleanSection($section)
    {
        // Remove anything before a ":"
        $section = preg_replace('/^[^\n]*:\s*/', '', $section);
        
        return trim($section);
    }

}
?>