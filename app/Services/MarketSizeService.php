<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Already imported
use Illuminate\Support\Facades\Log;

class MarketSizeService
{
    public function generateAndSaveMarketSize($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this
    
        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
                [
                    'role' => 'system',
                    'content' => 'Generate market size and trend based on the project description.'
                ],
                [
                    'role' => 'user',
                    'content' => "
                        Generate market size and trend based on the project description below:
                        
                        Project Description: {$projectDescription}
                        
                        Please create market size and trend based on this. Market size and trend should include:
                        - total_addressable_market_title
                        - total_addressable_market_value
                        - total_servicable_market_title
                        - total_servicable_market_value
                        - total_obtainable_market_title
                        - total_obtainable_market_value
                        - description
                        - virtual_reality_in_tourism_description
                        - virtual_reality_in_tourism_key_point_1
                        - virtual_reality_in_tourism_key_point_2
                        - virtual_reality_in_tourism_key_point_3
                        - virtual_reality_in_tourism_how_to_leverage_1
                        - virtual_reality_in_tourism_how_to_leverage_2
                        - virtual_reality_in_tourism_how_to_leverage_3
                        - personalized_travel_experiences_description
                        - personalized_travel_experiences_key_point_1
                        - personalized_travel_experiences_key_point_2
                        - personalized_travel_experiences_key_point_3
                        - personalized_travel_experiences_how_to_leverage_1
                        - personalized_travel_experiences_how_to_leverage_2
                        - personalized_travel_experiences_how_to_leverage_3
                        - local_culture_immersion_description
                        - local_culture_immersion_key_point_1
                        - local_culture_immersion_key_point_2
                        - local_culture_immersion_key_point_3
                        - local_culture_immersion_how_to_leverage_1
                        - local_culture_immersion_how_to_leverage_2
                        - local_culture_immersion_how_to_leverage_3
                        - description_2
                        - industry_trends
                        - market_size
                        
                        Example:
                        
                        total_addressable_market_title: Total market for travel services in Saudi Arabia
                        total_addressable_market_value: 10
                        total_servicable_market_title: Available market among tourists interested in cultural experiences in Saudi Arabia
                        total_servicable_market_value: 5
                        total_obtainable_market_title: Obtainable market among tourists who used travel planning apps in the past year in Saudi Arabia
                        total_obtainable_market_value: 2
                        description: As Swah aims to revolutionize the tourism industry in Saudi Arabia with its AI-powered app, it is important to consider the current market trends that can impact the business strategies and growth potential.
                        virtual_reality_in_tourism_description: The integration of virtual reality technology in the tourism sector has been steadily growing, offering immersive experiences to travelers worldwide.
                        virtual_reality_in_tourism_key_point_1: Virtual reality enhances travel experiences
                        virtual_reality_in_tourism_key_point_2: Provides interactive and engaging storytelling
                        virtual_reality_in_tourism_key_point_3: Appeals to tech-savvy travelers
                        virtual_reality_in_tourism_how_to_leverage_1: Offer virtual reality tours of Saudi landmarks
                        virtual_reality_in_tourism_how_to_leverage_2: Create interactive experiences showcasing local culture
                        virtual_reality_in_tourism_how_to_leverage_3: Partner with VR technology providers for innovative solutions
                        personalized_travel_experiences_description: Travelers are increasingly seeking personalized and unique travel experiences that cater to their preferences and interests, moving away from traditional tour packages.
                        personalized_travel_experiences_key_point_1: Customized itineraries based on traveler preferences
                        personalized_travel_experiences_key_point_2: Tailored recommendations for attractions and activities
                        personalized_travel_experiences_key_point_3: Focus on experiential travel rather than mass tourism
                        personalized_travel_experiences_how_to_leverage_1: Offer guided tours led by local experts
                        personalized_travel_experiences_how_to_leverage_2: Organize cultural workshops and activities
                        personalized_travel_experiences_how_to_leverage_3: Partner with local artisans and businesses for exclusive access
                        local_culture_immersion_description: There is a growing demand for authentic and immersive travel experiences that allow tourists to connect with the local culture, traditions, and communities.
                        local_culture_immersion_key_point_1: Emphasis on cultural exchange and learning
                        local_culture_immersion_key_point_2: Off-the-beaten-path experiences
                        local_culture_immersion_key_point_3: Supporting local communities and businesses
                        local_culture_immersion_how_to_leverage_1: Offer guided tours led by local experts
                        local_culture_immersion_how_to_leverage_2: Organize cultural workshops and activities
                        local_culture_immersion_how_to_leverage_3: Partner with local artisans and businesses for exclusive access
                        description_2: The tourism industry in Saudi Arabia has been experiencing significant growth in recent years, with the government investing heavily in infrastructure and initiatives to promote the country as a tourist destination. According to data from the Saudi Commission for Tourism and National Heritage, the number of international tourists visiting the kingdom has been steadily increasing. In 2019, Saudi Arabia saw a record number of international visitors, with the country welcoming over 17 million tourists. Additionally, the domestic tourism market is also on the rise, with more Saudis exploring their own country.
                        industry_trends: One of the key trends in the tourism industry is the increasing demand for personalized and unique travel experiences. Travelers are seeking out more authentic and immersive experiences, looking to engage with local culture and discover hidden gems off the beaten path. This trend is driven by a desire for memorable experiences and a deeper connection with the destinations they visit. Virtual reality tours have also emerged as a popular trend, offering travelers a new way to explore and experience destinations before they even arrive.
                        market_size: The market size for personalized travel services in Saudi Arabia is growing rapidly, with more tourists looking for bespoke itineraries that cater to their specific interests and preferences. As more travelers seek out unique and authentic experiences, there is a growing demand for innovative travel solutions like Swah. With our AI-powered app offering personalized travel itineraries and exclusive access to hidden gems, we are poised to capitalize on this growing market and provide travelers with unforgettable experiences in Saudi Arabia.
                        
                        For market value values, please send me units in abbreviated form (M, B, ..etc).
                        There are a total of 31 fields, don't miss any field, this is very important. Please generate 31 fields, I only receive data with 31 fields above, do not send anything else.
                        "
                ]
            ],
            'max_tokens' => 4000
        ];
        for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
            
            if (!$response) {
                // Handle error or retry
                continue;
            }
        
            $content = $response['choices'][0]['message']['content'];
            
            $data_abr = [
                'model' => 'gpt-3.5-turbo-1106',
                'temperature' => 0.3,
                'messages' => [
                    [
                        'role' => 'system',
                        'content' => 'Generate market size and trend based on the project description.'
                    ],
                    [
                        'role' => 'user',
                        'content' => "
                            Please help me translate the content of this data into Arabic:
                            
                            {$content}
                            
                            The result will be similar to example.
                            
                            Example:
    
                            total_addressable_market_title_abr: السوق الإجمالي لخدمات السفر في المملكة العربية السعودية
                            total_addressable_market_value_abr: 10
                            total_servicable_market_title_abr: السوق المتاح بين السياح الذين يهتمون بالتجارب الثقافية في المملكة العربية السعودية
                            total_servicable_market_value_abr: 5
                            total_obtainable_market_title_abr: السوق المتاح بين السياح الذين استخدموا تطبيقات تخطيط السفر في السنة الماضية في المملكة العربية السعودية
                            total_obtainable_market_value_abr: 2
                            description_abr: بما أن Swah تهدف إلى ثورة صناعة السياحة في المملكة العربية السعودية باستخدام تطبيقها المدعوم بالذكاء الاصطناعي، فمن المهم أن نأخذ في الاعتبار الاتجاهات السوقية الحالية التي يمكن أن تؤثر على استراتيجيات الأعمال والإمكانات النمو.
                            virtual_reality_in_tourism_description_abr: نمت تكنولوجيا الواقع الافتراضي في قطاع السياحة بشكل مطرد، مما يوفر تجارب غامرة للمسافرين في جميع أنحاء العالم.
                            virtual_reality_in_tourism_key_point_1_abr: يعزز الواقع الافتراضي تجارب السفر
                            virtual_reality_in_tourism_key_point_2_abr: يوفر قصصًا تفاعلية وجذابة
                            virtual_reality_in_tourism_key_point_3_abr: يستهوي المسافرين المتعصبين للتكنولوجيا
                            virtual_reality_in_tourism_how_to_leverage_1_abr: قدم جولات واقع افتراضي للمعالم السعودية
                            virtual_reality_in_tourism_how_to_leverage_2_abr: أنشئ تجارب تفاعلية تعرض الثقافة المحلية
                            virtual_reality_in_tourism_how_to_leverage_3_abr: شارك مع مزودي تكنولوجيا الواقع الافتراضي لحلول مبتكرة
                            personalized_travel_experiences_description_abr: يبحث المسافرون بشكل متزايد عن تجارب سفر شخصية وفريدة تلبي تفضيلاتهم واهتماماتهم، متحولين بعيدًا عن الحزم السياحية التقليدية.
                            personalized_travel_experiences_key_point_1_abr: جداول زمنية مخصصة استنادًا إلى تفضيلات المسافر
                            personalized_travel_experiences_key_point_2_abr: توصيات مخصصة للمعالم السياحية والأنشطة
                            personalized_travel_experiences_key_point_3_abr: التركيز على السفر التجريبي بدلاً من السياحة الجماعية
                            personalized_travel_experiences_how_to_leverage_1_abr: قدم جولات موجهة من قبل خبراء محليين
                            personalized_travel_experiences_how_to_leverage_2_abr: نظم ورش عمل وأنشطة ثقافية
                            personalized_travel_experiences_how_to_leverage_3_abr: شارك مع الحرفيين المحليين والشركات للوصول الحصري
                            local_culture_immersion_description_abr: هناك طلب متزايد على تجارب السفر الأصيلة والغامرة التي تسمح للسياح بالتواصل مع الثقافة المحلية والتقاليد والمجتمعات.
                            local_culture_immersion_key_point_1_abr: التركيز على التبادل الثقافي والتعلم
                            local_culture_immersion_key_point_2_abr: تجارب بعيدة عن المسارات المعروفة
                            local_culture_immersion_key_point_3_abr: دعم المجتمعات والأعمال المحلية
                            local_culture_immersion_how_to_leverage_1_abr: قدم جولات موجهة من قبل خبراء محليين
                            local_culture_immersion_how_to_leverage_2_abr: نظم ورش عمل وأنشطة ثقافية
                            local_culture_immersion_how_to_leverage_3_abr: شارك مع الحرفيين المحليين والشركات للوصول الحصري
                            description_2_abr: شهدت صناعة السياحة في المملكة العربية السعودية نموًا كبيرًا في السنوات الأخيرة، حيث قامت الحكومة بالاستثمار بشكل كبير في البنية التحتية والمبادرات للترويج للبلاد كوجهة سياحية. وفقًا للبيانات من الهيئة السعودية للسياحة والتراث الوطني، فإن عدد السياح الدوليين الزائرين للمملكة قد ازداد بشكل مطرد. في عام 2019، شهدت المملكة العربية السعودية عددًا قياسيًا من الزوار الدوليين، حيث استقبلت البلاد أكثر من 17 مليون سائح. بالإضافة إلى ذلك، فإن السوق السياحية المحلية أيضًا في تصاعد، مع المزيد من السعوديين يستكشفون بلدهم.
                            industry_trends_abr: واحدة من الاتجاهات الرئيسية في صناعة السياحة هي الطلب المتزايد على تجارب السفر الشخصية والفريدة. يبحث المسافرون عن تجارب أكثر أصالة وغمرًا، يسعون إلى التفاعل مع الثقافة المحلية واكتشاف الجواهر الخفية بعيدًا عن المسارات المعروفة. يدفع هذا الاتجاه برغبة في تجارب لا تُنسى واتصال أعمق مع الوجهات التي يزورونها. ظهرت جولات الواقع الافتراضي أيضًا كاتجاه شائع، مما يقدم للمسافرين طريقة جديدة لاستكشاف وتجربة الوجهات قبل وصولها حتى.
                            market_size_abr: حجم السوق لخدمات السفر الشخصية في المملكة العربية السعودية ينمو بسرعة، مع المزيد من السياح يبحثون عن جداول زمنية مخصصة تلبي اهتماماتهم وتفضيلاتهم الخاصة. مع تزايد عدد المسافرين الذين يبحثون عن تجارب فريدة وأصيلة، هناك طلب متزايد على حلول السفر المبتكرة مثل Swah. بتطبيقنا المدعوم بالذكاء الاصطناعي الذي يقدم جداول زمنية للسفر شخصية ووصولًا حصريًا إلى الجواهر الخفية، نحن على استعداد للاستفادة من هذا السوق المتنامي وتوفير تجارب لا تُنسى للمسافرين في المملكة العربية السعودية.
                            
                            For market value values, please send me units in abbreviated form (M, B, ..etc).
                            There are a total of 31 fields, don't miss any field, this is very important. Please generate 31 fields, I only receive data with 31 fields above, do not send anything else.
                            "
                    ]
                ],
                'max_tokens' => 4000
            ];
            $response_abr = $this->sendRequestToOpenAI($data_abr, $apiKey);
            
            if (!$response_abr) {
                // Handle error or retry
                continue;
            }
            
            $content_abr = $response_abr['choices'][0]['message']['content'];
            
            $cleanedData_abr = preg_replace('/\n+/', '|', $content_abr);
            $dataArray_abr = explode('|', $cleanedData_abr);
            
            $cleanedData = preg_replace('/\n+/', '|', $content);
            $dataArray = explode('|', $cleanedData);
            $sections = array_map('trim', $dataArray);
            $sections_abr = array_map('trim', $dataArray_abr);
            $merged_sections = array_merge(
                array_map('trim', $sections),
                array_map('trim', $sections_abr)
            );
            if (count($merged_sections) < 60) {
                Log::info('continue ' . $i);
                sleep(5);
                continue;
            }
            
            $parts = $this->processContent($merged_sections);
            if ($this->isAnyFieldNullObject($parts)) {
                Log::info('Skipped a part due to incomplete data');
                continue;
            }

            DB::table('markets_size')->insert([
                'project_id' => $project->id,
                'created_at' => now(),
                'updated_at' => now()
            ] + $parts);
            break;
        }
        return true;
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }

    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return json_decode($response, true);
        }
    }

    private function processContent($content)
    {
        $parts = [
            'total_addressable_market_title' => '',
            'total_addressable_market_value' => '',
            'total_servicable_market_title' => '',
            'total_servicable_market_value' => '',
            'total_obtainable_market_title' => '',
            'total_obtainable_market_value' => '',
            'description' => '',
            'virtual_reality_in_tourism_description' => '',
            'virtual_reality_in_tourism_key_point_1' => '',
            'virtual_reality_in_tourism_key_point_2' => '',
            'virtual_reality_in_tourism_key_point_3' => '',
            'virtual_reality_in_tourism_how_to_leverage_1' => '',
            'virtual_reality_in_tourism_how_to_leverage_2' => '',
            'virtual_reality_in_tourism_how_to_leverage_3' => '',
            'personalized_travel_experiences_description' => '',
            'personalized_travel_experiences_key_point_1' => '',
            'personalized_travel_experiences_key_point_2' => '',
            'personalized_travel_experiences_key_point_3' => '',
            'personalized_travel_experiences_how_to_leverage_1' => '',
            'personalized_travel_experiences_how_to_leverage_2' => '',
            'personalized_travel_experiences_how_to_leverage_3' => '',
            'local_culture_immersion_description' => '',
            'local_culture_immersion_key_point_1' => '',
            'local_culture_immersion_key_point_2' => '',
            'local_culture_immersion_key_point_3' => '',
            'local_culture_immersion_how_to_leverage_1' => '',
            'local_culture_immersion_how_to_leverage_2' => '',
            'local_culture_immersion_how_to_leverage_3' => '',
            'description_2' => '',
            'industry_trends' => '',
            'market_size' => '',
            'total_addressable_market_title_abr' => '',
            'total_addressable_market_value_abr' => '',
            'total_servicable_market_title_abr' => '',
            'total_servicable_market_value_abr' => '',
            'total_obtainable_market_title_abr' => '',
            'total_obtainable_market_value_abr' => '',
            'description_abr' => '',
            'virtual_reality_in_tourism_description_abr' => '',
            'virtual_reality_in_tourism_key_point_1_abr' => '',
            'virtual_reality_in_tourism_key_point_2_abr' => '',
            'virtual_reality_in_tourism_key_point_3_abr' => '',
            'virtual_reality_in_tourism_how_to_leverage_1_abr' => '',
            'virtual_reality_in_tourism_how_to_leverage_2_abr' => '',
            'virtual_reality_in_tourism_how_to_leverage_3_abr' => '',
            'personalized_travel_experiences_description_abr' => '',
            'personalized_travel_experiences_key_point_1_abr' => '',
            'personalized_travel_experiences_key_point_2_abr' => '',
            'personalized_travel_experiences_key_point_3_abr' => '',
            'personalized_travel_experiences_how_to_leverage_1_abr' => '',
            'personalized_travel_experiences_how_to_leverage_2_abr' => '',
            'personalized_travel_experiences_how_to_leverage_3_abr' => '',
            'local_culture_immersion_description_abr' => '',
            'local_culture_immersion_key_point_1_abr' => '',
            'local_culture_immersion_key_point_2_abr' => '',
            'local_culture_immersion_key_point_3_abr' => '',
            'local_culture_immersion_how_to_leverage_1_abr' => '',
            'local_culture_immersion_how_to_leverage_2_abr' => '',
            'local_culture_immersion_how_to_leverage_3_abr' => '',
            'description_2_abr' => '',
            'industry_trends_abr' => '',
            'market_size_abr' => '',
            // Add other sections if needed
        ];
    
        // Sort the keys by length in descending order
        $keys = array_keys($parts);
        usort($keys, function($a, $b) {
            return strlen($b) - strlen($a);
        });
    
        foreach ($content as $section) {
            foreach ($keys as $key) {
                if (stristr($section, "$key:")) {
                    $parts[$key] = $this->cleanSection($section);
                    break;
                }
            }
        }
        return $parts;
    }



    private function cleanSection($section)
    {
        // Remove anything before a ":"
        $section = preg_replace('/^[^\n]*:\s*/', '', $section);
        
        return trim($section);
    }

}
?>