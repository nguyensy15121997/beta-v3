<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Already imported
use Illuminate\Support\Facades\Log;

class MarketingOverviewService
{
    public function generateAndSaveMarketingOverview($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this
    
        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
                [
                    'role' => 'system',
                    'content' => 'Generate marketing overview based on the project description.'
                ],
                [
                    'role' => 'user',
                    'content' => "
                        Generate marketing overview based on the project description below:
                        
                        Project Description: {$projectDescription}
                        
                        Please create marketing overview based on this. Marketing overview should include:
                        - awareness_objective
                        - awareness_tatics
                        - interest_objective
                        - interest_tatics
                        - consideration_objective
                        - consideration_tatics
                        - conversion_objective
                        - conversion_tatics
                        - retention_objective
                        - retention_tatics
                        - marketing_strategy_overview
                        - approach
                        - goals
                        - primary_channels
                        - budget

                        For the Arabic version, ensure to include:
                        - awareness_objective_abr
                        - awareness_tatics_abr
                        - interest_objective_abr
                        - interest_tatics_abr
                        - consideration_objective_abr
                        - consideration_tatics_abr
                        - conversion_objective_abr
                        - conversion_tatics_abr
                        - retention_objective_abr
                        - retention_tatics_abr
                        - marketing_strategy_overview_abr
                        - approach_abr
                        - goals_abr
                        - primary_channels_abr
                        - budget_abr
                        
                        Example:
                        
                        awareness_objective: Introduce Swah to the target audience and create brand awareness
                        awareness_tatics: Social media ads, Influencer collaborations, Content marketing, SEO, Public relations
                        interest_objective: Generate interest and curiosity about Swah's unique features and benefits
                        interest_tatics: Interactive website design, Email marketing campaigns, Virtual reality teasers, Blogging, Webinars
                        consideration_objective: Encourage potential customers to consider Swah as their travel planning solution
                        consideration_tatics: User reviews and testimonials, Comparison guides, Free trial offer, Targeted online ads, Social proof from influencers
                        conversion_objective: Convert interested leads into paying customers for Swah's premium services
                        conversion_tatics: Limited-time offers, Personalized recommendations, Booking discounts, Loyalty programs, Customer support engagement
                        retention_objective: Retain customers by providing exceptional service and maintaining engagement
                        retention_tatics: Customer feedback surveys, Exclusive access perks, Membership rewards, Regular updates and notifications, Personalized travel reminders
                        marketing_strategy_overview: Our marketing strategy for Swah focuses on leveraging digital channels to reach our target audience, create brand awareness, and drive user adoption. By utilizing a mix of social media, influencer partnerships, and targeted advertising, we aim to establish Swah as the go-to app for travelers looking to explore Saudi Arabia in a unique and personalized way.
                        approach: Our approach to marketing will revolve around creating engaging and shareable content that showcases the exclusive features and benefits of using Swah. By highlighting the personalized itineraries, virtual reality tours, and cultural experiences offered by the app, we aim to capture the attention of travel enthusiasts and adventurers seeking new and exciting experiences.
                        goals: **Our primary marketing goal is to increase app downloads and user engagement within the first six months of launching Swah.** We aim to achieve a high retention rate among users by delivering value through curated travel experiences and exceptional customer service.
                        primary_channels: We will primarily use social media platforms such as Instagram, Facebook, and Twitter to promote Swah and engage with our target audience. Additionally, we will explore partnerships with travel influencers and bloggers to reach a wider audience and generate buzz around the app. Paid advertising through Google AdWords and social media ads will also be part of our marketing mix to drive app downloads and user acquisition.
                        budget: Our marketing budget for the first year will be allocated towards social media advertising, influencer partnerships, content creation, and public relations efforts. We plan to closely monitor and analyze the performance of each channel to optimize our marketing spend and maximize ROI.
                        
                        awareness_objective_abr: قدم Swah للجمهور المستهدف وأنشئ الوعي بالعلامة التجارية
                        awareness_tatics_abr: إعلانات وسائل التواصل الاجتماعي، تعاون مع المؤثرين، تسويق المحتوى، تحسين محركات البحث، العلاقات العامة
                        interest_objective_abr: توليد الاهتمام والفضول حول الميزات والفوائد الفريدة لـ Swah
                        interest_tatics_abr: تصميم موقع إلكتروني تفاعلي، حملات تسويق بالبريد الإلكتروني، تشويقات الواقع الافتراضي، التدوين، الندوات عبر الإنترنت
                        consideration_objective_abr: تشجيع العملاء المحتملين على النظر في Swah كحل لتخطيط السفر الخاص بهم
                        consideration_tatics_abr: تقييمات وشهادات المستخدمين، الدلائل المقارنة، عرض الفترة التجريبية المجانية، الإعلانات المستهدفة عبر الإنترنت، دليل اجتماعي من المؤثرين
                        conversion_objective_abr: تحويل العملاء المهتمين إلى عملاء مدفوعين لخدمات Swah الرئيسية
                        conversion_tatics_abr: عروض لفترة محدودة، توصيات مخصصة، تخفيضات على الحجوزات، برامج الولاء، مشاركة دعم العملاء
                        retention_objective_abr: الاحتفاظ بالعملاء من خلال تقديم خدمة استثنائية والمحافظة على التفاعل
                        retention_tatics_abr: استطلاعات رأي العملاء، مزايا الوصول الحصرية، مكافآت العضوية، التحديثات الدورية والإشعارات، تذكيرات السفر المخصصة
                        marketing_strategy_overview_abr: تركز استراتيجيتنا التسويقية لـ Swah على استغلال القنوات الرقمية للوصول إلى جمهورنا المستهدف وخلق الوعي بالعلامة التجارية وتعزيز اعتماد المستخدم. من خلال استخدام مزيج من وسائل التواصل الاجتماعي وشراكات المؤثرين والإعلانات المستهدفة، نهدف إلى تأسيس Swah كتطبيق يلجأ إليه المسافرون الذين يبحثون عن استكشاف المملكة العربية السعودية بطريقة فريدة وشخصية.
                        approach_abr: ستتمحور مقاربتنا للتسويق حول إنشاء محتوى جذاب وقابل للمشاركة يعرض الميزات الحصرية والفوائد لاستخدام Swah. من خلال إبراز الجداول الزمنية الشخصية وجولات الواقع الافتراضي والتجارب الثقافية التي يقدمها التطبيق، نهدف إلى لفت انتباه عشاق السفر والمغامرين الذين يبحثون عن تجارب جديدة ومثيرة.
                        goals_abr: الهدف التسويقي الرئيسي لدينا هو زيادة عدد تنزيلات التطبيق والتفاعل مع المستخدمين خلال الستة أشهر الأولى من إطلاق Swah. نهدف إلى تحقيق معدل احتفاظ عالي بين المستخدمين من خلال تقديم قيمة من خلال تجارب السفر المخصصة وخدمة العملاء الاستثنائية.
                        primary_channels_abr: سنستخدم في الأساس منصات وسائل التواصل الاجتماعي مثل إنستجرام وفيسبوك وتويتر للترويج لـ Swah والتفاعل مع جمهورنا المستهدف. بالإضافة إلى ذلك، سنستكشف الشراكات مع المؤثرين في السفر والمدونين للوصول إلى جمهور أوسع وت.
                        budget: سيتم تخصيص ميزانيتنا التسويقية للسنة الأولى للإعلان عبر وسائل التواصل الاجتماعي وشراكات المؤثرين وإنشاء المحتوى وجهود العلاقات العامة. نخطط لمراقبة وتحليل أداء كل قناة عن كثب لتحسين إنفاقنا التسويقي وتحقيق أقصى قدر من عائد الاستثمار.
                        
                        Just break the line with /n, do not add any special characters.
                        Remeber arabic part.
                        There are a total of 30 fields, don't miss any field, this is very important. Please generate 30 fields, I only receive data with 30 fields above, do not send anything else.
                        "
                ]
            ],
            'max_tokens' => 4000
        ];
        for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
        
            if (!$response) {
                // Handle error or retry
                continue;
            }
        
            $content = $response['choices'][0]['message']['content'];
            
            $cleanedData = preg_replace('/\n+/', '|', $content);
            $dataArray = explode('|', $cleanedData);
            $sections = array_map('trim', $dataArray);
            // dd($sections);
            if (count($sections) < 30) {
                Log::info('continue ' . $i);
                sleep(5);
                continue;
            }
            
            $parts = $this->processContent($sections);

            if ($this->isAnyFieldNullObject($parts)) {
                Log::info('Skipped a part due to incomplete data');
                continue;
            }

            DB::table('marketing_overview')->insert([
                'project_id' => $project->id,
                'created_at' => now(),
                'updated_at' => now()
            ] + $parts);
            break;
        }
        return true;
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }

    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return json_decode($response, true);
        }
    }

    private function processContent($content)
    {
        $parts = [
            'awareness_objective' => '',
            'awareness_tatics' => '',
            'interest_objective' => '',
            'interest_tatics' => '',
            'consideration_objective' => '',
            'consideration_tatics' => '',
            'conversion_objective' => '',
            'conversion_tatics' => '',
            'retention_objective' => '',
            'retention_tatics' => '',
            'marketing_strategy_overview' => '',
            'approach' => '',
            'goals' => '',
            'primary_channels' => '',
            'budget' => '',
            'awareness_objective_abr' => '',
            'awareness_tatics_abr' => '',
            'interest_objective_abr' => '',
            'interest_tatics_abr' => '',
            'consideration_objective_abr' => '',
            'consideration_tatics_abr' => '',
            'conversion_objective_abr' => '',
            'conversion_tatics_abr' => '',
            'retention_objective_abr' => '',
            'retention_tatics_abr' => '',
            'marketing_strategy_overview_abr' => '',
            'approach_abr' => '',
            'goals_abr' => '',
            'primary_channels_abr' => '',
            'budget_abr' => '',
            // Add other sections if needed
        ];
    
        foreach ($content as $section) {
            if (stristr($section, 'awareness_objective:')) {
                $parts['awareness_objective'] = $this->cleanSection($section);
            } elseif (stristr($section, 'awareness_tatics:')) {
                $parts['awareness_tatics'] = $this->cleanSection($section);
            } elseif (stristr($section, 'interest_objective:')) {
                $parts['interest_objective'] = $this->cleanSection($section);
            } elseif (stristr($section, 'interest_tatics:')) {
                $parts['interest_tatics'] = $this->cleanSection($section);
            } elseif (stristr($section, 'consideration_objective:')) {
                $parts['consideration_objective'] = $this->cleanSection($section);
            } elseif (stristr($section, 'consideration_tatics:')) {
                $parts['consideration_tatics'] = $this->cleanSection($section);
            } elseif (stristr($section, 'conversion_objective:')) {
                $parts['conversion_objective'] = $this->cleanSection($section);
            } elseif (stristr($section, 'conversion_tatics:')) {
                $parts['conversion_tatics'] = $this->cleanSection($section);
            } elseif (stristr($section, 'retention_objective:')) {
                $parts['retention_objective'] = $this->cleanSection($section);
            } elseif (stristr($section, 'retention_tatics:')) {
                $parts['retention_tatics'] = $this->cleanSection($section);
            } elseif (stristr($section, 'marketing_strategy_overview:')) {
                $parts['marketing_strategy_overview'] = $this->cleanSection($section);
            } elseif (stristr($section, 'approach:')) {
                $parts['approach'] = $this->cleanSection($section);
            } elseif (stristr($section, 'goals:')) {
                $parts['goals'] = $this->cleanSection($section);
            } elseif (stristr($section, 'primary_channels:')) {
                $parts['primary_channels'] = $this->cleanSection($section);
            } elseif (stristr($section, 'budget:')) {
                $parts['budget'] = $this->cleanSection($section);
            } elseif (stristr($section, 'awareness_objective_abr:')) {
                $parts['awareness_objective_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'awareness_tatics_abr:')) {
                $parts['awareness_tatics_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'interest_objective_abr:')) {
                $parts['interest_objective_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'interest_tatics_abr:')) {
                $parts['interest_tatics_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'consideration_objective_abr:')) {
                $parts['consideration_objective_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'consideration_tatics_abr:')) {
                $parts['consideration_tatics_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'conversion_objective_abr:')) {
                $parts['conversion_objective_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'conversion_tatics_abr:')) {
                $parts['conversion_tatics_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'retention_objective_abr:')) {
                $parts['retention_objective_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'retention_tatics_abr:')) {
                $parts['retention_tatics_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'marketing_strategy_overview_abr:')) {
                $parts['marketing_strategy_overview_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'approach_abr:')) {
                $parts['approach_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'goals_abr:')) {
                $parts['goals_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'primary_channels_abr:')) {
                $parts['primary_channels_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'budget_abr:')) {
                $parts['budget_abr'] = $this->cleanSection($section);
            }
        }
        return $parts;
    }


    private function cleanSection($section)
    {
        // Remove anything before a ":"
        $section = preg_replace('/^[^\n]*:\s*/', '', $section);
        
        return trim($section);
    }

}
?>