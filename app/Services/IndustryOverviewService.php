<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Already imported
use Illuminate\Support\Facades\Log;

class IndustryOverviewService
{
    public function generateAndSaveIndustryOverview($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this
    
        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
                [
                    'role' => 'system',
                    'content' => 'Generate industry overview based on the project description.'
                ],
                [
                    'role' => 'user',
                    'content' => "
                        Generate industry overview based on the project description below:
                        
                        Project Description: {$projectDescription}
                        
                        Please create industry overview based on this. Industry overview should include:
                        - highlight_name_1
                        - highlight_name_2
                        - highlight_name_3
                        - unit_relate_highlight_1
                        - unit_relate_highlight_2
                        - unit_relate_highlight_3
                        - description
                        - market_trends_title_1
                        - market_trends_title_2
                        - market_trends_title_3
                        - market_trends_title_4
                        - market_trends_description_1
                        - market_trends_description_2
                        - market_trends_description_3
                        - market_trends_description_4
                        - competitive_landscape_title_1
                        - competitive_landscape_title_2
                        - competitive_landscape_title_3
                        - competitive_landscape_description_1
                        - competitive_landscape_description_2
                        - competitive_landscape_description_3
                        - challenges_title_1
                        - challenges_title_2
                        - challenges_title_3
                        - challenges_description_1
                        - challenges_description_2
                        - challenges_description_3
                        - epilogue

                        For the Arabic version, ensure to include:
                        - highlight_name_1_abr
                        - highlight_name_2_abr
                        - highlight_name_3_abr
                        - unit_relate_highlight_1_abr
                        - unit_relate_highlight_2_abr
                        - unit_relate_highlight_3_abr
                        - description_abr
                        - market_trends_title_1_abr
                        - market_trends_title_2_abr
                        - market_trends_title_3_abr
                        - market_trends_title_4_abr
                        - market_trends_description_1_abr
                        - market_trends_description_2_abr
                        - market_trends_description_3_abr
                        - market_trends_description_4_abr
                        - competitive_landscape_title_1_abr
                        - competitive_landscape_title_2_abr
                        - competitive_landscape_title_3_abr
                        - competitive_landscape_description_1_abr
                        - competitive_landscape_description_2_abr
                        - competitive_landscape_description_3_abr
                        - challenges_title_1_abr
                        - challenges_title_2_abr
                        - challenges_title_3_abr
                        - challenges_description_1_abr
                        - challenges_description_2_abr
                        - challenges_description_3_abr
                        - epilogue_abr
                        
                        Example:
                        
                        highlight_name_1: Annual Tourism Revenue in Saudi Arabia
                        highlight_name_2: Percentage of International Tourists in Saudi Arabia
                        highlight_name_3: Average Daily Tourist Spending
                        unit_relate_highlight_1: $1.2B
                        unit_relate_highlight_2: 20%
                        unit_relate_highlight_3: $200
                        description: The tourism industry in Saudi Arabia has been rapidly growing in recent years, with the government focusing on promoting the country as a top tourist destination. The Vision 2030 initiative has brought about significant changes.
                        market_trends_title_1: Increase in Tourism
                        market_trends_title_2: Digital Transformation
                        market_trends_title_3: Personalization
                        market_trends_title_4: Virtual Reality
                        market_trends_description_1: The number of international tourists visiting Saudi Arabia has been steadily increasing, driven by the country's historical and cultural attractions.
                        market_trends_description_2: The travel industry is undergoing a digital transformation, with travelers increasingly relying on technology.
                        market_trends_description_3: There is a growing trend towards personalized travel experiences, with tourists seeking unique.
                        market_trends_description_4: Virtual reality technology is becoming increasingly popular in the travel industry, offering immersive experiences.
                        competitive_landscape_title_1: Traditional Travel Agencies
                        competitive_landscape_title_2: Tech Startups
                        competitive_landscape_title_3: Local Competition
                        competitive_landscape_description_1: While traditional travel agencies still play a significant role in the industry, there is a shift towards online travel platforms.
                        competitive_landscape_description_2: Tech startups are disrupting the travel industry with innovative solutions and cutting-edge technology, catering to the changing demands of modern travelers.
                        competitive_landscape_description_3: There are local competitors in Saudi Arabia offering similar travel planning services, but Swah differentiates itself by focusing on personalized itineraries and exclusive access to hidden gems.
                        challenges_title_1: Regulations
                        challenges_title_2: Cultural Sensitivity
                        challenges_title_3: Competition
                        challenges_description_1: The tourism industry in Saudi Arabia is still evolving, with regulations and policies that may impact the operations of travel companies.
                        challenges_description_2: As a destination known for its rich cultural heritage, it is important for travel companies to be culturally sensitive and respectful.
                        challenges_description_3: With the growing number of players in the industry, standing out and gaining market share can be a challenge for new entrants like Swah.
                        epilogue: In this dynamic and evolving industry, Swah aims to leverage technology and innovation to provide a unique and unparalleled travel.
                        
                        highlight_name_1_abr: الإيرادات السياحية السنوية في المملكة العربية السعودية
                        highlight_name_2_abr: نسبة السياح الدوليين في المملكة العربية السعودية
                        highlight_name_3_abr: متوسط الإنفاق اليومي للسائح
                        unit_relate_highlight_1_abr: 1.2 مليار دولار
                        unit_relate_highlight_2_abr: 20٪
                        unit_relate_highlight_3_abr: 200 دولار
                        description_abr: تشهد صناعة السياحة في الركدة. جلبت مبادرة رؤية 2030 تغييرات واستثمارات هامما جعل المملكة العربية السعودية لاعبًا رئيسيًا في السوق السياحية العالمية.
                        market_trends_title_1_abr: زيادة في السياحة
                        market_trends_title_2_abr: التحول الرقمي
                        market_trends_title_3_abr: التخصيص
                        market_trends_title_4_abr: الواقع الافتراضي
                        market_trends_description_1_abr: تتزايد عدطرد، نتيجةً لمعالمها التاريخية والثقافية، بالإضافة إلى جهود الحكومة لتعزيز السياحة.
                        market_trends_description_2_abr: تخضع صقمي، حيث يعتمد المسافرون بشكل متزايد على التكنولوجيا لتخطيط رحلاتهم وتعزيز تجاربهم.
                        market_trends_description_3_abr: هناك ات السفر المخصصة، حيث يبحث السياح عن تجارب فريدة وأصيلة تلبي اهتماماتهم وتفضيلاتهم.
                        market_trends_description_4_abr: تصبح زايد في صناعة السفر، مما يقدم تجارب غامرة تسمح للمسافرين باستكشاف الوجهات قبل وصولهم إليها.
                        competitive_landscape_title_1_abr: وكالات السفر التقليدية
                        competitive_landscape_title_2_abr: الشركات الناشئة التكنولوجية
                        competitive_landscape_title_3_abr: المنافسة المحلية
                        competitive_landscape_description_1_abr: بينما لا تاعة، هناك تحول نحو منصات السفر عبر الإنترنت والتطبيقات التي تقدم خدمات أكثر تخصيصًا وراحة.
                        competitive_landscape_description_2_abr: تعمل الشركات يب صناعة السفر باستخدام حلول مبتكرة وتكنولوجيا عصرية، لتلبية متطلبات المسافرين الحديثين.
                        competitive_landscape_description_3_abr: توجد منافسون محليون في المملكة العربية السعودية يقدمون خدمات تخطيط السفر المماثلة، لكن Swah يمي المخفية.
                        challenges_title_1_abr: التنظيمات
                        challenges_title_2_abr: الحساسية الثقافية
                        challenges_title_3_abr: المنافسة
                        challenges_description_1_abr: تعاني صناعة السياحة في المملكة العربية السعودية من تطور مستمر، مع اللوائح والسياسات التي قد تؤثر على عمليات شركات السفر.
                        challenges_description_2_abr: كوجهة معروفة بتراثها الثقافي الغني، من المهم على شركات السفر أن تكون حساسة ثقافيًا ومحترمة لتقديم تجارب أصيلة ومميزة للسياح.
                        challenges_description_3_abr: مع زيادة عدد اللاعبين في الصناعة، يمكن أن يكون من الصعب التمييز وكسب حصة السوق للمشاركين الجدد مثل Swah.
                        epilogue_abr: في هذه الصناعة الديناميكية والمتطورة، تهدف Swah إلى اة ولا مثيل لها للسياح القادمين إلى المملكة العربية السعودية.
                        
                        Between English and Arabic, please give one more line breaks.
                        Just break the line with /n, do not add any special characters.
                        Please send me only all the fields that I have provided above.
                        "
                ]
            ],
            'max_tokens' => 4000
        ];
        for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
        
            if (!$response) {
                // Handle error or retry
                continue;
            }
        
            $content = $response['choices'][0]['message']['content'];
            
            $cleanedData = preg_replace('/\n+/', '|', $content);
            $dataArray = explode('|', $cleanedData);
            $sections = array_map('trim', $dataArray);
            
            if (count($sections) < 52) {
                Log::info('continue ' . $i);
                continue;
            }
            
            $parts = $this->processContent($sections);

            if ($this->isAnyFieldNullObject($parts)) {
                Log::info('Skipped a part due to incomplete data');
                continue;
            }

            DB::table('industryies_overview')->insert([
                'project_id' => $project->id,
                'created_at' => now(),
                'updated_at' => now()
            ] + $parts);
            break;
        }
        return true;
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }

    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return json_decode($response, true);
        }
    }

    private function processContent($content)
    {
        $parts = [
            'highlight_name_1' => '',
            'highlight_name_2' => '',
            'highlight_name_3' => '',
            'value_relate_highlight_1' => '',
            'value_relate_highlight_2' => '',
            'value_relate_highlight_3' => '',
            'description' => '',
            'market_trends_title_1' => '',
            'market_trends_title_2' => '',
            'market_trends_title_3' => '',
            'market_trends_title_4' => '',
            'competitive_landscape_title_1' => '',
            'competitive_landscape_title_2' => '',
            'competitive_landscape_title_3' => '',
            'market_trends_description_1' => '',
            'market_trends_description_2' => '',
            'market_trends_description_3' => '',
            'market_trends_description_4' => '',
            'competitive_landscape_description_1' => '',
            'competitive_landscape_description_2' => '',
            'competitive_landscape_description_3' => '',
            'challenges_title_1' => '',
            'challenges_title_2' => '',
            'challenges_title_3' => '',
            'challenges_description_1' => '',
            'challenges_description_2' => '',
            'challenges_description_3' => '',
            'epilogue' => '',
            'highlight_name_1_abr' => '',
            'highlight_name_2_abr' => '',
            'highlight_name_3_abr' => '',
            'value_relate_highlight_1_abr' => '',
            'value_relate_highlight_2_abr' => '',
            'value_relate_highlight_3_abr' => '',
            'description_abr' => '',
            'market_trends_title_1_abr' => '',
            'market_trends_title_2_abr' => '',
            'market_trends_title_3_abr' => '',
            'market_trends_title_4_abr' => '',
            'competitive_landscape_title_1_abr' => '',
            'competitive_landscape_title_2_abr' => '',
            'competitive_landscape_title_3_abr' => '',
            'market_trends_description_1_abr' => '',
            'market_trends_description_2_abr' => '',
            'market_trends_description_3_abr' => '',
            'market_trends_description_4_abr' => '',
            'competitive_landscape_description_1_abr' => '',
            'competitive_landscape_description_2_abr' => '',
            'competitive_landscape_description_3_abr' => '',
            'challenges_title_1_abr' => '',
            'challenges_title_2_abr' => '',
            'challenges_title_3_abr' => '',
            'challenges_description_1_abr' => '',
            'challenges_description_2_abr' => '',
            'challenges_description_3_abr' => '',
            'epilogue_abr' => '',
            // Add other sections if needed
        ];
    
        foreach ($content as $section) {
            if (stristr($section, 'highlight_name_1:')) {
                $parts['highlight_name_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'highlight_name_2:')) {
                $parts['highlight_name_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'highlight_name_3:')) {
                $parts['highlight_name_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'unit_relate_highlight_1:')) {
                $parts['value_relate_highlight_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'unit_relate_highlight_2:')) {
                $parts['value_relate_highlight_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'unit_relate_highlight_3:')) {
                $parts['value_relate_highlight_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'description:')) {
                $parts['description'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_title_1:')) {
                $parts['market_trends_title_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_title_2:')) {
                $parts['market_trends_title_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_title_3:')) {
                $parts['market_trends_title_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_title_4:')) {
                $parts['market_trends_title_4'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_title_1:')) {
                $parts['competitive_landscape_title_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_title_2:')) {
                $parts['competitive_landscape_title_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_title_3:')) {
                $parts['competitive_landscape_title_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_description_1:')) {
                $parts['market_trends_description_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_description_2:')) {
                $parts['market_trends_description_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_description_3:')) {
                $parts['market_trends_description_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_description_4:')) {
                $parts['market_trends_description_4'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_description_1:')) {
                $parts['competitive_landscape_description_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_description_2:')) {
                $parts['competitive_landscape_description_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_description_3:')) {
                $parts['competitive_landscape_description_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_title_1:')) {
                $parts['challenges_title_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_title_2:')) {
                $parts['challenges_title_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_title_3:')) {
                $parts['challenges_title_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_description_1:')) {
                $parts['challenges_description_1'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_description_2:')) {
                $parts['challenges_description_2'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_description_3:')) {
                $parts['challenges_description_3'] = $this->cleanSection($section);
            } elseif (stristr($section, 'epilogue:')) {
                $parts['epilogue'] = $this->cleanSection($section);
            } elseif (stristr($section, 'highlight_name_1_abr:')) {
                $parts['highlight_name_1_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'highlight_name_2_abr:')) {
                $parts['highlight_name_2_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'highlight_name_3_abr:')) {
                $parts['highlight_name_3_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'unit_relate_highlight_1_abr:')) {
                $parts['value_relate_highlight_1_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'unit_relate_highlight_2_abr:')) {
                $parts['value_relate_highlight_2_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'unit_relate_highlight_3_abr:')) {
                $parts['value_relate_highlight_3_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'description_abr:')) {
                $parts['description_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_title_1_abr:')) {
                $parts['market_trends_title_1_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_title_2_abr:')) {
                $parts['market_trends_title_2_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_title_3_abr:')) {
                $parts['market_trends_title_3_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_title_4_abr:')) {
                $parts['market_trends_title_4_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_title_1_abr:')) {
                $parts['competitive_landscape_title_1_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_title_2_abr:')) {
                $parts['competitive_landscape_title_2_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_title_3_abr:')) {
                $parts['competitive_landscape_title_3_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_description_1_abr:')) {
                $parts['market_trends_description_1_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_description_2_abr:')) {
                $parts['market_trends_description_2_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_description_3_abr:')) {
                $parts['market_trends_description_3_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'market_trends_description_4_abr:')) {
                $parts['market_trends_description_4_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_description_1_abr:')) {
                $parts['competitive_landscape_description_1_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_description_2_abr:')) {
                $parts['competitive_landscape_description_2_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'competitive_landscape_description_3_abr:')) {
                $parts['competitive_landscape_description_3_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_title_1_abr:')) {
                $parts['challenges_title_1_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_title_2_abr:')) {
                $parts['challenges_title_2_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_title_3_abr:')) {
                $parts['challenges_title_3_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_description_1_abr:')) {
                $parts['challenges_description_1_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_description_2_abr:')) {
                $parts['challenges_description_2_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'challenges_description_3_abr:')) {
                $parts['challenges_description_3_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'epilogue_abr:')) {
                $parts['epilogue_abr'] = $this->cleanSection($section);
            }
        }
        return $parts;
    }


    private function cleanSection($section)
    {
        // Remove anything before a ":"
        $section = preg_replace('/^[^\n]*:\s*/', '', $section);
        
        return trim($section);
    }

}
?>