<?php

namespace App\Services;

use Illuminate\Support\Facades\DB; // Already imported
use Illuminate\Support\Facades\Log;

class UserStoriesService
{
    public function generateAndSaveUserStoriesService($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this
        
        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
                ['role' => 'system', 'content' => 'Generate user stories on the project description.'],
                ['role' => 'user', 'content' => "
                        Generate 3 user stories based on the project description below:
                            
                        Project Description: {$projectDescription}
                        
                        Please create 3 user stories based on this. Each user story should include:
                        - user_story
                        - acceptance_criterea
                        - acceptance_criterea_title
                        - acceptance_criterea_priority
                        - acceptance_criterea_estimate
                        
                        For the Arabic version, ensure to include:
                        - user_story_abr
                        - acceptance_criterea_abr
                        - acceptance_criterea_title_abr
                        - acceptance_criterea_priority_abr
                        - acceptance_criterea_estimate_abr
                        
                        Please make sure there is only one line break between each field.
                        
                        Example:
                        
                        user_story: As a user, I want to be able to login to the platform.
                        acceptance_criterea: User should be able to navigate to the login page. User should be able to input their email address and password. User should see appropriate error messages if the login credentials are incorrect. User should be redirected to the dashboard upon successful login.
                        acceptance_criterea_title: User login action
                        acceptance_criterea_priority: high
                        acceptance_criterea_estimate: 8 (hours)
                        
                        user_story_abr: كمستخدم، أريد أن أتمكن من تسجيل الدخول إلى المنصة.
                        acceptance_criterea_abr: يجب أن يتمكن المستخدم من الانتقال إلى صفحة تسجيل الدخول. يجب أن يتمكن المستخدم من إدخال عنوان بريده الإلكتروني وكلمة المرور. يجب أن يرى المستخدم رسائل الخطأ المناسبة إذا كانت بيانات تسجيل الدخول غير صحيحة. يجب توجيه المستخدم إلى لوحة القيادة بعد تسجيل الدخول بنجاح.
                        acceptance_criterea_title_abr: إجراء تسجيل الدخول للمستخدم
                        acceptance_criterea_priority_abr: عالية
                        acceptance_criterea_estimate_abr: 8 (ساعات)
                        
                        Just break the line with /n, do not add any special characters.
                        Send 3 different user story information.
                "]
            ],
            'max_tokens' => 4000
        ];
    
        for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
    
            if (!$response) {
                continue;
            }
    
            $userStoriesContent = $response['choices'][0]['message']['content'];
            $sections = preg_split("/\n\n+/", $userStoriesContent);
        
            if (count($sections) !== 6) {
                Log::info('continue ' . $i);
                continue;
            }
    
            for ($key = 0; $key < 3; $key++) {
                $index = 2 * $key;
                $userStoriesParts = $this->processUserStoriesContent($sections[$index], $sections[$index + 1]);
                if ($this->isAnyFieldNullObject($userStoriesParts)) {
                    Log::info('continue ' . $key);
                    continue;
                }
    
                DB::table('user_stories')->insert([
                            'project_id' => $project->id,
                            'created_at' => now(),
                            'updated_at' => now()
                        ] + $userStoriesParts);
            }
    
            Log::info('break ' . $i);
            break;
        }
        return true;
    }
    
    public function generateAndSaveOneUserStoriesService($project)
    {
        $projectDescription = $project->description;
        $apiKey = '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'; // Make sure to secure this
        
        $data = [
            'model' => 'gpt-3.5-turbo-1106',
            'temperature' => 0.3,
            'messages' => [
                ['role' => 'system', 'content' => 'Generate user stories on the project description.'],
                ['role' => 'user', 'content' => "
                        Generate 1 user stories based on the project description below:
                            
                        Project Description: {$projectDescription}
                        
                        Please create 1 user stories based on this. Each user story should include:
                        - user_story
                        - acceptance_criterea
                        - acceptance_criterea_title
                        - acceptance_criterea_priority
                        - acceptance_criterea_estimate
                        
                        For the Arabic version, ensure to include:
                        - user_story_abr
                        - acceptance_criterea_abr
                        - acceptance_criterea_title_abr
                        - acceptance_criterea_priority_abr
                        - acceptance_criterea_estimate_abr
                        
                        Please make sure there is only one line break between each field.
                        
                        Example:
                        
                        user_story: As a user, I want to be able to login to the platform.
                        acceptance_criterea: User should be able to navigate to the login page. User should be able to input their email address and password. User should see appropriate error messages if the login credentials are incorrect. User should be redirected to the dashboard upon successful login.
                        acceptance_criterea_title: User login action
                        acceptance_criterea_priority: high
                        acceptance_criterea_estimate: 8 (hours)
                        
                        user_story_abr: كمستخدم، أريد أن أتمكن من تسجيل الدخول إلى المنصة.
                        acceptance_criterea_abr: يجب أن يتمكن المستخدم من الانتقال إلى صفحة تسجيل الدخول. يجب أن يتمكن المستخدم من إدخال عنوان بريده الإلكتروني وكلمة المرور. يجب أن يرى المستخدم رسائل الخطأ المناسبة إذا كانت بيانات تسجيل الدخول غير صحيحة. يجب توجيه المستخدم إلى لوحة القيادة بعد تسجيل الدخول بنجاح.
                        acceptance_criterea_title_abr: إجراء تسجيل الدخول للمستخدم
                        acceptance_criterea_priority_abr: عالية
                        acceptance_criterea_estimate_abr: 8 (ساعات)
                        
                        Just break the line with /n, do not add any special characters.
                        Send 1 different user story information.
                "]
            ],
            'max_tokens' => 4000
        ];
    
        for ($i = 0; $i < 2; $i++) {
            $response = $this->sendRequestToOpenAI($data, $apiKey);
    
            if (!$response) {
                continue;
            }
    
            $userStoriesContent = $response['choices'][0]['message']['content'];
            $sections = preg_split("/\n\n+/", $userStoriesContent);
        
            if (count($sections) !== 2) {
                Log::info('continue ' . $i);
                continue;
            }
    
            $userStoriesParts = $this->processUserStoriesContent($sections[0], $sections[1]);
            if ($this->isAnyFieldNullObject($userStoriesParts)) {
                Log::info('continue ' . $key);
                continue;
            }

            DB::table('user_stories')->insert([
                        'project_id' => $project->id,
                        'created_at' => now(),
                        'updated_at' => now()
                    ] + $userStoriesParts);
    
            Log::info('break ' . $i);
            break;
        }
        return true;
    }
    
    function isAnyFieldNullObject($object): bool {
        foreach ($object as $value) {
            
            Log::info('continue '.$value);
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }

    private function sendRequestToOpenAI($data, $apiKey)
    {
        $ch = curl_init('https://api.openai.com/v1/chat/completions');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $apiKey,
            'Content-Type: application/json'
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            // Log error or handle it according to your application's error handling policy
            return null;
        } else {
            return json_decode($response, true);
        }
    }

    private function processUserStoriesContent($sections, $sections_abr)
    {
        // Initialize an array to hold each part of the Lean Canvas
        $userStoriesParts = [
            'user_story' => '',
            'user_story_abr' => '',
            'acceptance_criterea' => '',
            'acceptance_criterea_abr' => '',
            'acceptance_criterea_title_abr' => '',
            'acceptance_criterea_priority_abr' => '',
            'acceptance_criterea_estimate_abr' => '',
            'acceptance_criterea_title' => '',
            'acceptance_criterea_priority' => '',
            'acceptance_criterea_estimate' => '',
            // Add other sections if needed
        ];

        // Split the content into sections and process
        $sections = preg_split("/\n+/", $sections);
        $sections_abr = preg_split("/\n+/", $sections_abr);

        foreach ($sections as $section) {
            if (stristr($section, 'user_story:')) {
                $userStoriesParts['user_story'] = $this->cleanSection($section);
            } elseif (stristr($section, 'acceptance_criterea_title:')) {
                $userStoriesParts['acceptance_criterea_title'] = $this->cleanSection($section);
            } elseif (stristr($section, 'acceptance_criterea_priority:')) {
                $userStoriesParts['acceptance_criterea_priority'] = $this->cleanSection($section);
            } elseif (stristr($section, 'acceptance_criterea_estimate:')) {
                $userStoriesParts['acceptance_criterea_estimate'] = $this->cleanSection($section);
            } elseif (stristr($section, 'acceptance_criterea:')) {
                $userStoriesParts['acceptance_criterea'] = $this->cleanSection($section);
            }
        }

        foreach ($sections_abr as $section) {
            if (stristr($section, 'user_story_abr:')) {
                $userStoriesParts['user_story_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'acceptance_criterea_title_abr:')) {
                $userStoriesParts['acceptance_criterea_title_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'acceptance_criterea_priority_abr:')) {
                $userStoriesParts['acceptance_criterea_priority_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'acceptance_criterea_estimate_abr:')) {
                $userStoriesParts['acceptance_criterea_estimate_abr'] = $this->cleanSection($section);
            } elseif (stristr($section, 'acceptance_criterea_abr:')) {
                $userStoriesParts['acceptance_criterea_abr'] = $this->cleanSection($section);
            }
        }
    
        return $userStoriesParts;
    }

private function cleanSection($section)
{
    // Remove anything before a ":"
    $section = preg_replace('/^[^\n]*:\s*/', '', $section);
    
    return trim($section);
}

}
?>