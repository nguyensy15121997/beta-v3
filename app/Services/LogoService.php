<?php

namespace App\Services;
use Modules\Taskly\Http\Controllers\ProjectController;

use App\Models\Logo; // Make sure this matches the namespace of your SwotAnalysis model
use Illuminate\Support\Facades\DB; // Add this line at the top of your controller to import the DB facade


class LogoService
{
    public function generateLogo($project)
    {
        $logos = [];
            for ($i = 0; $i < 2; $i++) {
            $projectDescription = $project->description;
            $apiKey = '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'; // Make sure to secure this
    
            $data = [
                'model' => 'dall-e-3',
                'prompt' => 'Create a logo for the following idea: ' . $projectDescription . '. Keep it simple and modern.',
                'n' => 1, // Number of images to generate
                'quality' => 'standard',
                'size' => '1024x1024' // Size of the image
            ];
    
            $ch = curl_init('https://api.openai.com/v1/images/generations'); // Updated endpoint for GPT-4
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . $apiKey,
                'Content-Type: application/json'
            ]);
            
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
            
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $responseArray = json_decode($response, true);
                if ($responseArray) {
                    $logoImage = $responseArray['data'][0]['url']; // This is the URL of the generated image
                    $logos[] = $logoImage;
                }
            }
        }
        return $logos;
    }
  } 

?>
