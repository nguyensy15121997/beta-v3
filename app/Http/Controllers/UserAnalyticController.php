<?php

namespace App\Http\Controllers;

use App\Models\UserOpenaiChat;
use App\Models\UserOpenai;
use App\Models\User;
use App\Models\Project;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserAnalyticController extends Controller
{
    // Analytics pages
    public function analytics(){
        if (Auth::user()->view_analytics) {
            $projects = Project::all();
            $user_count = User::where('status', 1)->get()->count();
            $idea_count = $projects->count();
            $chat_count = UserOpenaiChat::all()->count();
            $blog_idea_count = UserOpenai::select('*')->join('projects', 'projects.id', '=', 'user_openai.project_id')->where('user_openai.openai_id', '9')->count();
            $article_count = UserOpenai::select('*')->join('projects', 'projects.id', '=', 'user_openai.project_id')->where('user_openai.openai_id', '4')->count();
            $user_personas_count = DB::table('storytelling')->count();
            $user_stories_count = DB::table('user_stories')->groupBy('project_id')->count();
            $storytelling_count = DB::table('user_personas')->groupBy('project_id')->count();
            $competitive_analysis_count = DB::table('competitive_analysis')->groupBy('project_id')->count();
            $swot_analyses_count = DB::table('swot_analyses')->count();
            $lean_canvases = DB::table('lean_canvases')->count();
                                  
            $data = [
                'user_count' => $user_count ?? 0,
                'idea_count' => $idea_count ?? 0,
                'chat_count' => $chat_count ?? 0,
                'blog_idea_count' => $blog_idea_count ?? 0,
                'article_count' => $article_count ?? 0,
                'user_personas_count' => $user_personas_count ?? 0,
                'user_stories_count' => $user_stories_count ?? 0,
                'storytelling_count' => $storytelling_count ?? 0,
                'competitive_analysis_count' => $competitive_analysis_count ?? 0,
                'swot_analyses_count' => $swot_analyses_count ?? 0,
                'lean_canvases' => $lean_canvases ?? 0,
            ];
            
            return view('panel.user.analytics.index', compact('data'));
        } 
        return back()->with('error', '__(Permission Denied.)');
    }
    
    // Users page
    public function users(){
        if (Auth::user()->view_users) {
            $data = User::where('status', 1)->paginate(10);
            return view('panel.user.users.index', compact('data'));
        } 
        return back()->with('error', '__(Permission Denied.)');
    }
}