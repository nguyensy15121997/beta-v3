<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Models\Challenge;
use App\Models\UserOpenai;
use App\Models\User;
use App\Models\Project;

class ChallengeController extends Controller
{
    // challenge list
    public function index(Request $request){
        $challenges = Challenge::all();
        return view('panel.user.challenges.index', compact('challenges'));
    }
    
    // challenge page
    public function show(Challenge $challenge){
        return view('panel.user.challenges.component.show', compact('challenge'));
    }
    
    // store challenge
    public function store(Request $request){
        request()->validate([
            'image' => 'required|max:2048',
        ]);
        $objUser          = Auth::user();
        
        $post = $request->all();
        $post['created_by'] = $objUser->id;
        if ($request->hasFile('image')) {
            $path = 'upload/images/challenge/';
            $image = $request->file('image');
            $image_name = Str::random(4).'-'.Str::slug($request->title).'-image.'.$image->getClientOriginalExtension();

            //Resim uzantı kontrolü
            $imageTypes = ['jpg', 'jpeg', 'png', 'svg', 'webp'];
            if (! in_array(Str::lower($image->getClientOriginalExtension()), $imageTypes)) {
                $data = [
                    'errors' => ['The file extension must be jpg, jpeg, png, webp or svg.'],
                ];
                return response()->json($data, 419);
            }
            $image->move($path, $image_name);
            $post['image'] = $path.$image_name;
        
            $challenge = Challenge::create($post);
        
            return response()->json(['status' => 'success', 'message' =>  __('Create challenge successful.'), 'id' => $challenge->id], 200);
        }
        return response()->json([
                    'status' => 'error',
                    'message' => 'Failed to create challenge.',
                ], 500);
    }

    // destroy challenge
    public function destroy(Challenge $challenge){
        $challenge->delete();
        return response()->json(['status' => 'success', 'message' =>  __('Delete challenge successful.')], 200);
    }
    
}